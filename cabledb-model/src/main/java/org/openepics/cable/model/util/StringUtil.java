/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <tt>StringUtil</tt> contains some utility methods that deal with strings.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
public class StringUtil {

    private static final String JOIN  = ", ";
    private static final String SPLIT = "\\s*,\\s*";

    /**
     * This class is not to be instantiated.
     */
    private StringUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return a string with the given length.
     *
     * @param length the string length
     * @return the string
     */
    public static String getString(int length) {
        char[] charArray = new char[length];
        Arrays.fill(charArray, 'a');
        return new String(charArray);
    }

    /**
     * Join a list of elements to string and return string.
     *
     * @param elements list of elements to join
     * @return string
     */
    public static String joinToString(List<String> elements) {
        // trim before return

        String str = elements != null ? String.join(JOIN, elements) : null;
        return str != null ? str.trim() : null;
    }

    /**
     * Split string to list of elements and return list.
     *
     * @param str string to split
     * @return list of elements
     */
    public static List<String> splitToList(String str) {
        // trim before split

        String[] elements = str != null ? str.trim().split(SPLIT) : null;
        return elements != null ?  Arrays.asList(elements) : Collections.<String> emptyList();
    }

}
