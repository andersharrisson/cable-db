/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

/**
 * This represents an instance of query.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class Query extends Persistable {

    private static final long serialVersionUID = -1758834602114783386L;

    private String description;
    private String owner;
    private Date created;
    private Date executed;
    @Enumerated(EnumType.STRING)
    private EntityType entityType;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
     @JoinTable(
            name="query_querycondition",
            joinColumns = @JoinColumn( name="query_id"),
            inverseJoinColumns = @JoinColumn( name="conditions_id")
        )
    private List<QueryCondition> conditions;

    /** Constructor for JPA entity. */
    public Query() {}

    /**
     * Constructs query.
     *
     * @param description
     *            query description
     * @param entityType
     *            entity type
     * @param owner
     *            query owner
     * @param created
     *            query creation date
     * @param conditions
     *            query conditions
     */
    public Query(String description, EntityType entityType, String owner, Date created,
            List<QueryCondition> conditions) {
        this.description = description;
        this.entityType = entityType;
        this.owner = owner;
        this.created = created != null ? new Date(created.getTime()) : null;
        this.conditions = conditions;
    }

    /**
     * Updates query execution date.
     *
     * @param executed
     *            executed
     */
    public void updateExecutionDate(Date executed) {
        this.executed = executed != null ? new Date(executed.getTime()) : null;
    }

    /** @return query description. */
    public String getDescription() {
        return description;
    }

    /** @return query owner. */
    public String getOwner() {
        return owner;
    }

    /** @return query creation date. */
    public Date getCreated() {
        return created != null ? new Date(created.getTime()) : null;
    }

    /** @return query execution date. */
    public Date getExecuted() {
        return executed != null ? new Date(executed.getTime()) : null;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    /** @return set of query conditions. */
    public List<QueryCondition> getConditions() {
        return conditions;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setCreated(Date created) {
        this.created = created != null ? new Date(created.getTime()) : null;
    }

    public void setExecuted(Date executed) {
        this.executed = executed != null ? new Date(executed.getTime()) : null;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public void setConditions(List<QueryCondition> conditions) {
        this.conditions = conditions;
    }
}
