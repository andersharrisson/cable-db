/**
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.cable.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

/**
 * Class representing an an artifact uploaded by the user to the database,
 * i.e. typically a file (text, binary...). Internally the entity is persisted
 * as a BLOB in the underlying DB engine.
 *
 * @author Georg Weiss, ESS
 *
 */
@Entity
@Table(name = "generic_artifact")
@NamedQueries({
    @NamedQuery(name = "GenericArtifact.findAll", query = "SELECT c FROM GenericArtifact c")
})
public class GenericArtifact extends Persistable implements Serializable{

    private static final long serialVersionUID = 5978236447905123684L;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "name")
    private String name;

    @Basic(optional = true)
    @Size(max = 255)
    @Column(name = "description")
    private String description;

    @Basic(optional = false)
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;

    @Basic(optional = false)
    @Size(min = 1, max = 64)
    @Column(name = "modified_by")
    private String modifiedBy;


    @Basic(optional = true, fetch = FetchType.LAZY)
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "content")
    private byte[] legacyContent;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "binary_data_id")
    private BinaryData binaryData;


    /** Constructor. */
    public GenericArtifact() {
    }

    /**
     * Constructs a new artifact
     *
     * @param name
     *            the name of the artifact, i.e. the original name of the uploaded file
     * @param description
     *            the user specified description
     *
     * @param content The binary content of this entity
     */
    public GenericArtifact(String name, String description, byte[] content) {
        this.name = name;
        this.description = description;
        this.binaryData = new BinaryData(content);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    /** @return The timestamp of the last modification of this database entity */
    public Date getModifiedAt() {
        return modifiedAt != null ? new Date(modifiedAt.getTime()) : null;
    }

    /**
     * The setter stores a new copy of the param.
     *
     * @param modifiedAt
     *            The timestamp of the last modification of this database entity
     */
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt != null ? new Date(modifiedAt.getTime()) : null;
    }

    /** @return The user performing the last modification of the database entity */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The user performing the last modification of the database entity
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public byte[] getContent() {
        return binaryData.getContent();
    }

    public void setContent(byte[] content){
        if(binaryData == null){
            binaryData = new BinaryData();
        }
        binaryData.setContent(content);
    }

    public int getContentLength(){
        if(binaryData == null){
            return 0;
        }
        return binaryData.getContentLength();
    }

    public byte[] getLegacyContent(){
        return legacyContent != null ? Arrays.copyOf(legacyContent, legacyContent.length) : null;
    }

    @Override
    public String toString() {
        return getName();
    }

}
