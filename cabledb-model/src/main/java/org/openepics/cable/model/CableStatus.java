/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

/**
 * This represents the status of a {@link Cable}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableStatus {
    /** The cable is inserted. */
    INSERTED("INSERTED"),
    /** The cable is routed. */
    ROUTED("ROUTED"),
    /** The cable is approved, this is the default status. */
    APPROVED("APPROVED"),
    /** The cable is deleted. */
    DELETED("DELETED"),
    /** Filters all Pulled cables */
    PULLED("PULLED"),
    /** Filters all commissioned cables */
    COMMISSIONED("COMMISSIONED"),
    /** Filters all not in use cables */
    NOT_IN_USE("NOT IN USE");

    private final String displayName;

    private CableStatus(String displayName) {
        this.displayName = displayName;
    }

    /** @return the display name for this status */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Converts string value into Cable Status
     * 
     * @param value
     *            string value of status
     * @return the cable status that matches received value
     */
    public static CableStatus convertToCableStatus(String value) {
        for (CableStatus e : values()) {
            if (e.displayName.equalsIgnoreCase(value)) {
                return e;
            }
        }
        return null;
    }
}
