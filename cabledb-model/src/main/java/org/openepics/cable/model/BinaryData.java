/**
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.openepics.cable.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;


/**
 * Class representing an an artifact uploaded by the user to the database,
 * i.e. typically a file (text, binary...). Internally the entity is persisted
 * as a BLOB in the underlying DB engine.
 *
 * @author Georg Weiss, ESS
 *
 */
@Entity
@Table(name = "binary_data")
@NamedQueries({
    @NamedQuery(name = "BinaryData.findAll", query = "SELECT c FROM BinaryData c")
})
public class BinaryData extends Persistable implements Serializable{

    private static final long serialVersionUID = 6069867860785934244L;

    @Basic(optional = false)
    @NotNull
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "content")
    private byte[] content;

    @Basic(optional = false)
    @NotNull
    @Column(name = "content_length")
    private int contentLength;

    /** Constructor. */
    public BinaryData() {
    }

    /**
     * Constructs a new instance of BinaryData with given byte array content.
     *
     * @param content byte array content
     */
    public BinaryData(byte[] content){
        this.content = content != null ? Arrays.copyOf(content, content.length) : null;
        this.contentLength = content != null ? content.length : 0;
    }

    public byte[] getContent() {
        return content != null ? Arrays.copyOf(content, content.length) : null;
    }

    public void setContent(byte[] content){
        this.content = content != null ? Arrays.copyOf(content, content.length) : null;
        this.contentLength = content != null ? content.length : 0;
    }

    public int getContentLength(){
        return contentLength;
    }

}
