/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import com.google.common.base.Preconditions;

/**
 * This represents the endpoint of a {@link Cable}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Entity
public class Endpoint extends Persistable {

    // Note
    //     FBS - Facility Breakdown Structure
    //
    //     device - device fbs tag
    //     rack   - rack fbs tag

    private static final long serialVersionUID = -5142520290025465864L;

    /** Maximum accepted length for label field. */
    public static final int MAX_LABEL_SIZE = 255;

    /** This represents the endpoint data validity. */
    public enum Validity {
        /** The endpoint data is valid. */
        VALID,
        /** The endpoint device is not present. */
        DANGLING
    }

    private String device;
    @Column(name = "device_fbs_tag")
    private String deviceFbsTag;
    private String building;
    private String rack;
    @Column(name = "rack_fbs_tag")
    private String rackFbsTag;
    private String uuid;
    @OneToOne
    private Connector connector;

    private String label;
    @Enumerated(EnumType.STRING)
    private Validity validity = Validity.VALID;
    @Enumerated(EnumType.STRING)
    private NameStatus nameStatus = NameStatus.ACTIVE;

    @Column(name = "device_chess_id")
    private String deviceChessId;
    @Column(name = "rack_chess_id")
    private String rackChessId;

    /** Constructor for JPA entity. */
    protected Endpoint() {}

    /**
     * Constructs a new instance of cable endpoint.
     *
     * @param device
     *            The name of this endpoint device. The name cannot be null.
     */
    public Endpoint(String device) {
        this(device, null, null, null, null);
    }

    /**
     * Constructs a new instance of cable endpoint.
     *
     * @param device the name of this endpoint device. The name cannot be null.
     * @param building the building the endpoint is at
     * @param rack the rack the endpoint is at
     * @param connector the connector between cable and device
     * @param label the label
     */
    public Endpoint(String device, String building, String rack, Connector connector, String label) {
        Preconditions.checkArgument(label == null || label.length() <= MAX_LABEL_SIZE);

        this.device = device;
        this.building = building;
        this.rack = rack;
        this.connector = connector;
        this.label = label;
        this.uuid = null;
    }

    /**
     * Constructs a new instance of cable endpoint.
     *
     * @param device the name of this endpoint device. The name cannot be null.
     * @param building the building the endpoint is at
     * @param rack the rack the endpoint is at
     * @param connector the connector between cable and device
     * @param label the label
     * @param deviceFbsTag the device fbs tag
     * @param rackFbsTag the rack fbs tag
     */
    public Endpoint(String device, String building, String rack, Connector connector,
            String label, String deviceFbsTag, String rackFbsTag) {
        // similar but not same as non-fbs create
        //     difference for device/deviceFbsTag
        this(device, building, rack, connector, label, deviceFbsTag, rackFbsTag, true);
    }

    /**
     * Constructs a new instance of cable endpoint.
     *
     * @param device the name of this endpoint device. The name cannot be null.
     * @param building the building the endpoint is at
     * @param rack the rack the endpoint is at
     * @param connector the connector between cable and device
     * @param label the label
     * @param deviceFbsTag the device fbs tag
     * @param rackFbsTag the rack fbs tag
     * @param checkArgument if arguments to be checked or not (trusted)
     */
    public Endpoint(String device, String building, String rack, Connector connector,
            String label, String deviceFbsTag, String rackFbsTag, boolean checkArgument) {
        // similar but not same as non-fbs create
        //     difference for device/deviceFbsTag
        if (checkArgument) {
            Preconditions.checkArgument(
                    (device != null && !device.isEmpty()) ||
                    (deviceFbsTag != null && !deviceFbsTag.isEmpty()));
            Preconditions.checkArgument(label == null || label.length() <= MAX_LABEL_SIZE);
        }

        this.device = device;
        this.building = building;
        this.rack = rack;
        this.connector = connector;
        this.label = label;
        this.uuid = null;
        this.deviceFbsTag = deviceFbsTag;
        this.rackFbsTag = rackFbsTag;

        this.deviceChessId = null;
        this.rackChessId = null;
    }

    /**
     * Updates the attribute information of this endpoint device.
     *
     * @param device the name of this endpoint device. The name cannot be null.
     * @param building the building the endpoint is at
     * @param rack the rack the endpoint is at
     * @param connector the connector between cable and device.
     * @param label the label
     *
     * @see Endpoint#update(String, String, String, Connector, String, String, String)
     */
    public void update(String device, String building, String rack, Connector connector,
            String label) {
        Preconditions.checkArgument(device != null && !device.isEmpty());
        Preconditions.checkArgument(label == null || label.length() <= MAX_LABEL_SIZE);

        this.device = device;
        this.building = building;
        this.rack = rack;
        this.connector = connector;
        this.label = label;
    }

    /**
     * Updates the attribute information of this endpoint device.
     *
     * @param device the name of this endpoint device. The name cannot be null.
     * @param building the building the endpoint is at
     * @param rack the rack the endpoint is at
     * @param connector the connector between cable and device.
     * @param label the label
     * @param deviceFbsTag the device fbs tag
     * @param rackFbsTag the rack fbs tag
     */
    public void update(String device, String building, String rack, Connector connector,
            String label, String deviceFbsTag, String rackFbsTag) {
        // similar but not same as non-fbs update
        //     difference for device/deviceFbsTag
        update(device, building, rack, connector, label, deviceFbsTag, rackFbsTag, true);
    }

    /**
     * Updates the attribute information of this endpoint device.
     *
     * @param device the name of this endpoint device. The name cannot be null.
     * @param building the building the endpoint is at
     * @param rack the rack the endpoint is at
     * @param connector the connector between cable and device.
     * @param label the label
     * @param deviceFbsTag the device fbs tag
     * @param rackFbsTag the rack fbs tag
     * @param checkArgument if arguments to be checked or not (trusted)
     */
    public void update(String device, String building, String rack, Connector connector,
            String label, String deviceFbsTag, String rackFbsTag, boolean checkArgument) {
        // similar but not same as non-fbs update
        //     difference for device/deviceFbsTag
        if (checkArgument) {
            Preconditions.checkArgument(
                    (device != null && !device.isEmpty()) ||
                    (deviceFbsTag != null && !deviceFbsTag.isEmpty()));
            Preconditions.checkArgument(label == null || label.length() <= MAX_LABEL_SIZE);
        }

        this.device = device;
        this.building = building;
        this.rack = rack;
        this.connector = connector;
        this.label = label;
        this.deviceFbsTag = deviceFbsTag;
        this.rackFbsTag = rackFbsTag;
    }

    /**
     * @param status
     *            the endpoint status to set
     */
    public void setValidity(Validity status) {
        this.validity = status;
    }

    /**
     * @param status
     *            the endpoint name status to set
     */
    public void setNameStatus(NameStatus status) {
        this.nameStatus = status;
    }

    /** @return the name of the endpoint device */
    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDeviceFbsTag() {
        return deviceFbsTag;
    }

    public void setDeviceFbsTag(String deviceFbsTag) {
        this.deviceFbsTag = deviceFbsTag;
    }

    /** @return the building the endpoint is at */
    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    /** @return the rack the endpoint is at */
    public String getRack() {
        return rack;
    }

    public void setRack(String rack) {
        this.rack = rack;
    }

    public String getRackFbsTag() {
        return rackFbsTag;
    }

    public void setRackFbsTag(String rackFbsTag) {
        this.rackFbsTag = rackFbsTag;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /** @return the connector between cable and device */
    public Connector getConnector() {
        return connector;
    }

    public void setConnector(Connector connector) {
        this.connector = connector;
    }

    /** @return the label */
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /** @return the endpoint data validity */
    public Validity getValidity() {
        return validity;
    }

    /** @return true if the endpoint data is valid, else false */
    public boolean isValid() {
        return validity == Validity.VALID;
    }

    /** @return the endpoint data validity */
    public NameStatus getNameStatus() {
        return nameStatus;
    }

    public String getDeviceChessId() {
        return deviceChessId;
    }

    public void setDeviceChessId(String deviceChessId) {
        this.deviceChessId = deviceChessId;
    }

    public String getRackChessId() {
        return rackChessId;
    }

    public void setRackChessId(String rackChessId) {
        this.rackChessId = rackChessId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((device == null) ? 0 : device.hashCode());
        result = prime * result + ((deviceFbsTag == null) ? 0 : deviceFbsTag.hashCode());
        result = prime * result + ((building == null) ? 0 : building.hashCode());
        result = prime * result + ((rack == null) ? 0 : rack.hashCode());
        result = prime * result + ((rackFbsTag == null) ? 0 : rackFbsTag.hashCode());
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        result = prime * result + ((connector == null) ? 0 : connector.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((nameStatus == null) ? 0 : nameStatus.hashCode());
        result = prime * result + ((deviceChessId == null) ? 0 : deviceChessId.hashCode());
        result = prime * result + ((rackChessId == null) ? 0 : rackChessId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Endpoint other = (Endpoint) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (device == null) {
            if (other.device != null)
                return false;
        } else if (!device.equals(other.device))
            return false;
        if (deviceFbsTag == null) {
            if (other.deviceFbsTag != null)
                return false;
        } else if (!deviceFbsTag.equals(other.deviceFbsTag))
            return false;
        if (building == null) {
            if (other.building != null)
                return false;
        } else if (!building.equals(other.building))
            return false;
        if (rack == null) {
            if (other.rack != null)
                return false;
        } else if (!rack.equals(other.rack))
            return false;
        if (rackFbsTag == null) {
            if (other.rackFbsTag != null)
                return false;
        } else if (!rackFbsTag.equals(other.rackFbsTag))
            return false;
        if (uuid == null) {
            if (other.uuid != null)
                return false;
        } else if (!uuid.equals(other.uuid))
            return false;
        if (connector == null) {
            if (other.connector != null)
                return false;
        } else if (!connector.equals(other.connector))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (validity != other.validity)
            return false;
        if (nameStatus != other.nameStatus)
            return false;
        if (deviceChessId == null) {
            if (other.deviceChessId != null)
                return false;
        } else if (!deviceChessId.equals(other.deviceChessId))
            return false;
        if (rackChessId == null) {
            if (other.rackChessId != null)
                return false;
        } else if (!rackChessId.equals(other.rackChessId))
            return false;
        return true;
    }

}
