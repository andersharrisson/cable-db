/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of cable.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Cable.findAll", query = "SELECT c FROM Cable c"),
})
public class Cable extends Persistable {

    // Note
    //     FBS - Facility Breakdown Structure
    //
    //     name (system, subsystem, cableClass, seqNumber) - corresponding fbs tag

    private static final long serialVersionUID = -7654156206144341815L;

    /** This represents the data validity of the cable. */
    public enum Validity {
        /** The cable state approved, this is the default status. */
        VALID,
        /** One of the cable endpoints is not in valid state. */
        DANGLING
    }

    private String system;
    private String subsystem;
    private String cableClass;
    private Integer seqNumber;
    @Column(name = "fbs_tag")
    private String fbsTag;
    @ElementCollection(fetch = FetchType.LAZY)
    @OrderColumn(name = "position")
    private List<String> owners = new ArrayList<>();
    private String ownersString;

    @ManyToOne
    private CableArticle cableArticle;
    @ManyToOne
    private CableType cableType;
    private String container;
    @OneToOne
    private Endpoint endpointA;
    @OneToOne
    private Endpoint endpointB;
    @ManyToOne
    @JoinColumn(name = "installation_package_id")
    private InstallationPackage installationPackage;

    private Date installationBy;
    private Date terminationBy;
    private String comments;
    private Date created;
    private Date modified;
    @Enumerated(EnumType.STRING)
    private CableStatus status = CableStatus.INSERTED;
    @Enumerated(EnumType.STRING)
    private Validity validity = Validity.VALID;
    private String revision;
    @Transient
    private boolean hasProblem;
    @Basic
    @Column(name = "electrical_documentation")
    private String electricalDocumentation;
    @Column(name = "chess_id")
    private String chessId;


    /** Constructor for JPA entity. */
    private Cable() {}

    /**
     * Creates a new instance of a cable.
     *
     * @param system
     *            the cable system digit
     * @param subsystem
     *            the cable subsystem digit
     * @param cableClass
     *            the cable class letter
     * @param seqNumber
     *            the cable sequential number
     * @param owners
     *            the cable owners
     * @param created
     *            the creation date of this cable
     * @param modified
     *            the modification date of this cable
     */
    public Cable(String system, String subsystem, String cableClass, Integer seqNumber, List<String> owners,
            Date created, Date modified) {
        this(system, subsystem, cableClass, seqNumber, owners,
                CableStatus.INSERTED, null, null,
                null, null, null,
                created, modified,
                null, null, null, null, null);
    }

    /**
     * Creates a new instance of a cable.
     *
     * @param system
     *            the cable system digit
     * @param subsystem
     *            the cable subsystem digit
     * @param cableClass
     *            the cable class letter
     * @param seqNumber
     *            the cable sequential number
     * @param owners
     *            the cable owners
     * @param status
     *            the cable status
     * @param cableArticle
     *            the cable article
     * @param cableType
     *            the cable type
     * @param container
     *            the container cable number of this cable
     * @param endpointA
     *            endpoint a of the cable
     * @param endpointB
     *            endpoint b of the cable
     * @param created
     *            the creation date of this cable
     * @param modified
     *            the modification date of this cable
     * @param installationPackage
     *            the installation package value for this cable instance
     * @param installationBy
     *            the date by which this cable is installed
     * @param comments
     *            the comments of this cable
     * @param revision
     *            the revision of this cable
     * @param electricalDocumentation
     *            link to electrical documentation
     */
    public Cable(String system, String subsystem, String cableClass, Integer seqNumber, List<String> owners,
            CableStatus status, CableArticle cableArticle, CableType cableType,
            String container, Endpoint endpointA, Endpoint endpointB,
            Date created, Date modified,
            InstallationPackage installationPackage, Date installationBy,
            String comments, String revision, String electricalDocumentation) {

        Preconditions.checkArgument(owners != null && !owners.isEmpty());
        Preconditions.checkNotNull(status);
        Preconditions.checkNotNull(created);
        Preconditions.checkNotNull(modified);

        final CableName cableName = new CableName(system, subsystem, cableClass, seqNumber != null ? seqNumber : 0);
        this.system = cableName.getSystem();
        this.subsystem = cableName.getSubsystem();
        this.cableClass = cableName.getCableClass();
        this.seqNumber = seqNumber != null ? cableName.getSeqNumber() : null;

        this.owners.addAll(owners);
        updateOwnersString();
        this.status = status;
        this.cableArticle = cableArticle;
        this.cableType = cableType;
        this.container = container;
        this.endpointA = endpointA;
        this.endpointB = endpointB;
        this.created = new Date(created.getTime());
        this.modified = new Date(modified.getTime());

        setInstallationPackage(installationPackage);

        this.installationBy = installationBy != null ? new Date(installationBy.getTime()) : null;
        this.comments = comments;
        this.revision = revision;
        this.electricalDocumentation = electricalDocumentation;

        this.fbsTag = null;
        this.chessId = null;
    }

    /** @return the cable number (example: 12A012345) */
    public String getName() {
        return CableName.asString(system, subsystem, cableClass, seqNumber);
    }

    /** @return the cable system digit */
    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    /** @return the cable subsystem digit */
    public String getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(String subsystem) {
        this.subsystem = subsystem;
    }

    /** @return the cable class letter */
    public String getCableClass() {
        return cableClass;
    }

    public void setCableClass(String cableClass) {
        this.cableClass = cableClass;
    }

    /** @return the cable sequential number */
    public Integer getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(Integer seqNumber) {
        this.seqNumber = seqNumber;
    }

    public String getFbsTag() {
        return fbsTag;
    }

    public void setFbsTag(String fbsTag) {
        this.fbsTag = fbsTag;
    }

    /** @return the cable owners */
    public List<String> getOwners() {
        return owners;
    }

    public void setOwners(List<String> owners) {
        if (this.owners != owners) {
            this.owners.clear();
            if (owners != null) {
                this.owners.addAll(owners);
            }
            updateOwnersString();
        }
    }

    public void setOwnersString(String ownersString) {
        this.ownersString = ownersString;
    }

    /** @return the cable owners joined in a string */
    public String getOwnersString() {
        return ownersString;
    }

    /** Updates owners string. */
    public void updateOwnersString() {
        ownersString = String.join(", ", owners);
    }

    /** @return the cable article */
    public CableArticle getCableArticle() {
        return cableArticle;
    }

    public void setCableArticle(CableArticle cableArticle) {
        this.cableArticle = cableArticle;
    }

    /** @return the cable type */
    public CableType getCableType() {
        return cableType;
    }

    public void setCableType(CableType cableType) {
        this.cableType = cableType;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    /** @return the endpoint a of the cable */
    public Endpoint getEndpointA() {
        return endpointA;
    }

    public void setEndpointA(Endpoint endpointA) {
        this.endpointA = endpointA;
    }

    /** @return the endpoint b of the cable */
    public Endpoint getEndpointB() {
        return endpointB;
    }

    public void setEndpointB(Endpoint endpointB) {
        this.endpointB = endpointB;
    }

    /** @return the installation package */
    public InstallationPackage getInstallationPackage() {
        return installationPackage;
    }

    public void setInstallationPackage(InstallationPackage installationPackage) {
        this.installationPackage = installationPackage;
    }

    /** @return the date by which this cable is installed */
    public Date getInstallationBy() {
        return installationBy != null ? new Date(installationBy.getTime()) : null;
    }

    public void setInstallationBy(Date installationBy) {
        this.installationBy = installationBy != null ? new Date(installationBy.getTime()) : null;
    }

    /** @return the date by which this cable is terminated */
    public Date getTerminationBy() {
        return terminationBy != null ? new Date(terminationBy.getTime()) : null;
    }

    public void setTerminationBy(Date terminationBy) {
        this.terminationBy = terminationBy != null ? new Date(terminationBy.getTime()) : null;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    /** @return the creation date of this cable */
    public Date getCreated() {
        return created != null ? new Date(created.getTime()) : null;
    }

    public void setCreated(Date created) {
        this.created = created != null ? new Date(created.getTime()) : null;
    }

    /** @return the modification date of this cable */
    public Date getModified() {
        return modified != null ? new Date(modified.getTime()) : null;
    }

    /**
     * Sets the modified date of this cable instance.
     *
     * @param modified
     *            the modified date to set
     */
    public void setModified(Date modified) {
        this.modified = modified != null ? new Date(modified.getTime()) : null;
    }

    /** @return the cable status */
    public CableStatus getStatus() {
        return status;
    }

    /**
     * Sets the status of this cable instance.
     *
     * @param status
     *            the status to set
     */
    public void setStatus(CableStatus status) {
        Preconditions.checkNotNull(status);
        this.status = status;
    }

    /** @return the data validity of the cable */
    public Validity getValidity() {
        return validity;
    }

    /**
     * Sets the validity of this cable instance.
     *
     * @param validity
     *            the validity to set
     */
    public void setValidity(Validity validity) {
        this.validity = validity;
    }

    /** @return true if the cable data is valid, else false */
    public boolean isValid() {
        return validity == Validity.VALID;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public boolean isHasProblem() {
        return hasProblem;
    }

    public void setHasProblem(boolean hasProblem) {
        this.hasProblem = hasProblem;
    }

    public String getElectricalDocumentation() {
        return electricalDocumentation;
    }

    public void setElectricalDocumentation(String electricalDocumentation) {
        this.electricalDocumentation = electricalDocumentation;
    }

    public String getChessId() {
        return chessId;
    }

    public void setChessId(String chessId) {
        this.chessId = chessId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        result = prime * result + ((subsystem == null) ? 0 : subsystem.hashCode());
        result = prime * result + ((cableClass == null) ? 0 : cableClass.hashCode());
        result = prime * result + ((seqNumber == null) ? 0 : seqNumber.hashCode());
        result = prime * result + ((fbsTag == null) ? 0 : fbsTag.hashCode());
        result = prime * result + ((owners == null) ? 0 : owners.hashCode());
        result = prime * result + ((cableArticle == null) ? 0 : cableArticle.hashCode());
        result = prime * result + ((cableType == null) ? 0 : cableType.hashCode());
        result = prime * result + ((container == null) ? 0 : container.hashCode());
        result = prime * result + ((endpointA == null) ? 0 : endpointA.hashCode());
        result = prime * result + ((endpointB == null) ? 0 : endpointB.hashCode());
        result = prime * result + ((installationPackage == null) ? 0 : installationPackage.hashCode());
        result = prime * result + ((installationBy == null) ? 0 : installationBy.hashCode());
        result = prime * result + ((terminationBy == null) ? 0 : terminationBy.hashCode());
        result = prime * result + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((electricalDocumentation == null) ? 0 : electricalDocumentation.hashCode());
        result = prime * result + ((chessId == null) ? 0 : chessId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cable other = (Cable) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (system == null) {
            if (other.system != null)
                return false;
        } else if (!system.equals(other.system))
            return false;
        if (subsystem == null) {
            if (other.subsystem != null)
                return false;
        } else if (!subsystem.equals(other.subsystem))
            return false;
        if (cableClass == null) {
            if (other.cableClass != null)
                return false;
        } else if (!cableClass.equals(other.cableClass))
            return false;
        if (seqNumber == null) {
            if (other.seqNumber != null)
                return false;
        } else if (!seqNumber.equals(other.seqNumber))
            return false;
        if (fbsTag == null) {
            if (other.fbsTag != null)
                return false;
        } else if (!fbsTag.equals(other.fbsTag))
            return false;
        if (owners == null) {
            if (other.owners != null)
                return false;
        } else if (!owners.equals(other.owners))
            return false;
        if (cableArticle == null) {
            if (other.cableArticle != null)
                return false;
        } else if (!cableArticle.equals(other.cableArticle))
            return false;
        if (cableType == null) {
            if (other.cableType != null)
                return false;
        } else if (!cableType.equals(other.cableType))
            return false;
        if (container == null) {
            if (other.container != null)
                return false;
        } else if (!container.equals(other.container))
            return false;
        if (endpointA == null) {
            if (other.endpointA != null)
                return false;
        } else if (!endpointA.equals(other.endpointA))
            return false;
        if (endpointB == null) {
            if (other.endpointB != null)
                return false;
        } else if (!endpointB.equals(other.endpointB))
            return false;
        if (installationPackage == null) {
            if (other.installationPackage != null)
                return false;
        } else if (!installationPackage.equals(other.installationPackage))
            return false;
        if (installationBy == null) {
            if (other.installationBy != null)
                return false;
        } else if (!installationBy.equals(other.installationBy))
            return false;
        if (terminationBy == null) {
            if (other.terminationBy != null)
                return false;
        } else if (!terminationBy.equals(other.terminationBy))
            return false;
        if (comments == null) {
            if (other.comments != null)
                return false;
        } else if (!comments.equals(other.comments))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (status != other.status)
            return false;
        if (validity != other.validity)
            return false;
        if (revision == null) {
            if (other.revision != null)
                return false;
        } else if (!revision.equals(other.revision))
            return false;
        if (electricalDocumentation == null) {
            if (other.electricalDocumentation != null)
                return false;
        } else if (!electricalDocumentation.equals(other.electricalDocumentation))
            return false;
        if (chessId == null) {
            if (other.chessId != null)
                return false;
        } else if (!chessId.equals(other.chessId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getName();
    }

}
