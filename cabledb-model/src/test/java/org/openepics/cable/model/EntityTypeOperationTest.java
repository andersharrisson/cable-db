package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link EntityTypeOperation}.
 *
 * @author Lars Johansson
 */
public class EntityTypeOperationTest {

    /**
     * Tests return of display name for <tt>EntityTypeOperation</tt.
     *
     * @see EntityTypeOperation#getDisplayName()
     * @see EntityTypeOperation#CREATE
     * @see EntityTypeOperation#DELETE
     * @see EntityTypeOperation#UPDATE
     * @see EntityTypeOperation#VALIDATE
     */
    @Test
    public void getDisplayName() {
        assertEquals("Create", EntityTypeOperation.CREATE.getDisplayName());
        assertEquals("Delete", EntityTypeOperation.DELETE.getDisplayName());
        assertEquals("Update", EntityTypeOperation.UPDATE.getDisplayName());
        assertEquals("Validate", EntityTypeOperation.VALIDATE.getDisplayName());
    }

}
