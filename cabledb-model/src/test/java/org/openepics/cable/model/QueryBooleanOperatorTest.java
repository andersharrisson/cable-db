package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link QueryBooleanOperator}.
 *
 * @author Lars Johansson
 */
public class QueryBooleanOperatorTest {

    /**
     * Tests return of operator for <tt>QueryBooleanOperator</tt.
     *
     * @see QueryBooleanOperator#getOperator()
     * @see QueryBooleanOperator#AND
     * @see QueryBooleanOperator#NONE
     * @see QueryBooleanOperator#NOT
     * @see QueryBooleanOperator#OR
     */
    @Test
    public void getOperator() {
        assertEquals("AND", QueryBooleanOperator.AND.getOperator());
        assertEquals("", QueryBooleanOperator.NONE.getOperator());
        assertEquals("NOT", QueryBooleanOperator.NOT.getOperator());
        assertEquals("OR", QueryBooleanOperator.OR.getOperator());
    }

}
