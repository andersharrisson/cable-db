/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

/**
 * Tests {@link CableArticle}.
 *
 * @author Lars Johansson
 */
public class CableArticleTest {

    private static final String DESCRIPTION            = "description";
    private static final String ERP_NUMBER             = "erpNumber";
    private static final String EXTERNAL_ID            = "externalId";
    private static final String ISO_CLASS              = "isoClass";
    private static final String LONG_DESCRIPTION       = "longDescription";
    private static final String MANUFACTURER2          = "manufacturer";
    private static final String MODEL_TYPE             = "modelType";
    private static final String SHORT_NAME             = "shortName";
    private static final String SHORT_NAME_EXTERNAL_ID = "shortNameExternalId";
    private static final String URL_CHESS_PART         = "urlChessPart";

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = NullPointerException.class)
    public void createCableArticleManufacturerNull() {
        String manufacturer = null;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = NullPointerException.class)
    public void createCableArticleExternalIdNull() {
        String manufacturer = MANUFACTURER2;
        String externalId = null;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = NullPointerException.class)
    public void createCableArticleEprNumberNull() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = null;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = NullPointerException.class)
    public void createCableArticleShortNameExternalIdNull() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = null;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleManufacturerEmpty() {
        String manufacturer = "";
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleExternalIdEmpty() {
        String manufacturer = MANUFACTURER2;
        String externalId = "";
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleErpNumberEmpty() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = "";
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleShortNameExternalIdEmpty() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = "";
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleBendingRadiusNegative() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = -1f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleWeightPerLengthNegative() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = -1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleOuterDiameterNegative() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = -3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableArticleRatedVoltageNegative() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = -220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = NullPointerException.class)
    public void createCableArticleCreatedNull() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = null;
        Date modified = new Date();

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test(expected = NullPointerException.class)
    public void createCableArticleModifiedNull() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = null;

        new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
    }

    /**
     * Test for
     * {@link CableArticle#CableArticle(String, String, String, String, String, String, String, Float, Float, Float, Float, String, String, String, boolean, Date, Date)}
     * constructor.
     */
    @Test
    public void createCableArticle() {
        String manufacturer = MANUFACTURER2;
        String externalId = EXTERNAL_ID;
        String erpNumber = ERP_NUMBER;
        String isoClass = ISO_CLASS;
        String description = DESCRIPTION;
        String longDescription = LONG_DESCRIPTION;
        String modelType = MODEL_TYPE;
        Float bendingRadius = 0f;
        Float outerDiameter = 3.1415926535f;
        Float ratedVoltage = 220f;
        Float weightPerLength = 1f;
        String shortName = SHORT_NAME;
        String shortNameExternalId = SHORT_NAME_EXTERNAL_ID;
        String urlChessPart = URL_CHESS_PART;
        boolean active = true;
        Date created = new Date();
        Date modified = new Date();

        CableArticle cableArticle = new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);

        assertEquals(manufacturer, cableArticle.getManufacturer());
        assertEquals(externalId, cableArticle.getExternalId());
        assertEquals(erpNumber, cableArticle.getErpNumber());
        assertEquals(isoClass, cableArticle.getIsoClass());
        assertEquals(description, cableArticle.getDescription());
        assertEquals(longDescription, cableArticle.getLongDescription());
        assertEquals(modelType, cableArticle.getModelType());
        assertEquals(bendingRadius, cableArticle.getBendingRadius());
        assertEquals(outerDiameter, cableArticle.getOuterDiameter());
        assertEquals(ratedVoltage, cableArticle.getRatedVoltage());
        assertEquals(weightPerLength, cableArticle.getWeightPerLength());
        assertEquals(shortName, cableArticle.getShortName());
        assertEquals(shortNameExternalId, cableArticle.getShortNameExternalId());
        assertEquals(urlChessPart, cableArticle.getUrlChessPart());
        assertEquals(active, cableArticle.isActive());
        assertEquals(created, cableArticle.getCreated());
        assertEquals(modified, cableArticle.getModified());
    }

}
