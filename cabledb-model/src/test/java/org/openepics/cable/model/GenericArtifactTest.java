package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Tests {@link GenericArtifact}.
 *
 * @author Lars Johansson
 */
public class GenericArtifactTest {

    /**
     * Tests create of <tt>GenericArtifact</tt>.
     *
     * @see GenericArtifact#GenericArtifact()
     */
    @Test
    public void createGenericArtifact_noParameters() {
        final GenericArtifact genericArtifact = new GenericArtifact();

        assertNull(genericArtifact.getName());
        assertNull(genericArtifact.getDescription());
        assertNull(genericArtifact.getModifiedBy());
        assertNull(genericArtifact.getLegacyContent());
        //	assertNull(genericArtifact.getContent());
        assertEquals(0, genericArtifact.getContentLength());
    }

    /**
     * Tests create of <tt>GenericArtifact</tt>.
     *
     * @see GenericArtifact#GenericArtifact(String, String, byte[])
     */
    @Test
    public void createGenericArtifact() {
        String name = "name";
        String description = "description";
        byte[] contentToCreate = new byte[] {1, 1, 2, 3, 5, 8, 13, 21};

        final GenericArtifact genericArtifact = new GenericArtifact(name, description, contentToCreate);

        assertEquals(name, genericArtifact.getName());
        assertEquals(description, genericArtifact.getDescription());
        byte[] content = genericArtifact.getContent();
        assertNotNull(content);
        assertEquals(8, content.length);
        assertEquals(1, content[0]);
        assertEquals(1, content[1]);
        assertEquals(2, content[2]);
        assertEquals(3, content[3]);
        assertEquals(5, content[4]);
        assertEquals(8, content[5]);
        assertEquals(13, content[6]);
        assertEquals(21, content[7]);
        assertEquals(8, genericArtifact.getContentLength());
    }

}
