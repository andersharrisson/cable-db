/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests {@link CableName}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
public class CableNameTest {

    /**
     * Tests create of <tt>CableName</tt> with invalid system.
     *
     * @see CableName#CableName(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableName_invalidSystem() {
        new CableName("a2c000001");
    }

    /**
     * Tests create of <tt>CableName</tt> with invalid subsystem.
     *
     * @see CableName#CableName(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableName_invalidSubsystem() {
        new CableName("1-c000001");
    }

    /**
     * Tests create of <tt>CableName</tt> with invalid class.
     *
     * @see CableName#CableName(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableName_invalidClass() {
        new CableName("121000001");
    }

    /**
     * Tests create of <tt>CableName</tt> with invalid sequential number length.
     *
     * @see CableName#CableName(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableName_invalidSeqNumber() {
        new CableName("12c00001");
    }

    /**
     * Tests create of <tt>CableName</tt> with valid name.
     *
     * @see CableName#CableName(String)
     */
    @Test
    public void createCableName_name() {
        final CableName cableName = new CableName("12c000001");

        assertEquals(String.valueOf(1),  cableName.getSystem());
        assertEquals(String.valueOf(2),  cableName.getSubsystem());
        assertEquals("C",                cableName.getCableClass());
        assertEquals(Integer.valueOf(1), cableName.getSeqNumber());
    }

    /**
     * Tests create of <tt>CableName</tt> with valid attributes.
     *
     * @see CableName#CableName(String, String, String, Integer)
     */
    @Test
    public void createCableName_attributes() {
        String system     = "1";
        String subsystem  = "6";
        String cableClass = "B";
        Integer seqNumber = 123654;

        final CableName cableName = new CableName(system, subsystem, cableClass, seqNumber);

        assertEquals(String.valueOf(1),       cableName.getSystem());
        assertEquals(String.valueOf(6),       cableName.getSubsystem());
        assertEquals("B",                     cableName.getCableClass());
        assertEquals(Integer.valueOf(123654), cableName.getSeqNumber());

    }

    /**
     * Tests if <tt>system</tt> is valid.
     *
     * @see CableName#isValidSystem(String)
     */
    @Test
    public void isValidSystem() {
        assertFalse(CableName.isValidSystem(null));
        assertFalse(CableName.isValidSystem(""));
        assertFalse(CableName.isValidSystem("32"));
        assertFalse(CableName.isValidSystem("GH"));
        assertTrue (CableName.isValidSystem("1"));
        assertFalse(CableName.isValidSystem("A"));
    }

    /**
     * Tests if <tt>subsystem</tt> is valid.
     *
     * @see CableName#isValidSubsystem(String)
     */
    @Test
    public void isValidSubsystem() {
        assertFalse(CableName.isValidSubsystem(null));
        assertFalse(CableName.isValidSubsystem(""));
        assertFalse(CableName.isValidSubsystem("5678"));
        assertFalse(CableName.isValidSubsystem("qwer"));
        assertTrue (CableName.isValidSubsystem("5"));
        assertTrue (CableName.isValidSubsystem("B"));
    }

    /**
     * Tests if <tt>cableClass</tt> is valid.
     *
     * @see CableName#isValidCableClass(String)
     */
    @Test
    public void isValidCableClass() {
        assertFalse(CableName.isValidCableClass(null));
        assertFalse(CableName.isValidCableClass(""));
        assertFalse(CableName.isValidCableClass("1234"));
        assertFalse(CableName.isValidCableClass("asdf"));
        assertFalse(CableName.isValidCableClass("1"));
        assertTrue (CableName.isValidCableClass("E"));
    }

    /**
     * Tests if <tt>cableName</tt> is valid.
     *
     * @see CableName#isValidName(String)
     */
    @Test
    public void isValidName() {
        assertFalse(CableName.isValidName(null));
        assertFalse(CableName.isValidName(""));
        assertFalse(CableName.isValidName("12c0000014634563456"));
        assertTrue (CableName.isValidName("12c000001"));
        assertTrue (CableName.isValidName("16B123654"));
    }

    /**
     * Tests if <tt>asString</tt> returns <tt>name</tt> as should do.
     *
     * @see CableName#asString(String, String, String, Integer)
     */
    @Test
    public void asString() {
        String system     = "1";
        String subsystem  = "6";
        String cableClass = "B";
        Integer seqNumber = 123654;

        final CableName cableName = new CableName(system, subsystem, cableClass, seqNumber);

        assertEquals(system,     cableName.getSystem());
        assertEquals(subsystem,  cableName.getSubsystem());
        assertEquals(cableClass, cableName.getCableClass());
        assertEquals(seqNumber,  cableName.getSeqNumber());

        assertEquals(cableName.toString(), CableName.asString(system, subsystem, cableClass, seqNumber));
    }

    /**
     * Tests if <tt>tosString</tt> returns <tt>name</tt> as should do.
     *
     * @see CableName#toString()
     */
    @Test
    public void testToString() {
        String system     = "1";
        String subsystem  = "6";
        String cableClass = "B";
        Integer seqNumber = 123654;

        final CableName cableName = new CableName(system, subsystem, cableClass, seqNumber);

        assertEquals(system,     cableName.getSystem());
        assertEquals(subsystem,  cableName.getSubsystem());
        assertEquals(cableClass, cableName.getCableClass());
        assertEquals(seqNumber,  cableName.getSeqNumber());

        assertEquals(system + subsystem + cableClass + String.valueOf(seqNumber), cableName.toString());
    }

}
