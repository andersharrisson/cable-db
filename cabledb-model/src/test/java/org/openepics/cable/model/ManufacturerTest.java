package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;

/**
 * Tests {@link Manufacturer}.
 *
 * @author Lars Johansson
 */
public class ManufacturerTest {

    /**
     * Tests create of <tt>Manufacturer</tt>.
     *
     * @see Manufacturer#Manufacturer()
     */
    @Test
    public void createManufacturer_noParameters() {
        final Manufacturer manufacturer = new Manufacturer();

        assertNull(manufacturer.getName());
        assertNull(manufacturer.getAddress());
        assertNull(manufacturer.getPhoneNumber());
        assertNull(manufacturer.getEmail());
        assertNull(manufacturer.getCountry());

        assertNull(manufacturer.getCreated());
        assertNull(manufacturer.getModified());
        assertEquals(ManufacturerStatus.APPROVED, manufacturer.getStatus());
    }

    /**
     * Tests create of <tt>Manufacturer</tt>.
     *
     * @see Manufacturer#Manufacturer(String, String, String, String, String, java.util.Date, java.util.Date)
     */
    @Test
    public void createManufacturer() {
        String name = "name";
        String address = "address";
        String phoneNumber = "phoneNumber";
        String email = "email";
        String country = "country";
        Date created = new Date();
        Date modified = new Date();

        final Manufacturer manufacturer = new Manufacturer(name, address, phoneNumber, email, country, created, modified);

        assertEquals(name, manufacturer.getName());
        assertEquals(address, manufacturer.getAddress());
        assertEquals(phoneNumber, manufacturer.getPhoneNumber());
        assertEquals(email, manufacturer.getEmail());
        assertEquals(country, manufacturer.getCountry());

        assertEquals(created, manufacturer.getCreated());
        assertEquals(modified, manufacturer.getModified());
        assertEquals(ManufacturerStatus.APPROVED, manufacturer.getStatus());
    }

    /**
     * Tests create of <tt>Manufacturer</tt>; to fail with faulty data (name).
     *
     * @see Manufacturer#Manufacturer(String, String, String, String, String, java.util.Date, java.util.Date)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createManufacturer_noName() {
        String name = null;
        String address = "address";
        String phoneNumber = "phoneNumber";
        String email = "email";
        String country = "country";
        Date created = new Date();
        Date modified = new Date();

        new Manufacturer(name, address, phoneNumber, email, country, created, modified);
    }

    /**
     * Tests create of <tt>Manufacturer</tt>; to fail with faulty data (created).
     *
     * @see Manufacturer#Manufacturer(String, String, String, String, String, java.util.Date, java.util.Date)
     */
    @Test(expected = NullPointerException.class)
    public void createManufacturer_noCreated() {
        String name = "name";
        String address = "address";
        String phoneNumber = "phoneNumber";
        String email = "email";
        String country = "country";
        Date created = null;
        Date modified = new Date();

        new Manufacturer(name, address, phoneNumber, email, country, created, modified);
    }

    /**
     * Tests create of <tt>Manufacturer</tt>; to fail with faulty data (modified).
     *
     * @see Manufacturer#Manufacturer(String, String, String, String, String, java.util.Date, java.util.Date)
     */
    @Test(expected = NullPointerException.class)
    public void createManufacturer_noModified() {
        String name = "name";
        String address = "address";
        String phoneNumber = "phoneNumber";
        String email = "email";
        String country = "country";
        Date created = new Date();
        Date modified = null;

        new Manufacturer(name, address, phoneNumber, email, country, created, modified);
    }

    /**
     * Tests update of <tt>Manufacturer</tt>.
     *
     * @see Manufacturer#Manufacturer(String, String, String, String, String, java.util.Date, java.util.Date)
     * @see Manufacturer#setAddress(String)
     */
    @Test
    public void updateManufacturer() {
        String name = "name";
        String address = "address";
        String phoneNumber = "phoneNumber";
        String email = "email";
        String country = "country";
        Date created = new Date();
        Date modified = new Date();

        final Manufacturer manufacturer = new Manufacturer(name, address, phoneNumber, email, country, created, modified);
        manufacturer.setAddress("address2");

        assertEquals("address2", manufacturer.getAddress());
    }

}
