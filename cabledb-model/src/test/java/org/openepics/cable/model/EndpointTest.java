/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.openepics.cable.model.util.StringUtil;

/**
 * Tests {@link Endpoint}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
public class EndpointTest {

    /**
     * Tests create of <tt>Endpoint</tt>.
     *
     * @see Endpoint#Endpoint()
     */
    @Test
    public void createEndpoint_noParameters() {
        final Endpoint endpoint = new Endpoint();

        assertNull(endpoint.getDevice());
        assertNull(endpoint.getDeviceFbsTag());
        assertNull(endpoint.getBuilding());
        assertNull(endpoint.getRack());
        assertNull(endpoint.getRackFbsTag());
        assertNull(endpoint.getConnector());
        assertNull(endpoint.getLabel());
        assertNull(endpoint.getUuid());

        assertEquals(NameStatus.ACTIVE, endpoint.getNameStatus());
        assertEquals(Endpoint.Validity.VALID, endpoint.getValidity());

        assertNull(endpoint.getDeviceChessId());
        assertNull(endpoint.getRackChessId());
    }

    /**
     * Tests create of <tt>Endpoint</tt>.
     *
     * @see Endpoint#Endpoint(String)
     */
    @Test
    public void createEndpoint_device() {
        final String device = "device";
        final Endpoint endpoint = new Endpoint(device);

        assertEquals(device, endpoint.getDevice());
        assertNull(endpoint.getDeviceFbsTag());
        assertNull(endpoint.getBuilding());
        assertNull(endpoint.getRack());
        assertNull(endpoint.getRackFbsTag());
        assertNull(endpoint.getConnector());
        assertNull(endpoint.getLabel());
        assertNull(endpoint.getUuid());

        assertEquals(NameStatus.ACTIVE, endpoint.getNameStatus());
        assertEquals(Endpoint.Validity.VALID, endpoint.getValidity());

        assertNull(endpoint.getDeviceChessId());
        assertNull(endpoint.getRackChessId());
    }

    /**
     * Tests create of <tt>Endpoint</tt>.
     *
     * @see Endpoint#Endpoint(String, String, String, Connector, GenericArtifact, String)
     */
    @Test
    public void createEndpoint() {
        final String device = "device";
        final String building = "building";
        final String rack = "rack";
        final String label = StringUtil.getString(Endpoint.MAX_LABEL_SIZE);

        final Endpoint endpoint = new Endpoint(device, building, rack, null, label);

        assertEquals(device, endpoint.getDevice());
        assertNull(endpoint.getDeviceFbsTag());
        assertEquals(building, endpoint.getBuilding());
        assertEquals(rack, endpoint.getRack());
        assertNull(endpoint.getRackFbsTag());
        assertNull(endpoint.getConnector());
        assertEquals(label, endpoint.getLabel());
        assertNull(endpoint.getUuid());

        assertEquals(NameStatus.ACTIVE, endpoint.getNameStatus());
        assertEquals(Endpoint.Validity.VALID, endpoint.getValidity());

        assertNull(endpoint.getDeviceChessId());
        assertNull(endpoint.getRackChessId());
    }

    /**
     * Tests create of <tt>Endpoint</tt>.
     *
     * @see Endpoint#Endpoint(String, String, String, Connector, GenericArtifact, String)
     */
    @Test
    public void createEndpoint_noData() {
        final String device = "device";
        final Endpoint endpoint = new Endpoint(device, null, null, null, null);

        assertEquals(device, endpoint.getDevice());
        assertNull(endpoint.getDeviceFbsTag());
        assertNull(endpoint.getBuilding());
        assertNull(endpoint.getRack());
        assertNull(endpoint.getRackFbsTag());
        assertNull(endpoint.getConnector());
        assertNull(endpoint.getLabel());

        assertNull(endpoint.getDeviceChessId());
        assertNull(endpoint.getRackChessId());
    }

    /**
     * Tests create of <tt>Endpoint</tt>; to fail with faulty data (device).
     *
     * @see Endpoint#Endpoint(String)
     */
    @Test
    public void createEndpoint_noDevice() {
        final Endpoint endpoint = new Endpoint(null);

        assertNotNull(endpoint);
        assertNull(endpoint.getDevice());
    }

    /**
     * Tests create of <tt>Endpoint</tt>; to fail with faulty data (label exceeding limit).
     *
     * @see Endpoint#Endpoint(String, String, String, Connector, String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createEndpoint_labelExceedLimit() {
        // label exceeding max size
        new Endpoint("device", null, null, null, StringUtil.getString(Endpoint.MAX_LABEL_SIZE + 1));
    }

    /**
     * Tests update of <tt>Endpoint</tt>
     *
     * @see Endpoint#update(String, String, String, Connector, String)
     */
    @Test
    public void updateEndpoint() {
        final Endpoint endpoint = new Endpoint("device");
        endpoint.update("new device", null, null, null, null);
    }

    /**
     * Tests update of <tt>Endpoint</tt>; to fail with faulty data (device).
     *
     * @see Endpoint#update(String, String, String, Connector, String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void updateEndpoint_noDevice() {
        final Endpoint endpoint = new Endpoint("device");
        endpoint.update(null, null, null, null, null);
    }

    /**
     * Tests update of <tt>Endpoint</tt>; to fail with faulty data (label exceeding limit).
     *
     * @see Endpoint#update(String, String, String, Connector, String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void updateEndpoint_labelExceedLimit() {
        final Endpoint endpoint = new Endpoint("device");
        // label exceeding max size
        endpoint.update("new device", null, null, null, StringUtil.getString(Endpoint.MAX_LABEL_SIZE + 1));
    }

}
