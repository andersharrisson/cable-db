package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link EntityType}.
 *
 * @author Lars Johansson
 */
public class EntityTypeTest {

    /**
     * Tests return of display name for <tt>EntityType</tt.
     *
     * @see EntityType#getDisplayName()
     * @see EntityType#CABLE
     * @see EntityType#CABLE_ARTICLE
     * @see EntityType#CABLE_TYPE
     * @see EntityType#CONNECTOR
     * @see EntityType#INSTALLATION_PACKAGE
     * @see EntityType#MANUFACTURER
     */
    @Test
    public void getDisplayName() {
        assertEquals("Cable", EntityType.CABLE.getDisplayName());
        assertEquals("Cable Article", EntityType.CABLE_ARTICLE.getDisplayName());
        assertEquals("Cable Type", EntityType.CABLE_TYPE.getDisplayName());
        assertEquals("Connector", EntityType.CONNECTOR.getDisplayName());
        assertEquals("Installation Package", EntityType.INSTALLATION_PACKAGE.getDisplayName());
        assertEquals("Manufacturer", EntityType.MANUFACTURER.getDisplayName());
    }

}
