package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link ManufacturerStatus}.
 *
 * @author Lars Johansson
 */
public class ManufacturerStatusTest {

    /**
     * Tests return of display name for <tt>ManufacturerStatus</tt.
     *
     * @see ManufacturerStatus#getDisplayName()
     * @see ManufacturerStatus#APPROVED
     * @see ManufacturerStatus#DELETED
     */
    @Test
    public void getDisplayName() {
        assertEquals("Approved", ManufacturerStatus.APPROVED.getDisplayName());
        assertEquals("Deleted", ManufacturerStatus.DELETED.getDisplayName());
    }

}
