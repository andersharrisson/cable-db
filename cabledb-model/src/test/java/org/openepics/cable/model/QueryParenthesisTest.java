package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link QueryParenthesis}.
 *
 * @author Lars Johansson
 */
public class QueryParenthesisTest {

    /**
     * Tests return of parenthesis for <tt>QueryParenthesis</tt.
     *
     * @see QueryParenthesis#getParenthesis()
     * @see QueryParenthesis#CLOSE
     * @see QueryParenthesis#NONE
     * @see QueryParenthesis#OPEN
     */
    @Test
    public void getParenthesis() {
        assertEquals(")", QueryParenthesis.CLOSE.getParenthesis());
        assertEquals("", QueryParenthesis.NONE.getParenthesis());
        assertEquals("(", QueryParenthesis.OPEN.getParenthesis());
    }

}
