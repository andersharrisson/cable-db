/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.openepics.cable.client.impl.ClosableResponse;
import org.openepics.cable.client.impl.ResponseException;
import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.CableEndpointsElement;
import org.openepics.cable.jaxb.CableResource;

/**
 * This is CableDB service clientdataType parser that is used to get data from server.
 * <p>
 * All class methods are static.
 * </p>
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */

class CableClient implements CableResource {

    private static final Logger LOG = Logger.getLogger(CableClient.class.getName());

    private static final String PATH_CABLE = "cable";
    private static final String PATH_CABLE_ENDPOINTS = "cableEndpoints";

    @Nonnull private final CableDBClient client;

    CableClient(CableDBClient client) { this.client = client; }

    /**
     * Requests a {@link List} of all {@link CableElement}s from the REST service.
     *
     * @param field a list of string cable fields to search using regular expression; if <code>null</code> or empty,
     *      all fields are searched
     * @param regExp the regular expression to use to search the cables; if <code>null</code> or empty,
     *      all cables are returned
     *
     * @throws ResponseException if data couldn't be retrieved
     *
     * @return {@link List} of all {@link CableElement}s
     */
    @Override
    public List<CableElement> getAllCables(List<String> field, String regExp) {
        LOG.fine("Invoking getAllCables");

        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        if ((regExp != null) && !regExp.isEmpty()) {
            map.add("regexp", regExp);

            if (field != null) {
                for (final String fieldElement : field) {
                    map.add("field", fieldElement);
                }
            }
        }

        final String url = client.buildUrl(PATH_CABLE);

        List<CableElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            throw new ResponseException("Couldn't retrieve data from service at " + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException("The service didn't return any data. Possible problems with parameters.");
        return responseData;
    }

    public List<CableEndpointsElement> getAllCableEndpoints(Date newerThan) {
        final String url = client.buildUrl(PATH_CABLE_ENDPOINTS);
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        
        if (newerThan != null){            
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss z");
            map.add("regexp", df.format(newerThan));
            map.add("field", "Modified");
        }
                
        List<CableEndpointsElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableEndpointsElement>>() {});
        } catch (Exception e) {
            throw new ResponseException("Couldn't retrieve data from service at " + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException("The service didn't return any data. Possible problems with parameters.");
        return responseData;
    }
    
    /**
     * Requests particular {@link CableElement} from the REST service.
     *
     * @param number the number of the desired CableElement
     *
     * @throws ResponseException  if data couldn't be retrieved
     *
     * @return {@link CableElement}
     */
    @Override
    public CableElement getCable(String number) {
        LOG.fine("Invoking getCable");

        final String url = client.buildUrl(PATH_CABLE, number);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(CableElement.class);
        } catch (Exception e) {
            throw new ResponseException("Couldn't retrieve data from service at " + url + ".", e);
        }
    }

    @Override
    public Response addCable(String token, List<CableElement> cables) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Response editCable(String token, List<CableElement> cables) {
        // TODO Auto-generated method stub
        return null;
    }
}
