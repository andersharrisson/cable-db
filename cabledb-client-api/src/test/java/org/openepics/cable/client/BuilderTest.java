/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.client;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for UriBuilder.
 */
public class BuilderTest {

    /**
     * Test creation of UriBuilder.
     */
    @Test
    public void UriBuilderTest() {
        MultivaluedMap<String, Object> queryParameters = new MultivaluedHashMap<String, Object>();
        queryParameters.add("one", "abc");
        queryParameters.add("multi", "1");
        queryParameters.add("multi", "2");

        final UriBuilder ub = CableDBClient.getBuilder("http://localhost/test", queryParameters);

        Assert.assertEquals("http://localhost/test?one=abc&multi=1&multi=2", ub.build(new Object[] {}).toString());
    }
}
