The Cable Database (CDB) is meant to properly manage the information about cables that the ESS Machine Directorate’s Divisions (and, eventually, its in-kind collaborators) will be responsible for.

More specifically, the CDB supports the tracking, configuration and labeling of (thousands of) cables in all phases of the ESS project (design, installation, maintenance and troubleshooting).

This information is then consumed both by end-users and other ICS applications (e.g. CCDB) to enable these to successfully perform their domain specific businesses.
