-- fbs tag attributes added to cable
--     cable fbs tag
--     from a - fbs tag, rack fbs tag
--     from b - fbs tag, rack fbs tag
-- corresponding tables, columns for attributes
--     cable
--         fbs_tag
--     endpoint
--         device_fbs_tag
--         rack_fbs_tag

ALTER TABLE cable ADD COLUMN fbs_tag character varying(255);

ALTER TABLE endpoint ADD COLUMN device_fbs_tag character varying(255);
ALTER TABLE endpoint ADD COLUMN rack_fbs_tag character varying(255);
