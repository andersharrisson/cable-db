-- cable article table with attributes
--
-- Manufacturer
-- External ID                (Manufacturer’s product code)
-- ERP Number                 (Name, ESS number)
-- ISO15926 Classification
-- Description
-- Long description
-- Model type
-- Bending radius             (mm)
-- Outer diameter             (mm)
-- Rated Voltage              (V)
-- Weight per length          (kg/km)
-- Short name
-- Short name.External Id
-- URL for CHESS part

CREATE TABLE cablearticle (
    id bigint NOT NULL,
    manufacturer character varying(255) NOT NULL,
    external_id character varying(255) NOT NULL,
    erp_number character varying(255) NOT NULL,
    iso_class character varying(255),
    description character varying(65545),
    long_description character varying(65545),
    model_type character varying(255) NOT NULL,
    bending_radius real,
    outer_diameter real,
    rated_voltage real,
    weight_per_length real,
    short_name character varying(255) NOT NULL,
    short_name_external_id character varying(255) NOT NULL,
    url_chess_part character varying(255) NOT NULL,
    active boolean NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);

ALTER TABLE ONLY cablearticle OWNER TO ${user};

CREATE SEQUENCE cablearticle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE cablearticle_id_seq OWNED BY cablearticle.id;
ALTER SEQUENCE cablearticle_id_seq OWNER TO ${user};

ALTER TABLE ONLY cablearticle ALTER COLUMN id SET DEFAULT nextval('cablearticle_id_seq'::regclass);

ALTER TABLE ONLY cablearticle
    ADD CONSTRAINT cablearticle_pkey PRIMARY KEY (id);

-- reference to cable article

ALTER TABLE cable ADD cablearticle_id bigint;

ALTER TABLE ONLY cable
    ADD CONSTRAINT fk_cable_cablearticle FOREIGN KEY (cablearticle_id) REFERENCES cablearticle(id);
