CREATE TABLE public.connectormanufacturer (
	id bigserial NOT NULL,
	"position" int4 NOT NULL,
	connector_id int8 NULL,
	manufacturer_id int8 NULL,
	artifact_id int8 NULL,
	CONSTRAINT connectormanufacturer_pkey PRIMARY KEY (id),
	CONSTRAINT connectormanufacturer__connector_fkey FOREIGN KEY (connector_id) REFERENCES connector(id),
	CONSTRAINT connectormanufacturer_manufacturer_fkey FOREIGN KEY (manufacturer_id) REFERENCES manufacturer(id),
	CONSTRAINT fk_connectormanufacturer_generic_artifact FOREIGN KEY (artifact_id) REFERENCES generic_artifact(id)
);

