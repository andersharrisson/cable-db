/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util.webservice;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.StringUtils;

/**
 * Purpose of class to provide utility methods for test of REST interface.
 *
 * @author Lars Johansson
 *
 * @see ClosableResponse
 */
public class WebserviceUtility {

    @Nonnull static final Client client = ClientBuilder.newClient();

    // Note
    //     MediaType.APPLICATION_JSON_TYPE
    //     MediaType.APPLICATION_XML_TYPE
    //     dependencies to org.glassfish.jersey.* in order to read response

    /**
     * This class is not to be instantiated.
     */
    private WebserviceUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return <tt>UriBuilder</tt> from given url.
     *
     * @param url a url, i.e. pointer to network resource
     * @return response for request
     */
    public static UriBuilder getUriBuilder(String url) {
        return getUriBuilder(url, (MultivaluedMap<String, Object>) null);
    }

    /**
     * Return <tt>UriBuilder</tt> from given url and query parameters map (key-value pairs).
     *
     * @param url a url, i.e. pointer to network resource
     * @param queryParameters a map with name-value pairs for query parameters
     * @return response for request
     */
    public static UriBuilder getUriBuilder(String url,
            @Nullable final MultivaluedMap<String, Object> queryParameters) {
        // build uri with url, parameters

        if (StringUtils.isEmpty(StringUtils.trim(url))) {
            return null;
        }
        url = url.trim();

        UriBuilder ub = UriBuilder.fromUri(url);
        if (queryParameters != null) {
            for (Entry<String, List<Object>> entry : queryParameters.entrySet()) {
                ub.queryParam(entry.getKey(), entry.getValue().toArray());
            }
        }

        return ub;
    }

    /**
     * Return <tt>UriBuilder</tt> from given url and query parameters map (key-value pairs).
     *
     * @param url a url, i.e. pointer to network resource
     * @param queryParameters a map with name-value pairs for query parameters
     * @return response for request
     */
    public static UriBuilder getUriBuilder(String url,
                                           @Nullable final Map<String, String> queryParameters) {
        // build uri with url, parameters

        if (StringUtils.isEmpty(StringUtils.trim(url))) {
            return null;
        }
        url = url.trim();

        UriBuilder ub = UriBuilder.fromUri(url);
        if (queryParameters != null) {
            for (Entry<String, String> entry : queryParameters.entrySet()) {
                ub.queryParam(entry.getKey(), entry.getValue());
            }
        }

        return ub;
    }

    /**
     * Build a web resource target from given parameter, build a request, invoke HTTP GET synchronously
     * and return <tt>ClosableResponse</tt> object.
     * <p>
     * Note <tt>MediaType.APPLICATION_JSON_TYPE</tt>.
     *
     * @param url a url, i.e. pointer to network resource
     * @return response for request
     */
    public static ClosableResponse getResponseJson(final String url) {
        // build uri with url
        // request response

        return getResponseJson(url, null);
    }

    /**
     * Build a web resource target from given parameters, build a request, invoke HTTP GET synchronously
     * and return <tt>ClosableResponse</tt> object.
     * <p>
     * Note <tt>MediaType.APPLICATION_JSON_TYPE</tt>.
     *
     * @param url a url, i.e. pointer to network resource
     * @param queryParameters a map with name-value pairs for query parameters
     * @return response for request
     */
    public static ClosableResponse getResponseJson(final String url,
            @Nullable final MultivaluedMap<String, Object> queryParameters) {
        // build uri with url, parameters
        // request response

        return getResponseJson(getUriBuilder(url, queryParameters));
    }

    /**
     * Return <tt>ClosableResponse</tt> object for <tt>MediaType.APPLICATION_JSON_TYPE</tt>
     * given <tt>UriBuilder</tt> object.
     *
     * @param ub a <tt>UriBuilder</tt> object for content and construction of URI
     * @return response for request
     */
    public static ClosableResponse getResponseJson(final UriBuilder ub) {
        // request response
        // dependencies to org.glassfish.jersey.* in order to read response

        return getResponse(ub, MediaType.APPLICATION_JSON_TYPE);
    }

    /**
     * Build a web resource target from given parameter, build a request, invoke HTTP GET synchronously
     * and return <tt>ClosableResponse</tt> object.
     * <p>
     * Note <tt>MediaType.APPLICATION_XML_TYPE</tt>.
     *
     * @param url a url, i.e. pointer to network resource
     * @return response for request
     */
    public static ClosableResponse getResponseXml(final String url) {
        // build uri with url
        // request response

        return getResponseXml(url, null);
    }

    /**
     * Build a web resource target from given parameters, build a request, invoke HTTP GET synchronously
     * and return <tt>ClosableResponse</tt> object.
     * <p>
     * Note <tt>MediaType.APPLICATION_XML_TYPE</tt>.
     *
     * @param url a url, i.e. pointer to network resource
     * @param queryParameters a map with name-value pairs for query parameters
     * @return response for request
     */
    public static ClosableResponse getResponseXml(final String url,
            @Nullable final MultivaluedMap<String, Object> queryParameters) {
        // build uri with url, parameters
        // request response

        return getResponseXml(getUriBuilder(url, queryParameters));
    }

    /**
     * Return <tt>ClosableResponse</tt> object for <tt>MediaType.APPLICATION_XML_TYPE</tt>
     * given <tt>UriBuilder</tt> object.
     *
     * @param ub a <tt>UriBuilder</tt> object for content and construction of URI
     * @return response for request
     */
    public static ClosableResponse getResponseXml(final UriBuilder ub) {
        // request response
        // dependencies to org.glassfish.jersey.* in order to read response

        return getResponse(ub, MediaType.APPLICATION_XML_TYPE);
    }

    /**
     * Return <tt>ClosableResponse</tt> object given <tt>UriBuilder</tt> and <tt>MediaType</tt> objects.
     *
     * @param ub a <tt>UriBuilder</tt> object for content and construction of URI
     * @param mediaType the response media type
     * @return response for request
     */
    public static ClosableResponse getResponse(final UriBuilder ub, MediaType mediaType) {
        // request response
        // dependencies to org.glassfish.jersey.* in order to read response

        return new ClosableResponse(client.target(ub).request(mediaType).get());
    }

}
