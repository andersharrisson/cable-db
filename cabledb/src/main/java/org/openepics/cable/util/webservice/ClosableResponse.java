/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util.webservice;

import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Link.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

/**
 * JAX RS Does not support the {@link AutoCloseable} for responses.
 *
 * This is a wrapper class to alleviate this unfortunate design error.
 *
 * @author <a href=mailto:miroslav.pavleski@cosylab.com>Miroslav Pavleski</a>
 */
public class ClosableResponse implements AutoCloseable {

    private Response response;

    /**
     * Initiation of <tt>AutoCloseable</tt> <tt>Response</tt> object.
     *
     * @param response response for request
     *
     * @see java.lang.AutoCloseable
     * @see javax.ws.rs.core.Response
     */
    public ClosableResponse(Response response) {
        this.response = response;
    }

    /**
     * {@link javax.ws.rs.core.Response#getStatus()}
     *
     * @return {@link javax.ws.rs.core.Response#getStatus()}
     */
    public int getStatus() {
        return response.getStatus();
    }

    /**
     * {@link javax.ws.rs.core.Response#getStatusInfo()}
     *
     * @return {@link javax.ws.rs.core.Response#getStatusInfo()}
     */
    public StatusType getStatusInfo() {
        return response.getStatusInfo();
    }

    /**
     * {@link javax.ws.rs.core.Response#getEntity()}
     *
     * @return {@link javax.ws.rs.core.Response#getEntity()}
     */
    public Object getEntity() {
        return response.getEntity();
    }

    /**
     * {@link javax.ws.rs.core.Response#readEntity(Class)}
     *
     * @param entityType {@link javax.ws.rs.core.Response#readEntity(Class)}
     * @return {@link javax.ws.rs.core.Response#readEntity(Class)}
     */
    public <T> T readEntity(Class<T> entityType) {
        return response.readEntity(entityType);
    }

    /**
     * {@link javax.ws.rs.core.Response#readEntity(GenericType)}
     *
     * @param entityType {@link javax.ws.rs.core.Response#readEntity(GenericType)}
     * @return {@link javax.ws.rs.core.Response#readEntity(GenericType)}
     */
    public <T> T readEntity(GenericType<T> entityType) {
        return response.readEntity(entityType);
    }

    /**
     * {@link javax.ws.rs.core.Response#readEntity(Class, Annotation[])}
     *
     * @param entityType {@link javax.ws.rs.core.Response#readEntity(Class, Annotation[])}
     * @param annotations {@link javax.ws.rs.core.Response#readEntity(Class, Annotation[])}
     * @return {@link javax.ws.rs.core.Response#readEntity(Class, Annotation[])}
     */
    public <T> T readEntity(Class<T> entityType, Annotation[] annotations) {
        return response.readEntity(entityType, annotations);
    }

    /**
     * {@link javax.ws.rs.core.Response#readEntity(GenericType, Annotation[])}
     *
     * @param entityType {@link javax.ws.rs.core.Response#readEntity(GenericType, Annotation[])}
     * @param annotations {@link javax.ws.rs.core.Response#readEntity(GenericType, Annotation[])}
     * @return {@link javax.ws.rs.core.Response#readEntity(GenericType, Annotation[])}
     */
    public <T> T readEntity(GenericType<T> entityType, Annotation[] annotations) {
        return response.readEntity(entityType, annotations);
    }

    /**
     * {@link javax.ws.rs.core.Response#hasEntity()}
     *
     * @return {@link javax.ws.rs.core.Response#hasEntity()}
     */
    public boolean hasEntity() {
        return response.hasEntity();
    }

    /**
     * {@link javax.ws.rs.core.Response#bufferEntity()}
     *
     * @return {@link javax.ws.rs.core.Response#bufferEntity()}
     */
    public boolean bufferEntity() {
        return response.bufferEntity();
    }

    /**
     * {@link javax.ws.rs.core.Response#getMediaType()}
     *
     * @return {@link javax.ws.rs.core.Response#getMediaType()}
     */
    public MediaType getMediaType() {
        return response.getMediaType();
    }

    /**
     * {@link javax.ws.rs.core.Response#getLanguage()}
     *
     * @return {@link javax.ws.rs.core.Response#getLanguage()}
     */
    public Locale getLanguage() {
        return response.getLanguage();
    }

    /**
     * {@link javax.ws.rs.core.Response#getLength()}
     *
     * @return {@link javax.ws.rs.core.Response#getLength()}
     */
    public int getLength() {
        return response.getLength();
    }

    /**
     * {@link javax.ws.rs.core.Response#getAllowedMethods()}
     *
     * @return {@link javax.ws.rs.core.Response#getAllowedMethods()}
     */
    public Set<String> getAllowedMethods() {
        return response.getAllowedMethods();
    }

    /**
     * {@link javax.ws.rs.core.Response#getCookies()}
     *
     * @return {@link javax.ws.rs.core.Response#getCookies()}
     */
    public Map<String, NewCookie> getCookies() {
        return response.getCookies();
    }

    /**
     * {@link javax.ws.rs.core.Response#getEntityTag()}
     *
     * @return {@link javax.ws.rs.core.Response#getEntityTag()}
     */
    public EntityTag getEntityTag() {
        return response.getEntityTag();
    }

    /**
     * {@link javax.ws.rs.core.Response#getDate()}
     *
     * @return {@link javax.ws.rs.core.Response#getDate()}
     */
    public Date getDate() {
        return response.getDate();
    }

    /**
     * {@link javax.ws.rs.core.Response#getLastModified()}
     *
     * @return {@link javax.ws.rs.core.Response#getLastModified()}
     */
    public Date getLastModified() {
        return response.getLastModified();
    }

    /**
     * {@link javax.ws.rs.core.Response#getLocation()}
     *
     * @return {@link javax.ws.rs.core.Response#getLocation()}
     */
    public URI getLocation() {
        return response.getLocation();
    }

    /**
     * {@link javax.ws.rs.core.Response#getLinks()}
     *
     * @return {@link javax.ws.rs.core.Response#getLinks()}
     */
    public Set<Link> getLinks() {
        return response.getLinks();
    }

    /**
     * {@link javax.ws.rs.core.Response#hasLink(String)}
     *
     * @param relation {@link javax.ws.rs.core.Response#hasLink(String)}
     * @return {@link javax.ws.rs.core.Response#hasLink(String)}
     */
    public boolean hasLink(String relation) {
        return response.hasLink(relation);
    }

    /**
     * {@link javax.ws.rs.core.Response#getLink(String)}
     *
     * @param relation {@link javax.ws.rs.core.Response#getLink(String)}
     * @return {@link javax.ws.rs.core.Response#getLink(String)}
     */
    public Link getLink(String relation) {
        return response.getLink(relation);
    }

    /**
     * {@link javax.ws.rs.core.Response#getLinkBuilder(String)}
     *
     * @param relation {@link javax.ws.rs.core.Response#getLinkBuilder(String)}
     * @return {@link javax.ws.rs.core.Response#getLinkBuilder(String)}
     */
    public Builder getLinkBuilder(String relation) {
        return response.getLinkBuilder(relation);
    }

    /**
     * {@link javax.ws.rs.core.Response#getMetadata()}
     *
     * @return {@link javax.ws.rs.core.Response#getMetadata()}
     */
    public MultivaluedMap<String, Object> getMetadata() {
        return response.getMetadata();
    }

    /**
     * {@link javax.ws.rs.core.Response#getHeaders()}
     *
     * @return {@link javax.ws.rs.core.Response#getHeaders()}
     */
    public MultivaluedMap<String, Object> getHeaders() {
        return response.getHeaders();
    }

    /**
     * {@link javax.ws.rs.core.Response#getStringHeaders()}
     *
     * @return {@link javax.ws.rs.core.Response#getStringHeaders()}
     */
    public MultivaluedMap<String, String> getStringHeaders() {
        return response.getStringHeaders();
    }

    /**
     * {@link javax.ws.rs.core.Response#getHeaderString(String)}
     *
     * @param name {@link javax.ws.rs.core.Response#getHeaderString(String)}
     * @return {@link javax.ws.rs.core.Response#getHeaderString(String)}
     */
    public String getHeaderString(String name) {
        return response.getHeaderString(name);
    }

    /**
     * @see javax.ws.rs.core.Response#close()
     */
    @Override
    public void close() {
        response.close();
    }

    /**
     * @see javax.ws.rs.core.Response#hashCode()
     */
    @Override
    public int hashCode() {
        return response.hashCode();
    }

    /**
     * @see javax.ws.rs.core.Response#equals(Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Response)
            return response.equals((Response)obj);
        else
            return false;
    }

    /**
     * @see javax.ws.rs.core.Response#toString()
     */
    @Override
    public String toString() {
        return response.toString();
    }

}
