/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.primefaces.context.RequestContext;

import com.google.common.base.CharMatcher;

/**
 * Contains some utility methods.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class Utility {

    public static final String MESSAGE_SUMMARY_SUCCESS = "Success";
    public static final String MESSAGE_SUMMARY_ERROR = "Error";

    private Utility() {}

    /**
     * Utility method used to display a message to the user.
     *
     * @param severity
     *            Severity of the message
     * @param summary
     *            Summary of the message
     * @param message
     *            Detailed message contents
     */
    public static void showMessage(FacesMessage.Severity severity, String summary, String message) {
        final FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severity, summary, message));
    }

    /**
     * Utility method used to update component.
     *
     * @param componentId
     *            component id
     */
    public static void updateComponent(String componentId) {
        final RequestContext context = RequestContext.getCurrentInstance();
        context.update(componentId);
    }

    /**
     * Utility method used for system validation.
     *
     * @param value
     *            value
     * @throws ValidatorException
     *             if value is not valid system.
     */
    public static void isValidSystem(Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty() || stringValue.length() != 1) {
            return;
        }
        if (!Character.isDigit(stringValue.charAt(0))) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid system value.", null));
        }
    }

    /**
     * Utility method used for subsystem validation.
     *
     * @param value
     *            value
     * @throws ValidatorException
     *             if value is not valid subsystem.
     */
    public static void isValidSubsystem(Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty() || stringValue.length() != 1) {
            return;
        }
        if (!Character.isDigit(stringValue.charAt(0)) && !Character.isLetter(stringValue.charAt(0))) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Entered value is not a valid subsystem value.", null));
        }
    }

    /**
     * Utility method used for cable class validation.
     *
     * @param value
     *            value
     * @throws ValidatorException
     *             if value is not valid cable class.
     */
    public static void isValidCableClass(Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty() || stringValue.length() != 1) {
            return;
        }
        if (!Character.isLetter(stringValue.charAt(0))) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Entered value is not a valid cable class value.", null));
        }
    }

    /**
     * Utility method used for URL validation.
     *
     * @param value
     *            value
     * @throws ValidatorException
     *             if value is not valid URL.
     */
    public static void isURLEntered(Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        try {
            new URL(stringValue);
        } catch (MalformedURLException e) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid url.", null));
        }
    }

    /**
     * Utility method for Integer validation.
     *
     * @param value
     *            value
     * @throws ValidatorException
     *             if value is not valid Integer.
     */
    public static void isIntegerEntered(Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        try {
            Integer.parseInt(stringValue);
        } catch (NumberFormatException ne) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid number", null));
        }
    }

    /**
     * Utility method used for Double validation.
     *
     * @param value
     *            value
     * @throws ValidatorException
     *             if value is not valid Double.
     */
    public static void isFloatEntered(Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        try {
            Float.parseFloat(stringValue);
        } catch (NumberFormatException ne) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid number", null));
        }
    }

    /**
     * Checks whether a {@link Collection} is <code>null</code> or empty.
     * 
     * @param collection
     *            the collection to test
     * @return <code>true</code> if collection is <code>null</code> or empty, <code>false</code> otherwise
     */
    public static boolean isNullOrEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Checks whether a {@link Map} is <code>null</code> or empty.
     * 
     * @param map
     *            the map to test
     * @return <code>true</code> if map is <code>null</code> or empty, <code>false</code> otherwise
     */
    public static boolean isNullOrEmpty(final Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    /**
     * Formats given string by collapsing and trimming all whitespaces
     * 
     * @param string
     *            to format
     * @return formatted string or null if string to format is null
     */
    public static String formatWhitespaces(String string) {
        return string != null ? CharMatcher.whitespace().trimAndCollapseFrom(string, ' ') : null;
    }

    /**
     * Breaks string if it is too long.
     *
     * @param string
     *            string to format.
     *
     * @return new reformatted string.
     */
    public static String formatOverlayContentText(String string) {
        StringBuilder sb = new StringBuilder(string);
        int i = 0;
        while ((i = sb.indexOf(" ", i + 37)) != -1) {
            sb.replace(i, i + 1, "\n");
        }
        return sb.toString();
    }

    /**
     * Splits a string at "," and returns results as a list
     * 
     * @param data
     *            string of data to split
     * @return a list of separated values
     */
    public static List<String> splitStringIntoList(String data) {
        return Arrays.asList(data.split("\\s*,\\s*"));
    }
}
