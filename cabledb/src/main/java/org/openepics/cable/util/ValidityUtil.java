/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.dto.NotificationDTO;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.NameStatus;
import org.openepics.cable.services.Names;
import org.openepics.cable.services.dl.CableColumn;

public class ValidityUtil {

    /*
       note
           currently no check on cable container
           original idea - check names
               isCableContainerInvalid (CableService cableService, Cable cable)
                   if container not empty
                       get cable(s) with name as container name
                           CableService#getCablesByName(Iterable<String>, Boolean, boolean)
                       if empty return true
                   return false
           usage of container include cable name and also (mostly) just a strap around cable(s)
               as strap is not cable, if strap name is available, it is not ESS name
               and any check should not be based on ESS name
     */

    // error tooltip-messages to UI
    private static final String INVALID_CABLE_ARTICLE                       = "Cable Article is invalid";
    private static final String INVALID_CABLE_TYPE                          = "Cable Type is invalid";
    private static final String NEITHER_CABLE_ARTICLE_NOR_CABLE_TYPE_ACTIVE = "Neither Cable Article nor Cable Type is active";
    private static final String INVALID_FROM_ENDPOINT_ESS_NAME_FBS_TAG      = "From Endpoint ESS Name and FBS Tag are invalid";
    private static final String DELETED_FROM_ENDPOINT_ESS_NAME              = "From Endpoint ESS Name is deleted";
    private static final String INVALID_FROM_ENDPOINT_ESS_NAME              = "From Endpoint ESS Name is invalid";
    private static final String INVALID_FROM_ENDPOINT_CONNECTOR             = "From Endpoint Connector is invalid";
    private static final String INVALID_TO_ENDPOINT_ESS_NAME_FBS_TAG        = "To Endpoint ESS Name and FBS Tag are invalid";
    private static final String DELETED_TO_ENDPOINT_ESS_NAME                = "To Endpoint ESS Name is deleted";
    private static final String INVALID_TO_ENDPOINT_ESS_NAME                = "To Endpoint ESS Name is invalid";
    private static final String INVALID_TO_ENDPOINT_CONNECTOR               = "To Endpoint Connector is invalid";
    private static final String INVALID_CABLE                               = "Cable is invalid";
    private static final String TOOLTIP_MESSAGE_SEPARATOR                   = "; ";

    /**
     * This class is not to be instantiated.
     */
    private ValidityUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return boolean if cable article is invalid.
     *
     * @param cable cable
     * @return boolean if cable article is invalid
     */
    public static boolean isCableArticleInvalid(Cable cable) {
        return cable != null && cable.getCableArticle() != null && !cable.getCableArticle().isActive();
    }

    /**
     * Return boolean if cable type is invalid.
     *
     * @param cable cable
     * @return boolean if cable type is invalid
     */
    public static boolean isCableTypeInvalid(Cable cable) {
        return cable != null && cable.getCableType() != null && !cable.getCableType().isActive();
    }

    /**
     * Return boolean if cable article or cable type is active.
     *
     * @param cable cable
     * @return boolean if cable article or cable type is active
     */
    public static boolean isCableArticleCableTypeActive(Cable cable) {
        return cable != null && (
                    (cable.getCableArticle() != null && cable.getCableArticle().isActive())
                ||	(cable.getCableType() != null && cable.getCableType().isActive()));
    }

    /**
     * Return boolean if cable ESS name or FBS tag is available.
     *
     * @param cable cable
     * @param isEndpointA if endpointA or not, if true then endpointA, if false then endpointB
     * @return boolean if cable ESS name or FBS tag is available
     */
    public static boolean isCableEndpointEssNameFbsTagAvailable(Cable cable, boolean isEndpointA) {
        return isEndpointA
                ? cable != null
                    && cable.getEndpointA() != null
                    && (StringUtils.isNotEmpty(cable.getEndpointA().getDevice()) || StringUtils.isNotEmpty(cable.getEndpointA().getDeviceFbsTag()))
                : cable != null
                    && cable.getEndpointB() != null
                    && (StringUtils.isNotEmpty(cable.getEndpointB().getDevice()) || StringUtils.isNotEmpty(cable.getEndpointB().getDeviceFbsTag()));
    }

    /**
     * Return boolean if cable endpoint ESS name is invalid.
     *
     * @param cable cable
     * @param isEndpointA if endpointA or not, if true then endpointA, if false then endpointB
     * @param names handle to set of names
     * @return boolean if cable endpoint ESS name is invalid
     */
    public static boolean isCableEndpointEssNameDeleted(Cable cable, boolean isEndpointA, Names names) {
        return isEndpointA
                ? cable != null
                    && cable.getEndpointA() != null
                    && (names.getNameStatus(cable.getEndpointA().getDevice()) == NameStatus.DELETED)
                : cable != null
                    && cable.getEndpointB() != null
                    && (names.getNameStatus(cable.getEndpointB().getDevice()) == NameStatus.DELETED);
    }

    /**
     * Return boolean if cable endpoint ESS name uuid is invalid.
     *
     * @param cable cable
     * @param isEndpointA if endpointA or not, if true then endpointA, if false then endpointB
     * @param names handle to set of names
     * @return boolean if cable endpoint ESS name uuid is invalid
     */
    public static boolean isCableEndpointEssNameUuidInvalid(Cable cable, boolean isEndpointA, Names names) {
        // uuid may be null or empty
        // if uuid is not found in the naming service then name is invalid
        return isEndpointA
                ? cable != null
                    && cable.getEndpointA() != null
                    && StringUtils.isNotEmpty(cable.getEndpointA().getUuid())
                    && names.getNameByUuid(cable.getEndpointA().getUuid()) == null
                : cable != null
                    && cable.getEndpointB() != null
                    && StringUtils.isNotEmpty(cable.getEndpointB().getUuid())
                    && names.getNameByUuid(cable.getEndpointB().getUuid()) == null;
    }

    /**
     * Update cable endpoint ESS name based on uuid for name, if ESS name is outdated,
     * and return message to write to log (non-null if applicable).
     *
     * @param cable cable
     * @param isEndpointA if endpointA or not, if true then endpointA, if false then endpointB
     * @param names handle to set of names
     * @return message to write to log (non-null if applicable)
     */
    public static String uuidUpdateCableEndpointEssName(Cable cable, boolean isEndpointA, Names names) {
        if (isEndpointA) {
            if (cable != null
                    && cable.getEndpointA() != null
                    && StringUtils.isNotEmpty(cable.getEndpointA().getUuid())) {
                Endpoint endpoint = cable.getEndpointA();
                String nameInNaming = names.getNameByUuid(endpoint.getUuid());
                if (!nameInNaming.equals(endpoint.getDevice())) {
                    endpoint.setDevice(nameInNaming);
                    return "EndpointA outdated, updated it from " + endpoint.getDevice() + " to " + nameInNaming + "\n";
                }
            }
        } else {
            if (cable != null
                    && cable.getEndpointB() != null
                    && StringUtils.isNotEmpty(cable.getEndpointB().getUuid())) {
                Endpoint endpoint = cable.getEndpointB();
                String nameInNaming = names.getNameByUuid(endpoint.getUuid());
                if (!nameInNaming.equals(endpoint.getDevice())) {
                    endpoint.setDevice(nameInNaming);
                    return "EndpointB outdated, updated it from " + endpoint.getDevice() + " to " + nameInNaming + "\n";
                }
            }
        }
        return null;
    }

    /**
     * Return boolean if cable endpoint connector is invalid.
     *
     * @param cable cable
     * @param isEndpointA if endpointA or not, if true then endpointA, if false then endpointB
     * @return boolean of cable endpoint connector is invalid
     */
    public static boolean isCableEndpointConnectorInvalid(Cable cable, boolean isEndpointA) {
        return isEndpointA
                ? cable != null
                    && cable.getEndpointA() != null
                    && cable.getEndpointA().getConnector() != null
                    && !cable.getEndpointA().getConnector().isActive()
                : cable != null
                    && cable.getEndpointB() != null
                    && cable.getEndpointB().getConnector() != null
                    && !cable.getEndpointB().getConnector().isActive();
    }

    /**
     * Generates message with reason(s) for problem when there is a problem with cable.
     *
     * @param cable cable
     * @return message with reason(s) for problem
     */
    public static String getReasonsForProblem(Cable cable, Names names) {
        return getReasonsForProblem(cable, names, TOOLTIP_MESSAGE_SEPARATOR);
    }

    /**
     * Generates message with reason(s) for problem when there is a problem with cable.
     *
     * @param cable cable
     * @param names names
     * @param messageSeparator message separator
     * @return message with reason(s) for problem
     */
    public static String getReasonsForProblem(Cable cable, Names names, String messageSeparator) {
        List<String> problemList = new ArrayList<>();

        // attributes
        //     no check on cable container
        if (isCableArticleInvalid(cable)) {
            problemList.add(INVALID_CABLE_ARTICLE);
        }
        if (isCableTypeInvalid(cable)) {
            problemList.add(INVALID_CABLE_TYPE);
        }
        if (!isCableArticleCableTypeActive(cable)) {
            problemList.add(NEITHER_CABLE_ARTICLE_NOR_CABLE_TYPE_ACTIVE);
        }

        // endpoint a
        if (!isCableEndpointEssNameFbsTagAvailable(cable, true)) {
            problemList.add(INVALID_FROM_ENDPOINT_ESS_NAME_FBS_TAG);
        }
        if (isCableEndpointEssNameDeleted(cable, true, names)) {
            problemList.add(DELETED_FROM_ENDPOINT_ESS_NAME);
        }
        if (isCableEndpointEssNameUuidInvalid(cable, true, names)) {
            problemList.add(INVALID_FROM_ENDPOINT_ESS_NAME);
        }
        if (isCableEndpointConnectorInvalid(cable, true)) {
            problemList.add(INVALID_FROM_ENDPOINT_CONNECTOR);
        }

        // endpoint b
        if (!isCableEndpointEssNameFbsTagAvailable(cable, false)) {
            problemList.add(INVALID_TO_ENDPOINT_ESS_NAME_FBS_TAG);
        }
        if (isCableEndpointEssNameDeleted(cable, false, names)) {
            problemList.add(DELETED_TO_ENDPOINT_ESS_NAME);
        }
        if (isCableEndpointEssNameUuidInvalid(cable, false, names)) {
            problemList.add(INVALID_TO_ENDPOINT_ESS_NAME);
        }
        if (isCableEndpointConnectorInvalid(cable, false)) {
            problemList.add(INVALID_TO_ENDPOINT_CONNECTOR);
        }

        // other
        if((problemList.isEmpty()) && cable != null && !cable.isValid()) {
            problemList.add(INVALID_CABLE);
        }

        return StringUtils.join(problemList, StringUtils.isNotEmpty(messageSeparator) ? messageSeparator : TOOLTIP_MESSAGE_SEPARATOR);
    }

    public static List<NotificationDTO> getNotificationsForProblem(Cable cable, Names names) {
        List<NotificationDTO> notifications = new ArrayList<>();

        // note
        //     max one notification per column
        // columns
        //     cable article (*)
        //     cable type    (*)
        //     from ess name (*)
        //     from fbs tag
        //     from connector
        //     to ess name   (*)
        //     to fbs tag
        //     to connector
        // columns (*) may have multiple reasons
        //     keep track of multiple reasons
        //     sort out notification at end of method
        String reasonsCableArticle = "";
        String reasonsCableType    = "";
        String reasonsFromEssName  = "";
        String reasonsToEssName    = "";

        // attributes
        //     no check on cable container
        if (isCableArticleInvalid(cable)) {
            reasonsCableArticle += TOOLTIP_MESSAGE_SEPARATOR + INVALID_CABLE_ARTICLE;
        }
        if (isCableTypeInvalid(cable)) {
            reasonsCableType += TOOLTIP_MESSAGE_SEPARATOR + INVALID_CABLE_TYPE;
        }
        if (!isCableArticleCableTypeActive(cable)) {
            reasonsCableArticle += TOOLTIP_MESSAGE_SEPARATOR + NEITHER_CABLE_ARTICLE_NOR_CABLE_TYPE_ACTIVE;
            reasonsCableType += TOOLTIP_MESSAGE_SEPARATOR + NEITHER_CABLE_ARTICLE_NOR_CABLE_TYPE_ACTIVE;
        }

        // endpoint a
        if (!isCableEndpointEssNameFbsTagAvailable(cable, true)) {
            reasonsFromEssName += TOOLTIP_MESSAGE_SEPARATOR + INVALID_FROM_ENDPOINT_ESS_NAME_FBS_TAG;
            notifications.add(
                    new NotificationDTO(
                            null, cable.getEndpointA(), cable.getName(),
                            CableColumn.FROM_FBS_TAG, INVALID_FROM_ENDPOINT_ESS_NAME_FBS_TAG));
        }
        if (isCableEndpointEssNameDeleted(cable, true, names)) {
            reasonsFromEssName += TOOLTIP_MESSAGE_SEPARATOR + DELETED_FROM_ENDPOINT_ESS_NAME;
        }
        if (isCableEndpointEssNameUuidInvalid(cable, true, names)) {
            reasonsFromEssName += TOOLTIP_MESSAGE_SEPARATOR + INVALID_FROM_ENDPOINT_ESS_NAME;
        }
        if (isCableEndpointConnectorInvalid(cable, true)) {
            notifications.add(
                    new NotificationDTO(
                            null, cable.getEndpointA(), cable.getName(),
                            CableColumn.FROM_CONNECTOR, INVALID_FROM_ENDPOINT_CONNECTOR));
        }

        // endpoint b
        if (!isCableEndpointEssNameFbsTagAvailable(cable, false)) {
            reasonsToEssName += TOOLTIP_MESSAGE_SEPARATOR + INVALID_TO_ENDPOINT_ESS_NAME_FBS_TAG;
            notifications.add(
                    new NotificationDTO(
                            null, cable.getEndpointB(), cable.getName(),
                            CableColumn.TO_FBS_TAG, INVALID_TO_ENDPOINT_ESS_NAME_FBS_TAG));
        }
        if (isCableEndpointEssNameDeleted(cable, false, names)) {
            reasonsToEssName += TOOLTIP_MESSAGE_SEPARATOR + DELETED_TO_ENDPOINT_ESS_NAME;
        }
        if (isCableEndpointEssNameUuidInvalid(cable, false, names)) {
            reasonsToEssName += TOOLTIP_MESSAGE_SEPARATOR + INVALID_TO_ENDPOINT_ESS_NAME;
        }
        if (isCableEndpointConnectorInvalid(cable, false)) {
            notifications.add(
                    new NotificationDTO(
                            null, cable.getEndpointB(), cable.getName(),
                            CableColumn.TO_CONNECTOR, INVALID_TO_ENDPOINT_CONNECTOR));
        }

        // handle columns with potential multiple messages
        if (StringUtils.isNotEmpty(reasonsCableArticle)) {
            reasonsCableArticle = reasonsCableArticle.substring(1).trim();
            notifications.add(
                    new NotificationDTO(
                            cable, null, cable.getName(),
                            CableColumn.CABLE_ARTICLE, reasonsCableArticle));
        }
        if (StringUtils.isNotEmpty(reasonsCableType)) {
            reasonsCableType = reasonsCableType.substring(1).trim();
            notifications.add(
                    new NotificationDTO(
                            cable, null, cable.getName(),
                            CableColumn.CABLE_TYPE, reasonsCableType));
        }
        if (StringUtils.isNotEmpty(reasonsFromEssName)) {
            reasonsFromEssName = reasonsFromEssName.substring(1).trim();
            notifications.add(
                    new NotificationDTO(
                            null, cable.getEndpointA(), cable.getName(),
                            CableColumn.FROM_ESS_NAME, reasonsFromEssName));
        }
        if (StringUtils.isNotEmpty(reasonsToEssName)) {
            reasonsToEssName = reasonsToEssName.substring(1).trim();
            notifications.add(
                    new NotificationDTO(
                            null, cable.getEndpointB(), cable.getName(),
                            CableColumn.TO_ESS_NAME, reasonsToEssName));
        }

        // other
        if((notifications.isEmpty()) && cable != null && !cable.isValid()) {
            notifications.add(
                    new NotificationDTO(
                            cable, null, cable.getName(),
                            CableColumn.NAME, INVALID_CABLE));
        }

        return notifications;
    }

}
