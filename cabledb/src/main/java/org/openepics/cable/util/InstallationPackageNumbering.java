/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import java.io.Serializable;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

/**
 * <code>InstallationPackageNumbering</code> contains information such as labels for installation package.
 * Such labels are used in UI.
 *
 * @author Lars Johansson
 */
public class InstallationPackageNumbering implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8393174894084775160L;

    protected static final String INSTALLER_EMPTY            = "";
    protected static final String INSTALLER_ACTEMIUM         = "Actemium";
    protected static final String INSTALLER_ASSEMBLIN        = "Assemblin";
    protected static final String INSTALLER_COROMATIC        = "Coromatic";
    protected static final String INSTALLER_ESS_INSTALLATION = "ESS installation";
    protected static final String INSTALLER_IN_KIND          = "In-kind";
    protected static final String INSTALLER_WORK_PACKAGE     = "Work package";
    // .. TBA

    private static final String[] INSTALLER_LABELS = new String[] {
            INSTALLER_EMPTY,
            INSTALLER_ACTEMIUM,
            INSTALLER_ASSEMBLIN,
            INSTALLER_COROMATIC,
            INSTALLER_ESS_INSTALLATION,
            INSTALLER_IN_KIND,
            INSTALLER_WORK_PACKAGE };

    /**
     * Return installers for installation package.
     *
     * @return array of installers for installation package.
     */
    public static String[] getInstallerLabels() {
        return Arrays.copyOf(INSTALLER_LABELS, INSTALLER_LABELS.length);
    }

    /**
     * Return if given installer is valid
     *
     * @param installer installer to check if valid
     * @return if given installer is valid
     */
    public static boolean isValid(String installer) {
        if (installer == null) {
            return true;
        }
        for (String str : INSTALLER_LABELS) {
            if (StringUtils.equals(str, installer)) {
                return true;
            }
        }
        return false;
    }

}
