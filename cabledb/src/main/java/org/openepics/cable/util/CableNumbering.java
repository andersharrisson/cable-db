/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.util;

import java.io.Serializable;

/**
 * <code>CableNumberUI</code> contains cable system, subsystem and class labels. Labels are used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableNumbering implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String EMPTY_STRING = "";

    private static final String[] SYSTEM_NUMBERS = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    private static final String[] SUBSYSTEM_NUMBERS = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    private static final String[] CLASS_LETTERS = new String[] { "A", "B", "C", "D", "E", "F", "G" };
    private static final String[] SYSTEM_LABELS = new String[] { "1 (Safety & Environmental)", "2 (Front End)",
            "3 (Super Conducting Linac)", "4 (HEBT-A2T DmpL)", "5 (Target Systems)", "6 (NSS Instruments)",
            "7 (SI (CF))", "8 (ICS)", "9 (Cryogenics)" };
    private static final String[] CLASS_LABELS = new String[] { "A (Very Low Level Signals)",
            "B (Signal and Instrumentation)", "C (Control signals)", "D (Low Power AC)", "E (DC power cables)",
            "F (High Power AC cables)", "G (Medium AC Voltage cables)" };
    private static final String[] SUBSYSTEM_LABELS_1 = new String[] { "0 (TSS)", "1 (PSS/ARM)", "2 (ODH System)",
            "3 (Other Target safety)", "4 (Other safety)", "5 (Environmental compliance systems)", "6 (not defined)",
            "7 (not defined)", "8 (not defined)", "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_2 = new String[] { "0 (Magnets)", "1 (ISrc)", "2 (Diagnostic)",
            "3 (Vacuum)", "4 (Cooling)", "5 (RF)", "6 (not defined)", "7 (PS)", "8 (CNPW)", "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_3 = new String[] { "0 (Magnets)", "1 (not defined)",
            "2 (Diagnostics)", "3 (Vacuum)", "4 (Cooling)", "5 (RF)", "6 (Cryogenics)", "7 (PS)", "8 (CNPW)",
            "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_4 = new String[] { "0 (Magnets)", "1 (not defined)",
            "2 (Diagnostics)", "3 (Vacuum)", "4 (Cooling)", "5 (RF)", "6 (not defined)", "7 (PS)", "8 (CNPW)",
            "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_5 = new String[] { "0 (not defined)", "1 (not defined)",
            "2 (not defined)", "3 (not defined)", "4 (not defined)", "5 (not defined)", "6 (not defined)",
            "7 (not defined)", "8 (not defined)", "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_6 = new String[] { "0 (not defined)", "1 (not defined)",
            "2 (not defined)", "3 (not defined)", "4 (not defined)", "5 (not defined)", "6 (not defined)",
            "7 (not defined)", "8 (not defined)", "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_7 = new String[] { "0 (not defined)", "1 (not defined)",
            "2 (not defined)", "3 (not defined)", "4 (not defined)", "5 (not defined)", "6 (not defined)",
            "7 (not defined)", "8 (not defined)", "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_8 = new String[] { "0 (Timing)", "1 (MPS)", "2 (Network)", "3 (PLC)",
            "4 (IOC)", "5 (not defined)", "6 (not defined)", "7 (not defined)", "8 (not defined)", "9 (not defined)" };
    private static final String[] SUBSYSTEM_LABELS_9 = new String[] { "0 (ACCP compressor system)",
            "1 (ACCP coldbox system)", "2 (TICP compressor system)", "3 (TICP coldbox system)",
            "4 (TMCP compressor system)", "5 (TMCP coldbox system)", "6 (Helium recovery & storage)",
            "7 (Nitrogen system)", "8 (not defined)", "9 (not defined)" };

    /**
     * @return array of system numbers.
     */
    public static String[] getSystemNumbers() {
        return SYSTEM_NUMBERS;
    }

    /**
     * @return array of subsystem numbers.
     */
    public static String[] getSubsystemNumbers() {
        return SUBSYSTEM_NUMBERS;
    }

    /**
     * @return array of cable letters.
     */
    public static String[] getClassLetters() {
        return CLASS_LETTERS;
    }

    /**
     * @param systemNumber
     *            system number
     *
     * @return appropriate system label.
     */
    public static String getSystemLabel(String systemNumber) {
        try {
            return SYSTEM_LABELS[Integer.parseInt(systemNumber) - 1];
        } catch (NumberFormatException ex) {
            System.out.println(ex);
            return EMPTY_STRING;
        }
    }

    /**
     * @param system
     *            system number
     * @param subsystem
     *            subsystem number
     *
     * @return appropriate subsystem label.
     */
    public static String getSubsystemLabel(String system, String subsystem) {
        try {
            int subsystemNumber = Integer.parseInt(subsystem);
            switch (Integer.parseInt(system)) {
            case 1:
                return SUBSYSTEM_LABELS_1[subsystemNumber];
            case 2:
                return SUBSYSTEM_LABELS_2[subsystemNumber];
            case 3:
                return SUBSYSTEM_LABELS_3[subsystemNumber];
            case 4:
                return SUBSYSTEM_LABELS_4[subsystemNumber];
            case 5:
                return SUBSYSTEM_LABELS_5[subsystemNumber];
            case 6:
                return SUBSYSTEM_LABELS_6[subsystemNumber];
            case 7:
                return SUBSYSTEM_LABELS_7[subsystemNumber];
            case 8:
                return SUBSYSTEM_LABELS_8[subsystemNumber];
            case 9:
                return SUBSYSTEM_LABELS_9[subsystemNumber];
            }
        } catch (NumberFormatException ex) {
        }
        return EMPTY_STRING;
    }

    /**
     * @param cableClass
     *            cable class letter
     *
     * @return appropriate cable class label.
     */
    public static String getClassLabel(String cableClass) {
        switch (cableClass) {
        case "A":
            return CLASS_LABELS[0];
        case "B":
            return CLASS_LABELS[1];
        case "C":
            return CLASS_LABELS[2];
        case "D":
            return CLASS_LABELS[3];
        case "E":
            return CLASS_LABELS[4];
        case "F":
            return CLASS_LABELS[5];
        case "G":
            return CLASS_LABELS[6];
        }
        return EMPTY_STRING;
    }

    /**
     * @param system
     *            system label
     *
     * @return system number (first character).
     */
    public static String getSystemNumber(String system) {
        return system != null && !system.isEmpty() ? String.valueOf(system.charAt(0)) : EMPTY_STRING;
    }

    /**
     * @param subsystem
     *            subsystem label
     *
     * @return subsystem number (first character).
     */
    public static String getSubsystemNumber(String subsystem) {
        return subsystem != null && !subsystem.isEmpty() ? String.valueOf(subsystem.charAt(0)) : EMPTY_STRING;
    }

    /**
     * @param cableClass
     *            cable class label
     *
     * @return cable class letter (first character).
     */
    public static String getCableClassLetter(String cableClass) {
        return cableClass != null && !cableClass.isEmpty() ? String.valueOf(cableClass.charAt(0)) : EMPTY_STRING;
    }

    /**
     * @param system
     *            system label
     *
     * @return true if system is valid otherwise false
     */
    public static boolean isValidSystem(String system) {
        String systemNumber = String.valueOf(system.charAt(0));
        String systemLabel = getSystemLabel(systemNumber);
        return systemLabel != EMPTY_STRING;
    }

    /**
     * @param system
     *            system label
     * @param subsystem
     *            subsystem label
     *
     * @return true if subsystem is valid, otherwise false
     */
    public static boolean isValidSubsystem(String system, String subsystem) {
        char character = subsystem.charAt(0);
        String systemNumber = getSystemNumber(system);
        String subsystemNumber = String.valueOf(character);
        if (Character.isLetter(character)) {
            return false;
        }
        String subsystemLabel = getSubsystemLabel(systemNumber, subsystemNumber);
        return subsystemLabel != EMPTY_STRING;
    }

    /**
     * @param cableClass
     *            cable class label
     *
     * @return true if cable class is valid otherwise false.
     */
    public static boolean isValidCableClass(String cableClass) {
        String cableClassLetter = String.valueOf(cableClass.charAt(0));
        String cableClassLabel = getClassLabel(cableClassLetter);
        return cableClassLabel != EMPTY_STRING;
    }
}
