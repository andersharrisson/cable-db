/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import org.openepics.cable.model.CableStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Utility class that can be user to check if certain status transitions are allowed,
 * or which further statuses can be chosen from.
 *
 * @author Imre Toth <imre.toth@esss.se>
 **/
public class StatusTransitionUtil {

    //Utility class should not be instantiated
    private StatusTransitionUtil() {
    }

    //Map that contains the cable status transition rules
    //key = status from
    //value = set of statuses that are allowed to transfer to from the key status
    private static Map<CableStatus, Set<CableStatus>> cableTransitionStatuses;

    //static init for transition rules
    static {
        cableTransitionStatuses = new EnumMap<>(CableStatus.class);

        cableTransitionStatuses.put(CableStatus.INSERTED,
                Collections.unmodifiableSet(
                        new HashSet<>(
                                Arrays.asList(CableStatus.APPROVED,
                                        CableStatus.DELETED))));

        cableTransitionStatuses.put(CableStatus.APPROVED,
                Collections.unmodifiableSet(
                        new HashSet<>(
                                Arrays.asList(CableStatus.ROUTED,
                                        CableStatus.INSERTED,
                                        CableStatus.DELETED))));

        cableTransitionStatuses.put(CableStatus.ROUTED,
                Collections.unmodifiableSet(
                        new HashSet<>(
                                Arrays.asList(CableStatus.APPROVED,
                                        CableStatus.PULLED,
                                        CableStatus.COMMISSIONED,
                                        CableStatus.DELETED))));

        cableTransitionStatuses.put(CableStatus.PULLED,
                Collections.unmodifiableSet(
                        new HashSet<>(
                                Arrays.asList(CableStatus.COMMISSIONED,
                                        CableStatus.NOT_IN_USE))));

        cableTransitionStatuses.put(CableStatus.COMMISSIONED,
                Collections.unmodifiableSet(
                        new HashSet<>(
                                Arrays.asList(CableStatus.PULLED,
                                        CableStatus.NOT_IN_USE))));

        cableTransitionStatuses.put(CableStatus.NOT_IN_USE,
                Collections.unmodifiableSet(
                        new HashSet<>(
                                Arrays.asList(CableStatus.PULLED,
                                        CableStatus.DELETED))));
    }

    /**
     * Shows a collection of statuses that can be used during status transition.
     * Used by the web UI to fill UI element with the possible values
     *
     * @param currentStatus the status for which the allowed status should be returned
     * @return NULL, if no other statuses can be chosen from, or
     * collection of statuses from which can be chosen during status transition
     */
    public static Collection<CableStatus> possibleStatuses(CableStatus currentStatus) {

        if (currentStatus == null) {
            throw new StatusTransitionException("Cable status can not be null");
        }

        return cableTransitionStatuses.get(currentStatus);
    }


    /**
     * Checks if a transition from one cable status is allowed to another status.
     * Throws StatusTransitionException if transition is not allowed.
     *
     * @param currentStatus the cable status to transform from
     * @param newStatus     the new status of the cable to transform to
     * @return <code>true</code> if transition is allowed, or throws StatusTransitionException
     * @throws StatusTransitionException if status transition is not allowed, or if any of the status parameters are NULL value.
     *                                   The exception message will contain the possibly allowed statuses
     */
    public static boolean transitionAllowed(CableStatus currentStatus, CableStatus newStatus) {

        //parameters shouldn't be NULL
        if ((currentStatus == null) || (newStatus == null)) {
            throw new StatusTransitionException("Cable status can not be NULL value!");
        }

        //allow "transition" if the new, and old status is the same
        if (currentStatus.equals(newStatus)) {
            return true;
        }

        Set<CableStatus> possibleStatuses = cableTransitionStatuses.get(currentStatus);

        //no possible transition
        if (possibleStatuses == null) {
            throw new StatusTransitionException("Status " + currentStatus + " can not be changed!");
        }

        //OLD status found, and NEW status is in the possible transition set, -> status transition is allowed
        if (possibleStatuses.contains(newStatus)) {
            return true;
        }

        //create error message that will contain the chose-able statuses
        String allowedStatuses = possibleStatuses.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(" or "));

        throw new StatusTransitionException("This cable status can only be changed to " + allowedStatuses);
    }
}
