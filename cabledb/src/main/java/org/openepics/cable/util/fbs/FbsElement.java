/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util.fbs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Element of FBS list.
 *
 * <p>Note
 * <ul>
 * <li> Abbreviations
 * <ul>
 * <li> FBS - Facility Breakdown Structure
 * <li> LBS - Location Breakdown Structure
 * </ul>
 * <li> Information retrieved from ITIP
 * <ul>
 * <li> integration platform with CHESS information
 * </ul>
 * <li> CHESS
 * <ul>
 * <li> a cable name can be associated with zero or one FBS tag
 * <li> an  ESS name can be associated with zero or one FBS tag
 * <li> an  FBS tag  can lack association with cable name and ESS name
 * <li> an  FBS tag  can be associated with maximum one cable name and ESS name
 * <li> an  FBS tag  can be associated with unlimited amount (zero or more) of LBS tags
 * <li> an  LBS tag  can be associated with zero or more FBS tags
 * </ul>
 * <li> Additional
 * <ul>
 * <li> id (not parent id) is used once for one set of information
 * </ul>
 * </ul>
 *
 * @author Lars Johansson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FbsElement {

    // Note
    //     CHESS/ITIP
    //         xml
    //         json
    // --------------------------------------------------------------------------------
    //     Examples
    //         xml
    //             ----------
    //		       cableName
    //             ----------
    //	           <fbslisting>
    //	               <fbs cableName="21F009765" alternateTag="" syncInfo="Y" ssc="" isoClass="High Voltage Power Cable"
    //                     tag="=ESS.ACC.E01.W03.WB02" id="ESS-0415608" level="5"
    //                     description="ISrc - High Voltage Power Cable "
    //                     state="Preliminary" modified="2021-05-12T11:11:43" parent="ESS-1949824">
    //	                   <locatedIn tag="+ESS.G01.090" id="ESS-0052761" level="3"
    //                         description="G01 Level 090" state="Released" modified="2021-05-24T09:17:13">
    //	                       <documents/>
    //	                   </locatedIn>
    //	                   <documents/>
    //	               </fbs>
    //	           </fbslisting>
    //             ----------
    //		       essName
    //             ----------
    //	           <fbslisting>
    //	               <fbs essName="TS2-010Row:CnPw-U-006" alternateTag="" syncInfo="Y" ssc=""
    //                     isoClass="Instrument Rack"
    //                     tag="=ESS.ACC.A06.E02.UH02" id="ESS-0401755" level="5" description="Control Rack - Mon"
    //                     state="Preliminary" modified="2021-05-12T11:11:39" parent="ESS-0108818">
    //	                   <locatedIn tag="+ESS.G02.100.1001.102.104.006" id="ESS-0116567" level="7"
    //                         description="TS2-010ROW:CNPW-U-006" state="Preliminary" modified="2021-02-23T11:01:34">
    //	                       <documents/>
    //	                   </locatedIn>
    //	                   <documents/>
    //	               </fbs>
    //	           </fbslisting>
    // --------------------------------------------------------------------------------
    //         json
    //             ----------
    //             cableName
    //             ----------
    //             [{"cableName":"21F009765","description":"ISrc - High Voltage Power Cable ","essName":null,
    //               "id":"ESS-0415608","level":5,"modified":"2021-05-12T11:11:43","parent":"ESS-1949824",
    //               "state":"Preliminary","tag":"=ESS.ACC.E01.W03.WB02"}]
    //             ----------
    //             essName
    //             ----------
    //             [{"cableName":null,"description":"Control Rack - Mon","essName":"TS2-010Row:CnPw-U-006",
    //               "id":"ESS-0401755","level":5,"modified":"2021-05-12T11:11:39","parent":"ESS-0108818",
    //               "state":"Preliminary","tag":"=ESS.ACC.A06.E02.UH02"}]
    // --------------------------------------------------------------------------------
    //     fbs element contains other information than cable name, ESS name, FBS tag
    //         among other pieces of information
    //             documents
    //             LBS tag, i.e. locatedIn tag
    //
    //     e.g. cableName
    //         <fbs cableName="82B033868" alternateTag="" syncInfo="" ssc="" isoClass="Network Cable"
    //             tag="=ESS.INFR.W02.W03.W09.WF62" id="ESS-1418450" level="6" description="COPPER DISTRIBUTION"
    //             state="Preliminary" modified="2019-08-16T14:26:40" parent="ESS-0116914">
    //             <locatedIn tag="+ESS.G02.100.2001.102.107.003" id="ESS-0115990" level="7"
    //                 description="MBL-050ROW:CNPW-U-003" state="Preliminary" modified="2020-01-23T07:18:25">
    //                 <documents/>
    //             </locatedIn>
    //             <documents/>
    //         </fbs>
    //
    //     e.g. essName
    //         <fbs essName="HBL-070RFC:RFS-DirC-420" alternateTag="" syncInfo="" ssc="" isoClass="Signal Splitter"
    //             tag="=ESS.ACC.A05.A08.E04.T01.GF05" id="ESS-0438896" level="7" description="Directional Coupler"
    //             state="Preliminary" modified="2018-10-17T15:13" parent="ESS-0438861">
    //             <documents/>
    //         </fbs>
    // --------------------------------------------------------------------------------
    //     fields to be public for json to work
    // --------------------------------------------------------------------------------

    /**
     * Cable name
     */
    @SuppressFBWarnings("ClassVariableVisibilityCheck")
    public String cableName;

    /**
     * ESS name
     */
    @SuppressFBWarnings("ClassVariableVisibilityCheck")
    public String essName;

    /**
     * Id
     */
    @SuppressFBWarnings("ClassVariableVisibilityCheck")
    public String id;

    /**
     * FBS tag
     */
    @SuppressFBWarnings("ClassVariableVisibilityCheck")
    public String tag;

}
