package org.openepics.cable.export;

import java.util.List;

import org.openepics.cable.services.dl.ConnectorColumn;
import org.openepics.cable.ui.ConnectorUI;
import org.openepics.cable.util.Utility;

public class SimpleConnectorTableExporter extends SimpleTableExporter {

    private List<ConnectorUI> selectedConnectors;
    private List<ConnectorUI> filteredConnectors;

    public SimpleConnectorTableExporter(List<ConnectorUI> selectedConnectors, List<ConnectorUI> filteredConnectors) {
        this.selectedConnectors = selectedConnectors;
        this.filteredConnectors = filteredConnectors;
    }

    @Override
    protected String getTableName() {
        return "Connectors";
    }

    @Override
    protected String getFileName() {
        return "cdb_connectors";
    }

    @Override
    protected void addHeaderRow(ExportTable exportTable) {
        exportTable.addHeaderRow(ConnectorColumn.NAME.getColumnLabel(), ConnectorColumn.DESCRIPTION.getColumnLabel(),
                ConnectorColumn.TYPE.getColumnLabel());
    }

    @Override
    protected void addData(ExportTable exportTable) {
        final List<ConnectorUI> exportData = Utility.isNullOrEmpty(filteredConnectors) ? selectedConnectors
                : filteredConnectors;
        for (final ConnectorUI record : exportData) {
            exportTable.addDataRow(record.getName(), record.getDescription(), record.getType());
        }
    }

    @Override
    protected String getExcelTemplatePath() {
        // Connectors do not have a template
        return null;
    }

    @Override
    protected int getExcelDataStartRow() {
        // Connectors do not have a template
        return 0;
    }
}
