package org.openepics.cable.export;

import java.util.List;

import org.openepics.cable.services.dl.ManufacturerColumn;
import org.openepics.cable.ui.ManufacturerUI;
import org.openepics.cable.util.Utility;

public class SimpleManufacturerTableExporter extends SimpleTableExporter {

    private List<ManufacturerUI> selectedManufacturers;
    private List<ManufacturerUI> filteredManufacturers;

    public SimpleManufacturerTableExporter(List<ManufacturerUI> selectedManufacturers,
            List<ManufacturerUI> filteredManufacturers) {
        this.selectedManufacturers = selectedManufacturers;
        this.filteredManufacturers = filteredManufacturers;
    }

    @Override
    protected String getTableName() {
        return "Manufacturers";
    }

    @Override
    protected String getFileName() {
        return "cdb_manufacturers";
    }

    @Override
    protected void addHeaderRow(ExportTable exportTable) {
        exportTable.addHeaderRow(ManufacturerColumn.NAME.getColumnLabel(), ManufacturerColumn.ADDRESS.getColumnLabel(),
                ManufacturerColumn.PHONE.getColumnLabel(), ManufacturerColumn.EMAIL.getColumnLabel(),
                ManufacturerColumn.COUNTRY.getColumnLabel());
    }

    @Override
    protected void addData(ExportTable exportTable) {
        final List<ManufacturerUI> exportData = Utility.isNullOrEmpty(filteredManufacturers) ? selectedManufacturers
                : filteredManufacturers;
        for (final ManufacturerUI record : exportData) {
            exportTable.addDataRow(record.getName(), record.getAddress(), record.getPhoneNumber(), record.getEmail(),
                    record.getCountry());
        }
    }

    @Override
    protected String getExcelTemplatePath() {
        // Manufacturers do not have a template
        return null;
    }

    @Override
    protected int getExcelDataStartRow() {
        // Manufacturers do not have a template
        return 0;
    }
}
