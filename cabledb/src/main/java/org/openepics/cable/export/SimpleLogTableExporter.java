package org.openepics.cable.export;

import java.util.List;

import org.openepics.cable.ui.HistoryUI;

public class SimpleLogTableExporter extends SimpleTableExporter {

    private List<HistoryUI> selectedEntityHistory;

    public SimpleLogTableExporter(List<HistoryUI> selectedEntityHistory) {
        this.selectedEntityHistory = selectedEntityHistory;
    }

    @Override
    protected String getTableName() {
        return "Log";
    }

    @Override
    protected String getFileName() {
        return "cdb_log";
    }

    @Override
    protected void addHeaderRow(ExportTable exportTable) {
        exportTable.addHeaderRow("Timestamp", "User", "Entity Type", "Operation", "Entity name", "Entity ID", "Change");
    }

    @Override
    protected void addData(ExportTable exportTable) {
        final List<HistoryUI> exportData = selectedEntityHistory;
        for (final HistoryUI record : exportData) {
            exportTable.addDataRow(record.getTimestamp(), record.getUser(), record.getEntityType(),
                    record.getOperation().toString(), record.getEntityName(), record.getEntityId(), record.getEntry());
        }
    }

    @Override
    protected String getExcelTemplatePath() {
        // log does not have a template
        return null;
    }

    @Override
    protected int getExcelDataStartRow() {
        // log does not have a template
        return 0;
    }

    public int getEntriesToExportSize() {
        return selectedEntityHistory.size();
    }
}
