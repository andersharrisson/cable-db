/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of history (history record).
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class History extends Persistable {

    private static final long serialVersionUID = 3562933281489391801L;

    public static final int ENTRY_FIELD_LENGTH = 65535;

    private Date timestamp;
    @Enumerated(EnumType.STRING)
    private EntityTypeOperation operation;
    private String userId;
    private String entityName;
    @Enumerated(EnumType.STRING)
    private EntityType entityType;
    private Long entityId;
    @Column(length = ENTRY_FIELD_LENGTH)
    private String entry;

    /** Constructor for JPA entity. */
    public History() {}

    /**
     * Creates a new instance of a history.
     * 
     * @param timestamp
     *            history entry timestamp
     * @param operation
     *            history entry operation
     * @param userId
     *            history entry user id
     * @param entityName
     *            history entry entity name
     * @param entityType
     *            history entry entity type
     * @param entityId
     *            history entry entity id
     * @param entry
     *            history entry
     */
    public History(Date timestamp, EntityTypeOperation operation, String userId, String entityName,
            EntityType entityType, Long entityId, String entry) {

        Preconditions.checkNotNull(timestamp);
        Preconditions.checkNotNull(operation);
        Preconditions.checkNotNull(userId);
        Preconditions.checkNotNull(entityName);
        Preconditions.checkNotNull(entityType);
        Preconditions.checkNotNull(entityId);
        Preconditions.checkNotNull(entry);

        this.timestamp = timestamp;
        this.operation = operation;
        this.userId = userId;
        this.entityName = entityName;
        this.entityType = entityType;
        this.entityId = entityId;
        this.entry = entry;
    }

    /** @return history entry time stamp */
    public Date getTimestamp() {
        return timestamp;
    }

    /** @return operation for history entry. */
    public EntityTypeOperation getOperation() {
        return operation;
    }

    /** @return user for history entry. */
    public String getUserId() {
        return userId;
    }

    /** @return history entity name. */
    public String getEntityName() {
        return entityName;
    }

    /** @return entity type for history entry. */
    public EntityType getEntityType() {
        return entityType;
    }

    /** @return entity id for history entry. */
    public Long getEntityId() {
        return entityId;
    }

    /** @return history entry. */
    public String getEntry() {
        return entry;
    }
}
