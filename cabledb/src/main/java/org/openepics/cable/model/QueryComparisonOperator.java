/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

/**
 * This represents the query comparison operators that are present in the query builder.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum QueryComparisonOperator {
    LESS_THAN("<", false),
    LESS_THAN_OR_EQUAL_TO("<=", false),
    EQUAL("=", false),
    GREATER_THAN(">", false),
    GREATER_THAN_OR_EQUAL_TO(">=", false),
    NOT_EQUAL("!=", false),

    // String operators
    STARTS_WITH("starts", true),
    ENDS_WITH("ends", true),
    CONTAINS("contains", true),
    LIKE("like", true);

    private final String operator;
    private final boolean isStringComparisonOperator;

    private QueryComparisonOperator(String operator, boolean isStringComparisonOperator) {
        this.operator = operator;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getOperator() {
        return operator;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }
}
