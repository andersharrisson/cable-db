/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

/**
 * This represents the installation type allowed for a cable type.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum InstallationType {
    /** No installation type allowed, set for cable types that are always local. */
    UNKNOWN("UNKNOWN"),
    /** Indoor installation type */
    INDOOR("INDOOR"),
    /** Outdoor installation type */
    OUTDOOR("OUTDOOR");

    private final String displayName;

    private InstallationType(String displayName) {
        this.displayName = displayName;
    }

    /** @return the display name for this installation type */
    public String getDisplayName() {
        return displayName;
    }

    public static InstallationType convertToInstallationType(String value) {
        if (UNKNOWN.getDisplayName().equalsIgnoreCase(value)) {
            return UNKNOWN;
        } else if (INDOOR.getDisplayName().equalsIgnoreCase(value)) {
            return INDOOR;
        } else if (OUTDOOR.getDisplayName().equalsIgnoreCase(value)) {
            return OUTDOOR;
        }
        return null;
    }
}
