package org.openepics.cable.model;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;

import org.openepics.cable.util.CableNumbering;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of routing.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class Routing extends Persistable {
    private static final long serialVersionUID = 3248815346130689290L;

    private String name;
    private String description;
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> cableClasses = new HashSet<>();
    private String location;
    private Float length;
    private Date created;
    private Date modified;
    private String owner;
    private boolean active;

    /**
     * Creates a new empty instance of a routing. Also serves as constructor for JPA entity.
     */
    public Routing() {
        setName("");
        setCableClasses(new HashSet<>());
        setActive(true);
    }

    /**
     * Creates a new instance of a routing.
     *
     * @param name
     *            the name of the routing
     * @param description
     *            the description of the routing
     * @param length
     *            the length of the routing
     * @param owner
     *            routing owner
     */
    public Routing(String name, String description, Float length, String owner) {
        this();
        setName(name);
        setDescription(description);
        setLength(length);
        setOwner(owner);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Preconditions.checkNotNull(name);
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getCableClasses() {
        return cableClasses;
    }

    public void setCableClasses(Set<String> cableClasses) {
        this.cableClasses = cableClasses;

    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCableClassesAsString() {
        return String.join(", ", cableClasses);
    }

    public void setCableClasses(String cableClasses) {
        this.cableClasses = getCableClassesAsSet(cableClasses);
    }

    public static boolean areCableClassesValid(String cableClasses) {
        Set<String> classes = getCableClassesAsSet(cableClasses);
        for (String klass : classes) {
            if (!CableNumbering.isValidCableClass(klass))
                return false;
        }
        return true;
    }

    private static Set<String> getCableClassesAsSet(String cableClasses) {
        return new HashSet<>(Arrays.asList(cableClasses.split("\\s*,\\s*")));
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        Preconditions.checkNotNull(length);
        this.length = length;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        Preconditions.checkArgument(owner != null && !owner.isEmpty());
        this.owner = owner;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean status) {
        this.active = status;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((cableClasses == null) ? 0 : cableClasses.hashCode());
        result = prime * result + ((length == null) ? 0 : length.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((owner == null) ? 0 : owner.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Routing other = (Routing) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (this.id != null)
            return id.equals(other.id);
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (cableClasses == null) {
            if (other.cableClasses != null)
                return false;
        } else if (!cableClasses.equals(other.cableClasses))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (length == null) {
            if (other.length != null)
                return false;
        } else if (!length.equals(other.length))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (owner == null) {
            if (other.owner != null)
                return false;
        } else if (!owner.equals(other.owner))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
