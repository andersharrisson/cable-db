/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.net.MalformedURLException;
import java.net.URL;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import com.google.common.base.Preconditions;

/**
 * This represents the endpoint of a {@link Cable}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class Endpoint extends Persistable {

    private static final long serialVersionUID = -5142520290025465864L;
    /** Maximum accepted length for label field. */
    public static final int MAX_LABEL_SIZE = 45;

    /** This represents the endpoint data validity. */
    public enum Validity {
        /** The endpoint data is valid. */
        VALID,
        /** The endpoint device is not present. */
        DANGLING
    }

    private String device;
    private String building;
    private String rack;
    @OneToOne
    private Connector connector;
    private String drawing;
    private String label;
    @Enumerated(EnumType.STRING)
    private Validity validity = Validity.VALID;

    /** Constructor for JPA entity. */
    protected Endpoint() {}

    /**
     * Constructs a new instance of cable endpoint.
     *
     * @param device
     *            The name of this endpoint device. The name cannot be null.
     */
    public Endpoint(String device) {
        this(device, null, null, null, null, null);
    }

    /**
     * Constructs a new instance of cable endpoint.
     *
     * @param device
     *            The name of this endpoint device. The name cannot be null.
     * @param building
     *            the building the endpoint is at
     * @param rack
     *            the rack the endpoint is at
     * @param connector
     *            the connector between cable and device.
     * @param drawing
     *            the reference to the wiring drawing of this endpoint
     * @param label
     *            the label
     */
    public Endpoint(String device, String building, String rack, Connector connector, URL drawing, String label) {
        Preconditions.checkArgument(device != null && !device.isEmpty());
        Preconditions.checkArgument(label == null || label.length() <= MAX_LABEL_SIZE);
        this.device = device;
        this.building = building;
        this.rack = rack;
        this.connector = connector;
        this.drawing = drawing != null ? drawing.toString() : null;
        this.label = label;
    }

    /**
     * Updates the attribute information of this endpoint device.
     * 
     * @param device
     *            The name of this endpoint device. The name cannot be null.
     * @param building
     *            the building the endpoint is at
     * @param rack
     *            the rack the endpoint is at
     * @param connector
     *            the connector between cable and device.
     * @param drawing
     *            the reference to the wiring drawing of this endpoint
     * @param label
     *            the label
     */
    public void update(String device, String building, String rack, Connector connector, URL drawing, String label) {
        Preconditions.checkArgument(device != null && !device.isEmpty());
        Preconditions.checkArgument(label == null || label.length() <= MAX_LABEL_SIZE);
        this.device = device;
        this.building = building;
        this.rack = rack;
        this.connector = connector;
        this.drawing = drawing != null ? drawing.toString() : null;
        this.label = label;
    }

    /**
     * @param status
     *            the endpoint status to set
     */
    public void setValidity(Validity status) {
        this.validity = status;
    }

    /** @return the name of the endpoint device */
    public String getDevice() {
        return device;
    }

    /** @return the building the endpoint is at */
    public String getBuilding() {
        return building;
    }

    /** @return the rack the endpoint is at */
    public String getRack() {
        return rack;
    }

    /** @return the connector between cable and device */
    public Connector getConnector() {
        return connector;
    }

    /** @return the reference to the wiring drawing of this endpoint */
    public URL getDrawing() {
        if (drawing == null) {
            return null;
        }
        try {
            return new URL(drawing);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /** @return the label */
    public String getLabel() {
        return label;
    }

    /** @return the endpoint data validity */
    public Validity getValidity() {
        return validity;
    }

    /** @return true if the endpoint data is valid, else false */
    public boolean isValid() {
        return validity == Validity.VALID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((building == null) ? 0 : building.hashCode());
        result = prime * result + ((device == null) ? 0 : device.hashCode());
        result = prime * result + ((connector == null) ? 0 : connector.hashCode());
        result = prime * result + ((drawing == null) ? 0 : drawing.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((rack == null) ? 0 : rack.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Endpoint other = (Endpoint) obj;
        if (this.id != null)
            return id.equals(other.id);
        if (building == null) {
            if (other.building != null)
                return false;
        } else if (!building.equals(other.building))
            return false;
        if (device == null) {
            if (other.device != null)
                return false;
        } else if (!device.equals(other.device))
            return false;
        if (connector == null) {
            if (other.connector != null)
                return false;
        } else if (!connector.equals(other.connector))
            return false;
        if (drawing == null) {
            if (other.drawing != null)
                return false;
        } else if (!drawing.equals(other.drawing))
            return false;
        if (label == null) {
            if (other.label != null)
                return false;
        } else if (!label.equals(other.label))
            return false;
        if (rack == null) {
            if (other.rack != null)
                return false;
        } else if (!rack.equals(other.rack))
            return false;
        if (validity != other.validity)
            return false;
        return true;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public void setRack(String rack) {
        this.rack = rack;
    }

    public void setConnector(Connector connector) {
        this.connector = connector;
    }

    public void setDrawing(String drawing) {
        this.drawing = drawing;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
