package org.openepics.cable.model;

/**
 * This represents the auto calculated status of a {@link Cable}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableAutoCalculatedLength {
    /** Cable length is not auto calculated. */
    NO("NO"),
    /** Cable length is auto calculated. */
    YES("YES");

    private final String displayName;

    private CableAutoCalculatedLength(String displayName) {
        this.displayName = displayName;
    }

    /** @return the display name for this status */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Converts String "YES"/"NO" value to CableAutoCalculatedLength values YES/NO.
     * 
     * @param value
     *            String to covert. Converts <code>"YES"</code> to <code>YES</code>, <code>"NO"</code> to
     *            <code>NO</code>, and everything else to <code>null</code>.
     * @return CableAutoCalculatedLength representation of <code>value</code>, or <code>null</code> for values other
     *         than YES or NO.
     */
    public static CableAutoCalculatedLength convertToCableAutoCalculatedLength(String value) {
        if (NO.getDisplayName().equalsIgnoreCase(value)) {
            return NO;
        } else if (YES.getDisplayName().equalsIgnoreCase(value)) {
            return YES;
        }
        return null;
    }

    /**
     * Coverts Boolean <code>true</code>/<code>false</code> to CableAutoCalculatedLength values YES/NO.
     * 
     * @param value
     *            Boolean to convert. Converts <code>true</code> to <code>YES</code>, <code>false</code> to
     *            <code>NO</code>, and everything else (<code>null</code>) to <code>null</code>.
     * @return CableAutoCalculatedLength representation of <code>value</code>, or <code>null</code> for invalid value.
     */
    public static CableAutoCalculatedLength convertToCableAutoCalculatedLength(Boolean value) {
        if (value == null) {
            return null;
        } else if (value) {
            return YES;
        } else {
            return NO;
        }
    }
}
