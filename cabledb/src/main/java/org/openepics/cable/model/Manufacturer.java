/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of manufacturer details.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class Manufacturer extends Persistable {

    private static final long serialVersionUID = -7654156206144341815L;

    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String country;
    private Date created;
    private Date modified;
    @Enumerated(EnumType.STRING)
    private ManufacturerStatus status = ManufacturerStatus.APPROVED;

    /** Constructor for JPA entity. */
    public Manufacturer() {}

    /**
     * Creates a new instance of manufacturer details.
     *
     * @param name
     *            manufacturer name
     * @param address
     *            manufacturer address
     * @param phoneNumber
     *            manufacturer phone
     * @param email
     *            manufacturer email address
     * @param country
     *            manufacturer country
     * @param created
     *            date when manufacturer details were created
     * @param modified
     *            date when manufacturer details were modified
     */
    public Manufacturer(String name, String address, String phoneNumber, String email, String country, Date created,
            Date modified) {
        Preconditions.checkArgument(name != null && !name.isEmpty());
        Preconditions.checkNotNull(created);
        Preconditions.checkNotNull(modified);

        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.country = country;
        this.created = created; // NOSONAR
        this.modified = modified; // NOSONAR
    }

    /**
     * Sets the modified date of the manufacturer details.
     *
     * @param modified
     *            the modified date to set
     */
    public void setModified(Date modified) {
        this.modified = modified; // NOSONAR
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getCountry() {
        return country;
    }

    public Date getCreated() {
        return created;// NOSONAR
    }

    public Date getModified() {
        return modified;// NOSONAR
    }

    public ManufacturerStatus getStatus() {
        return status;
    }

    public void setStatus(ManufacturerStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Manufacturer other = (Manufacturer) obj;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (phoneNumber == null) {
            if (other.phoneNumber != null)
                return false;
        } else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (status != other.status)
            return false;
        return true;
    }

}
