/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This represents an instance of query condition.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class QueryCondition extends Persistable implements Comparable<QueryCondition> {

    private static final long serialVersionUID = -6007734251143927822L;

    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_CONDITION_query"), name = "query", referencedColumnName = "id")
    private Query query;
    @Enumerated(EnumType.STRING)
    private QueryParenthesis parenthesisOpen;
    private String field;
    @Enumerated(EnumType.STRING)
    private QueryComparisonOperator comparisonOperator;
    private String value;
    @Enumerated(EnumType.STRING)
    private QueryParenthesis parenthesisClose;
    @Enumerated(EnumType.STRING)
    private QueryBooleanOperator booleanOperator;
    private Integer position;

    /** Constructor for JPA entity. */
    public QueryCondition() {}

    /**
     * Constructs new query condition.
     *
     * @param query
     *            query
     * @param parenthesisOpen
     *            open parenthesis
     * @param field
     *            field name
     * @param comparisonOperator
     *            comparison operator
     * @param value
     *            value
     * @param parenthesisClose
     *            close parenthesis
     * @param booleanOperator
     *            boolean operator
     * @param position
     *            position
     */
    public QueryCondition(Query query, QueryParenthesis parenthesisOpen, String field,
            QueryComparisonOperator comparisonOperator, String value, QueryParenthesis parenthesisClose,
            QueryBooleanOperator booleanOperator, Integer position) {
        this.query = query;
        this.parenthesisOpen = parenthesisOpen;
        this.field = field;
        this.comparisonOperator = comparisonOperator;
        this.value = value;
        this.parenthesisClose = parenthesisClose;
        this.booleanOperator = booleanOperator;
        this.position = position;
    }

    /** @return query condition parent (query). */
    public Query getQuery() {
        return query;
    }

    /** @return query condition open parenthesis. */
    public QueryParenthesis getParenthesisOpen() {
        return parenthesisOpen;
    }

    /** @return query condition field name. */
    public String getField() {
        return field;
    }

    /** @return query condition comparison operator. */
    public QueryComparisonOperator getComparisonOperator() {
        return comparisonOperator;
    }

    /** @return query condition value. */
    public String getValue() {
        return value;
    }

    /** @return query condition close parenthesis. */
    public QueryParenthesis getParenthesisClose() {
        return parenthesisClose;
    }

    /** @return query condition boolean operator. */
    public QueryBooleanOperator getBooleanOperator() {
        return booleanOperator;
    }

    /** @return query condition position for execution. */
    public Integer getPosition() {
        return position;
    }

    /** @return query condition as stirng */
    public String toString() {
        return parenthesisOpen.getParenthesis() + field + " " + comparisonOperator.getOperator() + " " + value
                + parenthesisClose.getParenthesis() + " " + booleanOperator.getOperator();
    }

    public QueryComparisonOperator getComparisonOperator(String comparisonOperator) {
        if (comparisonOperator.equals(QueryComparisonOperator.GREATER_THAN.getOperator())) {
            return QueryComparisonOperator.GREATER_THAN;
        } else if (comparisonOperator.equals(QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO.getOperator())) {
            return QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO;
        } else if (comparisonOperator.equals(QueryComparisonOperator.LESS_THAN.getOperator())) {
            return QueryComparisonOperator.LESS_THAN;
        } else if (comparisonOperator.equals(QueryComparisonOperator.LESS_THAN_OR_EQUAL_TO.getOperator())) {
            return QueryComparisonOperator.LESS_THAN_OR_EQUAL_TO;
        } else if (comparisonOperator.equals(QueryComparisonOperator.NOT_EQUAL.getOperator())) {
            return QueryComparisonOperator.NOT_EQUAL;
        } else if (comparisonOperator.equals(QueryComparisonOperator.STARTS_WITH.getOperator())) {
            return QueryComparisonOperator.STARTS_WITH;
        } else if (comparisonOperator.equals(QueryComparisonOperator.CONTAINS.getOperator())) {
            return QueryComparisonOperator.CONTAINS;
        } else if (comparisonOperator.equals(QueryComparisonOperator.ENDS_WITH.getOperator())) {
            return QueryComparisonOperator.ENDS_WITH;
        }
        return QueryComparisonOperator.EQUAL;
    }

    private QueryParenthesis getParenthesis(String parenthesis) {
        if (parenthesis.equals(QueryParenthesis.OPEN.getParenthesis())) {
            return QueryParenthesis.OPEN;
        } else if (parenthesis.equals(QueryParenthesis.CLOSE.getParenthesis())) {
            return QueryParenthesis.CLOSE;
        }
        return QueryParenthesis.NONE;
    }

    private QueryBooleanOperator getBooleanOperator(String booleanOperator) {
        if (booleanOperator.equals(QueryBooleanOperator.AND.getOperator())) {
            return QueryBooleanOperator.AND;
        } else if (booleanOperator.equals(QueryBooleanOperator.OR.getOperator())) {
            return QueryBooleanOperator.OR;
        } else if (booleanOperator.equals(QueryBooleanOperator.NOT.getOperator())) {
            return QueryBooleanOperator.NOT;
        }
        return QueryBooleanOperator.NONE;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public void setParenthesisOpen(QueryParenthesis parenthesisOpen) {
        this.parenthesisOpen = parenthesisOpen;
    }

    public void setField(String field) {
        this.field = field;
    }

    public void setComparisonOperator(QueryComparisonOperator comparisonOperator) {
        this.comparisonOperator = comparisonOperator;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setParenthesisClose(QueryParenthesis parenthesisClose) {
        this.parenthesisClose = parenthesisClose;
    }

    public void setBooleanOperator(QueryBooleanOperator booleanOperator) {
        this.booleanOperator = booleanOperator;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public int compareTo(QueryCondition o) {
        if (this.position < o.getPosition()) {
            return -1;
        } else if (this.position > o.getPosition()) {
            return 1;
        }
        return 0;
    }

}
