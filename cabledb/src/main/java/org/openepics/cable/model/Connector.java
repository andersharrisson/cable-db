/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.Date;

import javax.persistence.Entity;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of connector details.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
public class Connector extends Persistable {

    private static final long serialVersionUID = -7654156206144341815L;

    private String name;
    private String description;
    private String connectorType;
    private Date created;
    private Date modified;
    private boolean active = true;

    /** Constructor for JPA entity. */
    public Connector() {}

    /**
     * Creates a new instance of connector details.
     *
     * @param name
     *            connector name
     * @param created
     *            creation date
     * @param modified
     *            modification date
     */
    public Connector(String name, Date created, Date modified) {
        this(name, null, null, created, modified);
    }

    /**
     * Creates a new instance of connector details.
     *
     * @param name
     *            connector name
     * @param description
     *            connector description
     * @param type
     *            connector type
     * @param created
     *            date when connector details were created
     * @param modified
     *            date when connector details were modified
     */
    public Connector(String name, String description, String type, Date created, Date modified) {
        Preconditions.checkArgument(name != null && !name.isEmpty());
        Preconditions.checkNotNull(created);
        Preconditions.checkNotNull(modified);

        this.name = name;
        this.description = description;
        this.connectorType = type;
        this.created = created; // NOSONAR
        this.modified = modified; // NOSONAR
    }

    /**
     * Sets the modified date of the manufacturer details.
     *
     * @param modified
     *            the modified date to set
     */
    public void setModified(Date modified) {
        this.modified = modified; // NOSONAR
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return connectorType;
    }

    public Date getCreated() {
        return created;// NOSONAR
    }

    public Date getModified() {
        return modified;// NOSONAR
    }

    /**
     * Set the connector active state.
     *
     * @param active
     *            true if this connector is in active use, false if it was obsoleted
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /** @return true if this connector is in active use, false if it was obsoleted */
    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void setType(String connectorType) {
        this.connectorType = connectorType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((connectorType == null) ? 0 : connectorType.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Connector other = (Connector) obj;
        if (active != other.active)
            return false;
        if (connectorType == null) {
            if (other.connectorType != null)
                return false;
        } else if (!connectorType.equals(other.connectorType))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
