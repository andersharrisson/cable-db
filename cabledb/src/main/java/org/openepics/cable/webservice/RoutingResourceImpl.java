/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;

import org.openepics.cable.jaxb.RoutingElement;
import org.openepics.cable.jaxb.RoutingResource;
import org.openepics.cable.model.Routing;
import org.openepics.cable.services.RoutingService;

/**
 * This resource provides bulk approved and specific routing data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("routing")
public class RoutingResourceImpl implements RoutingResource {

    @Inject
    private RoutingService routingService;

    @Override
    public List<RoutingElement> getAllRoutings() {
        final List<RoutingElement> routingElements = new ArrayList<>();

        for (final Routing routing : routingService.getRoutings()) {
            if (!routing.isActive())
                continue;
            routingElements.add(getRoutingElement(routing));
        }
        return routingElements;
    }

    @Override
    public RoutingElement getRouting(String name) {
        final Routing routing = routingService.getRoutingByName(name);
        if (routing == null)
            return null;
        if (!routing.isActive())
            return null;

        return getRoutingElement(routing);
    }

    /**
     * Creates an instance of {@link RoutingElement} from database model object {@link Routing}.
     *
     * @param routing
     *            the database model object
     * @return the JAXB object
     */
    public static RoutingElement getRoutingElement(Routing routing) {
        RoutingElement routingElement = new RoutingElement();
        routingElement.setName(routing.getName());
        routingElement.setDescription(routing.getDescription());
        routingElement.setRoutingClass(routing.getCableClassesAsString());
        // routingElement.setLocation(routing.getLocation());
        routingElement.setLength(routing.getLength());
        routingElement.setActive(routing.isActive());
        return routingElement;
    }
}
