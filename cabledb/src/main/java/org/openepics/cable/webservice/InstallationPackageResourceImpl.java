/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;

import org.openepics.cable.jaxb.InstallationPackageElement;
import org.openepics.cable.jaxb.InstallationPackageResource;
import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.InstallationPackageService;

/**
 * This resource provides bulk approved and specific installation package data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("installationPackage")
public class InstallationPackageResourceImpl implements InstallationPackageResource {

    @Inject
    private InstallationPackageService installationPackageService;

    @Override
    public List<InstallationPackageElement> getAllInstallationPackages() {
        final List<InstallationPackageElement> installationPackageElements = new ArrayList<>();

        for (final InstallationPackage installationPackage : installationPackageService.getInstallationPackages()) {
            if (!installationPackage.isActive())
                continue;
            installationPackageElements.add(getInstallationPackageElement(installationPackage));
        }
        return installationPackageElements;
    }

    @Override
    public InstallationPackageElement getInstallationPackage(String name) {
        final InstallationPackage installationPackage = installationPackageService.getInstallationPackageByName(name);
        if (installationPackage == null)
            return null;
        if (!installationPackage.isActive())
            return null;

        return getInstallationPackageElement(installationPackage);
    }

    /**
     * Creates an instance of {@link InstallationPackageElement} from database model object {@link InstallationPackage}.
     *
     * @param installationPackage
     *            the database model object
     * @return the JAXB object
     */
    protected static InstallationPackageElement getInstallationPackageElement(InstallationPackage installationPackage) {
        InstallationPackageElement installationPackageElement = new InstallationPackageElement();
        installationPackageElement.setName(installationPackage.getName());
        installationPackageElement.setDescription(installationPackage.getDescription());
        installationPackageElement.setLocation(installationPackage.getLocation());
        installationPackageElement.setCreated(installationPackage.getCreated());
        installationPackageElement.setModified(installationPackage.getModified());
        installationPackageElement.setCableCoordinator(installationPackage.getCableCoordinator());
        installationPackageElement.setInstallerCable(installationPackage.getInstallerCable());
        installationPackageElement.setInstallerConnectorA(installationPackage.getInstallerConnectorA());
        installationPackageElement.setInstallerConnectorB(installationPackage.getInstallerConnectorB());
        installationPackageElement.setInstallationPackageLeader(installationPackage.getInstallationPackageLeader());
        installationPackageElement.setActive(installationPackage.isActive());
        return installationPackageElement;
    }
}
