package org.openepics.cable.webservice;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This represents the response message that is used for the RESTful interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement
public class ResponseMessage {

    @XmlElement
    String message;

    /**
     * Default constructor.
     */
    public ResponseMessage() {
        super();
    }

    /**
     * Constructs a new object with the specified message.
     * 
     * @param message
     *            message
     */
    public ResponseMessage(String message) {
        super();
        this.message = message;
    }
}
