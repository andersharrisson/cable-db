/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * A UI controller bean for the main menu.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class MenuController implements Serializable {

    private static final long serialVersionUID = -8204749848916147212L;

    public String getActiveIndex() {
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        switch (viewId) {
            case "/cables.xhtml":
                return "0";
            case "/cable-articles.xhtml":
                return "1";
            case "/cable-types.xhtml":
                return "2";
            case "/installation-packages.xhtml":
                return "3";
            case "/connectors.xhtml":
                return "4";
            case "/manufacturers.xhtml":
                return "5";
            case "/log.xhtml":
                return "6";
            case "/help.xhtml":
                return "7";
            default:
                return "0";
        }
    }
}
