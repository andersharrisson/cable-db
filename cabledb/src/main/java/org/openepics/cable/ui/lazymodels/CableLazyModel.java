package org.openepics.cable.ui.lazymodels;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.openepics.cable.model.Cable;
import org.openepics.cable.services.CableService;
import org.openepics.cable.ui.CableColumnUIConstants;
import org.openepics.cable.ui.CableRequestManager;
import org.openepics.cable.ui.CableUI;
import org.openepics.cable.ui.QueryUI;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 * Cable Lazy loading model implementing all necessary methods for lazy loading on cables data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableLazyModel extends LazyDataModel<CableUI> {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(CableLazyModel.class.getCanonicalName());

    private final CableService cableService;
    private boolean empty = true;
    private String sortField;
    private SortOrder sortOrder;
    private Map<String, Object> filters;
    private QueryUI customQuery;
    public CableRequestManager requestManager;

    public CableLazyModel(CableService cableService, CableRequestManager manager) {
        this.cableService = cableService;
        sortField = "";
        sortOrder = SortOrder.ASCENDING;
        filters = Collections.<String, Object> emptyMap();
        this.requestManager = manager;
    }

    /**
     * Loads a section of data from database
     * 
     * @param first
     *            index of first row
     * @param pageSize
     *            amount of rows do load
     * @param sortField
     *            field by which data should be sorted
     * @param sortOrder
     *            order by which data should be sorted
     * @param filters
     *            filters for data
     * @return section of data sorted and filtered as given in parameters, and additionally filtered by custom query.
     */
    @Override
    public List<CableUI> load(int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters) {
        LOGGER.log(Level.FINEST, "---->pageSize: " + pageSize);
        LOGGER.log(Level.FINEST, "---->first: " + first);

        for (final String filterKey : filters.keySet()) {
            String value = filters.get(filterKey).toString();
            if (value.equals(CableColumnUIConstants.EMPTY_FILTER_DROPDOWN_VALUE)) {
                LOGGER.log(Level.FINER, "replacing filter value \"{}\" to \"\"", CableColumnUIConstants.EMPTY_FILTER_DROPDOWN_VALUE);
                filters.replace(filterKey, value, "");
            }
            LOGGER.log(Level.FINER, "filter[" + filterKey + "] = " + value);
        }

        setLatestLoadData(sortField, sortOrder, filters);

        final List<Cable> results = cableService.findLazy(first, pageSize, sortField, sortOrder, filters,
                customQuery != null ? customQuery.getQuery() : null);
        final List<CableUI> transformedResults = results == null ? new ArrayList<CableUI>()
                : results.stream().map(CableUI::new).collect(Collectors.toList());
        setEmpty(first, transformedResults);

        requestManager.setCables(transformedResults);
        return transformedResults;
    }

    /**
     * Important parameters of the data load request
     * 
     * @param sortField
     *            name of the sort field
     * @param sortOrder
     *            the sort order
     * @param filters
     *            active filters
     */
    protected void setLatestLoadData(final @Nullable String sortField, final @Nullable SortOrder sortOrder,
            final @Nullable Map<String, Object> filters) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
        this.filters = filters;
    }

    /**
     * The lazy data collection is empty, if it returns no data for the first page.
     * 
     * @param first
     *            the index of the first element to be loaded
     * @param results
     *            the database results
     */
    protected void setEmpty(final int first, final @Nullable List<CableUI> results) {
        empty = (first == 0) && ((results == null) || results.isEmpty());
    }

    /** @return <code>true</code> if the current filter returns no data, <code>false</code> otherwise */
    public boolean isEmpty() {
        return empty;
    }

    public String getSortField() {
        return sortField;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public Map<String, Object> getFilters() {
        return filters;
    }

    public void setQuery(QueryUI customQuery) {
        this.customQuery = customQuery;
    }

    public QueryUI getQueryUI() {
        return customQuery;
    }

    @Override
    public int getRowCount() {
        final long rowCount = cableService.getRowCount(filters, customQuery != null ? customQuery.getQuery() : null);
        return rowCount > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) rowCount;
    }

    @Override
    public Object getRowKey(CableUI object) {
        return object.getId();
    }

    @Override
    public CableUI getRowData(String rowKey) {
        final Cable foundCable = cableService.getCableById(Long.parseLong(rowKey));
        return foundCable != null ? new CableUI(foundCable) : null;
    }
}
