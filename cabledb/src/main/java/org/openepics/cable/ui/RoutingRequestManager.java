/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Routing Database.
 * Routing Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.openepics.cable.model.Routing;
import org.openepics.cable.services.RoutingService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.services.dl.RoutingImportExportService;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.Utility;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the backing requests bean for routings.xhtml.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class RoutingRequestManager implements Serializable {

    private static final long serialVersionUID = 9208792434306606451L;

    private static final String EMPTY_STRING = "";
    private static final Logger LOGGER = Logger.getLogger(RoutingRequestManager.class.getName());
    private static final List<RoutingUI> EMPTY_LIST = new ArrayList<>();

    private List<RoutingColumnUI> columns;
    private List<String> columnTemplate = RoutingColumnUI.getAllColumns();

    @Inject
    private transient RoutingService routingService;
    @Inject
    private transient RoutingImportExportService routingImportExportService;

    @Inject
    private SessionService sessionService;

    private List<RoutingUI> routings;
    private List<RoutingUI> filteredRoutings;
    private List<RoutingUI> selectedRoutings = EMPTY_LIST;
    private List<RoutingUI> deletedRoutings;

    private String globalFilter;

    private RoutingUI selectedRouting;
    private boolean isAddPopupOpened;

    // for overlay panels
    private String displayDescription;
    private String displayService;
    private String displayComments;

    private boolean isRoutingRequested;
    private String requestedRoutingName;

    private byte[] fileToBeImported;
    private LoaderResult<Routing> importResult;
    private String importFileName;
    private int routingsToExportSize = 0;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;

    private Routing oldRouting;

    /** Initializes the bean for initial view display. */
    @PostConstruct
    public void init() {
        isAddPopupOpened = false;
        clearImportState();
        selectedRoutings.clear();
        selectedRouting = null;
        requestedRoutingName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest()).getParameter("routingName");
        createDynamicColumns();
        refreshRoutings();
    }

    public void onLoad() {
        refreshRoutings();
        navigateToUrlSelectedRouting();
    }

    private void navigateToUrlSelectedRouting() {
        RoutingUI routingToSelect = null;
        this.isRoutingRequested = false;
        if (requestedRoutingName != null) {
            isRoutingRequested = true;
            routingToSelect = getRoutingFromRoutingName(requestedRoutingName);
        }

        selectedRoutings.clear();
        if (routingToSelect != null) {
            selectedRouting = routingToSelect;
            selectedRoutings.add(selectedRouting);

            int elementPosition = 0;
            for (RoutingUI routing : routings) {
                if (routing.getName().equals(requestedRoutingName)) {
                    RequestContext.getCurrentInstance()
                            .execute("selectEntityInTable(" + elementPosition + ", 'routingTable');");
                    return;
                }
                ++elementPosition;
            }

        } else if (isRoutingRequested) {
            RequestContext.getCurrentInstance().update("cannotFindRoutingForm:cannotFindRouting");
            RequestContext.getCurrentInstance().execute("PF('cannotFindRouting').show();");
        }
    }

    private RoutingUI getRoutingFromRoutingName(final String routingName) {
        if (routingName == null || routingName.isEmpty()) {
            return null;
        }

        RoutingUI routingToSelect = null;
        for (RoutingUI routing : routings) {
            if (routing.getName().equals(routingName)) {
                routingToSelect = routing;
            }
        }
        return routingToSelect;
    }

    /** Refreshes routings list. */
    private void refreshRoutings() {
        routings = buildRoutingUIs();
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /**
     * Returns the routings to be exported, which are the currently filtered and selected routings, or all filtered
     * cables without deleted ones if none selected.
     *
     * @return the routings to be exported
     */
    public List<RoutingUI> getRoutingsToExport() {

        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        final List<RoutingUI> routingsToExport = new ArrayList<>();
        for (RoutingUI routing : getRoutings()) {
            if (isIncludedByFilter(routing) && routing.isActive())
                routingsToExport.add(routing);
        }
        LOGGER.fine("Returning routings to export: " + routingsToExport.size());
        return routingsToExport;
    }

    private boolean isIncludedByFilter(RoutingUI routing) {
        return !filteredRoutingsExist() || getFilteredRoutings().contains(routing);
    }

    private boolean filteredRoutingsExist() {
        return getFilteredRoutings() != null && !getFilteredRoutings().isEmpty();
    }

    /** @return the result of a test or true import */
    public LoaderResult<Routing> getImportResult() {
        return importResult;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear import state.");
        fileToBeImported = null;
        importFileName = null;
        importResult = null;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void routingFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked routing file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            importFileName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the routing import from the file that was last uploaded.
     */
    public void routingImportTest() {
        LOGGER.fine("Invoked routing import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = routingImportExportService.importRoutings(inputStream, true);
                LOGGER.fine("Import test result: " + importResult.toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the routing import from the file that was last uploaded.
     */
    public void routingImport() {
        LOGGER.fine("Invoked routing import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = routingImportExportService.importRoutings(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshRoutings();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /** @return the routing sheet with routings that were just imported */
    public StreamedContent getRoutingSheetWithImportedRoutings() {

        // submit back only non-deleted routings
        final List<Routing> createdOrUpdatedRoutings = new ArrayList<>();
        for (Routing routing : importResult.getAffected()) {
            if (routing.isActive())
                createdOrUpdatedRoutings.add(routing);
        }
        return new DefaultStreamedContent(routingImportExportService.exportRoutings(createdOrUpdatedRoutings), "xlsx",
                importFileName);
    }

    /** @return the routing sheet with the routings from routingsToExport list */
    public StreamedContent getRoutingSheetWithRoutingsToExport() {

        final List<Routing> routingsToExport = new ArrayList<>();
        for (RoutingUI routingUI : getRoutingsToExport()) {
            routingsToExport.add(routingUI.getRouting());
        }
        return new DefaultStreamedContent(routingImportExportService.exportRoutings(routingsToExport), "xlsx",
                "cdb_routings.xlsx");
    }

    /** @return current routing list. */
    private List<RoutingUI> buildRoutingUIs() {

        final List<RoutingUI> routingUIs = new ArrayList<RoutingUI>();
        List<Routing> routings = new ArrayList<Routing>();
        routings = routingService.getRoutings();

        selectedRouting = null;
        selectedRoutings = EMPTY_LIST;

        for (Routing routing : routings) {
            final RoutingUI routingUI = new RoutingUI(routing);
            routingUIs.add(routingUI);
        }
        return routingUIs;
    }

    /** @return true if edit routing button is enabled, otherwise false. */
    public boolean isEditButtonEnabled() {
        return sessionService.isLoggedIn() && selectedRouting != null && selectedRouting.isActive()
                && sessionService.canAdminister();
    }

    /** @return true if delete routing button is enabled, otherwise false. */
    public boolean isDeleteButtonEnabled() {
        if (selectedRoutings == null || selectedRoutings.isEmpty()) {
            return false;
        }
        for (RoutingUI routingToDelete : selectedRoutings) {
            if (!routingToDelete.isActive()) {
                return false;
            }
        }
        return sessionService.canAdminister();
    }

    public String getRequestedRoutingName() {
        return requestedRoutingName;
    }

    public void setRequestedRoutingName(String requestedRoutingName) {
        this.requestedRoutingName = requestedRoutingName;
    }

    public boolean isRoutingRequested() {
        return isRoutingRequested;
    }

    /** @return the routings to be displayed */
    public List<RoutingUI> getRoutings() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return routings;
    }

    /** @return the filtered routings to be displayed */
    public List<RoutingUI> getFilteredRoutings() {
        return filteredRoutings;
    }

    /**
     * @param filteredRoutings
     *            the filtered routings to set
     */
    public void setFilteredRoutings(List<RoutingUI> filteredRoutings) {
        this.filteredRoutings = filteredRoutings;
        LOGGER.fine("Setting filtered routings: " + (filteredRoutings != null ? filteredRoutings.size() : "no")
                + " routings.");
    }

    /** @return the global text filter */
    public String getGlobalFilter() {
        return globalFilter;
    }

    /**
     * @param globalFilter
     *            the global text filter to set
     */
    public void setGlobalFilter(String globalFilter) {
        this.globalFilter = globalFilter;
        LOGGER.fine("Setting global filter: " + this.globalFilter);
    }

    /** Refreshes routings list based on filters. */
    public void filterRoutings() {
        LOGGER.fine("Invoked filter routings.");
        refreshRoutings();
    }

    /** @return the currently selected routings, cannot return null */
    public List<RoutingUI> getSelectedRoutings() {
        return selectedRoutings;
    }

    /**
     * @param selectedRoutings
     *            the routings to select
     */
    public void setSelectedRoutings(List<RoutingUI> selectedRoutings) {
        this.selectedRoutings = selectedRoutings != null ? selectedRoutings : EMPTY_LIST;
        LOGGER.fine("Setting selected routings: " + this.selectedRoutings.size());
    }

    /** Clears the current routing selection. */
    public void clearSelectedRoutings() {
        LOGGER.fine("Invoked clear routing  selection.");
        setSelectedRoutings(null);
    }

    public void onRowSelect() {
        if (selectedRoutings != null && !selectedRoutings.isEmpty()) {
            if (selectedRoutings.size() == 1) {
                selectedRouting = selectedRoutings.get(0);
            } else {
                selectedRouting = null;
            }
        } else {
            selectedRouting = null;
        }
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    public void prepareAddPopup() {
        selectedRouting = new RoutingUI();
        isAddPopupOpened = true;
    }

    public void resetValues() {
        clearSelectedRoutings();
        deletedRoutings = null;
    }

    /**
     * Creates routing.
     */
    public void onRoutingAdd() {
        final String name = selectedRouting.getName();
        formatRouting(selectedRouting);
        selectedRouting.setOwner(sessionService.getLoggedInName());
        routingService.createRouting(selectedRouting.getRouting());
        filterRoutings();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Routing  '" + name + "' added.", null);
        Utility.updateComponent("cableDBGrowl");
    }

    public void prepareEditPopup() {
        Preconditions.checkNotNull(selectedRouting);
        oldRouting = selectedRouting.getRouting();
        routingService.detachRouting(oldRouting);
        selectedRouting = new RoutingUI(routingService.getRoutingById(selectedRouting.getId()));
        isAddPopupOpened = false;
    }

    /**
     * Updates routing.
     */
    public void onRoutingEdit() {
        Preconditions.checkNotNull(selectedRouting);
        formatRouting(selectedRouting);
        final Routing newRouting = selectedRouting.getRouting();
        routingService.updateRouting(newRouting, oldRouting, sessionService.getLoggedInName());
        filterRoutings();
        final String name = newRouting.getName();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Routing  '" + name + "' updated.", null);
        Utility.updateComponent("routingDBGrowl");
    }

    /**
     * @return the list of deleted routings.
     */
    public List<RoutingUI> getDeletedRoutings() {
        return deletedRoutings;
    }

    /**
     * The method builds a list of routings that are already deleted. If the list is not empty, it is displayed to the
     * user and the user is prevented from deleting them.
     */
    public void checkRoutingsForDeletion() {
        Preconditions.checkNotNull(selectedRoutings);
        Preconditions.checkState(!selectedRoutings.isEmpty());

        deletedRoutings = Lists.newArrayList();
        for (final RoutingUI routingToDelete : selectedRoutings) {
            if (!routingToDelete.isActive()) {
                deletedRoutings.add(routingToDelete);
            }
        }
    }

    /**
     * Event handler which handles the routing delete.
     */
    public void onRoutingDelete() {
        Preconditions.checkNotNull(deletedRoutings);
        Preconditions.checkState(deletedRoutings.isEmpty());
        Preconditions.checkNotNull(selectedRoutings);
        Preconditions.checkState(!selectedRoutings.isEmpty());
        int deletedRoutingsCounter = 0;
        for (final RoutingUI routingToDelete : selectedRoutings) {
            final Routing deleteRouting = routingToDelete.getRouting();
            routingService.deleteRouting(deleteRouting, sessionService.getLoggedInName());
            deletedRoutingsCounter++;
        }
        clearSelectedRoutings();
        filteredRoutings = null;
        deletedRoutings = null;
        filterRoutings();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Deleted " + deletedRoutingsCounter + " routings.", null);
    }

    public void isRoutingNameValid(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }

        boolean adding = selectedRouting.getId() == null;
        Routing routing = routingService.getRoutingByName(stringValue);
        boolean alreadyExists = routing != null && !routing.getId().equals(selectedRouting.getId());
        boolean deleted = routing != null && !routing.isActive();

        if ((adding && alreadyExists && !deleted) || (!adding && alreadyExists)) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Routing  with this name already exists.", null));
        }
    }

    /**
     * Validates if entered value is Integer number.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not Integer number
     */
    public void isIntegerEntered(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isIntegerEntered(value);
    }

    /**
     * Validates if entered value is Float number.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not Double number
     */
    public void isFloatEntered(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isFloatEntered(value);
    }

    /**
     * Validates if entered value is URL.
     *
     * @param ctx
     *            faces context
     * @param component
     *            component
     * @param value
     *            entered value
     *
     * @throws ValidatorException
     *             if entered value is not URL
     */
    public void isURLEntered(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        Utility.isURLEntered(value);
    }

    /** @return the selected routing */
    public RoutingUI getSelectedRouting() {
        return selectedRouting;
    }

    /** @return true if the current user can edit routing, else false */
    public boolean getEditRouting() {
        return sessionService.canAdminister();
    }

    /** @return description string for display. */
    public String getDisplayDescription() {
        return displayDescription;
    }

    /**
     * Sets description string.
     *
     * @param displayDescription
     *            string
     */
    public void setDisplayDescription(String displayDescription) {
        this.displayDescription = displayDescription;
    }

    /** Formats description string. */
    public void handleDisplayDescription() {
        displayDescription = formatString(displayDescription);
    }

    /** @return service string for display. */
    public String getDisplayService() {
        return displayService;
    }

    /**
     * Sets service string.
     *
     * @param displayService
     *            service string
     */
    public void setDisplayService(String displayService) {
        this.displayService = displayService;
    }

    /** Formats service string. */
    public void handleDisplayService() {
        displayService = formatString(displayService);
    }

    /** @return comments string for display. */
    public String getDisplayComments() {
        return displayComments;
    }

    /**
     * Sets comments string.
     *
     * @param displayComments
     *            comments string.
     */
    public void setDisplayComments(String displayComments) {
        this.displayComments = displayComments;
    }

    /** Formats comments string. */
    public void handleDisplayComments() {
        displayComments = formatString(displayComments);
    }

    /**
     * Breaks string if it is too long.
     *
     * @param string
     *            string
     *
     * @return new string.
     */
    private String formatString(String string) {
        StringBuilder sb = new StringBuilder(string);
        int i = 0;
        while ((i = sb.indexOf(" ", i + 37)) != -1) {
            sb.replace(i, i + 1, "\n");
        }
        return sb.toString();
    }

    /**
     * Returns current column template.
     *
     * @return string list of column names
     */
    private List<String> getColumnTemplate() {
        return columnTemplate;
    }

    /** Resets column template to default. */
    private void resetColumnTemplate() {
        columnTemplate = RoutingColumnUI.getAllColumns();
    }

    /**
     * Sets column template.
     *
     * @param columnTemplate
     *            of column names to which we should set template
     */
    private void setColumnTemplate(List<String> columnTemplate) {
        this.columnTemplate = columnTemplate;
    }

    /** Resets displayView to default. */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in routing data table
     *
     * @return columns
     */
    public List<RoutingColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<RoutingColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();
            RoutingColumnUI column = RoutingColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(":routingTableForm:routingTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /** @return number of column in current display view */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    /**
     * Formats the given routing by trimming and collapsing all whitespaces in its string fields.
     * 
     * @param selectedRouting
     *            routing to format
     */
    private void formatRouting(RoutingUI selectedRouting) {
        selectedRouting.setDescription(Utility.formatWhitespaces(selectedRouting.getDescription()));
        selectedRouting.setName(Utility.formatWhitespaces(selectedRouting.getName()));
    }

    /**
     * @return available classes.
     */
    public String[] getCableClasses() {
        return CableNumbering.getClassLetters();
    }

    /**
     * Auto-complete support for cable classes.
     * 
     * @param query
     *            String to auto-complete to a cable class
     * @return the list of matching cable classes
     */
    public List<String> completeCableClass(String query) {
        List<String> allCableClasses = Arrays.asList(CableNumbering.getClassLetters());
        List<String> filteredCableClasses = new ArrayList<String>();

        for (int i = 0; i < allCableClasses.size(); i++) {
            String cableClass = allCableClasses.get(i);
            if (cableClass.toLowerCase().startsWith(query.toLowerCase())
                    && !selectedRouting.getRouting().getCableClasses().contains(cableClass)) {
                filteredCableClasses.add(cableClass);
            }
        }
        return filteredCableClasses;
    }

    /**
     * @return cable classes on selected routing
     */
    public List<String> getSelectedCableClasses() {
        return selectedRouting != null ? new ArrayList<String>(selectedRouting.getRouting().getCableClasses())
                : Collections.<String> emptyList();
    }

    /**
     * @param cableClasses
     *            cable classes to set on selected routing
     */
    public void setSelectedCableClasses(List<String> cableClasses) {
        if (selectedRouting != null) {
            selectedRouting.setCableClasses(new HashSet<>(cableClasses));
        }
    }

    /**
     * @param cableClass
     *            letter
     *
     * @return routing class name which corresponds to routing class letter.
     */
    public String retrieveCableClassLabel(String cableClass) {
        return CableNumbering.getClassLabel(cableClass);
    }

    public int getRoutingsToExportSize() {
        return routingsToExportSize;
    }

    public void updateRoutingsToExportSize() {
        this.routingsToExportSize = getRoutingsToExport().size();
    }

    /** @return true if the current user can import routings, else false */
    public boolean canImportRoutings() {
        return routingImportExportService.canImportRoutings();
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /** Clears the newly created selected routing so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedRouting = null;
        }
    }
}
