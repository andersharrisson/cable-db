/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Routing Database.
 * Routing Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.openepics.cable.services.dl.RoutingColumn;

/**
 * Enum wrapper for RoutingColumn. Contains information for displaying cable column , wrapping only columns that are
 * available to be displayed in cable data table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum RoutingColumnUI {
    NAME(RoutingColumn.NAME, RoutingColumnUIConstants.NAME_VALUE, RoutingColumnUIConstants.NAME_TOOLTIP,
            RoutingColumnUIConstants.NAME_STYLECLASS, RoutingColumnUIConstants.NAME_STYLE,
            RoutingColumnUIConstants.NAME_FILTERMODE, RoutingColumnUIConstants.NAME_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    DESCRIPTION(RoutingColumn.DESCRIPTION, RoutingColumnUIConstants.DESCRIPTION_VALUE,
            RoutingColumnUIConstants.DESCRIPTION_TOOLTIP, RoutingColumnUIConstants.DESCRIPTION_STYLECLASS,
            RoutingColumnUIConstants.DESCRIPTION_STYLE, RoutingColumnUIConstants.DESCRIPTION_FILTERMODE,
            RoutingColumnUIConstants.DESCRIPTION_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.ELLIPSIS),
    CLASSES(RoutingColumn.CLASSES, RoutingColumnUIConstants.CLASSES_VALUE, RoutingColumnUIConstants.CLASSES_TOOLTIP,
            RoutingColumnUIConstants.CLASSES_STYLECLASS, RoutingColumnUIConstants.CLASSES_STYLE,
            RoutingColumnUIConstants.CLASSES_FILTERMODE, RoutingColumnUIConstants.CLASSES_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    LOCATION(RoutingColumn.LOCATION, RoutingColumnUIConstants.LOCATION_VALUE, RoutingColumnUIConstants.LOCATION_TOOLTIP,
            RoutingColumnUIConstants.LOCATION_STYLECLASS, RoutingColumnUIConstants.LOCATION_STYLE,
            RoutingColumnUIConstants.LOCATION_FILTERMODE, RoutingColumnUIConstants.LOCATION_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.ELLIPSIS),
    LENGTH(RoutingColumn.LENGTH, RoutingColumnUIConstants.LENGTH_VALUE, RoutingColumnUIConstants.LENGTH_TOOLTIP,
            RoutingColumnUIConstants.LENGTH_STYLECLASS, RoutingColumnUIConstants.LENGTH_STYLE,
            RoutingColumnUIConstants.LENGTH_FILTERMODE, RoutingColumnUIConstants.LENGTH_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    OWNER(RoutingColumn.OWNER, RoutingColumnUIConstants.OWNER_VALUE, RoutingColumnUIConstants.OWNER_TOOLTIP,
            RoutingColumnUIConstants.OWNER_STYLECLASS, RoutingColumnUIConstants.OWNER_STYLE,
            RoutingColumnUIConstants.OWNER_FILTERMODE, RoutingColumnUIConstants.OWNER_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private RoutingColumn parent;
    private String tooltip;
    private String value;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private CableColumnStyle columnStyle;

    /**
     * RoutingColumnUI constructor Decides what labels in column to display according to property.
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     */
    private RoutingColumnUI(RoutingColumn parent, String value, String tooltip, String styleClass, String style,
            String filterMode, String filterStyle, CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /**
     * Searches for RoutingColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return RoutingColumnUI enum
     */
    public static RoutingColumnUI convertColumnLabel(String columnLabel) {
        for (RoutingColumnUI value : RoutingColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (RoutingColumnUI value : RoutingColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }
}
