/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 */
package org.openepics.cable.ui;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.CableProperties;
import org.openepics.cable.dto.ReuseCableDetails;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableArticle;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.CableArticleService;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.CableValidationService;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.DateUtil;
import org.openepics.cable.services.Names;
import org.openepics.cable.services.QueryService;
import org.openepics.cable.services.InstallationPackageService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;
import org.openepics.cable.services.dl.CableImportExportService;
import org.openepics.cable.services.dl.CableParams;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.ui.lazymodels.CableLazyModel;
import org.openepics.cable.ui.lazymodels.PagerUtil;
import org.openepics.cable.util.*;
import org.openepics.cable.util.fbs.FbsService;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the backing requests bean for cables.xhtml.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@ManagedBean
@ViewScoped
public class CableRequestManager implements Serializable {

    // Note
    //     update fbs tag mappings - cableName/tag - essName/tag - tag/essName - from CHESS
    //         cableName/tag
    //             done server side
    //         essName/tag - tag/essName
    //             done in ui
    //

    // performing service validations
    private static final long serialVersionUID = 8161254904185694693L;

    private static final Logger LOGGER = Logger.getLogger(CableRequestManager.class.getName());

    private static final String CABLE_DB_GROWL = "cableDBGrowl";
    private static final String EMPTY_STRING   = "";

    // pages
    private static final String NAMING_DEVICE_PAGE = "devices.xhtml?i=2&deviceName=";
    private static final String CCDB_DEVICE_PAGE   = "?name=";

    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final int MAX_AUTOCOMPLETE            = 20;

    private final String chessURL = getChessURLCableProperties();

    @Inject
    private transient CableService cableService;
    @Inject
    private transient CableArticleService cableArticleService;
    @Inject
    private transient CableTypeService cableTypeService;
    @Inject
    private transient ConnectorService connectorService;
    @Inject
    private transient InstallationPackageService installationPackageService;
    @Inject
    private transient CableImportExportService cableImportExportService;
    @Inject
    private Names names;

    @Inject
    private FbsService fbsService;

    @Inject
    private SessionService sessionService;

    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;
    @Inject
    private transient CableValidationService cableValidationService;
    @Inject
    private QueryService queryService;

    @Inject
    private SessionObject sessionObject;

    private CableLazyModel lazyModel;

    private List<CableUI> cables;
    private List<CableUI> deletedCables;
    private List<CableUI> selectedCables = new ArrayList<>();
    private List<CableArticle> cableArticles;
    private List<CableType> cableTypes;
    private List<Connector> connectors;
    private List<InstallationPackage> installationPackages;

    private byte[] fileToBeImported;
    private LoaderResult<Cable> importResult;
    private String importFileName;
    private String selectedDevice;

    private CableUI selectedCable;
    private Cable oldCable;
    private QueryUI selectedQuery;
    private String requestedCableName;
    private boolean isAddPopupOpened;
    private Long cablesToExportSize = 0L;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;
    private String longOverlayURL;

    private List<InstallationPackage> availableInstallationPackages;

    private HashMap<String, Cable> oldCablesMap;

    // to be used from add/edit dialog
    private boolean isEmptyDeviceA;
    private boolean isEmptyDeviceAFbsTag;
    private boolean isEmptyDeviceB;
    private boolean isEmptyDeviceBFbsTag;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    private ReuseCableDetails reuseCableDetails;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public CableRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns (incl history column)
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page

        numberOfColumns = CableColumnUI.values().length;
        columnVisibility = ManagerUtil.setAllColumnVisible(numberOfColumns);
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display, and invokes service availability check at first invocation. */
    @PostConstruct
    public void init() {
        try {
            isAddPopupOpened = false;
            cables = new ArrayList<>();

            requestedCableName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getParameter("cableName");

            Map<String, Object> defaultFilter = new HashMap<String, Object>();
            if (!StringUtils.isEmpty(requestedCableName)) {
                defaultFilter.put(CableColumnUI.NAME.getValue(), requestedCableName);
            } else {
                defaultFilter.put(CableColumnUI.STATUS.getValue(), sessionObject.getCableStatusFilterValue());
            }

            lazyModel = new CableLazyModel(cableService, defaultFilter, this);
            selectedCables.clear();
            selectedCable = null;
            updatePropertiesForSelectedCable();

            clearImportState();

            refreshCables();

            initStatusFilter();

            initReuseCableDetails();
        } catch (Exception e) {
            throw new UIException("Cable display initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Initializes the object for reusing a cable
     */
    public void initReuseCableDetails() {
        reuseCableDetails = new ReuseCableDetails();
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    private void refreshCables() {
        lazyModel.setQuery(selectedQuery);
        selectedCables = new ArrayList<>();
        selectedCable = null;
        updatePropertiesForSelectedCable();
    }

    /** @return true if the current user can import cables, else false */
    public boolean canImportCables() {
        return cableImportExportService.canImportCables();
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /** @return the cables to be displayed */
    public List<CableUI> getCables() {
        if (!sessionService.isLoggedIn())
            return new ArrayList<>();

        return cables;
    }

    /** @return the currently selected cables, cannot return null */
    public List<CableUI> getSelectedCables() {
        return selectedCables;
    }

    /**
     * @param selectedCables
     *            the cables to select
     */
    public void setSelectedCables(List<CableUI> selectedCables) {
        this.selectedCables = selectedCables != null ? selectedCables : new ArrayList<>();
        LOGGER.fine("Setting selected cables: " + this.selectedCables.size());
    }

    /** Clears the current cable selection. */
    public void clearSelectedCables() {
        LOGGER.fine("Invoked clear cable selection.");
        setSelectedCables(null);
    }

    /**
     * Returns the cables to be exported, which are the currently filtered and selected cables, or all filtered cables
     * without deleted ones if none selected.
     *
     * @return the cables to be exported
     */
    public List<Cable> getCablesToExport() {

        if (!sessionService.isLoggedIn())
            return new ArrayList<>();

        return PagerUtil.findLazyPaged(
                cableService.getLazyService(), 0, Integer.MAX_VALUE, CableLazyModel.MAX_PAGE_SIZE,
                lazyModel.getSortField(), lazyModel.getSortOrder(), lazyModel.getFilters(), createCustomQuery());
    }

    private Query createCustomQuery() {
        return cableImportExportService
                .getNotDeletedQuery(lazyModel.getQueryUI() != null ? lazyModel.getQueryUI().getQuery() : null);
    }


    /** @return the result of a test or true import */
    public LoaderResult<Cable> getImportResult() {
        return importResult;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear import state.");
        fileToBeImported = null;
        importFileName = null;
        importResult = null;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void cableFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked cable file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            importFileName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the cable import from the file that was last uploaded.
     */
    public void cableImportTest() {
        LOGGER.fine("Invoked cable import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableImportExportService.importCables(inputStream, true);
                LOGGER.fine("Import test result: " + importResult.toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the cable import from the file that was last uploaded.
     */
    public void cableImport() {
        LOGGER.fine("Invoked cable import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableImportExportService.importCables(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshCables();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Invokes explicit cable validation.
     */
    public void validateCables() {
        names.refresh();
        UiUtility.showInfoMessage("Naming cache updated.");
        cableValidationService.runOperationBlocking(sessionService.getLoggedInName());
        refreshCables();
        UiUtility.updateComponent(CABLE_DB_GROWL);
        UiUtility.showInfoMessage("Validation of cables finished.");
    }

    /** @return the cable sheet with cables that were just imported */
    public StreamedContent getCableSheetWithImportedCables() {

        // submit back only non-deleted cables
        final List<Cable> createdOrUpdatedCables = new ArrayList<>();
        for (Cable cable : importResult.getAffected()) {
            createdOrUpdatedCables.add(cable);
        }
        return new DefaultStreamedContent(cableImportExportService.exportCables(createdOrUpdatedCables),
                Utility.XLSX_CONTENT_TYPE,
                importFileName);
    }

    /** @return the cable sheet with the cables from cablesToExport list */
    public StreamedContent getCableSheetWithCablesToExport() {
        LOGGER.fine("Get cables to export");
        final List<Cable> cablesToExport = getCablesToExport();
        LOGGER.fine("Cable to export count: " + cablesToExport.size());
        return new DefaultStreamedContent(cableImportExportService.exportCables(cablesToExport),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_cables.xlsx");
    }

    /** @return the cable import template */
    public StreamedContent getImportTemplate() {
        LOGGER.fine("Get cables template");
        return new DefaultStreamedContent(cableImportExportService.exportCables(new ArrayList<>()),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_cables.xlsx");
    }

    /** @return the cable numbering sheet URL */
    public String getCableNumberingSheetURL() {
        final String url = CableProperties.getInstance().getCableNumberingDocumentURL();
        LOGGER.fine("Cable Numbering Document URL: " + url);
        return url;
    }

    /** @return the date format string to use to display dates */
    public String getDateFormatString() {
        return DateUtil.DATE_FORMAT_STRING;
    }

    /**
     * @return selected device.
     */
    public String getSelectedDevice() {
        return selectedDevice;
    }

    /**
     * Set selected device.
     *
     * @param selectedDevice
     *            selected device
     */
    public void setSelectedDevice(String selectedDevice) {
        this.selectedDevice = selectedDevice;
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
        }
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index
     * @return if column is visible
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Returns column visibility for given column.
     *
     * @param column given column
     * @return column visibility
     */
    public boolean isColumnVisible(CableColumnUI column) {
        int colIndex = 0;
        for(int i=0; i<numberOfColumns; i++) {
            if(CableColumnUI.values()[i].equals(column)) {
                colIndex = i;
                break;
            }
        }
        return columnVisibility.get(colIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected cables and row selection.
     */
    public void unselectAllRows() {
        clearSelectedCables();

        onRowSelect();
    }

    /**
     * Event triggered when row is selected in table in UI.
     */
    public void onRowSelect() {
        if (selectedCables != null && !selectedCables.isEmpty()) {
            if (selectedCables.size() == 1) {
                selectedCable = selectedCables.get(0);
            } else {
                selectedCable = null;
            }
        } else {
            selectedCable = null;
        }
        updatePropertiesForSelectedCable();
    }

    /** @return <code>true</code> if a single cable is selected, <code>false</code> otherwise */
    public boolean isSingleCableSelected() {
        return (selectedCables != null) && (selectedCables.size() == 1)
                && selectedCables.get(0).getStatus() != CableStatus.DELETED;
    }

    public String getSelectedCableName() {
        return selectedCable != null && selectedCable.getId() != null ? selectedCable.getName() : "";
    }

    /**
     * Return true if chess id not set for selected cable.
     *
     * @return true if chess id not set for selected cable
     */
    public boolean getIsEmptyChessId() {
        return getSelectedCable() != null && StringUtils.isEmpty(getSelectedCable().getChessId());
    }

    /**
     * Return inline style of field for cable fbs tag.
     *
     * @return inline style of field for cable fbs tag
     */
    public String getStyleCableFbsTag() {
        return getIsEmptyChessId()
                ? ""
                : "background-color:lightgray;";
    }

    /**
     * Return if cable article is required.
     *
     * @return if cable article is required
     */
    public boolean getIsRequiredArticle() {
        // cable article required if
        //        (user and add  cable)
        //     or (user and edit cable and cable article exists)

        return     (!getIsAdmin() && isAddPopupOpened)
                || (!getIsAdmin() && !isAddPopupOpened && !StringUtils.isEmpty(getSelectedCableCableArticle()))
                || StringUtils.isEmpty(getSelectedCableCableType());
    }

    /**
     * (Possibly) Clear cable type for selected cable.
     */
    public void conditionalClearCableType() {
        // if cable user then clear cable type
        // to be used from add/edit dialog
        // see also similar methods, e.g. clearFill...
        if (!sessionService.canAdminister() && selectedCable != null) {
            selectedCable.setCableType(null);
        }
    }

    /**
     * Return style of cable type field.
     *
     * @return style of cable type field
     */
    public String getStyleCableType() {
        return getIsAdmin()
                ? ""
                : "background-color:lightgray;";
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    /**
     * Prepare for Add cable dialog.
     */
    public void prepareAddPopup() {
        if (sessionService.isLoggedIn()) {
            List<String> owners = Arrays.asList(sessionService.getLoggedInName());
            selectedCable = new CableUI(new Cable(getSystems()[0], getSubsystems()[0], getCableClasses()[0], 0, owners,
                    new Date(), new Date()));
            // neither device nor device fbs tag set
            selectedCable.setEndpointA(new Endpoint(null));
            selectedCable.setEndpointB(new Endpoint(null));
            isAddPopupOpened = true;
            updatePropertiesForSelectedCable();
        }
    }

    /**
     * Event triggered when cable is created.
     */
    public void onCableAdd() {
        CableParams newCable = new CableParams.Builder()
                .withSystem(selectedCable.getSystem())
                .withSubSystem(selectedCable.getSubsystem())
                .withCableClass(selectedCable.getCableClass())
                .withFbsTag(selectedCable.getFbsTag())
                .withCableArticle(selectedCable.getCableArticle())
                .withCableType(selectedCable.getCableType())
                .withContainer(selectedCable.getContainer())
                .withElectricalDocumentation(selectedCable.getElectricalDocumentation())
                .withEndpointA(selectedCable.getEndpointA())
                .withEndpointB(selectedCable.getEndpointB())
                .withInstallationPackage(selectedCable.getInstallationPackage())
                .withOwners(selectedCable.getOwners())
                .withCableStatus(selectedCable.getStatus())
                .withInstallationBy(selectedCable.getInstallationByDate())
                .withComments(selectedCable.getComments())
                .withRevision(selectedCable.getRevision())
                .withUserId(sessionService.getLoggedInName())
                .withSendNotification(true)
                .build();

        cableService.createCable(newCable);
        refreshCables();
        UiUtility.showInfoMessage("Cable added.");
        UiUtility.updateComponent(CABLE_DB_GROWL);
    }

    /**
     * Prepare for Edit cable dialog.
     */
    public void prepareEditPopup() {
        Preconditions.checkState(isSingleCableSelected());
        Preconditions.checkNotNull(selectedCable);
        // We create a duplicate of the selected cable from database to prevent wrong data display on close, and to
        // preserve changes for history logging.
        oldCable = selectedCable.getCable();
        selectedCable = new CableUI(cableService.getCableById(selectedCable.getCable().getId()));
        isAddPopupOpened = false;
        updatePropertiesForSelectedCable();
    }

    /**
     * Event triggered when cable is updated.
     */
    public void onCableEdit() {
        Preconditions.checkState(isSingleCableSelected());
        Preconditions.checkNotNull(selectedCable);

        final String cableName = oldCable.getName();
        formatCable(selectedCable);
        final Cable newCable = selectedCable.getCable();
        final boolean updated = cableService.updateCable(newCable, oldCable, sessionService.getLoggedInName(), true);
        refreshCables();
        UiUtility.showInfoMessage("Cable '" + cableName + (updated ? "' updated." : "' not updated."));
        UiUtility.updateComponent(CABLE_DB_GROWL);
    }

    /**
     * Check if parameter matches cable edit
     *
     * @param value onCableAdd or onCableEdit
     * @return true if if parameter matches cable edit
     */
    public boolean isCableEdit(String value) {
        return StringUtils.equals("onCableEdit", value);
    }

    /**
     * Event triggered when cable is updated with apply.
     */
    public void onCableEditApply() {
        long id = selectedCable.getCable().getId();

        onCableEdit();

        selectedCable = new CableUI(cableService.getCableById(id));
        List<CableUI> editSelectedCables = getSelectedCables() != null ? getSelectedCables() : new ArrayList<CableUI>();
        editSelectedCables.add(selectedCable);
        setSelectedCables(editSelectedCables);
        isAddPopupOpened = false;
        updatePropertiesForSelectedCable();
    }

    /**
     * @return the list of deleted cables.
     */
    public List<CableUI> getDeletedCables() {
        return deletedCables;
    }

    /**
     * Reset values. May be used at e.g. close of delete dialog.
     */
    public void resetValues() {
        deletedCables = null;
    }

    /**
     * The method builds a list of cables that are already deleted. If the list is not empty, it is displayed to the
     * user and the user is prevented from deleting them.
     */
    public void checkCablesForDeletion() {
        Preconditions.checkNotNull(selectedCables);
        Preconditions.checkState(!selectedCables.isEmpty());

        deletedCables = Lists.newArrayList();
        for (final CableUI cableToDelete : selectedCables) {
            if (cableToDelete.getStatus() == CableStatus.DELETED) {
                deletedCables.add(cableToDelete);
            }
        }
    }

    /**
     * Event triggered when cable is deleted.
     */
    public void onCableDelete() {
        Preconditions.checkNotNull(deletedCables);
        Preconditions.checkState(deletedCables.isEmpty());
        Preconditions.checkNotNull(selectedCables);
        Preconditions.checkState(!selectedCables.isEmpty());
        int deletedCablesCounter = 0;
        for (final CableUI cableToDelete : selectedCables) {
            if (cableService.deleteCable(cableToDelete.getCable(), sessionService.getLoggedInName())) {
                deletedCablesCounter++;
            }
        }
        clearSelectedCables();
        deletedCables = null;
        refreshCables();
        UiUtility.showInfoMessage("Deleted " + deletedCablesCounter + (deletedCablesCounter == 1 ? " cable." : " cables."));
    }

    /**
     * @return available systems.
     */
    public String[] getSystems() {
        return CableNumbering.getSystemNumbers();
    }

    /**
     * @return available subsystems.
     */
    public String[] getSubsystems() {
        return CableNumbering.getSubsystemNumbers();
    }

    /**
     * Return available subsystems for given system.
     *
     * @param systemCharacter system character (number)
     * @return available subsystems for given system
     */
    public String[] getSubsystems(String systemCharacter) {
        return CableNumbering.getSubsystemNumbers(systemCharacter);
    }

    /**
     * @return available cables.
     */
    public String[] getCableClasses() {
        return CableNumbering.getClassLetters();
    }

    /**
     * Return system name which corresponds to system number.
     *
     * @param systemNumber system number
     * @return system name which corresponds to system number
     */
    public String retrieveSystemLabel(String systemNumber) {
        return CableNumbering.getSystemLabel(systemNumber);
    }

    /**
     * Return subsystem name which corresponds to subsystem number.
     *
     * @param subsystemNumber subsystem number
     * @return subsystem name which corresponds to subsystem number
     */
    public String retrieveSubsystemLabel(String subsystemNumber) {
        if (selectedCable != null) {
            return CableNumbering.getSubsystemLabel(selectedCable.getSystem(), subsystemNumber);
        }
        return EMPTY_STRING;
    }

    /**
     * Return cable class name which corresponds to cable class letter.
     *
     * @param cableClass cable class letter
     * @return cable class name which corresponds to cable class letter
     */
    public String retrieveCableClassLabel(String cableClass) {
        return CableNumbering.getClassLabel(cableClass);
    }

    /**
     * @return true if system is selected otherwise false.
     */
    public boolean getIsSystemSelected() {
        if (selectedCable != null) {
            return selectedCable.getSystem() != null && !selectedCable.getSystem().isEmpty();
        }
        return false;
    }

    /** @return the selected cable. */
    public CableUI getSelectedCable() {
        return selectedCable;
    }

    /** @return all available device names from naming service. */
    public List<String> getDeviceNames() {
        return names.getActiveNames().stream().collect(Collectors.toList());
    }

    /**
     * Autocomplete method for cable articles.
     *
     * @param query query to filter cable articles
     * @return cable articles
     */
    public List<String> completeCableArticle(String query) {
        if (cableArticles == null) {
            cableArticles = cableArticleService.getCableArticles();
        }
        List<String> results = new ArrayList<String>();

        for (CableArticle cableArticle : cableArticles) {
            String name = cableArticle.getName();
            if (cableArticle.isActive() && name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /**
     * Autocomplete method for cable types.
     *
     * @param query query to filter cable types
     * @return cable types
     */
    public List<String> completeCableType(String query) {
        if (cableTypes == null) {
            cableTypes = cableTypeService.getAllCableTypes();
        }
        List<String> results = new ArrayList<String>();

        for (CableType cableType : cableTypes) {
            String name = cableType.getName();
            if (cableType.isActive() && name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /**
     * Autocomplete method for device names.
     *
     * @param query query to filter device names
     * @return device names
     */
    public List<String> completeDeviceNames(String query) {
        List<String> results = new ArrayList<String>();
        for (String name : getDeviceNames()) {
            if (name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /**
     * Function used to autocomplete cable names to be reused
     *
     * @param query part of the cable name that has to be searched for
     *
     * @return list of cableNames for UI autocompletion
     */
    public List<String> completeCableNames(String query) {

        return cableService.cablesByName(query, MAX_AUTOCOMPLETE);
    }

    /**
     * Return a list of FBS tags starting with given parameter.
     *
     * @param query start of FBS tag
     * @return list of FBS tags
     */
    public List<String> completeFbsTags(String query) {
        List<String> results = new ArrayList<String>();
        // CHESS/ITIP web service used to retrieve FBS information
        for (String tag : fbsService.getFbsTagsForFbsTagStartsWith(query, MAX_AUTOCOMPLETE)) {
            if (tag.toLowerCase().contains(query.toLowerCase())) {
                results.add(tag);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /**
     * Autocomplete method for connectors.
     *
     * @param query query to filter connectors
     * @return connectors
     */
    public List<String> completeConnectors(String query) {
        if (connectors == null) {
            connectors = connectorService.getConnectors();
        }
        List<String> results = new ArrayList<String>();

        for (Connector connector : connectors) {
            String name = connector.getName();
            if (name.toLowerCase().contains(query.toLowerCase())) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /**
     * @return cable owners on selected cable
     */
    public List<String> getSelectedCableOwners() {
        return selectedCable != null ? new ArrayList<String>(selectedCable.getOwners())
                : Collections.<String> emptyList();
    }

    /**
     * @param cableOwners
     *            cable owners to set on selected cable
     */
    public void setSelectedCableOwners(List<String> cableOwners) {
        if (selectedCable != null) {
            selectedCable.setOwners(cableOwners);
        }
    }

    /**
     * Autocomplete method for users.
     *
     * @param query query to filter users
     * @return users
     */
    public List<String> completeUsers(String query) {
        List<String> users = new ArrayList<>(userDirectoryServiceFacade.getAllUsernames());

        List<String> results = new ArrayList<String>();

        for (String user : users) {
            String name = user;
            if (name.toLowerCase().contains(query.toLowerCase()) && !selectedCable.getOwners().contains(name)) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

    /** @return list of cable statuses that can be set to current cable. */
    public List<CableStatus> getPossibleCableStatus() {
        ArrayList<CableStatus> statuses = new ArrayList<>();
        if (selectedCable != null) {
            statuses.add(selectedCable.getStatus());

            if (sessionObject.canAdminister()) {
                Collection<CableStatus> possibleStatuses = StatusTransitionUtil.possibleStatuses(
                        selectedCable.getStatus());
                if (possibleStatuses != null) {
                    statuses.addAll(possibleStatuses);
                }
            }
        }
        return statuses;
    }

    /**
     * In order to determine if user can change status for selected cable
     *
     * @return true if cable status can be changed, and false, if status can not be changed
     */
    public boolean canChangeStatus() {
        return sessionService.canAdminister();
    }

    /**
     * Return boolean about if ability to change cable name is disabled.
     *
     * @return true if ability to change cable name is disabled
     */
    public boolean getIsDisabledChangeCableName() {
        return !isAddPopupOpened && !sessionService.canAdminister();
    }

    /**
     * Autocomplete method for installation packages.
     *
     * @param query query to filter installation packages
     * @return installation packages
     */
    public List<InstallationPackage> completeInstallationPackage(String query) {
        if (installationPackages == null) {
            installationPackages = installationPackageService.getActiveInstallationPackages();
        }
        List<InstallationPackage> results = new ArrayList<InstallationPackage>();
        if (!sessionService.isLoggedIn())
            return results;

        for (InstallationPackage installationPackage : installationPackages) {
            String name = installationPackage.getName();
            if (name.toLowerCase().contains(query.toLowerCase())) {
                results.add(installationPackage);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        availableInstallationPackages = results;
        return availableInstallationPackages;
    }

    public List<String> getInstallationPackageNames() {
        return installationPackageService.getActiveInstallationPackages()
                .stream()
                .map(installationPackage -> installationPackage.getName())
                .collect(Collectors.toList());
    }

    public String getSelectedCableInstallationPackage() {
        if (selectedCable == null || selectedCable.getInstallationPackage() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getInstallationPackage().getName();
    }

    public void setSelectedCableInstallationPackage(String name) {
        if (name != null && !name.isEmpty()) {
            InstallationPackage installationPackage = installationPackageService.getInstallationPackageByName(name);
            selectedCable.setInstallationPackage(installationPackage);
        } else {
            selectedCable.setInstallationPackage(null);
        }
    }

    /** @return selected cable cable article. */
    public String getSelectedCableCableArticle() {
        if (selectedCable == null || selectedCable.getCableArticle() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getCableArticle().getName();
    }

    /**
     * Set selected cable cable article.
     *
     * @param name
     *            cable article name (manufacturer.externalId)
     */
    public void setSelectedCableCableArticle(String name) {
        if (name != null && !name.isEmpty()) {
            CableArticle cableArticle = cableArticleService.getCableArticleByShortNameExternalId(name);
            selectedCable.setCableArticle(cableArticle);
        } else {
            selectedCable.setCableArticle(null);
        }
    }

    /** @return selected cable cable type. */
    public String getSelectedCableCableType() {
        if (selectedCable == null || selectedCable.getCableType() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getCableType().getName();
    }

    /**
     * Set selected cable cable type.
     *
     * @param name
     *            cable type name
     */
    public void setSelectedCableCableType(String name) {
        if (name != null && !name.isEmpty()) {
            CableType cableType = cableTypeService.getCableTypeByName(name);
            selectedCable.setCableType(cableType);
        } else {
            selectedCable.setCableType(null);
        }
    }

    /** @return selected cable connectorA. */
    public String getSelectedCableConnectorA() {
        if (selectedCable == null || selectedCable.getEndpointConnectorA() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getEndpointConnectorA().getName();
    }

    /**
     * Set selected cable connector A.
     *
     * @param name
     *            connector name
     */
    public void setSelectedCableConnectorA(String name) {
        if (name != null && !name.isEmpty()) {
            Connector connector = connectorService.getConnector(name);
            selectedCable.setEndpointConnectorA(connector);
        } else {
            selectedCable.setEndpointConnectorA(null);
        }
    }

    /** @return selected cable connectorB. */
    public String getSelectedCableConnectorB() {
        if (selectedCable == null || selectedCable.getEndpointConnectorB() == null) {
            return EMPTY_STRING;
        }
        return selectedCable.getEndpointConnectorB().getName();
    }

    /**
     * Set selected cable connector B.
     *
     * @param name connector name
     */
    public void setSelectedCableConnectorB(String name) {
        if (name != null && !name.isEmpty()) {
            Connector connector = connectorService.getConnector(name);
            selectedCable.setEndpointConnectorB(connector);
        } else {
            selectedCable.setEndpointConnectorB(null);
        }
    }

    /**
     * Validates if entered value is valid system.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not valid system
     */
    public void isValidSystem(FacesContext ctx, UIComponent component, Object value) {
        Utility.isValidSystem(value);
    }

    /**
     * Validates if entered value is valid subsystem.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not valid subsystem
     */
    public void isValidSubsystem(FacesContext ctx, UIComponent component, Object value) {
        Utility.isValidSubsystem(value);
    }

    /**
     * Validates if entered value is not valid cable class.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not valid cable class
     */
    public void isValidCableClass(FacesContext ctx, UIComponent component, Object value) {
        Utility.isValidCableClass(value);
    }

    /**
     * Validates if entered value is valid LBS tag.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not valid subsystem
     */
    public void isValidLbsTag(FacesContext ctx, UIComponent component, Object value) {
        Utility.isValidLbsTag(value);
    }

    /**
     * Validates if entered value is Float number.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not Double number
     */
    public void isFloatEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isFloatEntered(value);
    }

    /**
     * Validates if entered value is URL.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not URL
     */
    public void isURLEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isURLEntered(value);
    }

    /** @return true if the current user can edit cables, else false */
    public boolean getEditCable() {
        return sessionService.canAdminister() || sessionService.canManageOwnedCables();
    }

    /**
     * Function to determine if Edit button can be enabled, or disabled for user
     *
     * @return <code>true</code> if user can edit, and Edit button can be enabled
     * <code>false</code> if user can not edit, and Edit button should be disabled
     */
    public boolean isEditButtonEnabled() {
        return isCableEditable(selectedCable);
    }

    /**
     * Determines if user can reuse a cable name
     *
     * @return <code>true</code> if user can reuse cableNames, and <code>false</code> if user can not reuse cableNames
     */
    public boolean reuseCableNameButtonEnabled() {
        if(sessionService.canAdminister()) {
            return true;
        }

        return false;
    }

    public boolean isUploadButtonEnabled() {
        if (selectedCables == null || selectedCables.isEmpty()) {
            return false;
        }
        if (!sessionService.canAdminister()) {
            if (!sessionService.canManageOwnedCables()) {
                return false;
            } else {
                for (CableUI cable : selectedCables) {
                    if (!cable.getOwners().contains(sessionService.getLoggedInName())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean isChangeCableOwnerAllowed() {
        return sessionService.canAdminister();
    }

    public boolean isDeleteButtonEnabled() {
        if (selectedCables == null || selectedCables.isEmpty()) {
            return false;
        }
        for (CableUI cableToDelete : selectedCables) {
            if (cableToDelete.getStatus() == CableStatus.DELETED) {
                return false;
            }
        }
        if (sessionService.canAdminister()) {
            return true;
        } else if (sessionService.canManageOwnedCables()) {
            boolean foundNotOwned = false;
            for (CableUI cableToDelete : selectedCables) {
                if (!cableToDelete.getOwners().contains(sessionService.getLoggedInName())) {
                    foundNotOwned = true;
                    break;
                }
            }
            if (!foundNotOwned)
                return true;
        }
        return false;
    }

    public boolean isExportButtonEnabled() {
        if (cables.isEmpty()) {
            return false;
        } else if (cables.size() < NUMBER_OF_ENTITIES_PER_PAGE) {
            for (CableUI cable : cables) {
                if (cable.getStatus() != CableStatus.DELETED) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Redirects to configuration cable database device extremities.
     *
     * @param actionEvent
     *            action event
     */
    public void redirectToCCDB(ActionEvent actionEvent) {
        final String url = CableProperties.getInstance().getCCDBURL();
        redirect(url, CCDB_DEVICE_PAGE);
    }

    /**
     * Redirects to naming service device definition extremities.
     *
     * @param actionEvent
     *            action event
     */
    public void redirectToNamingService(ActionEvent actionEvent) {
        final String url = CableProperties.getInstance().getNamingURL();
        redirect(url, NAMING_DEVICE_PAGE);
    }

    /**
     * Redirects to the device extremities.
     *
     * @param url
     *            URL to redirect to
     * @param page
     *            page to redirect to
     */
    public void redirect(String url, String page) {
        if (url != null && !url.isEmpty()) {
            final StringBuilder redirectionUrl = new StringBuilder(url);
            if (redirectionUrl.charAt(redirectionUrl.length() - 1) != '/') {
                redirectionUrl.append('/');
            }
            redirectionUrl.append(page).append(selectedDevice);
            try {
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                externalContext.redirect(redirectionUrl.toString().trim());
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
    }

    /**
     * Provide filter options given cable column.
     *
     * @param column cable column
     * @return filter options given cable column
     */
    public List<SelectItem> filterOptions(CableColumnUI column) {
        return completeFilter(column)
                .stream()
                .map(SelectItem::new)
                .collect(Collectors.toList());

    }

    /**
     * Provide suggestions for cable given cable column.
     *
     * @param column cable column
     * @return suggestions for cable given cable column
     */
    public List<String> completeFilter(CableColumnUI column) {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<String> selectItems = new ArrayList<>();

        switch (column) {
            case STATUS:
                selectItems.add(CableColumnUIConstants.EMPTY_FILTER_DROPDOWN_VALUE);
                selectItems.add(CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE);
                for (CableStatus cableStatus : CableStatus.values()) {
                    selectItems.add(cableStatus.getDisplayName());
                }
                break;
            default:
                break;
        }

        return selectItems;
    }

    /**
     * Provide suggestions for cable given query to filter for.
     *
     * @param query query to filter for
     * @return suggestions for cable given query to filter for.
     */
    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<String> selectItems = new ArrayList<>();

        FacesContext context = FacesContext.getCurrentInstance();
        CableColumnUI column = (CableColumnUI) UIComponent.getCurrentComponent(context).getAttributes().get("column");
        switch (column) {
        case STATUS:
            selectItems.add(CableColumnUIConstants.EMPTY_FILTER_DROPDOWN_VALUE);
            selectItems.add(CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE);
            for (CableStatus cableStatus : CableStatus.values()) {
                selectItems.add(cableStatus.getDisplayName());
            }
            break;
        default:
            break;
        }
        return selectItems;
    }

    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    /**
     * Execute selected query.
     *
     * @param e action event
     */
    public void executeQuery(ActionEvent e) {
        refreshCables();
        queryService.updateQueryExecutionDate(selectedQuery.getQuery());
    }

    /**
     * Execute query with given id
     *
     * @param id query id
     */
    public void excuteQueryId(long id) {
        if (id != -1) {
            selectedQuery = new QueryUI(queryService.getQueryById(id));
            queryService.updateQueryExecutionDate(selectedQuery.getQuery());
            refreshCables();
        }
    }

    /**
     * Reset query and refresh cables.
     */
    public void resetQuery() {
        selectedQuery = null;
        refreshCables();
    }

    public String getNumberOfFilteredItems() {
        // total number of records - row count for lazy loading mechanism
        return String.valueOf(getLazyModel().getRowCount());
    }

    public void setCables(List<CableUI> cables) {
        this.cables = cables;
        if (!StringUtils.isEmpty(requestedCableName) && cables.size() == 1 &&
                cables.get(0).getName().equals(requestedCableName)) {
            selectedCable = cables.get(0);
            selectedCables.add(cables.get(0));

            requestedCableName = null;
            updatePropertiesForSelectedCable();
        }
    }

    /** @return the lazy loading data model */
    public LazyDataModel<CableUI> getLazyModel() {
        return lazyModel;
    }

    /**
     * Formats the given cable by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedCable
     *            cable to format
     */
    private void formatCable(CableUI selectedCable) {
        selectedCable.setCableClass(Utility.formatWhitespace(selectedCable.getCableClass()));
        selectedCable.setContainer(Utility.formatWhitespace(selectedCable.getContainer()));
        selectedCable.setEndpointBuildingA(Utility.formatWhitespace(selectedCable.getEndpointBuildingA()));
        selectedCable.setEndpointBuildingB(Utility.formatWhitespace(selectedCable.getEndpointBuildingB()));
        selectedCable.setEndpointDeviceNameA(Utility.formatWhitespace(selectedCable.getEndpointDeviceNameA()));
        selectedCable.setEndpointDeviceNameB(Utility.formatWhitespace(selectedCable.getEndpointDeviceNameB()));
        selectedCable.setEndpointLabelA(Utility.formatWhitespace(selectedCable.getEndpointLabelA()));
        selectedCable.setEndpointLabelB(Utility.formatWhitespace(selectedCable.getEndpointLabelB()));
        selectedCable.setEndpointRackA(Utility.formatWhitespace(selectedCable.getEndpointRackA()));
        selectedCable.setEndpointRackB(Utility.formatWhitespace(selectedCable.getEndpointRackB()));
        selectedCable.setSubsystem(Utility.formatWhitespace(selectedCable.getSubsystem()));
        selectedCable.setSystem(Utility.formatWhitespace(selectedCable.getSystem()));
        selectedCable.setComments(Utility.formatWhitespace(selectedCable.getComments()));
        //  no add/edit of revision, thus no format of revision
    }

    private List<InstallationPackage> getInstallationPackages() {
        if (installationPackages == null) {
            installationPackages = installationPackageService.getActiveInstallationPackages();
        }
        return installationPackages;
    }

    /**
     * Return counter for number of cables to be exported.
     *
     * @return number of cables to be exported
     */
    public Long getCablesToExportSize() {
        return cablesToExportSize;
    }

    /**
     * Update counter for number of cables to be exported.
     */
    public void updateCablesToExportSize() {
        this.cablesToExportSize = cableService.getRowCount(lazyModel.getFilters(), createCustomQuery());
    }

    /** @return true if current user is admin else false */
    public boolean getIsAdmin() {
        return sessionService.canAdminister();
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay conten text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Sets the URL for the long general popup.
     *
     * @param longOverlayURL
     *            the url to where the popup is pointing.
     */
    public void setLongOverlayURL(String longOverlayURL) {
        this.longOverlayURL = longOverlayURL;
    }

    /** @return URL for the long general popup. */
    public String getLongOverlayURL() {
        return this.longOverlayURL;
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    public List<InstallationPackage> getAvailableInstallationPackages() {
        return availableInstallationPackages;
    }

    public void setAvailableInstallationPackages(List<InstallationPackage> availableInstallationPackages) {
        this.availableInstallationPackages = availableInstallationPackages;
    }

    /** Clears the newly created selected cable so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedCable = null;
            updatePropertiesForSelectedCable();
        }
    }

    /**
     * Return the chess URL or empty string if there is no URL.
     *
     * @return the chess URL
     */
    private String getChessURLCableProperties() {
        // return the chess URL (with forward slash) or empty string if there is no URL.

        String url = CableProperties.getInstance().getChessURL();
        url = url == null ? "" : url.trim();
        url = !"".equals(url) && !url.endsWith("/") ? url + "/" : url;
        return url;
    }

    /** @return the chess URL */
    public String getChessURL() {
        return chessURL;
    }

    /**
     * Return <tt>boolean</tt> if given information allows link to Chess.
     *
     * @param cableUI The cable UI component
     * @param cableColumnUI The cable column UI component
     * @return Return <tt>boolean</tt> if given information allows link to Chess.
     */
    public boolean linkToChess(final CableUI cableUI, final CableColumnUI cableColumnUI) {
        return !"".equals(getChessURL())
                && cableUI != null
                && !"".equals(StringUtils.defaultString(cableUI.getName()))
                && cableColumnUI != null
                && CableColumnUI.NAME.name().equals(cableColumnUI.name());
    }

    /**
     * Encode a URL and return the encoded value.
     *
     * @param url the url to encode
     * @return the encoded value
     *
     * @see URLUtility#encodeURL(String)
     */
    public String encodeURL(String url) {
        return URLUtility.encodeURL(url);
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /***
     * Generates the tooltip message when there is a problem with cable
     *
     * @param cableUi cable
     * @return the error-tooltip message
     */
    public String tooltipForProblem(CableUI cableUi) {
        return ValidityUtil.getReasonsForProblem(cableUi.getCable(), names);
    }

    /**
     * Return tooltip for cable value.
     *
     * @param cable cable
     * @param label label
     * @param column column
     * @param value value
     * @return tooltip for cable value
     */
    public String tooltipForValue(CableUI cable, String label, String column, String value) {
        switch (column) {
            case CableColumnUIConstants.SYSTEM_VALUE:
                return CableNumbering.getSystemLabel(cable.getSystem());
            case CableColumnUIConstants.SUBSYSTEM_VALUE:
                return CableNumbering.getSubsystemLabel(cable.getSystem(), cable.getSubsystem());
            case CableColumnUIConstants.CLASS_VALUE:
                return CableNumbering.getClassLabel(cable.getCableClass());
            default:
                return null;
        }
    }

    /**
     * Return tooltip for cable link.
     *
     * @param label label
     * @param column column
     * @param value (part of) tooltip
     * @return tooltip for cable link
     */
    public String tooltipForLink(String label, String column, String value) {
        switch (column) {
            case CableColumnUIConstants.CABLEARTICLE_VALUE:
                return "Click to select \"" + value + "\" in Cable Articles tab";
            case CableColumnUIConstants.CABLETYPE_VALUE:
                return "Click to select \"" + value + "\" in Cable Types tab";
            case CableColumnUIConstants.FROM_ESS_NAME_VALUE:
            case CableColumnUIConstants.TO_ESS_NAME_VALUE:
                return "Click to open navigation dialog to Naming or CCDB application";
            case CableColumnUIConstants.FROM_CONNECTOR_VALUE:
            case CableColumnUIConstants.TO_CONNECTOR_VALUE:
                return "Click to select \"" + value + "\" in Connectors tab";
            case CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_VALUE:
                return "Click to open navigation dialog to " + value;
            default:
                return null;
        }
    }

    /**
     * Return tooltip for cable label.
     *
     * @param column cable column
     * @return tooltip for cable label
     */
    public String tooltipForLabel(CableColumnUI column) {
        return column.getTooltip();
    }

    /**
     * Initializes the filter for cable dataTable
     */
    private void initStatusFilter(){
        DataTable dt =
                (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":cableTableForm:cableTable");

        //set (default)filter only when no other filters were set before (filterMap is empty)
        if((dt != null) && (dt.getFilters().isEmpty())) {
            Map<String, Object> newFilter= new HashMap<>();
            newFilter.put(CableColumnUI.STATUS.getValue(), CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE);
            dt.setFilters(newFilter);
        }
    }


    // ----------------------------------------------------------------------------------------------------

    /**
     * @return whether Device A of selected cable is empty or not
     */
    public boolean getIsEmptyDeviceA() {
        return isEmptyDeviceA;
    }
    /**
     * @return whether Device A FBS Tag of selected cable is empty or not
     */
    public boolean getIsEmptyDeviceAFbsTag() {
        return isEmptyDeviceAFbsTag;
    }
    /**
     * @return whether Device B of selected cable is empty or not
     */
    public boolean getIsEmptyDeviceB() {
        return isEmptyDeviceB;
    }
    /**
     * @return whether Device B FBS Tag of selected cable is empty or not
     */
    public boolean getIsEmptyDeviceBFbsTag() {
        return isEmptyDeviceBFbsTag;
    }

    /**
     * Set variables for whether properties of selected cable are empty or not.
     */
    private void updatePropertiesForSelectedCable() {
        if (selectedCable != null) {
            isEmptyDeviceA = StringUtils.isEmpty(selectedCable.getEndpointDeviceNameA());
            isEmptyDeviceAFbsTag = StringUtils.isEmpty(selectedCable.getEndpointDeviceNameAFbsTag());
            isEmptyDeviceB = StringUtils.isEmpty(selectedCable.getEndpointDeviceNameB());
            isEmptyDeviceBFbsTag = StringUtils.isEmpty(selectedCable.getEndpointDeviceNameBFbsTag());
        } else {
            isEmptyDeviceA = true;
            isEmptyDeviceAFbsTag = true;
            isEmptyDeviceB = true;
            isEmptyDeviceBFbsTag = true;
        }
    }

    /**
     * Clear and (possibly) fill From ESS Name with value corresponding to From FBS Tag, for selected cable.
     */
    public void clearFillFromEssName() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear chess id (chess id handled server side)
            selectedCable.setEndpointDeviceNameA(
                    fbsService.getEssNameForFbsTag(selectedCable.getEndpointDeviceNameAFbsTag()));
            selectedCable.setEndpointADeviceChessId(null);

            updatePropertiesForSelectedCable();
        }
    }
    /**
     * Clear and (possibly) fill From FBS Tag with value corresponding to From ESS Name, for selected cable.
     */
    public void clearFillFromFbsTag() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear chess id (chess id handled server side)
            selectedCable.setEndpointDeviceNameAFbsTag(
                    fbsService.getFbsTagForEssName(selectedCable.getEndpointDeviceNameA()));
            selectedCable.setEndpointADeviceChessId(null);

            updatePropertiesForSelectedCable();
        }
    }
    /**
     * Clear and (possibly) fill From Enclosure ESS Name with value corresponding to From Enclosure FBS Tag,
     * for selected cable.
     */
    public void clearFillFromEnclosureEssName() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear enclosure chess id (chess id handled server side)
            selectedCable.setEndpointRackA(fbsService.getEssNameForFbsTag(
                    selectedCable.getEndpointRackAFbsTag()));
            selectedCable.setEndpointARackChessId(null);

            updatePropertiesForSelectedCable();
        }
    }
    /**
     * Clear and (possibly) fill From Enclosure FBS Tag with value corresponding to From Enclosure ESS Name,
     * for selected cable.
     */
    public void clearFillFromEnclosureFbsTag() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear enclosure chess id (chess id handled server side)
            selectedCable.setEndpointRackAFbsTag(fbsService.getFbsTagForEssName(
                    selectedCable.getEndpointRackA()));
            selectedCable.setEndpointARackChessId(null);

            updatePropertiesForSelectedCable();
        }
    }

    /**
     * Clear and (possibly) fill To ESS Name with value corresponding to To FBS Tag, for selected cable.
     */
    public void clearFillToEssName() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear chess id (chess id handled server side)
            selectedCable.setEndpointDeviceNameB(fbsService.getEssNameForFbsTag(
                    selectedCable.getEndpointDeviceNameBFbsTag()));
            selectedCable.setEndpointBDeviceChessId(null);

            updatePropertiesForSelectedCable();
        }
    }
    /**
     * Clear and (possibly) fill To FBS Tag with value corresponding to To ESS Name, for selected cable.
     */
    public void clearFillToFbsTag() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear chess id (chess id handled server side)
            selectedCable.setEndpointDeviceNameBFbsTag(fbsService.getFbsTagForEssName(
                    selectedCable.getEndpointDeviceNameB()));
            selectedCable.setEndpointBDeviceChessId(null);

            updatePropertiesForSelectedCable();
        }
    }
    /**
     * Clear and (possibly) fill To Enclosure ESS Name with value corresponding to To Enclosure FBS Tag,
     * for selected cable.
     */
    public void clearFillToEnclosureEssName() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear enclosure chess id (chess id handled server side)
            selectedCable.setEndpointRackB(fbsService.getEssNameForFbsTag(
                    selectedCable.getEndpointRackBFbsTag()));
            selectedCable.setEndpointBRackChessId(null);

            updatePropertiesForSelectedCable();
        }
    }
    /**
     * Clear and (possibly) fill To Enclosure FBS Tag with value corresponding to To Enclosure ESS Name,
     * for selected cable.
     */
    public void clearFillToEnclosureFbsTag() {
        // to be used from add/edit dialog
        if (selectedCable != null) {
            // CHESS/ITIP web service used to retrieve FBS information
            // clear enclosure chess id (chess id handled server side)
            selectedCable.setEndpointRackBFbsTag(fbsService.getFbsTagForEssName(
                    selectedCable.getEndpointRackB()));
            selectedCable.setEndpointBRackChessId(null);

            updatePropertiesForSelectedCable();
        }
    }

    public SessionObject getSessionObject() {
        return sessionObject;
    }

    public void setSessionObject(SessionObject sessionObject) {
        this.sessionObject = sessionObject;
    }

    public ReuseCableDetails getReuseCableDetails() {
        return reuseCableDetails;
    }

    /**
     * Reuses the cable with desired parameters, and shows result on Message
     */
    public void reuseCable() {
        try {
            Cable newCable = cableService.reuseCable(reuseCableDetails);
            UiUtility.showInfoMessage("Cable reused! New cable name: " + newCable.getName());
        } catch (Exception e) {
            UiUtility.showErrorMessage("Error while reusing cable!", e);
        } finally {
            initReuseCableDetails();
        }
    }

    /**
     * Lists cable statuses when reusing a cable on UI
     *
     * @return available cable statuses
     */
    public List<CableStatus> allCableStatuses() {
        return Arrays.asList(CableStatus.values());
    }

    /**
     * Tooltip util function for showing tooltip for cable Edit button.
     *
     * @return tooltip string for Edit menu
     */
    public String tooltipForEditMenu() {
        return tooltipForEditLock(selectedCable);
    }

    /**
     * Tooltip util function for showing tooltip for cable Lock column.
     * Generates different string for the menu if
     * - cable is locked for editing
     * - cable is not selected
     * - cable is selected, and is not locked for editing for the user
     * - user does not have privileges to edit cables
     *
     * @param cable cable for which to prepare tooltip for Lock column
     *
     * @return tooltip string for Lock column
     */
    public String tooltipForEditLock(CableUI cable) {
        if(cable == null) {
            return "Please, select a cable!";
        }

        //cable is locked for editing for the owner
        if(isCableLocked(cable.getCable())) {
            return "The editing for this cable is locked! \r\n" +
                    "Please, contact an admin if you want to modify it!";
        }

        //no RBAC permission
        if(!sessionService.canManageOwnedCables()) {
            return "You don't have permission to manage own cables! \r\n" +
                    "Please contact the admins!";
        }

        //only owners can edit cables
        if(!cable.getOwners().contains(sessionService.getLoggedInName())) {
            return "Only cable owners are able to modify the cable! \r\n" +
                    "Please, contact an admin if you want to modify it!";
        }

        return "Edit selected cable.";
    }

    /**
     * Decides if a cable cable can be edited, or not.
     * Cable with INSERTED status is unlocked, but any other status is locked
     *
     * @param cable the cable for which the lock state for editing has to be determined
     * @return <code>true</code> if cable can be edited
     * <code>false</code> if cable can not be edited
     */
    public boolean isCableLocked(Cable cable) {
        return !CableStatus.INSERTED.equals(cable.getStatus());
    }

    /**
     * Decides if a cable is locked-, or unlocked for editing for the currently logged in user.
     *
     * @param cable the cable for which the lock state for editing has to be determined
     * @return <code>true</code> if cable editing is locked
     * <code>false</code> if cable editing is unlocked
     */
    public boolean isCableEditable(CableUI cable) {
        if (cable == null || cable.getStatus() == CableStatus.DELETED) {
            return false;
        }
        if (sessionObject.canAdminister()) {
            return true;
        } else if (sessionService.canManageOwnedCables()) {
            return cable.getOwners().contains(sessionService.getLoggedInName()) &&
                    (!isCableLocked(cable.getCable()));
        }
        return false;
    }
}
