/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.openepics.cable.model.Artifact;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.RoutingRow;
import org.openepics.cable.services.DateUtil;

/**
 * <code>CableUI</code> is a presentation of {@link Cable} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableUI implements Serializable {

    private static final long serialVersionUID = 8115229002220900451L;
    private static final String EMPTY_STRING = "";
    private static final String CABLE_TYPE_BASE_URL = "cable-types.xhtml?cableTypeName=";
    private static final String CONNECTOR_BASE_URL = "connectors.xhtml?connectorName=";
    private static final String CONTAINER_BASE_URL = "cables.xhtml?cableName=";
    private static final String ENDPOINT_BASE_URL = "devices.xhtml?i=2&deviceName=";
    private Cable cable;

    /**
     * Constructs the ui object for the given cable instance.
     *
     * @param cable
     *            the cable instance
     */
    public CableUI(Cable cable) {
        this.cable = cable;

    }

    /** @return the cable instance this wraps */
    public Cable getCable() {
        return cable;
    }

    /** @return the cable database id. */
    public Long getId() {
        return cable.getId();
    }

    /**
     * @return the creation date of this cable as string
     *
     * @see Cable#getCreated()
     */
    public String getCreated() {
        return DateUtil.format(cable.getCreated());
    }

    /**
     * @return the modification date of this cable as string
     *
     * @see Cable#getModified()
     */
    public String getModified() {
        return DateUtil.format(cable.getModified());
    }

    /** @return the cable sequential number */
    public Integer getSeqNumber() {
        return cable.getSeqNumber();
    }

    /**
     * Set the cable system digit.
     *
     * @param system
     *            cable system digit
     */
    public void setSystem(String system) {
        cable.setSystem(system);
    }

    /** @return the cable system digit */
    public String getSystem() {
        return cable.getSystem();
    }

    /**
     * Set the cable subsystem digit.
     *
     * @param subsystem
     *            cable subsystem digit
     */
    public void setSubsystem(String subsystem) {
        cable.setSubsystem(subsystem);
    }

    /** @return the cable subsystem digit */
    public String getSubsystem() {
        return cable.getSubsystem();
    }

    /**
     * Set the cable class letter.
     *
     * @param cableClass
     *            cable class letter
     */
    public void setCableClass(String cableClass) {
        cable.setCableClass(cableClass);
    }

    /** @return the cable class letter */
    public String getCableClass() {
        return cable.getCableClass();
    }

    /**
     * Set the cable owner.
     *
     * @param owners
     *            cable owners
     */
    public void setOwners(List<String> owners) {
        cable.setOwners(owners);
    }

    /** @return the cable owners */
    public String getOwnersString() {
        return String.join(", ", getOwners());
    }

    /** @return the cable owners */
    public List<String> getOwners() {
        return cable.getOwners();
    }

    /**
     * Set the cable type.
     *
     * @param cableType
     *            cable type
     */
    public void setCableType(CableType cableType) {
        cable.setCableType(cableType);
    }

    /** @return the cable type */
    public CableType getCableType() {
        return cable.getCableType();
    }

    /** @return the cable number */
    public String getName() {
        return cable.getName();
    }

    /** @return the endpoint a of the cable */
    public Endpoint getEndpointA() {
        return cable.getEndpointA();
    }

    /**
     * set the endpoint a of the cable
     * 
     * @param endpointA
     *            endpoint to set
     */
    public void setEndpointA(Endpoint endpointA) {
        cable.setEndpointA(endpointA);
    }

    /** @return the location description of endpoint A */
    public String getLocationA() {
        if (cable.getEndpointA() != null) {
            return cable.getEndpointA().getBuilding() + " " + cable.getEndpointA().getRack();
        }
        return EMPTY_STRING;
    }

    /**
     * Set the name of the endpoint a device.
     *
     * @param endpointDeviceNameA
     *            the name of the endpoint a device
     */
    public void setEndpointDeviceNameA(String endpointDeviceNameA) {
        cable.getEndpointA().setDevice(endpointDeviceNameA);
    }

    /** @return the name of the endpoint a device. */
    public String getEndpointDeviceNameA() {
        return cable.getEndpointA().getDevice();
    }

    /**
     * Set the building the endpoint a is at.
     *
     * @param endpointBuildingA
     *            the building the endpoint a is at
     */
    public void setEndpointBuildingA(String endpointBuildingA) {
        cable.getEndpointA().setBuilding(endpointBuildingA);
    }

    /** @return the building the endpoint a is at. */
    public String getEndpointBuildingA() {
        return cable.getEndpointA().getBuilding();
    }

    /**
     * Set the rack the endpoint a is at.
     *
     * @param endpointRackA
     *            the rack the endpont a is at
     */
    public void setEndpointRackA(String endpointRackA) {
        cable.getEndpointA().setRack(endpointRackA);
    }

    /** @return the rack the endpoint a is at. */
    public String getEndpointRackA() {
        return cable.getEndpointA().getRack();
    }

    /**
     * Set the endpoint A connector.
     *
     * @param endpointConnectorA
     *            endpoint A connector
     */
    public void setEndpointConnectorA(Connector endpointConnectorA) {
        cable.getEndpointA().setConnector(endpointConnectorA);
    }

    /** @return the endpoint A connector. */
    public Connector getEndpointConnectorA() {
        return cable.getEndpointA().getConnector();
    }

    /**
     * Set the endpoint a reference to the wiring drawing.
     * 
     * @param endpointDrawingA
     *            endpoint a reference to the wiring drawing
     */
    public void setEndpointDrawingA(String endpointDrawingA) {
        if (endpointDrawingA == null || endpointDrawingA.isEmpty()) {
            cable.getEndpointA().setDrawing(null);
        } else {
            cable.getEndpointA().setDrawing(endpointDrawingA);
        }
    }

    /** @return the endpoint a reference to the wiring drawing */
    public String getEndpointDrawingA() {
        if (cable.getEndpointA().getDrawing() != null) {
            return cable.getEndpointA().getDrawing().toString();
        }
        return EMPTY_STRING;
    }

    /**
     * Set the endpoint a label of the cable.
     *
     * @param endpointLabelA
     *            endpoint a label of the cable
     */
    public void setEndpointLabelA(String endpointLabelA) {
        cable.getEndpointA().setLabel(endpointLabelA);
    }

    /** @return the endpoint a label of the cable */
    public String getEndpointLabelA() {
        return cable.getEndpointA().getLabel();
    }

    /** @return the endpoint b of the cable */
    public Endpoint getEndpointB() {
        return cable.getEndpointB();
    }

    /**
     * set the endpoint a of the cable
     * 
     * @param endpointB
     *            the endpoint to set
     */
    public void setEndpointB(Endpoint endpointB) {
        cable.setEndpointB(endpointB);
    }

    /** @return the location description of endpoint B */
    public String getLocationB() {
        if (cable.getEndpointB() != null) {
            return cable.getEndpointB().getBuilding() + " " + cable.getEndpointB().getRack();
        }
        return EMPTY_STRING;
    }

    /**
     * Set the name of the endpoint b device.
     *
     * @param endpointDeviceNameB
     *            the name of the endpoint b device
     */
    public void setEndpointDeviceNameB(String endpointDeviceNameB) {
        cable.getEndpointB().setDevice(endpointDeviceNameB);
    }

    /** @return the name of the endpoint b device. */
    public String getEndpointDeviceNameB() {
        return cable.getEndpointB().getDevice();
    }

    /**
     * Set the building the endpoint b is at.
     *
     * @param endpointBuildingB
     *            the building the endpoint b is at
     */
    public void setEndpointBuildingB(String endpointBuildingB) {
        cable.getEndpointB().setBuilding(endpointBuildingB);
    }

    /** @return the building the endpoint b is at. */
    public String getEndpointBuildingB() {
        return cable.getEndpointB().getBuilding();
    }

    /**
     * Set the endpoint b reference to the wiring drawing.
     *
     * @param endpointDrawingB
     *            endpoint b reference to the wiring drawing
     */
    public void setEndpointDrawingB(String endpointDrawingB) {
        if (endpointDrawingB == null || endpointDrawingB.isEmpty()) {
            cable.getEndpointB().setDrawing(null);
        } else {
            cable.getEndpointB().setDrawing(endpointDrawingB);
        }
    }

    /**
     * Set the rack the endpoint b is at.
     *
     * @param endpointRackB
     *            the rack the endpont b is at
     */
    public void setEndpointRackB(String endpointRackB) {
        cable.getEndpointB().setRack(endpointRackB);
    }

    /** @return the rack the endpoint b is at. */
    public String getEndpointRackB() {
        return cable.getEndpointB().getRack();
    }

    /** @return the endpoint b reference to the wiring drawing */
    public String getEndpointDrawingB() {
        if (cable.getEndpointB().getDrawing() != null) {
            return cable.getEndpointB().getDrawing().toString();
        }
        return EMPTY_STRING;
    }

    /**
     * Set the endpoint B connector.
     *
     * @param endpointConnectorB
     *            endpoint B connector
     */
    public void setEndpointConnectorB(Connector endpointConnectorB) {
        cable.getEndpointB().setConnector(endpointConnectorB);
    }

    /** @return the endpoint B connector. */
    public Connector getEndpointConnectorB() {
        return cable.getEndpointB().getConnector();
    }

    /**
     * Set the endpoint b label of the cable.
     *
     * @param endpointLabelB
     *            endpoint b label of the cable
     */
    public void setEndpointLabelB(String endpointLabelB) {
        cable.getEndpointB().setLabel(endpointLabelB);
    }

    /** @return the endpoint b label of the cable */
    public String getEndpointLabelB() {
        return cable.getEndpointB().getLabel();
    }

    /**
     * Set the routing value for this cable instance.
     *
     * @param routingRows
     *            the routing value for this cable instance
     */
    public void setRoutingRows(List<RoutingRow> routingRows) {
        cable.setRoutingRows(routingRows);
    }

    /** @return the routing value for this cable instance */
    public List<RoutingRow> getRoutingRows() {
        return cable.getRoutingRows();
    }

    public String getRoutingsString() {
        return cable.getRoutingsString();
    }

    /**
     * @return the date by which this cable is installed as string
     *
     * @see Cable#getInstallationBy()
     */
    public String getInstallationBy() {
        return DateUtil.format(cable.getInstallationBy());
    }

    /**
     * Set the date by which this cable is installed.
     *
     * @param installationBy
     *            the date by which this cable is installed.
     */
    public void setInstallationByDate(Date installationBy) {
        cable.setInstallationBy(installationBy);
    }

    /** @return the date by which this cable is installed. */
    public Date getInstallationByDate() {
        return cable.getInstallationBy();
    }

    /**
     * @return the date by which this cable is terminated as string
     *
     * @see Cable#getTerminationBy()
     */
    public String getTerminationBy() {
        return DateUtil.format(cable.getTerminationBy());
    }

    /**
     * Set the date by which this cable is terminated.
     *
     * @param terminationBy
     *            the date by which this cable is terminated
     */
    public void setTerminationByDate(Date terminationBy) {
        cable.setTerminationBy(terminationBy);
    }

    /**
     * @return the date by which this cable is terminated.
     */
    public Date getTerminationByDate() {
        return cable.getTerminationBy();
    }

    /**
     * @return the quality report artifact.
     */
    public Artifact getQualityReport() {
        return cable.getQualityReport();
    }

    /**
     * Set the quality report artifact.
     *
     * @param qualityReport
     *            quality report artifact.
     */
    public void setQualityReport(Artifact qualityReport) {
        cable.setQualityReport(qualityReport);
    }

    /**
     * Set the base cable length.
     *
     * @param baseLength
     *            user specified cable length
     */
    public void setBaseLength(Float baseLength) {
        cable.setBaseLength(baseLength != null ? baseLength : 0);
    }

    /** @return the base cable length */
    public Float getBaseLength() {
        return cable.getBaseLength();
    }

    /**
     * Set the user specified cable length.
     *
     * @param length
     *            user specified cable length
     */
    public void setLength(Float length) {
        cable.setLength(length);
    }

    /** @return the user specified cable length */
    public Float getLength() {
        return cable.getLength();
    }

    /** @return the cable status */
    public CableStatus getStatus() {
        return cable.getStatus();
    }

    /** @return true if the cable data is valid, else false */
    public boolean isValid() {
        return cable.isValid();
    }

    /**
     * @return true if cable is approved otherwise false.
     */
    public boolean getApproved() {
        CableStatus status = getStatus();
        if (status != null) {
            return CableStatus.APPROVED == getStatus();
        }
        return false;
    }

    public String getContainer() {
        return cable.getContainer();
    }

    public void setContainer(String container) {
        cable.setContainer(container);
    }

    public CableAutoCalculatedLength getAutoCalculatedLength() {
        return cable.getAutoCalculatedLength();
    }

    public void setAutoCalculatedLength(CableAutoCalculatedLength autoCalculatedLength) {
        cable.setAutoCalculatedLength(autoCalculatedLength);
    }

    public Boolean getAutoCalculatedLengthBoolean() {
        return cable.getAutoCalculatedLength() == CableAutoCalculatedLength.YES;
    }

    public void setAutoCalculatedLengthBoolean(Boolean autoCalculatedLengthBoolean) {
        cable.setAutoCalculatedLength(
                autoCalculatedLengthBoolean ? CableAutoCalculatedLength.YES : CableAutoCalculatedLength.NO);
    }

    /** @return current copy of the cable. */
    public Cable getCableCopy() {
        final Endpoint endpointA = new Endpoint(cable.getEndpointA().getDevice(), cable.getEndpointA().getBuilding(),
                cable.getEndpointA().getRack(), cable.getEndpointA().getConnector(), cable.getEndpointA().getDrawing(),
                cable.getEndpointA().getLabel());
        final Endpoint endpointB = new Endpoint(cable.getEndpointB().getDevice(), cable.getEndpointB().getBuilding(),
                cable.getEndpointB().getRack(), cable.getEndpointB().getConnector(), cable.getEndpointB().getDrawing(),
                cable.getEndpointB().getLabel());
        final Cable copy = new Cable(cable.getSystem(), cable.getSubsystem(), cable.getCableClass(),
                cable.getSeqNumber(), cable.getOwners(), cable.getStatus(), cable.getCableType(), cable.getContainer(),
                endpointA, endpointB, cable.getCreated(), cable.getModified(), cable.getRoutingRows(),
                cable.getInstallationBy(), cable.getTerminationBy(), cable.getQualityReport(),
                cable.getAutoCalculatedLength(), cable.getLength());
        return copy;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Cable Name:");
        sb.append(' ');
        sb.append(getName());
        return sb.toString();
    }

    public String getCableTypeUrl() {
        if (cable.getCableType() != null) {
            return CABLE_TYPE_BASE_URL + cable.getCableType().getName();
        } else {
            return CABLE_TYPE_BASE_URL;
        }
    }

    public String getEndpointConnectorAUrl() {
        if (cable.getEndpointA() != null && cable.getEndpointA().getConnector() != null) {
            return CONNECTOR_BASE_URL + cable.getEndpointA().getConnector().getName();
        } else {
            return CONNECTOR_BASE_URL;
        }
    }

    public String getEndpointConnectorBUrl() {
        if (cable.getEndpointB() != null && cable.getEndpointB().getConnector() != null) {
            return CONNECTOR_BASE_URL + cable.getEndpointB().getConnector().getName();
        } else {
            return CONNECTOR_BASE_URL;
        }
    }

    public String getContainerUrl() {
        return CONTAINER_BASE_URL + cable.getContainer();
    }

    public String getEndpointDeviceAUrl() {
        return ENDPOINT_BASE_URL + cable.getEndpointA().getDevice();
    }

    public String getEndpointDeviceBUrl() {
        return ENDPOINT_BASE_URL + cable.getEndpointB().getDevice();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cable == null) ? 0 : cable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CableUI other = (CableUI) obj;
        if (cable != null)
            return cable.equals(other.cable);
        return true;
    }

    public void setStatus(CableStatus status) {
        cable.setStatus(status);
    }
}
