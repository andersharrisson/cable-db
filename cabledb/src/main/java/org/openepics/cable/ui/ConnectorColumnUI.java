/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.openepics.cable.services.dl.ConnectorColumn;

/**
 * Enum wrapper for ConnectorColumn. Contains information for displaying Connector columns.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum ConnectorColumnUI {
    NAME(ConnectorColumn.NAME, ConnectorColumnUIConstants.NAME_VALUE,  ConnectorColumnUIConstants.NAME_TOOLTIP,
            ConnectorColumnUIConstants.NAME_STYLECLASS, ConnectorColumnUIConstants.NAME_STYLE,
            ConnectorColumnUIConstants.NAME_FILTERMODE, ConnectorColumnUIConstants.NAME_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    DESCRIPTION(ConnectorColumn.DESCRIPTION, ConnectorColumnUIConstants.DESCRIPTION_VALUE,
            ConnectorColumnUIConstants.DESCRIPTION_TOOLTIP, ConnectorColumnUIConstants.DESCRIPTION_STYLECLASS,
            ConnectorColumnUIConstants.DESCRIPTION_STYLE, ConnectorColumnUIConstants.DESCRIPTION_FILTERMODE,
            ConnectorColumnUIConstants.DESCRIPTION_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    TYPE(ConnectorColumn.TYPE, ConnectorColumnUIConstants.TYPE_VALUE,  ConnectorColumnUIConstants.TYPE_TOOLTIP,
            ConnectorColumnUIConstants.TYPE_STYLECLASS, ConnectorColumnUIConstants.TYPE_STYLE,
            ConnectorColumnUIConstants.TYPE_FILTERMODE, ConnectorColumnUIConstants.TYPE_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    ASSEMBLY_INSTRUCTIONS(ConnectorColumn.ASSEMBLY_INSTRUCTIONS, ConnectorColumnUIConstants.ASSEMBLY_INST_VALUE,
            ConnectorColumnUIConstants.ASSEMBLY_INST_TOOLTIP, ConnectorColumnUIConstants.ASSEMBLY_INST_STYLECLASS,
            ConnectorColumnUIConstants.ASSEMBLY_INST_STYLE, ConnectorColumnUIConstants.ASSEMBLY_INST_FILTERMODE,
            ConnectorColumnUIConstants.ASSEMBLY_INST_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    LINK_TO_DATASHEET(ConnectorColumn.LINK_TO_DATASHEET, ConnectorColumnUIConstants.LINK_TO_DATASHEET_VALUE,
            ConnectorColumnUIConstants.LINK_TO_DATASHEET_TOOLTIP,
            ConnectorColumnUIConstants.LINK_TO_DATASHEET_STYLECLASS,
            ConnectorColumnUIConstants.LINK_TO_DATASHEET_STYLE, ConnectorColumnUIConstants.LINK_TO_DATASHEET_FILTERMODE,
            ConnectorColumnUIConstants.LINK_TO_DATASHEET_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DATASHEET(ConnectorColumn.DATASHEET, ConnectorColumnUIConstants.DATASHEET_VALUE,
            ConnectorColumnUIConstants.DATASHEET_TOOLTIP, ConnectorColumnUIConstants.DATASHEET_STYLECLASS,
            ConnectorColumnUIConstants.DATASHEET_STYLE, ConnectorColumnUIConstants.DATASHEET_FILTERMODE,
            ConnectorColumnUIConstants.DATASHEET_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.DOWNLOAD),
    MANUFACTURERS(ConnectorColumn.MANUFACTURERS, ConnectorColumnUIConstants.MANUFACTURERS_VALUE,
            ConnectorColumnUIConstants.MANUFACTURERS_TOOLTIP, ConnectorColumnUIConstants.MANUFACTURERS_STYLECLASS,
            ConnectorColumnUIConstants.MANUFACTURERS_STYLE, ConnectorColumnUIConstants.MANUFACTURERS_FILTERMODE,
            ConnectorColumnUIConstants.MANUFACTURERS_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGLINK),
    STATUS(ConnectorColumn.STATUS, ConnectorColumnUIConstants.STATUS_VALUE,
            ConnectorColumnUIConstants.STATUS_TOOLTIP, ConnectorColumnUIConstants.STATUS_STYLECLASS,
            ConnectorColumnUIConstants.STATUS_STYLE, ConnectorColumnUIConstants.STATUS_FILTERMODE,
            ConnectorColumnUIConstants.STATUS_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private ConnectorColumn parent;
    private String tooltip;
    private String value;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private String filterOptions;
    private boolean filterDropdown;
    private CableColumnStyle columnStyle;

    /**
     * ConnectorColumnUI constructor
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     */
    private ConnectorColumnUI(ConnectorColumn parent, String value, String tooltip, String styleClass, String style,
            String filterMode, String filterStyle, CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
        if (ConnectorColumnUIConstants.STATUS_VALUE.equals(value)) {
            this.filterDropdown = true;
        }
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /**
     * Searches for ConnectorColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return ConnectorColumnUI enum
     */
    public static ConnectorColumnUI convertColumnLabel(String columnLabel) {
        for (ConnectorColumnUI value : ConnectorColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (ConnectorColumnUI value : ConnectorColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public String getFilterOptions() {
        return filterOptions;
    }

    public boolean isFilterDropdown() {
        return this.filterDropdown;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }

}
