/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.export.SimpleManufacturerTableExporter;
import org.openepics.cable.export.SimpleTableExporter;
import org.openepics.cable.export.SimpleTableExporterFactory;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.model.ManufacturerStatus;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.services.DateUtil;
import org.openepics.cable.services.ManufacturerService;
import org.openepics.cable.services.QueryService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.dl.ManufacturerColumn;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.Utility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * This is the backing requests bean for manufacturers.xhtml.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class ManufacturerRequestManager implements Serializable, SimpleTableExporterFactory {

    // Coupled to over 20 classes due to performing service validations
    private static final long serialVersionUID = 8161254904185694693L;
    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final Logger LOGGER = Logger.getLogger(ManufacturerRequestManager.class.getName());
    private static final List<ManufacturerUI> EMPTY_LIST = new ArrayList<>();

    private List<ManufacturerColumnUI> columns;
    private List<String> columnTemplate = ManufacturerColumnUI.getAllColumns();

    @Inject
    private transient ManufacturerService manufacturerService;

    @Inject
    private SessionService sessionService;

    @Inject
    private QueryService queryService;

    private List<ManufacturerUI> manufacturers;
    private List<ManufacturerUI> filteredManufacturers;
    private List<ManufacturerUI> deletedManufacturers;
    private List<ManufacturerUI> selectedManufacturers = EMPTY_LIST;

    private ManufacturerUI selectedManufacturer;
    private QueryUI selectedQuery;
    private String requestedManufacturerName;
    private boolean isAddPopupOpened;
    private boolean isManufacturerRequested;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;

    private Manufacturer oldManufacturer;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public ManufacturerRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns + history column
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = ManufacturerColumnUI.values().length + 1;
        columnVisibility = ManagerUtil.setAllColumnVisible(numberOfColumns);
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display, and invokes service availability check at first invocation. */
    @PostConstruct
    public void init() {
        try {
            isAddPopupOpened = false;
            manufacturers = new ArrayList<>();
            selectedManufacturers.clear();
            selectedManufacturer = null;
            requestedManufacturerName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getParameter("manufacturerName");
            createDynamicColumns();
            refreshManufacturers();

            // prepare datatable
            //     cookies
            //         rows per page
            //         column visibility
            //     row number (in all rows) for requested entry (if any)
            Cookie[] cookies = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                            initPaginationPageSize(cookie.getValue());
                            break;
                        case CookieUtility.CD_MANUFACTURER_COLUMN_VISIBILITY:
                            initColumnVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }
            rowNumber = rowNumber(requestedManufacturerName);
        } catch (Exception e) {
            throw new UIException("Manufacturers display initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        ManagerUtil.initColumnVisibility(visibility, numberOfColumns, columnVisibility);
    }

    /**
     * Find out row number, if applicable, for selected entry in list of all entries.
     *
     * @param manufacturerName
     * @return
     */
    private int rowNumber(String manufacturerName) {
        // note
        //     currently not consider filter

        ManufacturerUI manufacturerToSelect = null;
        this.isManufacturerRequested = false;
        if (requestedManufacturerName != null) {
            isManufacturerRequested = true;
            manufacturerToSelect = getManufacturerFromManufacturerName(requestedManufacturerName);
        }

        selectedManufacturers.clear();
        if (manufacturerToSelect != null) {
            selectedManufacturer = manufacturerToSelect;
            selectedManufacturers.add(selectedManufacturer);

            int elementPosition = 0;
            for (ManufacturerUI manufacturer : manufacturers) {
                if (manufacturer.getName().equals(requestedManufacturerName)) {
                    return elementPosition;
                }
                elementPosition++;
            }
        }

        return 0;
    }

    private ManufacturerUI getManufacturerFromManufacturerName(final String manufacturerName) {
        if (manufacturerName == null || manufacturerName.isEmpty()) {
            return null;
        }

        ManufacturerUI manufacturerToSelect = null;
        for (ManufacturerUI manufacturer : manufacturers) {
            if (manufacturer.getName().equals(manufacturerName)) {
                manufacturerToSelect = manufacturer;
            }
        }
        return manufacturerToSelect;
    }

    private void refreshManufacturers() {
        manufacturers = buildManufacturerUIs();
    }

    private List<ManufacturerUI> buildManufacturerUIs() {

        final List<ManufacturerUI> manufacturerUIs = new ArrayList<ManufacturerUI>();
        List<Manufacturer> manufacturers = null;
        if (selectedQuery != null) {
            manufacturers = new ArrayList<>(
                    manufacturerService.getFilteredManufacturers(getSqlQuery(), selectedQuery.getQuery()));
        } else {
            manufacturers = manufacturerService.getManufacturers();
        }
        selectedManufacturers = EMPTY_LIST;
        selectedManufacturer = null;

        for (Manufacturer manufacturer : manufacturers) {
            manufacturerUIs.add(new ManufacturerUI(manufacturer));
        }
        return manufacturerUIs;
    }

    public String getRequestedManufacturerName() {
        return requestedManufacturerName;
    }

    public void setRequestedManufacturerName(String requestedManufacturerName) {
        this.requestedManufacturerName = requestedManufacturerName;
    }

    /** @return the manufacturers to be displayed */
    public List<ManufacturerUI> getManufacturers() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return manufacturers;
    }

    /** @return the filtered manufacturers to be displayed */
    public List<ManufacturerUI> getFilteredManufacturers() {
        return filteredManufacturers;
    }

    /**
     * @param filteredManufacturers
     *            the filtered manufacturers to set
     */
    public void setFilteredManufacturers(List<ManufacturerUI> filteredManufacturers) {
        this.filteredManufacturers = filteredManufacturers;
        LOGGER.fine("Setting filtered manufacturers: "
                + (filteredManufacturers != null ? filteredManufacturers.size() : "none"));
    }

    /** @return the currently selected manufacturers, cannot return null */
    public List<ManufacturerUI> getSelectedManufacturers() {
        return selectedManufacturers;
    }

    /**
     * @param selectedManufacturers
     *            the manufacturers to select
     */
    public void setSelectedManufacturers(List<ManufacturerUI> selectedManufacturers) {
        this.selectedManufacturers = selectedManufacturers != null ? selectedManufacturers : EMPTY_LIST;
        LOGGER.fine("Setting selected manufacturers: " + this.selectedManufacturers.size());
    }

    /** Clears the current manufacturer selection. */
    public void clearSelectedManufacturers() {
        LOGGER.fine("Invoked clear manufacturer selection.");
        setSelectedManufacturers(null);
    }

    /**
     * Returns the manufacturers to be exported, which are the currently filtered and selected manufacturers, or all
     * filtered manufacturers if none selected.
     *
     * @return the manufacturer to be exported
     */
    public List<ManufacturerUI> getManufacturersToExport() {

        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        final List<ManufacturerUI> manufacturersToExport = new ArrayList<>();
        for (ManufacturerUI manufacturer : getManufacturers()) {

            if (isIncludedByFilter(manufacturer) && manufacturer.getStatus() != ManufacturerStatus.DELETED)
                manufacturersToExport.add(manufacturer);
        }
        LOGGER.fine("Returning manufacturers to export: " + manufacturersToExport.size());
        return manufacturersToExport;
    }

    private boolean isIncludedByFilter(ManufacturerUI manufacturer) {
        return !filteredManufacturersExist() || getFilteredManufacturers().contains(manufacturer);
    }

    private boolean filteredManufacturersExist() {
        return getFilteredManufacturers() != null && !getFilteredManufacturers().isEmpty();
    }

    @Override
    public SimpleTableExporter getSimpleTableExporter() {
        return new SimpleManufacturerTableExporter(getManufacturersToExport(), getManufacturersToExport());
    }

    /** @return the date format string to use to display dates */
    public String getDateFormatString() {
        return DateUtil.DATE_FORMAT_STRING;
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                + CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE
                + "', " + getRows()
                + ", {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);

            String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                    + CookieUtility.CD_MANUFACTURER_COLUMN_VISIBILITY
                    + "', '" + columnVisibility2String()
                    + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
            PrimeFaces.current().executeScript(statement);
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index for column in UI, counting from left
     * @return true if column is marked as visible
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected manufacturers and row selection.
     */
    public void unselectAllRows() {
        clearSelectedManufacturers();

        onRowSelect();
    }

    /**
     * Event triggered when row is selected in table in UI.
     */
    public void onRowSelect() {
        if (selectedManufacturers != null && !selectedManufacturers.isEmpty()) {
            if (selectedManufacturers.size() == 1) {
                selectedManufacturer = selectedManufacturers.get(0);
            } else {
                selectedManufacturer = null;
            }
        } else {
            selectedManufacturer = null;
        }
    }

    /** @return <code>true</code> if a single manufacturer is selected, <code>false</code> otherwise */
    public boolean isSingleManufacturerSelected() {
        return (selectedManufacturers != null) && (selectedManufacturers.size() == 1
                && selectedManufacturers.get(0).getStatus() != ManufacturerStatus.DELETED);
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    /**
     * Prepare for Add manufacturer dialog.
     */
    public void prepareAddPopup() {
        if (sessionService.isLoggedIn()) {
            selectedManufacturer = new ManufacturerUI();
            isAddPopupOpened = true;
        }
    }

    /**
     * Event triggered when manufacturer is created.
     */
    public void onManufacturerAdd() {
        formatManufacturer(selectedManufacturer);
        manufacturerService.createManufacturer(selectedManufacturer.getName(), selectedManufacturer.getAddress(),
                selectedManufacturer.getPhoneNumber(), selectedManufacturer.getEmail(),
                selectedManufacturer.getCountry(), sessionService.getLoggedInName());

        refreshManufacturers();
        UiUtility.showInfoMessage("Manufacturer added.");
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * Prepare for Edit manufacturer dialog.
     */
    public void prepareEditPopup() {
        Preconditions.checkState(isSingleManufacturerSelected());
        Preconditions.checkNotNull(selectedManufacturer);
        oldManufacturer = selectedManufacturer.getManufacturer();
        selectedManufacturer = new ManufacturerUI(manufacturerService.getManufacturer(selectedManufacturer.getName()));
        isAddPopupOpened = false;
    }

    /**
     * Event triggered when manufacturer is updated.
     */
    public void onManufacturerEdit() {
        Preconditions.checkState(isSingleManufacturerSelected());
        Preconditions.checkNotNull(selectedManufacturer);
        final String manufacturerName = oldManufacturer.getName();
        formatManufacturer(selectedManufacturer);
        final Manufacturer newManufacturer = selectedManufacturer.getManufacturer();
        final boolean updated = manufacturerService.updateManufacturer(newManufacturer, oldManufacturer, sessionService.getLoggedInName());
        refreshManufacturers();
        UiUtility.showInfoMessage("Manufacturer '" + manufacturerName + (updated ? "' updated." : "' not updated."));
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * @return the list of deleted manufacturers.
     */
    public List<ManufacturerUI> getDeletedManufacturers() {
        return deletedManufacturers;
    }

    /**
     * Reset values. May be used at e.g. close of delete dialog.
     */
    public void resetValues() {
        deletedManufacturers = null;
    }

    /**
     * The method builds a list of manufacturers that are already deleted. If the list is not empty, it is displayed to
     * the user and the user is prevented from deleting them.
     */
    public void checkManufacturersForDeletion() {
        Preconditions.checkNotNull(selectedManufacturers);
        Preconditions.checkState(!selectedManufacturers.isEmpty());

        deletedManufacturers = Lists.newArrayList();
        for (final ManufacturerUI manufacturerToDelete : selectedManufacturers) {
            if (manufacturerToDelete.getStatus() == ManufacturerStatus.DELETED) {
                deletedManufacturers.add(manufacturerToDelete);
            }
        }
    }

    /**
     * Event triggered when manufacturer is deleted.
     */
    public void onManufacturerDelete() {
        Preconditions.checkNotNull(deletedManufacturers);
        Preconditions.checkState(deletedManufacturers.isEmpty());
        Preconditions.checkNotNull(selectedManufacturers);
        Preconditions.checkState(!selectedManufacturers.isEmpty());
        int deletedManufacturersCounter = 0;
        for (final ManufacturerUI manufacturerToDelete : selectedManufacturers) {
            if (manufacturerService.deleteManufacturer(manufacturerToDelete.getManufacturer(), sessionService.getLoggedInName())) {
                deletedManufacturersCounter++;
            }
        }
        clearSelectedManufacturers();
        filteredManufacturers = null;
        deletedManufacturers = null;
        refreshManufacturers();
        UiUtility.showInfoMessage( "Deleted " + deletedManufacturersCounter + (deletedManufacturersCounter == 1 ? " manufacturer." : " manufacturers."));
    }

    /** @return the selected manufacturer. */
    public ManufacturerUI getSelectedManufacturer() {
        return selectedManufacturer;
    }

    /** @return true if the current user can edit manufacturers, else false */
    public boolean getEditManufacturer() {
        return sessionService.canAdminister();
    }

    public boolean isEditButtonEnabled() {
        if (selectedManufacturer == null || selectedManufacturer.getStatus() == ManufacturerStatus.DELETED) {
            return false;
        }
        return sessionService.canAdminister();
    }

    public boolean isDeleteButtonEnabled() {
        if (selectedManufacturers == null || selectedManufacturers.isEmpty()) {
            return false;
        }
        for (ManufacturerUI manufacturerToDelete : selectedManufacturers) {
            if (manufacturerToDelete.getStatus() == ManufacturerStatus.DELETED) {
                return false;
            }
        }
        return sessionService.canAdminister();
    }

    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    /**
     * Execute selected query.
     *
     * @param e action event
     */
    public void executeQuery(ActionEvent e) {
        refreshManufacturers();
    }

    /**
     * Execute query with given id.
     *
     * @param id query id
     */
    public void excuteQueryId(long id) {
        if (id != -1) {
            selectedQuery = new QueryUI(queryService.getQueryById(id));
            refreshManufacturers();
        }
    }

    /**
     * Reset query and refresh manufacturers.
     */
    public void resetQuery() {
        selectedQuery = null;
        refreshManufacturers();
    }

    public String getNumberOfFilteredItems() {
        return String.valueOf(manufacturers.size());
    }

    public boolean isManufacturerRequested() {
        return isManufacturerRequested;
    }

    private String getSqlQuery() {
        StringBuilder querySB = new StringBuilder(600);
        List<QueryCondition> queryConditions = selectedQuery.getQueryConditions();
        if (queryConditions != null && !queryConditions.isEmpty()) {
            querySB.append("SELECT c FROM Manufacturer c");
            querySB.append(' ').append("WHERE").append(' ');

            Collections.sort(queryConditions);

            for (QueryCondition condition : queryConditions) {

                ManufacturerColumn field = ManufacturerColumn.convertColumnLabel(condition.getField());

                // check if field is not null
                if (field == null)
                    continue;

                if (condition.getParenthesisOpen() == QueryParenthesis.OPEN) {
                    querySB.append(QueryParenthesis.OPEN.getParenthesis());
                }

                querySB.append("c.");

                querySB.append(field.getFieldName());

                querySB.append(' ');

                String value = condition.getValue();

                QueryComparisonOperator operator = condition.getComparisonOperator();
                if (operator.isStringComparisonOperator()) {
                    if (operator == QueryComparisonOperator.STARTS_WITH) {
                        querySB.append("LIKE").append(' ').append("'").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.CONTAINS) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.ENDS_WITH) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("'");
                    }
                } else if (field.isStringComparisonOperator() && operator == QueryComparisonOperator.EQUAL) {
                    querySB.append("LIKE").append(' ').append("'").append(value).append("'");
                } else {
                    querySB.append(operator.getOperator()).append(' ').append("'").append(value).append("'");
                }

                if (condition.getParenthesisClose() == QueryParenthesis.CLOSE) {
                    querySB.append(QueryParenthesis.CLOSE.getParenthesis()).append(' ');
                } else {
                    querySB.append(' ');
                }

                if (condition.getBooleanOperator() != QueryBooleanOperator.NONE) {
                    querySB.append(condition.getBooleanOperator().getOperator()).append(' ');
                }
            }
        }

        LOGGER.fine("Query built: " + querySB.toString());

        return querySB.toString();
    }

    /** Resets column template to default */
    private void resetColumnTemplate() {
        columnTemplate = ManufacturerColumnUI.getAllColumns();
    }

    /**
     * Sets column template.
     *
     * @param columnTemplate
     *            of column names to which we should set template
     */
    private void setColumnTemplate(List<String> columnTemplate) {
        this.columnTemplate = columnTemplate;
    }

    /** Resets displayView to default */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in manufacturer data table
     *
     * @return columns
     */
    public List<ManufacturerColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<ManufacturerColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();

            ManufacturerColumnUI column = ManufacturerColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(":manufacturerTableForm:manufacturerTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /** @return number of column in current display view. */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    /**
     * Checks if there is any other manufacturer apart from selected one, that already has given name.
     *
     * @param ctx ctx
     * @param component component
     * @param value value
     * @throws ValidatorException if manufacturer name is not unique
     */
    public void isManufacturerNameValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        for (ManufacturerUI manufacturer : manufacturers) {
            if (stringValue.equals(manufacturer.getName())
                    && !selectedManufacturer.equals(manufacturer)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Manufacturer with this name already exists.", null));
            }
        }
        return;
    }

    /**
     * Formats the given manufacturer by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedManufacturer
     *            manufacturer to format
     */
    private void formatManufacturer(ManufacturerUI selectedManufacturer) {
        selectedManufacturer.setAddress(Utility.formatWhitespace(selectedManufacturer.getAddress()));
        selectedManufacturer.setCountry(Utility.formatWhitespace(selectedManufacturer.getCountry()));
        selectedManufacturer.setEmail(Utility.formatWhitespace(selectedManufacturer.getEmail()));
        selectedManufacturer.setName(Utility.formatWhitespace(selectedManufacturer.getName()));
        selectedManufacturer.setPhoneNumber(Utility.formatWhitespace(selectedManufacturer.getPhoneNumber()));
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /** Clears the newly created selected manufacturer so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedManufacturer = null;
        }
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Provide suggestions for manufacturer given query to filter for.
     *
     * @param query query to filter for
     * @return suggestions for manufacturer given query to filter for.
     */
    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn()) {
            return Collections.emptyList();
        }

        final List<String> selectItems = new ArrayList<>();
        FacesContext context = FacesContext.getCurrentInstance();
        ManufacturerColumnUI column = (ManufacturerColumnUI) UIComponent.getCurrentComponent(context).getAttributes()
                .get("column");

        if (ManufacturerColumnUI.STATUS.equals(column)) {
            selectItems.add(ManufacturerStatus.DELETED.toString());
            selectItems.add(ManufacturerStatus.APPROVED.toString());
        }
        return selectItems;
    }

    /**
     * Return tooltip for manufacturer column.
     *
     * @param column manufacturer column
     * @return tooltip for manufacturer column
     */
    public String tooltipForManufacturer(ManufacturerColumnUI column) {
        String result;

        switch (column) {
            case ADDRESS:
                result ="Open Address dialog";
                break;
            case EMAIL:
                result ="Open Email dialog";
                break;
            default:
                result = "Open "+ column.getValue() + " dialog";
        }

        return result;
    }
}
