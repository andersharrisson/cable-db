/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.openepics.cable.export.SimpleConnectorTableExporter;
import org.openepics.cable.export.SimpleTableExporter;
import org.openepics.cable.export.SimpleTableExporterFactory;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.DisplayViewColumn;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.services.AuthenticationServiceStartup;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.DateUtil;
import org.openepics.cable.services.DisplayViewService;
import org.openepics.cable.services.QueryService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryService;
import org.openepics.cable.services.dl.ConnectorColumn;
import org.openepics.cable.util.Utility;
import org.primefaces.context.RequestContext;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * This is the backing requests bean for connectors.xhtml.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class ConnectorRequestManager implements Serializable, SimpleTableExporterFactory {
    private static final long serialVersionUID = 8161254904185694693L;

    private static final Logger LOGGER = Logger.getLogger(ConnectorRequestManager.class.getName());
    private static final List<ConnectorUI> EMPTY_LIST = new ArrayList<>();

    private List<ConnectorColumnUI> columns;
    private List<String> columnTemplate = ConnectorColumnUI.getAllColumns();

    @Inject
    private transient ConnectorService connectorService;

    @Inject
    private SessionService sessionService;

    @Inject
    private transient AuthenticationServiceStartup authenticationServiceStartup;
    @Inject
    private transient UserDirectoryService userDirectoryService;
    @Inject
    private QueryService queryService;
    @Inject
    private DisplayViewService displayViewService;

    private List<ConnectorUI> connectors;
    private List<ConnectorUI> filteredConnectors;
    private List<ConnectorUI> deletedConnectors;
    private List<ConnectorUI> selectedConnectors = EMPTY_LIST;

    private ConnectorUI selectedConnector;
    private QueryUI selectedQuery;
    private DisplayViewUI selectedDisplayView;
    private String requestedConnectorName;
    private boolean isAddPopupOpened;
    private boolean isConnectorRequested;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;

    private Connector oldConnector;

    /** Initializes the bean for initial view display, and invokes service availability check at first invocation. */
    @PostConstruct
    public void init() {

        // perform service availability check and validation when ui first displayed
        authenticationServiceStartup.validate();
        userDirectoryService.validate();

        isAddPopupOpened = false;
        connectors = new ArrayList<>();
        selectedConnectors.clear();
        selectedConnector = null;
        requestedConnectorName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest()).getParameter("connectorName");
        createDynamicColumns();
        refreshConnectors();
    }

    public void onLoad() {
        refreshConnectors();
        navigateToUrlSelectedConnector();
    }

    private void navigateToUrlSelectedConnector() {
        ConnectorUI connectorToSelect = null;
        this.isConnectorRequested = false;
        if (requestedConnectorName != null) {
            isConnectorRequested = true;
            connectorToSelect = getConnectorFromConnectorName(requestedConnectorName);
        }

        selectedConnectors.clear();
        if (connectorToSelect != null) {
            selectedConnector = connectorToSelect;
            selectedConnectors.add(selectedConnector);

            int elementPosition = 0;
            for (ConnectorUI connector : connectors) {
                if (connector.getName().equals(requestedConnectorName)) {
                    RequestContext.getCurrentInstance()
                            .execute("selectEntityInTable(" + elementPosition + ", 'connectorTable');");
                    return;
                }
                ++elementPosition;
            }

        } else if (isConnectorRequested) {
            RequestContext.getCurrentInstance().update("cannotFindConnectorForm:cannotFindConnector");
            RequestContext.getCurrentInstance().execute("PF('cannotFindConnector').show();");
        }
    }

    private ConnectorUI getConnectorFromConnectorName(final String connectorName) {
        if (connectorName == null || connectorName.isEmpty()) {
            return null;
        }

        ConnectorUI connectorToSelect = null;
        for (ConnectorUI connector : connectors) {
            if (connector.getName().equals(connectorName)) {
                connectorToSelect = connector;
            }
        }
        return connectorToSelect;
    }

    private void refreshConnectors() {
        connectors = buildConnectorUIs();
    }

    private List<ConnectorUI> buildConnectorUIs() {

        final List<ConnectorUI> connectorUIs = new ArrayList<ConnectorUI>();
        List<Connector> connectors = new ArrayList<Connector>();
        if (selectedQuery != null) {
            connectors = new ArrayList<>(
                    connectorService.getFilteredConnectors(getSqlQuery(), selectedQuery.getQuery()));
        } else {
            connectors = connectorService.getConnectors();
        }
        selectedConnectors = EMPTY_LIST;
        selectedConnector = null;

        for (Connector connector : connectors) {
            connectorUIs.add(new ConnectorUI(connector));
        }
        return connectorUIs;
    }

    public String getRequestedConnectorName() {
        return requestedConnectorName;
    }

    public void setRequestedConnectorName(String requestedConnectorName) {
        this.requestedConnectorName = requestedConnectorName;
    }

    /** @return the connectors to be displayed */
    public List<ConnectorUI> getConnectors() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return connectors;
    }

    /** @return the filtered connectors to be displayed */
    public List<ConnectorUI> getFilteredConnectors() {
        return filteredConnectors;
    }

    /**
     * @param filteredConnectors
     *            the filtered connectors to set
     */
    public void setFilteredConnectors(List<ConnectorUI> filteredConnectors) {
        this.filteredConnectors = filteredConnectors;
        LOGGER.fine(
                "Setting filtered connectors: " + (filteredConnectors != null ? filteredConnectors.size() : "none"));
    }

    /** @return the currently selected connectors, cannot return null */
    public List<ConnectorUI> getSelectedConnectors() {
        return selectedConnectors;
    }

    /**
     * @param selectedConnectors
     *            the connectors to select
     */
    public void setSelectedConnectors(List<ConnectorUI> selectedConnectors) {
        this.selectedConnectors = selectedConnectors != null ? selectedConnectors : EMPTY_LIST;
        LOGGER.fine("Setting selected connectors: " + this.selectedConnectors.size());
    }

    /** Clears the current connector selection. */
    public void clearSelectedConnectors() {
        LOGGER.fine("Invoked clear connector selection.");
        setSelectedConnectors(null);
    }

    /**
     * Returns the connectors to be exported, which are the currently filtered and selected connectors, or all filtered
     * connectors if none selected.
     *
     * @return the connector to be exported
     */
    public List<ConnectorUI> getConnectorsToExport() {

        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        final List<ConnectorUI> connectorsToExport = new ArrayList<>();
        for (ConnectorUI connector : getConnectors()) {
            if (isIncludedByFilter(connector) && connector.isActive())
                connectorsToExport.add(connector);
        }
        LOGGER.fine("Returning connectors to export: " + connectorsToExport.size());
        return connectorsToExport;
    }

    private boolean isIncludedByFilter(ConnectorUI connector) {
        return !filteredConnectorsExist() || getFilteredConnectors().contains(connector);
    }

    private boolean filteredConnectorsExist() {
        return getFilteredConnectors() != null && !getFilteredConnectors().isEmpty();
    }

    @Override
    public SimpleTableExporter getSimpleTableExporter() {
        return new SimpleConnectorTableExporter(getConnectorsToExport(), getConnectorsToExport());
    }

    /** @return the date format string to use to display dates */
    public String getDateFormatString() {
        return DateUtil.DATE_FORMAT_STRING;
    }

    public void onRowSelect() {
        if (selectedConnectors != null && !selectedConnectors.isEmpty()) {
            if (selectedConnectors.size() == 1) {
                selectedConnector = selectedConnectors.get(0);
            } else {
                selectedConnector = null;
            }
        } else {
            selectedConnector = null;
        }
    }

    /** @return <code>true</code> if a single connector is selected, <code>false</code> otherwise */
    public boolean isSingleConnectorSelected() {
        return (selectedConnectors != null) && (selectedConnectors.size() == 1 && selectedConnectors.get(0).isActive());
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    public void prepareAddPopup() {
        if (sessionService.isLoggedIn()) {
            selectedConnector = new ConnectorUI();
            isAddPopupOpened = true;
        }
    }

    public void onConnectorAdd() {
        formatConnector(selectedConnector);
        connectorService.createConnector(selectedConnector.getName(), selectedConnector.getDescription(),
                selectedConnector.getType(), sessionService.getLoggedInName());

        refreshConnectors();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Connector added.", null);
        Utility.updateComponent("cableDBGrowl");
    }

    public void prepareEditPopup() {
        Preconditions.checkState(isSingleConnectorSelected());
        Preconditions.checkNotNull(selectedConnector);
        oldConnector = selectedConnector.getConnector();
        selectedConnector = new ConnectorUI(connectorService.getConnector(selectedConnector.getName()));
        isAddPopupOpened = false;
    }

    /**
     * Updates an existing connector with new information from the dialog.
     */
    public void onConnectorEdit() {
        Preconditions.checkState(isSingleConnectorSelected());
        Preconditions.checkNotNull(selectedConnector);
        final String connectorName = oldConnector.getName();
        formatConnector(selectedConnector);
        final Connector newConnector = selectedConnector.getConnector();
        connectorService.updateConnector(newConnector, oldConnector, sessionService.getLoggedInName());
        refreshConnectors();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Connector '" + connectorName + "' updated.", null);
        Utility.updateComponent("cableDBGrowl");
    }

    /**
     * @return the list of deleted connectors.
     */
    public List<ConnectorUI> getDeletedConnectors() {
        return deletedConnectors;
    }

    public void resetValues() {
        // clearSelectedConnectors();
        deletedConnectors = null;
    }

    /**
     * The method builds a list of connectors that are already deleted. If the list is not empty, it is displayed to the
     * user and the user is prevented from deleting them.
     */
    public void checkConnectorsForDeletion() {
        Preconditions.checkNotNull(selectedConnectors);
        Preconditions.checkState(!selectedConnectors.isEmpty());

        deletedConnectors = Lists.newArrayList();
        for (final ConnectorUI connectorToDelete : selectedConnectors) {
            if (!connectorToDelete.isActive()) {
                deletedConnectors.add(connectorToDelete);
            }
        }
    }

    /**
     * Event handler which handles the connector delete.
     */
    public void onConnectorDelete() {
        Preconditions.checkNotNull(deletedConnectors);
        Preconditions.checkState(deletedConnectors.isEmpty());
        Preconditions.checkNotNull(selectedConnectors);
        Preconditions.checkState(!selectedConnectors.isEmpty());
        int deletedConnectorsCounter = 0;
        for (final ConnectorUI connectorToDelete : selectedConnectors) {
            final Connector deleteConnector = connectorToDelete.getConnector();
            connectorService.deleteConnector(deleteConnector, sessionService.getLoggedInName());
            deletedConnectorsCounter++;
        }
        clearSelectedConnectors();
        filteredConnectors = null;
        deletedConnectors = null;
        refreshConnectors();
        Utility.showMessage(FacesMessage.SEVERITY_INFO, "Deleted " + deletedConnectorsCounter + " connectors.", null);
    }

    /** @return the selected connector. */
    public ConnectorUI getSelectedConnector() {
        return selectedConnector;
    }

    /** @return true if the current user can edit connectors, else false */
    public boolean getEditConnector() {
        return sessionService.canAdminister();
    }

    public boolean isEditButtonEnabled() {
        if (selectedConnector == null || !selectedConnector.isActive()) {
            return false;
        }
        if (sessionService.canAdminister()) {
            return true;
        }
        return false;
    }

    public boolean isDeleteButtonEnabled() {
        if (selectedConnectors == null || selectedConnectors.isEmpty()) {
            return false;
        }
        for (ConnectorUI connectorToDelete : selectedConnectors) {
            if (!connectorToDelete.isActive()) {
                return false;
            }
        }
        if (sessionService.canAdminister()) {
            return true;
        }
        return false;
    }

    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    public void executeQuery(ActionEvent e) {
        refreshConnectors();
    }

    public void excuteQueryId(long id) {
        if (id != -1) {
            selectedQuery = new QueryUI(queryService.getQueryById(id));
            refreshConnectors();
        }
    }

    public void resetQuery() {
        selectedQuery = null;
        refreshConnectors();
    }

    public String getNumberOfFilteredItems() {
        return String.valueOf(connectors.size());
    }

    public boolean isConnectorRequested() {
        return isConnectorRequested;
    }

    private String getSqlQuery() {
        StringBuilder querySB = new StringBuilder(600);
        List<QueryCondition> queryConditions = selectedQuery.getQueryConditions();
        if (queryConditions != null && !queryConditions.isEmpty()) {
            querySB.append("SELECT c FROM Connector c");
            querySB.append(' ').append("WHERE").append(' ');

            Collections.sort(queryConditions);

            for (QueryCondition condition : queryConditions) {

                ConnectorColumn field = ConnectorColumn.convertColumnLabel(condition.getField());

                // check if field is not null
                if (field == null)
                    continue;

                if (condition.getParenthesisOpen() == QueryParenthesis.OPEN) {
                    querySB.append(QueryParenthesis.OPEN.getParenthesis());
                }

                querySB.append("c.");

                querySB.append(field.getFieldName());

                querySB.append(' ');

                String value = condition.getValue();

                QueryComparisonOperator operator = condition.getComparisonOperator();
                if (operator.isStringComparisonOperator()) {
                    if (operator == QueryComparisonOperator.STARTS_WITH) {
                        querySB.append("LIKE").append(' ').append("'").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.CONTAINS) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.ENDS_WITH) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("'");
                    }
                } else if (field.isStringComparisonOperator() && operator == QueryComparisonOperator.EQUAL) {
                    querySB.append("LIKE").append(' ').append("'").append(value).append("'");
                } else {
                    querySB.append(operator.getOperator()).append(' ').append("'").append(value).append("'");
                }

                if (condition.getParenthesisClose() == QueryParenthesis.CLOSE) {
                    querySB.append(QueryParenthesis.CLOSE.getParenthesis()).append(' ');
                } else {
                    querySB.append(' ');
                }

                if (condition.getBooleanOperator() != QueryBooleanOperator.NONE) {
                    querySB.append(condition.getBooleanOperator().getOperator()).append(' ');
                }
            }
        }

        LOGGER.fine("Query built: " + querySB.toString());

        return querySB.toString();
    }

    /**
     * Returns current column template.
     *
     * @return string list of column names
     */
    private List<String> getColumnTemplate() {
        return columnTemplate;
    }

    /** Resets column template to default */
    private void resetColumnTemplate() {
        columnTemplate = ConnectorColumnUI.getAllColumns();
    }

    /**
     * Sets column template.
     *
     * @param columnTemplate
     *            of column names to which we should set template
     */
    private void setColumnTemplate(List<String> columnTemplate) {
        this.columnTemplate = columnTemplate;
    }

    /** Resets displayView to default */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in connector data table
     *
     * @return columns
     */
    public List<ConnectorColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<ConnectorColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();

            ConnectorColumnUI column = ConnectorColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(":connectorTableForm:connectorTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /**
     * Returns currently selected display view.
     * 
     * @return selectedDisplayView
     */
    public DisplayViewUI getSelectedDisplayView() {
        return selectedDisplayView;
    }

    /**
     * Sets selected display view as current
     * 
     * @param selectedDisplayView
     *            selectedDisplayView
     */
    public void setSelectedDisplayView(DisplayViewUI selectedDisplayView) {
        this.selectedDisplayView = selectedDisplayView;
    }

    /** Executed currently selected displayView */
    public void executeSelectedDisplayView() {
        executeDisplayView(getSelectedDisplayView());
    }

    /** @return number of column in current display view. */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    /**
     * Executes displayView.
     *
     * @param displayView
     *            displayView to execute
     */
    public void executeDisplayView(DisplayViewUI displayView) {
        displayView.getDisplayView().updateExecutionDate(new Date());
        displayViewService.updateDisplayView(displayView.getDisplayView());
        List<String> newColumnTemplate = new ArrayList<String>();
        List<DisplayViewColumn> columns = displayView.getDisplayViewColumns();
        Collections.sort(columns);

        for (DisplayViewColumn column : columns) {
            newColumnTemplate.add(column.getColumnName());
        }
        setColumnTemplate(newColumnTemplate);
        createDynamicColumns();
    }

    /**
     * Checks if there is any other connector apart from selected one, that already has given name.
     * 
     * @param ctx
     *            ctx
     * @param component
     *            component
     * @param value
     *            value
     * @throws ValidatorException
     *             if connector name is not unique
     */
    public void isConnectorNameValid(FacesContext ctx, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        for (ConnectorUI connector : connectors) {
            if (stringValue.equals(connector.getName())) {
                if (!selectedConnector.equals(connector)) {
                    throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Connector with this name already exists.", null));
                }
            }
        }
        return;
    }

    /**
     * Formats the given connector by trimming and collapsing all whitespaces in its string fields.
     * 
     * @param selectedConnector
     *            connector to format
     */
    private void formatConnector(ConnectorUI selectedConnector) {
        selectedConnector.setDescription(Utility.formatWhitespaces(selectedConnector.getDescription()));
        selectedConnector.setName(Utility.formatWhitespaces(selectedConnector.getName()));
        selectedConnector.setType(Utility.formatWhitespaces(selectedConnector.getType()));
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /** Clears the newly created selected connector so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedConnector = null;
        }
    }
}
