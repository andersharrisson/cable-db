/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.ui;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.EncodingUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Boolean.TRUE;

/**
 * Utility class for UI purposes, in particular for manager backing beans.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class ManagerUtil {

    /**
     * This class is not to be instantiated.
     */
    private ManagerUtil() {
        throw new IllegalStateException("Utility class");
    }

    static List<Boolean> setAllColumnVisible(final int numberOfColumns) {
        return new ArrayList<>(Collections.nCopies(numberOfColumns, TRUE));
    }

    /**
     * Initial settings for column visibility
     *
     * @param visibility visibility value
     * @param numberOfColumns number of columns in datatable
     * @param columnVisibility {@link List} of {@link Boolean}s which contains the results of visibility settings
     */
    static void initColumnVisibility(final String visibility,
            final int numberOfColumns, final List<Boolean> columnVisibility) {

        if (!StringUtils.isEmpty(visibility)) {
            String[] split = EncodingUtility.decode(visibility).split(CookieUtility.DELIMITER_ENTRIES);
            if (split.length == numberOfColumns) {
                for (int i = 0; i< numberOfColumns; i++) {
                    columnVisibility.set(i, Boolean.valueOf(split[i]));
                }
            }
        }
    }
}
