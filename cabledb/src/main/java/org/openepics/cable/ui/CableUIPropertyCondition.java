/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

/**
 * This represents condition of the CableUIs property.
 * 
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */

public enum CableUIPropertyCondition {
    VALID("valid-property"),
    OBSOLETE("obsolete-property"),
    DELETED("deleted-property"),
    INVALID("invalid-property");

    private final String cablePropertyCondition;

    private CableUIPropertyCondition(String cablePropertyCondition) {
        this.cablePropertyCondition = cablePropertyCondition;
    }

    /** @return the condition of the cables property */
    public String getCablePropertyCondition() {
        return cablePropertyCondition;
    }

}
