/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * Connector UI constants.
 */
public final class ConnectorColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The name of the first connector";
    public static final String NAME_STYLECLASS = "data_table_header";
    public static final String NAME_STYLE = "width:10%; min-width:120px";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 100%;";

    public static final String DESCRIPTION_VALUE = "description";
    public static final String DESCRIPTION_TOOLTIP = "The description of the connector";
    public static final String DESCRIPTION_STYLECLASS = "data_table_header";
    public static final String DESCRIPTION_STYLE = "width:20%; min-width:160px";
    public static final String DESCRIPTION_FILTERMODE = "contains";
    public static final String DESCRIPTION_FILTERSTYLE = "width: 100%;";

    public static final String TYPE_VALUE = "type";
    public static final String TYPE_TOOLTIP = "The contact type for the connector";
    public static final String TYPE_STYLECLASS = "data_table_header";
    public static final String TYPE_STYLE = "width:15%; min-width:120px";
    public static final String TYPE_FILTERMODE = "contains";
    public static final String TYPE_FILTERSTYLE = "width: 100%;";

    public static final String DATASHEET_VALUE = "datasheetName";
    public static final String DATASHEET_TOOLTIP = "The datasheet for the connector";
    public static final String DATASHEET_STYLECLASS = "data_table_header";
    public static final String DATASHEET_STYLE = "width:20%; min-width:180px";
    public static final String DATASHEET_FILTERMODE = "contains";
    public static final String DATASHEET_FILTERSTYLE = "width: 100%;";

    public static final String MANUFACTURERS_VALUE = "manufacturerAndDatasheetNames";
    public static final String MANUFACTURERS_URL = "manufacturerUrl";
    public static final String MANUFACTURERS_TOOLTIP = "The manufacturer of the connector";
    public static final String MANUFACTURERS_STYLECLASS = "data_table_header fixed_width208";
    public static final String MANUFACTURERS_STYLE = null;
    public static final String MANUFACTURERS_FILTERMODE = "contains";
    public static final String MANUFACTURERS_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "valid";
    public static final String STATUS_TOOLTIP = "The status for the connector";
    public static final String STATUS_STYLECLASS = "data_table_header fixed_width92";
    public static final String STATUS_STYLE = null;
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = null;

    public static final String ASSEMBLY_INST_VALUE = "assemblyInstructions";
    public static final String ASSEMBLY_INST_URL = "assemblyInstUrl";
    public static final String ASSEMBLY_INST_TOOLTIP = "Assembly instructions";
    public static final String ASSEMBLY_INST_STYLECLASS = "data_table_header fixed_width208";
    public static final String ASSEMBLY_INST_STYLE = null;
    public static final String ASSEMBLY_INST_FILTERMODE = "contains";
    public static final String ASSEMBLY_INST_FILTERSTYLE = "width: 100%;";

    public static final String LINK_TO_DATASHEET_VALUE = "linkToDatasheet";
    public static final String LINK_TO_DATASHEET_URL = "linkToDatasheetUrl";
    public static final String LINK_TO_DATASHEET_TOOLTIP = "Link to Datasheet";
    public static final String LINK_TO_DATASHEET_STYLECLASS = "data_table_header fixed_width208";
    public static final String LINK_TO_DATASHEET_STYLE = null;
    public static final String LINK_TO_DATASHEET_FILTERMODE = "contains";
    public static final String LINK_TO_DATASHEET_FILTERSTYLE = "width: 100%;";

    /**
     * This class is not to be instantiated.
     */
    private ConnectorColumnUIConstants() {
        throw new IllegalStateException("Utility class");
    }

}
