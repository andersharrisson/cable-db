package org.openepics.cable.ui;

public interface ConnectorColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The name of the first connector.";
    public static final String NAME_STYLECLASS = null;
    public static final String NAME_STYLE = "width:10%; min-width:120px";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width:10%; min-width:120px";

    public static final String DESCRIPTION_VALUE = "description";
    public static final String DESCRIPTION_TOOLTIP = "The description of the connector.";
    public static final String DESCRIPTION_STYLECLASS = null;
    public static final String DESCRIPTION_STYLE = "width:20%; min-width:160px";
    public static final String DESCRIPTION_FILTERMODE = "contains";
    public static final String DESCRIPTION_FILTERSTYLE = "width:20%; min-width:160px";

    public static final String TYPE_VALUE = "type";
    public static final String TYPE_TOOLTIP = "The contact type for the connector.";
    public static final String TYPE_STYLECLASS = null;
    public static final String TYPE_STYLE = "width:15%; min-width:120px";
    public static final String TYPE_FILTERMODE = "contains";
    public static final String TYPE_FILTERSTYLE = "width:15%; min-width:120px";

    public static final String DRAWING_VALUE = "drawing";
    public static final String DRAWING_TOOLTIP = "The drawing for the connector.";
    public static final String DRAWING_STYLECLASS = null;
    public static final String DRAWING_STYLE = "width:20%; min-width:160px";
    public static final String DRAWING_FILTERMODE = "contains";
    public static final String DRAWING_FILTERSTYLE = "width:20%; min-width:160px";
}
