/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.Date;

import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.model.ManufacturerStatus;
import org.openepics.cable.services.DateUtil;

/**
 * <code>ManufacturerUI</code> is a presentation of {@link Manufacturer} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class ManufacturerUI implements Serializable {

    private static final long serialVersionUID = 8115229002220900451L;

    private final Manufacturer manufacturer;

    /**
     * Constructs Manufacturer UI object for empty manufacturer instance.
     */
    public ManufacturerUI() {
        this.manufacturer = new Manufacturer();
    }

    /**
     * Constructs Manufacturer UI object for given manufacturer instance.
     *
     * @param manufacturer manufacturer instance
     */
    public ManufacturerUI(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    /** @return the manufacturer instance this wraps */
    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    /** @return the manufacturer database id. */
    public Long getId() {
        return manufacturer.getId();
    }

    /**
     * @return the creation date of this manufacturer as string
     *
     * @see Manufacturer#getCreated()
     */
    public String getCreated() {
        return DateUtil.format(manufacturer.getCreated());
    }

    public String getName() {
        return manufacturer.getName();
    }

    public void setName(String name) {
        manufacturer.setName(name);
    }

    public String getAddress() {
        return manufacturer.getAddress();
    }

    public void setAddress(String address) {
        manufacturer.setAddress(address);
    }

    public String getPhoneNumber() {
        return manufacturer.getPhoneNumber();
    }

    public void setPhoneNumber(String phoneNumber) {
        manufacturer.setPhoneNumber(phoneNumber);
    }

    public String getEmail() {
        return manufacturer.getEmail();
    }

    public void setEmail(String email) {
        manufacturer.setEmail(email);
    }

    public String getCountry() {
        return manufacturer.getCountry();
    }

    public void setCountry(String country) {
        manufacturer.setCountry(country);
    }

    public void setCreated(Date created) {
        manufacturer.setCreated(created);
    }

    /** @return the manufacturer status */
    public ManufacturerStatus getStatus() {
        return manufacturer.getStatus();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (getClass() != other.getClass())
            return false;
        ManufacturerUI obj = (ManufacturerUI) other;
        if (manufacturer != null)
            return manufacturer.equals(obj.manufacturer);
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Manufacturer: ");
        sb.append(getName());
        return sb.toString();
    }
}
