package org.openepics.cable.ui;

public interface ManufacturerColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The name of the first manufacturer.";
    public static final String NAME_STYLECLASS = null;
    public static final String NAME_STYLE = "width:10%; min-width:120px";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width:10%; min-width:120px";

    public static final String ADDRESS_VALUE = "address";
    public static final String ADDRESS_TOOLTIP = "The address of the manufacturer.";
    public static final String ADDRESS_STYLECLASS = null;
    public static final String ADDRESS_STYLE = "width:20%; min-width:160px";
    public static final String ADDRESS_FILTERMODE = "contains";
    public static final String ADDRESS_FILTERSTYLE = "width:20%; min-width:160px";

    public static final String PHONE_VALUE = "phoneNumber";
    public static final String PHONE_TOOLTIP = "The contact phone number for the manufacturer.";
    public static final String PHONE_STYLECLASS = null;
    public static final String PHONE_STYLE = "width:15%; min-width:120px";
    public static final String PHONE_FILTERMODE = "contains";
    public static final String PHONE_FILTERSTYLE = "width:15%; min-width:120px";

    public static final String EMAIL_VALUE = "email";
    public static final String EMAIL_TOOLTIP = "The contact email address for the manufacturer.";
    public static final String EMAIL_STYLECLASS = null;
    public static final String EMAIL_STYLE = "width:20%; min-width:160px";
    public static final String EMAIL_FILTERMODE = "contains";
    public static final String EMAIL_FILTERSTYLE = "width:20%; min-width:160px";

    public static final String COUNTRY_VALUE = "country";
    public static final String COUNTRY_TOOLTIP = "Country of the manufacturer";
    public static final String COUNTRY_STYLECLASS = null;
    public static final String COUNTRY_STYLE = "width:10%; min-width:80px";
    public static final String COUNTRY_FILTERMODE = "contains";
    public static final String COUNTRY_FILTERSTYLE = "width:10%; min-width:80px";

    public static final String STATUS_VALUES = "statusValues";
}
