package org.openepics.cable.ui;

/**
 * Cable column style class.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableColumnStyle {

    /**
     * Enums representing possible styles.
     */
    public enum CableColumnStyleEnum {
        /** Field is in plain text. */
        PLAIN_TEXT,
        /** Long text in field is cropped and ellipsis are added. */
        ELLIPSIS,
        /** Field contains routings. */
        ROUTING,
        /** Field has an additional dialog. */
        DIALOG,
        /** Field is a short link. */
        SHORTLINK,
        /** Field is a download link. */
        DOWNLOAD,
        /** Field is a url. */
        URL,
        /** Field is custom made. */
        CUSTOM
    }

    private CableColumnStyleEnum styleEnum;

    /**
     * Instantiates a new Cable column style.
     * 
     * @param styleEnum
     *            the style.
     */
    public CableColumnStyle(CableColumnStyleEnum styleEnum) {
        this.styleEnum = styleEnum;
    }

    /** @return true if the style is PLAIN_TEXT. */
    public boolean isPlainText() {
        return this.styleEnum == CableColumnStyleEnum.PLAIN_TEXT;
    }

    /** @return true if the style is ELLIPSIS. */
    public boolean isEllipsis() {
        return this.styleEnum == CableColumnStyleEnum.ELLIPSIS;
    }

    /** @return true if the style is ROUTING. */
    public boolean isRouting() {
        return this.styleEnum == CableColumnStyleEnum.ROUTING;
    }

    /** @return true if the style is DIALOG. */
    public boolean isDialog() {
        return this.styleEnum == CableColumnStyleEnum.DIALOG;
    }

    /** @return true if the style is SHORTLINK. */
    public boolean isShortLink() {
        return this.styleEnum == CableColumnStyleEnum.SHORTLINK;
    }

    /** @return true if the style is DOWNLOAD. */
    public boolean isDownload() {
        return this.styleEnum == CableColumnStyleEnum.DOWNLOAD;
    }

    /** @return true if the style is URL. */
    public boolean isUrl() {
        return this.styleEnum == CableColumnStyleEnum.URL;
    }

    public boolean isCustom() {
        return this.styleEnum == CableColumnStyleEnum.CUSTOM;
    }

}
