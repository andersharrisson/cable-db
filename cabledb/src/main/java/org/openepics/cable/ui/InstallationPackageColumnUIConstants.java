/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * InstallationPackage UI constants.
 */
public final class InstallationPackageColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The installation package name";
    public static final String NAME_STYLECLASS = "data_table_header fixed_width108";
    public static final String NAME_STYLE = "width:10%;";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 100%;";

    public static final String MODIFIED_VALUE = "modified";
    public static final String MODIFIED_TOOLTIP = "Date when installation package was modified";
    public static final String MODIFIED_STYLECLASS = "data_table_header fixed_width160";
    public static final String MODIFIED_STYLE = null;
    public static final String MODIFIED_FILTERMODE = "contains";
    public static final String MODIFIED_FILTERSTYLE = "width: 100%;";

    public static final String DESCRIPTION_VALUE = "description";
    public static final String DESCRIPTION_TOOLTIP = "Description of the installation package";
    public static final String DESCRIPTION_STYLECLASS = "data_table_header fixed_width160";
    public static final String DESCRIPTION_STYLE = "width:10%;";
    public static final String DESCRIPTION_FILTERMODE = "contains";
    public static final String DESCRIPTION_FILTERSTYLE = "width: 100%;";

    public static final String LOCATION_VALUE = "location";
    public static final String LOCATION_TOOLTIP = "Location of the installation package";
    public static final String LOCATION_STYLECLASS = "data_table_header fixed_width160";
    public static final String LOCATION_STYLE = "width:10%;";
    public static final String LOCATION_FILTERMODE = "contains";
    public static final String LOCATION_FILTERSTYLE = "width: 100%;";

    public static final String CABLE_COORDINATOR_VALUE = "cableCoordinator";
    public static final String CABLE_COORDINATOR_TOOLTIP =
            "The user that is cable coordinator for this installation package,"
            + " usually the same person that is performing the upload";
    public static final String CABLE_COORDINATOR_STYLECLASS = "data_table_header fixed_width140";
    public static final String CABLE_COORDINATOR_STYLE = "width:10%;";
    public static final String CABLE_COORDINATOR_FILTERMODE = "contains";
    public static final String CABLE_COORDINATOR_FILTERSTYLE = "width: 100%;";

    public static final String ROUTING_DOCUMENTATION_VALUE = "routingDocumentation";
    public static final String ROUTING_DOCUMENTATION_URL = "rDocUrl";
    public static final String ROUTING_DOCUMENTATION_TOOLTIP = "Routing documentation";
    public static final String ROUTING_DOCUMENTATION_STYLECLASS = "data_table_header fixed_width208";
    public static final String ROUTING_DOCUMENTATION_STYLE = null;
    public static final String ROUTING_DOCUMENTATION_FILTERMODE = "contains";
    public static final String ROUTING_DOCUMENTATION_FILTERSTYLE = "width: 100%;";

    public static final String INSTALLER_CABLE_VALUE = "installerCable";
    public static final String INSTALLER_CABLE_TOOLTIP = "The user that is responsible as installer of cable";
    public static final String INSTALLER_CABLE_STYLECLASS = "data_table_header fixed_width140";
    public static final String INSTALLER_CABLE_STYLE = "width:10%;";
    public static final String INSTALLER_CABLE_FILTERMODE = "contains";
    public static final String INSTALLER_CABLE_FILTERSTYLE = "width: 100%;";

    public static final String INSTALLER_CONNECTOR_A_VALUE = "installerConnectorA";
    public static final String INSTALLER_CONNECTOR_A_TOOLTIP =
            "The user that is responsible as installer of connector A";
    public static final String INSTALLER_CONNECTOR_A_STYLECLASS = "data_table_header fixed_width140";
    public static final String INSTALLER_CONNECTOR_A_STYLE = "width:10%;";
    public static final String INSTALLER_CONNECTOR_A_FILTERMODE = "contains";
    public static final String INSTALLER_CONNECTOR_A_FILTERSTYLE = "width: 100%;";

    public static final String INSTALLER_CONNECTOR_B_VALUE = "installerConnectorB";
    public static final String INSTALLER_CONNECTOR_B_TOOLTIP =
            "The user that is responsible as installer of connector B";
    public static final String INSTALLER_CONNECTOR_B_STYLECLASS = "data_table_header fixed_width140";
    public static final String INSTALLER_CONNECTOR_B_STYLE = "width:10%;";
    public static final String INSTALLER_CONNECTOR_B_FILTERMODE = "contains";
    public static final String INSTALLER_CONNECTOR_B_FILTERSTYLE = "width: 100%;";

    public static final String INSTALLATION_PACKAGE_LEADER_VALUE = "installationPackageLeader";
    public static final String INSTALLATION_PACKAGE_LEADER_TOOLTIP =
            "The user that is responsible as installation package leader";
    public static final String INSTALLATION_PACKAGE_LEADER_STYLECLASS = "data_table_header fixed_width140";
    public static final String INSTALLATION_PACKAGE_LEADER_STYLE = "width:10%;";
    public static final String INSTALLATION_PACKAGE_LEADER_FILTERMODE = "contains";
    public static final String INSTALLATION_PACKAGE_LEADER_FILTERSTYLE = "width: 100%;";

    public static final String NUMBER_OF_CABLES_VALUE = "numberOfCables";
    public static final String NUMBER_OF_CABLES_TOOLTIP = "Number of Cables";
    public static final String NUMBER_OF_CABLES_STYLECLASS = "data_table_header fixed_width80";
    public static final String NUMBER_OF_CABLES_STYLE = "text-align:center";
    public static final String NUMBER_OF_CABLES_FILTERMODE = "exact";
    public static final String NUMBER_OF_CABLES_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "valid";
    public static final String STATUS_TOOLTIP = "The status of the installation package";
    public static final String STATUS_STYLECLASS = "data_table_header fixed_width92";
    public static final String STATUS_STYLE = "width:10%;";
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = "width: 100%;";

    /**
     * This class is not to be instantiated.
     */
    private InstallationPackageColumnUIConstants() {
        throw new IllegalStateException("Utility class");
    }

}
