/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.InstallationPackageService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.services.dl.InstallationPackageImportExportService;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.InstallationPackageNumbering;
import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.Utility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the backing requests bean for installation-packages.xhtml.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class InstallationPackageRequestManager implements Serializable {

    private static final long serialVersionUID = 9208792434306606451L;
    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final int MAX_AUTOCOMPLETE = 20;
    private static final Logger LOGGER = Logger.getLogger(InstallationPackageRequestManager.class.getName());
    private static final List<InstallationPackageUI> EMPTY_LIST = new ArrayList<>();

    private List<InstallationPackageColumnUI> columns;
    private List<String> columnTemplate = InstallationPackageColumnUI.getAllColumns();

    @Inject
    private transient InstallationPackageService installationPackageService;
    @Inject
    private transient InstallationPackageImportExportService installationPackageImportExportService;

    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;

    private List<InstallationPackageUI> installationPackages;
    private List<InstallationPackageUI> filteredInstallationPackages;
    private List<InstallationPackageUI> selectedInstallationPackages = EMPTY_LIST;
    private List<InstallationPackageUI> deletedInstallationPackages;

    private String globalFilter;

    private InstallationPackageUI selectedInstallationPackage;
    private boolean isAddPopupOpened;

    // for overlay panels
    private String displayDescription;
    private String displayService;
    private String displayComments;

    private boolean isInstallationPackageRequested;
    private String requestedInstallationPackageName;

    private byte[] fileToBeImported;
    private LoaderResult<InstallationPackage> importResult;
    private String importFileName;
    private int installationPackagesToExportSize = 0;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;
    private String longOverlayURL;

    private InstallationPackage oldInstallationPackage;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public InstallationPackageRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns + history column
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = InstallationPackageColumnUI.values().length + 1;
        columnVisibility = ManagerUtil.setAllColumnVisible(numberOfColumns);
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display. */
    @PostConstruct
    public void init() {
        try {
            isAddPopupOpened = false;
            clearImportState();
            selectedInstallationPackages.clear();
            selectedInstallationPackage = null;
            requestedInstallationPackageName =
                    ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                    .getParameter("installationPackageName");
            createDynamicColumns();
            refreshInstallationPackages();

            // prepare datatable
            //     cookies
            //         rows per page
            //         column visibility
            //     row number (in all rows) for requested entry (if any)
            Cookie[] cookies = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                            initPaginationPageSize(cookie.getValue());
                            break;
                        case CookieUtility.CD_INSTALLATION_PACKAGE_COLUMN_VISIBILITY:
                            initColumnVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }
            rowNumber = rowNumber(requestedInstallationPackageName);
        } catch (Exception e) {
            throw new UIException("Installation package display initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        ManagerUtil.initColumnVisibility(visibility, numberOfColumns, columnVisibility);
    }

    /**
     * Find out row number, if applicable, for selected entry in list of all entries.
     *
     * @param installationPackageName
     * @return
     */
    private int rowNumber(String installationPackageName) {
        // note
        //     currently not consider filter

        InstallationPackageUI installationPackageToSelect = null;
        this.isInstallationPackageRequested = false;
        if (requestedInstallationPackageName != null) {
            isInstallationPackageRequested = true;
            installationPackageToSelect =
                    getInstallationPackageFromInstallationPackageName(requestedInstallationPackageName);
        }

        selectedInstallationPackages.clear();
        if (installationPackageToSelect != null) {
            selectedInstallationPackage = installationPackageToSelect;
            selectedInstallationPackages.add(selectedInstallationPackage);

            int elementPosition = 0;
            for (InstallationPackageUI installationPackage : installationPackages) {
                if (installationPackage.getName().equals(requestedInstallationPackageName)) {
                    return elementPosition;
                }
                elementPosition++;
            }
        }

        return 0;
    }

    private InstallationPackageUI getInstallationPackageFromInstallationPackageName(
            final String installationPackageName) {

        if (installationPackageName == null || installationPackageName.isEmpty()) {
            return null;
        }

        InstallationPackageUI installationPackageToSelect = null;
        for (InstallationPackageUI installationPackage : installationPackages) {
            if (installationPackage.getName().equals(installationPackageName)) {
                installationPackageToSelect = installationPackage;
            }
        }
        return installationPackageToSelect;
    }

    /** Refreshes installation packages list. */
    private void refreshInstallationPackages() {
        installationPackages = buildInstallationPackageUIs();
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /**
     * Returns the installation packages to be exported,
     * which are the currently filtered and selected installation packages,
     * or all filtered cables without deleted ones if none selected.
     *
     * @return the installation packages to be exported
     */
    public List<InstallationPackageUI> getInstallationPackagesToExport() {

        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        final List<InstallationPackageUI> installationPackagesToExport = new ArrayList<>();
        for (InstallationPackageUI installationPackage : getInstallationPackages()) {
            if (isIncludedByFilter(installationPackage) && installationPackage.isActive())
                installationPackagesToExport.add(installationPackage);
        }
        LOGGER.fine("Returning installation packages to export: " + installationPackagesToExport.size());
        return installationPackagesToExport;
    }

    private boolean isIncludedByFilter(InstallationPackageUI installationPackage) {
        return !filteredInstallationPackagesExist() || getFilteredInstallationPackages().contains(installationPackage);
    }

    private boolean filteredInstallationPackagesExist() {
        return getFilteredInstallationPackages() != null && !getFilteredInstallationPackages().isEmpty();
    }

    /** @return the result of a test or true import */
    public LoaderResult<InstallationPackage> getImportResult() {
        return importResult;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear import state.");
        fileToBeImported = null;
        importFileName = null;
        importResult = null;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void installationPackageFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked installation package file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            importFileName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the installation package import from the file that was last uploaded.
     */
    public void installationPackageImportTest() {
        LOGGER.fine("Invoked installation package import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = installationPackageImportExportService.importInstallationPackages(inputStream, true);
                LOGGER.fine("Import test result: " + importResult.toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the installation package import from the file that was last uploaded.
     */
    public void installationPackageImport() {
        LOGGER.fine("Invoked installation package import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = installationPackageImportExportService.importInstallationPackages(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshInstallationPackages();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /** @return the installation package sheet with installation packages that were just imported */
    public StreamedContent getInstallationPackageSheetWithImportedInstallationPackages() {

        // submit back only non-deleted installation packages
        final List<InstallationPackage> createdOrUpdatedInstallationPackages = new ArrayList<>();
        for (InstallationPackage installationPackage : importResult.getAffected()) {
            if (installationPackage.isActive())
                createdOrUpdatedInstallationPackages.add(installationPackage);
        }
        return new DefaultStreamedContent(
                installationPackageImportExportService.exportInstallationPackages(createdOrUpdatedInstallationPackages),
                Utility.XLSX_CONTENT_TYPE,
                importFileName);
    }

    /** @return the installation package sheet with the installation packages from installationPackagesToExport list */
    public StreamedContent getInstallationPackageSheetWithInstallationPackagesToExport() {

        final List<InstallationPackage> installationPackagesToExport = new ArrayList<>();
        for (InstallationPackageUI installationPackageUI : getInstallationPackagesToExport()) {
            installationPackagesToExport.add(installationPackageUI.getInstallationPackage());
        }
        return new DefaultStreamedContent(
                installationPackageImportExportService.exportInstallationPackages(installationPackagesToExport),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_installation_packages.xlsx");
    }

    /** @return the installation package import template */
    public StreamedContent getImportTemplate() {
        LOGGER.fine("Get installation packages template");
        return new DefaultStreamedContent(
                installationPackageImportExportService.exportInstallationPackages(new ArrayList<>()),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_installation_packages.xlsx");
    }

    /** @return current installation package list. */
    private List<InstallationPackageUI> buildInstallationPackageUIs() {

        final List<InstallationPackageUI> installationPackageUIs = new ArrayList<InstallationPackageUI>();
        List<InstallationPackage> installationPackages = installationPackageService.getInstallationPackages();

        selectedInstallationPackage = null;
        selectedInstallationPackages = EMPTY_LIST;

        for (InstallationPackage installationPackage : installationPackages) {
            final InstallationPackageUI installationPackageUI = new InstallationPackageUI(installationPackage);
            installationPackageUIs.add(installationPackageUI);
        }
        return installationPackageUIs;
    }

    /** @return true if edit installation package button is enabled, otherwise false. */
    public boolean isEditButtonEnabled() {
        return sessionService.isLoggedIn()
                && selectedInstallationPackage != null
                && selectedInstallationPackage.isActive()
                && sessionService.canAdminister();
    }

    /** @return true if delete installation package button is enabled, otherwise false. */
    public boolean isDeleteButtonEnabled() {
        if (selectedInstallationPackages == null || selectedInstallationPackages.isEmpty()) {
            return false;
        }
        for (InstallationPackageUI installationPackageToDelete : selectedInstallationPackages) {
            if (!installationPackageToDelete.isActive()) {
                return false;
            }
        }
        return sessionService.canAdminister();
    }

    public String getRequestedInstallationPackageName() {
        return requestedInstallationPackageName;
    }

    public void setRequestedInstallationPackageName(String requestedInstallationPackageName) {
        this.requestedInstallationPackageName = requestedInstallationPackageName;
    }

    public boolean isInstallationPackageRequested() {
        return isInstallationPackageRequested;
    }

    /** @return the installation packages to be displayed */
    public List<InstallationPackageUI> getInstallationPackages() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return installationPackages;
    }

    /** @return the filtered installation packages to be displayed */
    public List<InstallationPackageUI> getFilteredInstallationPackages() {
        return filteredInstallationPackages;
    }

    /**
     * @param filteredInstallationPackages
     *            the filtered installation packages to set
     */
    public void setFilteredInstallationPackages(List<InstallationPackageUI> filteredInstallationPackages) {
        this.filteredInstallationPackages = filteredInstallationPackages;
        LOGGER.fine("Setting filtered installation packages: "
                + (filteredInstallationPackages != null ? filteredInstallationPackages.size() : "no")
                + " installation packages.");
    }

    /** @return the global text filter */
    public String getGlobalFilter() {
        return globalFilter;
    }

    /**
     * @param globalFilter
     *            the global text filter to set
     */
    public void setGlobalFilter(String globalFilter) {
        this.globalFilter = globalFilter;
        LOGGER.fine("Setting global filter: " + this.globalFilter);
    }

    /** Refreshes installation packages list based on filters. */
    public void filterInstallationPackages() {
        LOGGER.fine("Invoked filter installation packages.");
        refreshInstallationPackages();
    }

    /** @return the currently selected installation packages, cannot return null */
    public List<InstallationPackageUI> getSelectedInstallationPackages() {
        return selectedInstallationPackages;
    }

    /**
     * @param selectedInstallationPackages
     *            the installation packages to select
     */
    public void setSelectedInstallationPackages(List<InstallationPackageUI> selectedInstallationPackages) {
        this.selectedInstallationPackages =
                selectedInstallationPackages != null ? selectedInstallationPackages : EMPTY_LIST;
        LOGGER.fine("Setting selected installation packages: " + this.selectedInstallationPackages.size());
    }

    /** Clears the current installation package selection. */
    public void clearSelectedInstallationPackages() {
        LOGGER.fine("Invoked clear installation package  selection.");
        setSelectedInstallationPackages(null);
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                + CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE
                + "', " + getRows()
                + ", {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);

            String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                    + CookieUtility.CD_INSTALLATION_PACKAGE_COLUMN_VISIBILITY
                    + "', '" + columnVisibility2String()
                    + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
            PrimeFaces.current().executeScript(statement);
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index for column in UI, counting from left
     * @return true if column is marked as visible
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Returns column visibility for given column.
     *
     * @param column given column
     * @return column visibility
     */
    public boolean isColumnVisible(InstallationPackageColumnUI column) {
        int colIndex = 0;
        for(int i=0; i<numberOfColumns; i++) {
            if(InstallationPackageColumnUI.values()[i].equals(column)) {
                colIndex = i;
                break;
            }
        }
        return columnVisibility.get(colIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected installation packages and row selection.
     */
    public void unselectAllRows() {
        clearSelectedInstallationPackages();

        onRowSelect();
    }

    /**
     * Event triggered when row is selected in table in UI.
     */
    public void onRowSelect() {
        if (selectedInstallationPackages != null && !selectedInstallationPackages.isEmpty()) {
            if (selectedInstallationPackages.size() == 1) {
                selectedInstallationPackage = selectedInstallationPackages.get(0);
            } else {
                selectedInstallationPackage = null;
            }
        } else {
            selectedInstallationPackage = null;
        }
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    /**
     * Prepare for Add installation package dialog.
     */
    public void prepareAddPopup() {
        selectedInstallationPackage = new InstallationPackageUI();
        isAddPopupOpened = true;
    }

    /**
     * Reset values. May be used at e.g. close of delete dialog.
     */
    public void resetValues() {
        clearSelectedInstallationPackages();
        deletedInstallationPackages = null;
    }

    /**
     * Event triggered when installation package is created.
     */
    public void onInstallationPackageAdd() {
        final String name = selectedInstallationPackage.getName();
        formatInstallationPackage(selectedInstallationPackage);
        installationPackageService.createInstallationPackage(
                selectedInstallationPackage.getInstallationPackage(), sessionService.getLoggedInName());
        filterInstallationPackages();
        UiUtility.showInfoMessage("Installation package  '" + name + "' added.");
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * Prepare for Edit installation package dialog.
     */
    public void prepareEditPopup() {
        Preconditions.checkNotNull(selectedInstallationPackage);
        oldInstallationPackage = selectedInstallationPackage.getInstallationPackage();
        installationPackageService.detachInstallationPackage(oldInstallationPackage);
        selectedInstallationPackage =
                new InstallationPackageUI(
                        installationPackageService.getInstallationPackageById(selectedInstallationPackage.getId()));
        isAddPopupOpened = false;
    }

    /**
     * Event triggered when installation package is updated.
     */
    public void onInstallationPackageEdit() {
        Preconditions.checkNotNull(selectedInstallationPackage);
        formatInstallationPackage(selectedInstallationPackage);
        final InstallationPackage newInstallationPackage = selectedInstallationPackage.getInstallationPackage();
        final boolean updated = installationPackageService.updateInstallationPackage(
                newInstallationPackage, oldInstallationPackage, sessionService.getLoggedInName());
        filterInstallationPackages();
        final String name = newInstallationPackage.getName();
        UiUtility.showInfoMessage("Installation package  '" + name + (updated ? "' updated." : "' not updated."));
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * @return the list of deleted installation packages.
     */
    public List<InstallationPackageUI> getDeletedInstallationPackages() {
        return deletedInstallationPackages;
    }

    /**
     * The method builds a list of installation packages that are already deleted.
     * If the list is not empty, it is displayed to the user and the user is prevented from deleting them.
     */
    public void checkInstallationPackagesForDeletion() {
        Preconditions.checkNotNull(selectedInstallationPackages);
        Preconditions.checkState(!selectedInstallationPackages.isEmpty());

        deletedInstallationPackages = Lists.newArrayList();
        for (final InstallationPackageUI installationPackageToDelete : selectedInstallationPackages) {
            if (!installationPackageToDelete.isActive()) {
                deletedInstallationPackages.add(installationPackageToDelete);
            }
        }
    }

    /**
     * Event triggered when installation package is deleted.
     */
    public void onInstallationPackageDelete() {
        Preconditions.checkNotNull(deletedInstallationPackages);
        Preconditions.checkState(deletedInstallationPackages.isEmpty());
        Preconditions.checkNotNull(selectedInstallationPackages);
        Preconditions.checkState(!selectedInstallationPackages.isEmpty());
        int deletedInstallationPackagesCounter = 0;
        for (final InstallationPackageUI installationPackageToDelete : selectedInstallationPackages) {
            if (installationPackageService.deleteInstallationPackage(installationPackageToDelete.getInstallationPackage(), sessionService.getLoggedInName())) {
                deletedInstallationPackagesCounter++;
            }
        }
        clearSelectedInstallationPackages();
        filteredInstallationPackages = null;
        deletedInstallationPackages = null;
        filterInstallationPackages();
        UiUtility.showInfoMessage("Deleted " + deletedInstallationPackagesCounter + (deletedInstallationPackagesCounter == 1 ? " installation package." : " installation packages."));
    }

    /**
     * Validate installation package name.
     * Throws ValidatorException if installation package with given name already exists.
     *
     * @param ctx faces context
     * @param component ui component
     * @param value installation package name
     */
    public void isInstallationPackageNameValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }

        boolean adding = selectedInstallationPackage.getId() == null;
        InstallationPackage installationPackage = installationPackageService.getInstallationPackageByName(stringValue);
        boolean alreadyExists = installationPackage != null
                && !installationPackage.getId().equals(selectedInstallationPackage.getId());
        boolean deleted = installationPackage != null && !installationPackage.isActive();

        if ((adding && alreadyExists && !deleted) || (!adding && alreadyExists)) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Installation package  with this name already exists.", null));
        }
    }

    /**
     * Validates if entered value is Integer number.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not Integer number
     */
    public void isIntegerEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isIntegerEntered(value);
    }

    /**
     * Validates if entered value is Float number.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not Double number
     */
    public void isFloatEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isFloatEntered(value);
    }

    /**
     * Validates if entered value is URL.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not URL
     */
    public void isURLEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isURLEntered(value);
    }

    /** @return the selected installation package */
    public InstallationPackageUI getSelectedInstallationPackage() {
        return selectedInstallationPackage;
    }

    /** @return true if the current user can edit installation package, else false */
    public boolean getEditInstallationPackage() {
        return sessionService.canAdminister();
    }

    /** @return description string for display. */
    public String getDisplayDescription() {
        return displayDescription;
    }

    /**
     * Sets description string.
     *
     * @param displayDescription
     *            string
     */
    public void setDisplayDescription(String displayDescription) {
        this.displayDescription = displayDescription;
    }

    /** Formats description string. */
    public void handleDisplayDescription() {
        displayDescription = formatString(displayDescription);
    }

    /** @return service string for display. */
    public String getDisplayService() {
        return displayService;
    }

    /**
     * Sets service string.
     *
     * @param displayService
     *            service string
     */
    public void setDisplayService(String displayService) {
        this.displayService = displayService;
    }

    /** Formats service string. */
    public void handleDisplayService() {
        displayService = formatString(displayService);
    }

    /** @return comments string for display. */
    public String getDisplayComments() {
        return displayComments;
    }

    /**
     * Sets comments string.
     *
     * @param displayComments
     *            comments string.
     */
    public void setDisplayComments(String displayComments) {
        this.displayComments = displayComments;
    }

    /** Formats comments string. */
    public void handleDisplayComments() {
        displayComments = formatString(displayComments);
    }

    /**
     * Breaks string if it is too long.
     *
     * @param string
     *            string
     *
     * @return new string.
     */
    private String formatString(String string) {
        StringBuilder sb = new StringBuilder(string);
        int i = 0;
        while ((i = sb.indexOf(" ", i + 37)) != -1) {
            sb.replace(i, i + 1, "\n");
        }
        return sb.toString();
    }

    /** Resets column template to default. */
    private void resetColumnTemplate() {
        columnTemplate = InstallationPackageColumnUI.getAllColumns();
    }

    /** Resets displayView to default. */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in installation package data table
     *
     * @return columns
     */
    public List<InstallationPackageColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<InstallationPackageColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();
            InstallationPackageColumnUI column = InstallationPackageColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(":installationPackageTableForm:installationPackageTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /** @return number of column in current display view */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    /**
     * Formats the given installation package by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedInstallationPackage
     *            installation package to format
     */
    private void formatInstallationPackage(InstallationPackageUI selectedInstallationPackage) {
        selectedInstallationPackage.setDescription(
                Utility.formatWhitespace(selectedInstallationPackage.getDescription()));
        selectedInstallationPackage.setName(Utility.formatWhitespace(selectedInstallationPackage.getName()));
        //  no add/edit of revision, thus no format of revision
    }

    /**
     * Return counter for number of installation packages to be exported.
     *
     * @return number of installation packages to be exported
     */
    public int getInstallationPackagesToExportSize() {
        return installationPackagesToExportSize;
    }

    /**
     * Update counter for number of installation packages to be exported.
     */
    public void updateInstallationPackagesToExportSize() {
        this.installationPackagesToExportSize = getInstallationPackagesToExport().size();
    }

    /** @return true if the current user can import installation packages, else false */
    public boolean canImportInstallationPackages() {
        return installationPackageImportExportService.canImportInstallationPackages();
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Sets the URL for the long general popup.
     *
     * @param longOverlayURL
     *            the url to where the popup is pointing.
     */
    public void setLongOverlayURL(String longOverlayURL) {
        this.longOverlayURL = longOverlayURL;
    }

    /** @return URL for the long general popup. */
    public String getLongOverlayURL() {
        return this.longOverlayURL;
    }

    /** Clears the newly created selected installation package so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedInstallationPackage = null;
        }
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Provide suggestions for installation package given query to filter for.
     *
     * @param query query to filter for
     * @return suggestions for installation package given query to filter for.
     */
    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn()) {
            return Collections.emptyList();
        }

        final List<String> selectItems = new ArrayList<>();
        FacesContext context = FacesContext.getCurrentInstance();
        InstallationPackageColumnUI column =
                (InstallationPackageColumnUI) UIComponent.getCurrentComponent(context).getAttributes().get("column");

        if (InstallationPackageColumnUI.STATUS.equals(column)) {
            selectItems.add(InstallationPackageUI.STATUS_OBSOLETE);
            selectItems.add(InstallationPackageUI.STATUS_VALID);
        }
        return selectItems;
    }

    /**
     * Return tooltip for installation package column.
     *
     * @param column installation package column
     * @return tooltip for installation package column
     */
    public String tooltipForInstallationPackage(InstallationPackageColumnUI column) {
        String result;
        switch (column) {
            case DESCRIPTION:
                result = "Open Description dialog";
                break;
            case LOCATION:
                result = "Open Location dialog";
                break;
            default:
                result = "Open "+ column.getValue() + " dialog";
        }
        return result;
    }

    /**
     * Return tooltip for installation package link.
     *
     * @param label label
     * @param column column
     * @param value (part of) tooltip
     * @return tooltip for installation package link
     */
    public String tooltipForLink(String label, String column, String value) {
        switch (column) {
            case InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_VALUE:
                return "Click to open navigation dialog to " + value;
            default:
                return null;
        }
    }

    /**
     * @return available installers.
     */
    public String[] getInstallers() {
        return InstallationPackageNumbering.getInstallerLabels();
    }

    /**
     * Autocomplete method for users for cable coordinator.
     *
     * @param query query to filter users
     * @return users
     */
    public List<String> completeUsersCableCoordinator(String query) {
        return completeUsers(query, InstallationPackageColumnUI.CABLE_COORDINATOR);
    }

    /**
     * Autocomplete method for users for installation package leader.
     *
     * @param query query to filter users
     * @return users
     */
    public List<String> completeUsersInstallationPackageLeader(String query) {
        return completeUsers(query, InstallationPackageColumnUI.INSTALLATION_PACKAGE_LEADER);
    }

    /**
     * Autocomplete method for users for InstallationPackageColumnUI column.
     *
     * @param query query to filter users
     * @param column intended column (InstallationPackageColumnUI)
     * @return users
     */
    public List<String> completeUsers(String query, InstallationPackageColumnUI column) {
        List<String> stringList = null;
        switch (column) {
        case CABLE_COORDINATOR:
            stringList = selectedInstallationPackage.getCableCoordinators();
            break;
        case INSTALLATION_PACKAGE_LEADER:
            stringList = selectedInstallationPackage.getInstallationPackageLeaders();
            break;
        default:
            return Collections.emptyList();
        }

        List<String> users = new ArrayList<>(userDirectoryServiceFacade.getAllUsernames());
        List<String> results = new ArrayList<String>();

        for (String user : users) {
            String name = user;
            if (name.toLowerCase().contains(query.toLowerCase())
                    && !stringList.contains(name)) {
                results.add(name);
            }
            if (results.size() >= MAX_AUTOCOMPLETE) {
                break;
            }
        }
        return results;
    }

}
