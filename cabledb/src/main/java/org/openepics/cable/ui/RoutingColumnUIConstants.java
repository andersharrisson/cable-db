package org.openepics.cable.ui;

public interface RoutingColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The routing name.";
    public static final String NAME_STYLECLASS = "fixed_width108";
    public static final String NAME_STYLE = "text-align:center";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 108px;";

    public static final String MODIFIED_VALUE = "modified";
    public static final String MODIFIED_TOOLTIP = "Date when routing was modified.";
    public static final String MODIFIED_STYLECLASS = "fixed_width160";
    public static final String MODIFIED_STYLE = null;
    public static final String MODIFIED_FILTERMODE = "contains";
    public static final String MODIFIED_FILTERSTYLE = "width: 160px;";

    public static final String DESCRIPTION_VALUE = "description";
    public static final String DESCRIPTION_TOOLTIP = "description of the routing.";
    public static final String DESCRIPTION_STYLECLASS = "fixed_width160";
    public static final String DESCRIPTION_STYLE = null;
    public static final String DESCRIPTION_FILTERMODE = "contains";
    public static final String DESCRIPTION_FILTERSTYLE = "width: 160px;";

    public static final String CLASSES_VALUE = "cableClasses";
    public static final String CLASSES_TOOLTIP = "Cable classes";
    public static final String CLASSES_STYLECLASS = "fixed_width48";
    public static final String CLASSES_STYLE = "text-align:center";
    public static final String CLASSES_FILTERMODE = "contains";
    public static final String CLASSES_FILTERSTYLE = "width: 48px;";

    public static final String LOCATION_VALUE = "location";
    public static final String LOCATION_TOOLTIP = "Location of the routing.";
    public static final String LOCATION_STYLECLASS = "fixed_width160";
    public static final String LOCATION_STYLE = null;
    public static final String LOCATION_FILTERMODE = "contains";
    public static final String LOCATION_FILTERSTYLE = "width: 160px;";

    public static final String LENGTH_VALUE = "length";
    public static final String LENGTH_TOOLTIP = "The actual length in meters.";
    public static final String LENGTH_STYLECLASS = "fixed_width92";
    public static final String LENGTH_STYLE = null;
    public static final String LENGTH_FILTERMODE = "contains";
    public static final String LENGTH_FILTERSTYLE = "width: 92px;";

    public static final String OWNER_VALUE = "owner";
    public static final String OWNER_TOOLTIP = "The user that is responsible for this routing,"
            + " usually the same person that is performing the upload.";
    public static final String OWNER_STYLECLASS = "fixed_width140";
    public static final String OWNER_STYLE = null;
    public static final String OWNER_FILTERMODE = "contains";
    public static final String OWNER_FILTERSTYLE = "width: 140px;";

    public static final String STATUS_VALUE = "description";
    public static final String STATUS_TOOLTIP = "Routing description";
    public static final String STATUS_STYLECLASS = "fixed_width108";
    public static final String STATUS_STYLE = null;
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = null;

}
