package org.openepics.cable.ui.lazymodels;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.History;
import org.openepics.cable.services.HistoryService;
import org.openepics.cable.ui.HistoryRequestManager;
import org.openepics.cable.ui.HistoryUI;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 * History Lazy loading model implementing all necessary methods for lazy loading on history data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class HistoryLazyModel extends LazyDataModel<HistoryUI> {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(HistoryLazyModel.class.getCanonicalName());

    private final HistoryService historyService;
    private boolean empty = true;
    private String sortField;
    private SortOrder sortOrder;
    private Map<String, Object> filters;
    private HistoryRequestManager requestManager;

    public HistoryLazyModel(HistoryService historyService, HistoryRequestManager manager) {
        this.historyService = historyService;
        sortField = "";
        sortOrder = SortOrder.ASCENDING;
        filters = Collections.<String, Object> emptyMap();
        this.requestManager = manager;
    }

    /**
     * Loads a section of data from database
     * 
     * @param first
     *            index of first row
     * @param pageSize
     *            amount of rows do load
     * @param sortField
     *            field by which data should be sorted
     * @param sortOrder
     *            order by which data should be sorted
     * @param filters
     *            filters for data
     * @return section of data sorted and filtered as given in parameters, and additionally filtered by custom query.
     */
    @Override
    public List<HistoryUI> load(int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters) {
        LOGGER.log(Level.FINEST, "--> Getting history from database");
        LOGGER.log(Level.FINEST, "---->pageSize: " + pageSize);
        LOGGER.log(Level.FINEST, "---->first: " + first);

        for (final String filterKey : filters.keySet()) {
            LOGGER.log(Level.FINER, "filter[" + filterKey + "] = " + filters.get(filterKey).toString());
        }

        final Long entityId = requestManager.getEntityId();
        final EntityType entityType = requestManager.getEntityType();
        if (entityId != -1 && entityType != null) {
            filters.put("entityType", entityType);
            filters.put("entityId", entityId);
        }
        LOGGER.log(Level.FINEST, "---->Getting history for entityType: " + entityType);
        LOGGER.log(Level.FINEST, "---->Getting history for entityId: " + entityId);
        setLatestLoadData(sortField, sortOrder, filters);

        final List<History> results = historyService.findLazy(first, pageSize, sortField, sortOrder, filters);
        LOGGER.log(Level.FINEST, "------>Recieved " + results.size() + " results.");
        final List<HistoryUI> transformedResults = results == null ? null
                : results.stream().map(HistoryUI::new).collect(Collectors.toList());
        setEmpty(first, transformedResults);
        requestManager.setHistory(transformedResults);
        return transformedResults;
    }

    /**
     * Important parameters of the data load request
     * 
     * @param sortField
     *            name of the sort field
     * @param sortOrder
     *            the sort order
     * @param filters
     *            active filters
     */
    protected void setLatestLoadData(final @Nullable String sortField, final @Nullable SortOrder sortOrder,
            final @Nullable Map<String, Object> filters) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
        this.filters = filters;
    }

    /**
     * The lazy data collection is empty, if it returns no data for the first page.
     * 
     * @param first
     *            the index of the first element to be loaded
     * @param results
     *            the database results
     */
    protected void setEmpty(final int first, final @Nullable List<HistoryUI> results) {
        empty = (first == 0) && ((results == null) || results.isEmpty());
    }

    /** @return <code>true</code> if the current filter returns no data, <code>false</code> otherwise */
    public boolean isEmpty() {
        return empty;
    }

    public String getSortField() {
        return sortField;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public Map<String, Object> getFilters() {
        return filters;
    }

    @Override
    public int getRowCount() {
        final long rowCount = historyService.getRowCount(filters);
        return rowCount > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) rowCount;
    }

    @Override
    public Object getRowKey(HistoryUI object) {
        return object.getEntityId();
    }

    @Override
    public HistoryUI getRowData(String rowKey) {
        final History foundHistory = historyService.getHistoryById(Long.parseLong(rowKey));
        return foundHistory != null ? new HistoryUI(foundHistory) : null;
    }
}
