package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.List;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.services.DateUtil;

public class QueryUI implements Serializable {

    private static final long serialVersionUID = -5599461109679532439L;

    private Query query;

    public QueryUI(Query query) {
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    public Long getId() {
        return query.getId();
    }

    public String getCreated() {
        return DateUtil.format(query.getCreated());
    }

    public String getDescription() {
        return query.getDescription();
    }

    public void setDescription(String description) {
        query.setDescription(description);
    }

    public String getOwner() {
        return query.getOwner();
    }

    public EntityType getEntityType() {
        return query.getEntityType();
    }

    public void setEntityType(EntityType entityType) {
        query.setEntityType(entityType);
    }

    public List<QueryCondition> getQueryConditions() {
        return query.getConditions();
    }

    public void setQueryConditions(List<QueryCondition> conditions) {
        query.setConditions(conditions);
    }
}
