/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.List;

import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.DateUtil;

/**
 * <code>InstallationPackageUI</code> is a presentation of {@link InstallationPackage} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class InstallationPackageUI implements Serializable {

    private static final long serialVersionUID = 8115229002220900451L;

    /** String representing valid installation package status. */
    public static final String STATUS_VALID = "VALID";
    /** String representing obsolete installation package status. */
    public static final String STATUS_OBSOLETE = "OBSOLETE";

    private final InstallationPackage installationPackage;

    /**
     * Constructs InstallationPackage UI object for empty installation package instance.
     */
    public InstallationPackageUI() {
        this.installationPackage = new InstallationPackage();
    }

    /**
     * Constructs InstallationPackage UI object for given installation package instance.
     *
     * @param installationPackage installation package instance
     */
    public InstallationPackageUI(InstallationPackage installationPackage) {
        this.installationPackage = installationPackage;
    }

    /** @return the installation package instance this wraps */
    public InstallationPackage getInstallationPackage() {
        return installationPackage;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Installation Package: ");
        sb.append(installationPackage.getName());
        return sb.toString();
    }

    public Long getId() {
        return installationPackage.getId();
    }

    public String getName() {
        return installationPackage.getName();
    }

    public void setName(String name) {
        installationPackage.setName(name);
    }

    public String getDescription() {
        return installationPackage.getDescription();
    }

    public void setDescription(String description) {
        installationPackage.setDescription(description);
    }

    public String getLocation() {
        return installationPackage.getLocation();
    }

    public void setLocation(String location) {
        installationPackage.setLocation(location);
    }

    public String getCreated() {
        return DateUtil.format(installationPackage.getCreated());
    }

    public String getModified() {
        return DateUtil.format(installationPackage.getModified());
    }

    public String getCableCoordinator() {
        return installationPackage.getCableCoordinator();
    }

    public void setCableCoordinator(String cableCoordinator) {
        installationPackage.setCableCoordinator(cableCoordinator);
    }

    public List<String> getCableCoordinators() {
        return installationPackage.getCableCoordinators();
    }

    public void setCableCoordinators(List<String> cableCoordinators) {
        installationPackage.setCableCoordinators(cableCoordinators);
    }

    public String getRoutingDocumentation() {
        return installationPackage.getRoutingDocumentation();
    }

    public void setRoutingDocumentation(String routingDocumentation) {
        installationPackage.setRoutingDocumentation(routingDocumentation);
    }

    public String getInstallerCable() {
        return installationPackage.getInstallerCable();
    }

    public void setInstallerCable(String installerCable) {
        installationPackage.setInstallerCable(installerCable);
    }

    public String getInstallerConnectorA() {
        return installationPackage.getInstallerConnectorA();
    }

    public void setInstallerConnectorA(String installerConnectorA) {
        installationPackage.setInstallerConnectorA(installerConnectorA);
    }

    public String getInstallerConnectorB() {
        return installationPackage.getInstallerConnectorB();
    }

    public void setInstallerConnectorB(String installerConnectorB) {
        installationPackage.setInstallerConnectorB(installerConnectorB);
    }

    public String getInstallationPackageLeader() {
        return installationPackage.getInstallationPackageLeader();
    }

    public void setInstallationPackageLeader(String installationPackageLeader) {
        installationPackage.setInstallationPackageLeader(installationPackageLeader);
    }

    public List<String> getInstallationPackageLeaders() {
        return installationPackage.getInstallationPackageLeaders();
    }

    public void setInstallationPackageLeaders(List<String> installationPackageLeaders) {
        installationPackage.setInstallationPackageLeaders(installationPackageLeaders);
    }

    public boolean isActive() {
        return installationPackage.isActive();
    }

    /**
     * @return installationPackage.get {@link #STATUS_VALID} if installation package is in active use,
     * {@link #STATUS_OBSOLETE} else
     */
    public String getValid() {
        return installationPackage.isActive() ? STATUS_VALID : STATUS_OBSOLETE;
    }

    public void setActive(boolean status) {
        installationPackage.setActive(status);
    }

    public int getNumberOfCables() {
        return installationPackage.getCables() != null ? installationPackage.getCables().size() : 0;
    }

    @Override
    public int hashCode() {
        return installationPackage.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return installationPackage.equals(((InstallationPackageUI) obj).getInstallationPackage());
    }
}
