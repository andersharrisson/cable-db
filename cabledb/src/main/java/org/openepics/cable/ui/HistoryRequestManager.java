/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.openepics.cable.export.SimpleLogTableExporter;
import org.openepics.cable.export.SimpleTableExporter;
import org.openepics.cable.export.SimpleTableExporterFactory;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.services.HistoryService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.ui.lazymodels.HistoryLazyModel;
import org.openepics.cable.util.Utility;
import org.primefaces.model.LazyDataModel;

/**
 * This is the backing requests bean for history dialogs.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class HistoryRequestManager implements Serializable, SimpleTableExporterFactory {

    private static final long serialVersionUID = -3602673962371076246L;
    private static final Logger LOGGER = Logger.getLogger(HistoryRequestManager.class.getCanonicalName());

    @Inject
    private SessionService sessionService;
    @Inject
    private transient HistoryService historyService;
    private HistoryLazyModel lazyModel;
    private long entityId = -1;
    private EntityType entityType;
    private List<HistoryUI> history;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;

    /** Initializes lazy model. */
    @PostConstruct
    public void init() {
        lazyModel = new HistoryLazyModel(historyService, this);
    }

    public void prepareCableHistory(Long cableId) {
        LOGGER.log(Level.FINEST, "prepareCableHistory: " + cableId);
        entityType = EntityType.CABLE;
        entityId = cableId;
    }

    public void prepareCableTypeHistory(Long cableTypeId) {
        LOGGER.log(Level.FINEST, "prepareCableTypeHistory: " + cableTypeId);
        entityType = EntityType.CABLE_TYPE;
        entityId = cableTypeId;
    }

    public void prepareManufacturerHistory(Long manufacturerId) {
        LOGGER.log(Level.FINEST, "prepareManufacturerHistory: " + manufacturerId);
        entityType = EntityType.MANUFACTURER;
        entityId = manufacturerId;
    }

    public void prepareConnectorHistory(Long connectorId) {
        LOGGER.log(Level.FINEST, "prepareConnectorHistory: " + connectorId);
        entityType = EntityType.CONNECTOR;
        entityId = connectorId;
    }

    public void prepareRoutingHistory(Long routingId) {
        LOGGER.log(Level.FINEST, "prepareRoutingHistory: " + routingId);
        entityType = EntityType.ROUTING;
        entityId = routingId;
    }

    public void clearHistory() {
        LOGGER.log(Level.FINEST, "clearHistory");
        entityType = null;
        entityId = -1;
    }

    /** @return the list of all operation values. */
    public List<SelectItem> getOperationValues() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", "All"));
        for (EntityTypeOperation operation : EntityTypeOperation.values()) {
            selectItems.add(new SelectItem(operation, operation.getDisplayName()));
        }
        return selectItems;
    }

    /** @return the list of all entity types values. */
    public List<SelectItem> getEntityTypeValues() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", "All"));
        for (EntityType type : EntityType.values()) {
            selectItems.add(new SelectItem(type, type.getDisplayName()));
        }
        return selectItems;
    }

    @Override
    public SimpleTableExporter getSimpleTableExporter() {
        return new SimpleLogTableExporter(lazyModel.load(0, Integer.MAX_VALUE, lazyModel.getSortField(),
                lazyModel.getSortOrder(), lazyModel.getFilters()));
    }

    /** @return the lazy loading data model */
    public LazyDataModel<HistoryUI> getLazyModel() {
        return lazyModel;
    }

    public long getEntityId() {
        return entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public List<HistoryUI> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryUI> history) {
        this.history = history;
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }
}
