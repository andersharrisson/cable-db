/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import static org.omnifaces.util.Faces.getContext;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.ExceptionHandler;
import javax.faces.event.ExceptionQueuedEvent;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;
import org.primefaces.context.RequestContext;

/**
 * A global JSF exception handler that displays caught exceptions in the UI as popup messages or redirect to a new page.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CustomExceptionHandler extends FullAjaxExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger(CustomExceptionHandler.class.getName());

    private static final String UNEXPECTED_ERROR = "Unexpected error";

    public CustomExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    public void handle() {
        Iterator<ExceptionQueuedEvent> unhandledExceptionQueuedEvents = getUnhandledExceptionQueuedEvents().iterator();

        if (!unhandledExceptionQueuedEvents.hasNext()) {
            return; // There's no unhandled exception.
        }

        Throwable exception = unhandledExceptionQueuedEvents.next().getContext().getException();

        exception = super.findExceptionRootCause(getContext(), exception);
        if (!(exception instanceof javax.faces.application.ViewExpiredException)) {

            RequestContext.getCurrentInstance().execute("PF('errorDialog').show();");
            LOGGER.log(Level.SEVERE, UNEXPECTED_ERROR, exception);
        } else {
            super.handle();
        }
        // Note: wrapped.handle is called only in super.handle
    }
}
