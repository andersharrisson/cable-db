/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import org.openepics.cable.services.SessionService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * A bean exposing the information about the logged in user to the UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class UserManager {

    @Inject
    private SessionService sessionService;

    /** @return the name of currently logged in user or null if not logged in. */
    public String getLoggedInName() {
        return sessionService.getVisibleName();
    }

    /** @return true if the user is logged in */
    public boolean isLoggedIn() {
        return sessionService.isLoggedIn();
    }

    /** @return true if the user can administer database, else false; if no user logged in, false is returned */
    public boolean canAdminister() {
        return sessionService.canAdminister();
    }

    /**
     * @return true if the user can manage cables that they own, else false; if no user logged in, false is returned
     */
    public boolean canManageOwnedCables() {
        return sessionService.canManageOwnedCables();
    }
}
