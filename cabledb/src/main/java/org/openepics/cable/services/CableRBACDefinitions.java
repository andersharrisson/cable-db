/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

/**
 * This contains RBAC resource and permission definitions.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableRBACDefinitions {

    /** The RBAC resource corresponding to cable database. */
    public static final String CABLE_DB_RESOURCE = "CableDatabase";

    /** The RBAC permission for administer the cable database. */
    public static final String ADMINISTER_CABLE_DB_PERMISSION = "AdministerCableDB";

    /** The RBAC permission for managing owned cables. */
    public static final String MANAGE_OWNED_CABLES_PERMISSION = "ManageOwnedCables";

    /**
     * This class is not to be instantiated.
     */
    private CableRBACDefinitions() {
        throw new IllegalStateException("Utility class");
    }

}
