/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the spreadsheet Installation Package template.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum InstallationPackageColumn implements FieldIntrospection {

    NAME("NAME", "name", "Name", true, false, true),
    DESCRIPTION("DESCRIPTION", "description", "Description", true, false, true),
    LOCATION("LOCATION", "location", "Location", true, false, true),
    CREATED("CREATED", "created", "Created", false, false, false),
    MODIFIED("MODIFIED", "modified", "Modified", false, false, false),
    CABLE_COORDINATOR("CABLE COORDINATOR", "cableCoordinator", "Cable Coordinator", true, false, true),
    ROUTING_DOCUMENTATION("ROUTING DOCUMENTATION", "routingDocumentation", "Routing Documentation", true, false, true),
    INSTALLER_CABLE("INSTALLER CABLE", "installerCable", "Installer Cable", true, false, true),
    INSTALLER_CONNECTOR_A("INSTALLER CONNECTOR A", "installerConnectorA", "Installer Connector A", true, false, true),
    INSTALLER_CONNECTOR_B("INSTALLER CONNECTOR B", "installerConnectorB", "Installer Connector B", true, false, true),
    INSTALLATION_PACKAGE_LEADER(
            "INSTALLATION PACKAGE LEADER", "installationPackageLeader", "Installation Package Leader",
            true, false, true),
    NUMBER_OF_CABLES("NUMBER OF CABLES", "numberOfCables", "Number of Cables", false, false, false),
    STATUS("STATUS", "status", "Status", false, false, true);

    /*
     * isExcelColumn
     *      presence as column/field in Excel sheet
     * isExcelVolatileColumn
     *      presence as column/field in Excel sheet but volatile, i.e. presence not guaranteed
     *      if not considered column in Excel, then value does not matter
     */

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isExcelVolatileColumn;
    private final boolean isStringComparisonOperator;

    private InstallationPackageColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isExcelVolatileColumn, boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isExcelVolatileColumn = isExcelVolatileColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isExcelVolatileColumn() {
        return isExcelVolatileColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Return installation package column given column label.
     *
     * @param columnLabel column label
     * @return installation package column
     */
    public static InstallationPackageColumn convertColumnLabel(String columnLabel) {
        if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return DESCRIPTION;
        } else if (LOCATION.getColumnLabel().equals(columnLabel)) {
            return LOCATION;
        } else if (CREATED.getColumnLabel().equals(columnLabel)) {
            return CREATED;
        } else if (MODIFIED.getColumnLabel().equals(columnLabel)) {
            return MODIFIED;
        } else if (CABLE_COORDINATOR.getColumnLabel().equals(columnLabel)) {
            return CABLE_COORDINATOR;
        } else if (ROUTING_DOCUMENTATION.getColumnLabel().equals(columnLabel)) {
            return ROUTING_DOCUMENTATION;
        } else if (INSTALLER_CABLE.getColumnLabel().equals(columnLabel)) {
            return INSTALLER_CABLE;
        } else if (INSTALLER_CONNECTOR_A.getColumnLabel().equals(columnLabel)) {
            return INSTALLER_CONNECTOR_A;
        } else if (INSTALLER_CONNECTOR_B.getColumnLabel().equals(columnLabel)) {
            return INSTALLER_CONNECTOR_B;
        } else if (INSTALLATION_PACKAGE_LEADER.getColumnLabel().equals(columnLabel)) {
            return INSTALLATION_PACKAGE_LEADER;
        }
        return null;
    }
}
