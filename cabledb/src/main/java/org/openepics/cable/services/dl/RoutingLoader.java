/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.Routing;
import org.openepics.cable.services.RoutingService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceCache;
import org.openepics.cable.util.Utility;

/**
 * <code>RoutingLoader</code> is {@link DataLoader} that creates and persists routings from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class RoutingLoader extends DataLoader<Routing> {

    private static final Logger LOGGER = Logger.getLogger(RoutingLoader.class.getName());

    @Inject
    private RoutingService routingService;
    @Inject
    private UserDirectoryServiceCache userDirectoryServiceCache;
    @Inject
    private SessionService sessionService;

    private Set<String> validUsernames;

    @Override
    public LoaderResult<Routing> load(InputStream stream, boolean test) {

        validUsernames = userDirectoryServiceCache.getAllUsernames();

        return super.load(stream, test);
    }

    @Override
    public void updateRecord(DSRecord record) {

        checkAllColumnsPresent(record);

        if (report.isError()) {
            stopLoad = true;
            return;
        }

        switch (record.getCommand()) {
        case CREATE:
            createRouting(record);
            break;
        case UPDATE:
            updateRouting(record);
            break;
        case DELETE:
            deleteRouting(record);
            break;
        default:
            // do nothing
            break;
        }
    }

    /** Processes the record for routing creation. */
    private void createRouting(DSRecord record) {

        checkRoutingAttributeValidity(record);

        if (report.isError())
            return;

        final String owner = sessionService.getLoggedInName();
        if (!hasRoutingPermission(owner)) {
            report.addMessage(getErrorMessage(
                    "You do not have permission to add routings you don't own" + " " + getUserAndOwnerInfo(owner) + ".",
                    record, RoutingColumn.OWNER));
        }

        if (report.isError())
            return;

        final String name = Utility.formatWhitespaces(record.getField(RoutingColumn.NAME).toString());

        if (routingService.getRoutingByName(name) != null) {
            report.addMessage(getErrorMessage(getRoutingNameInfo(name) + " already exists in the database.", record,
                    RoutingColumn.NAME, name));
            return;
        }

        final String description = Utility.formatWhitespaces(record.getField(RoutingColumn.DESCRIPTION).toString());
        final String cableClass = Utility.formatWhitespaces(record.getField(RoutingColumn.CLASSES).toString());
        final String location = Utility.formatWhitespaces(record.getField(RoutingColumn.LOCATION).toString());
        final Float length = toFloat(record.getField(RoutingColumn.LENGTH));

        if (!test) {
            Routing routing = new Routing(name, description, length, owner);
            routing.setCableClasses(cableClass);
            routing.setLocation(location);
            routing = routingService.createRouting(routing);
            createRows++;
            report.addAffected(routing);
        }

        report.addMessage(new ValidationMessage("Adding new routing.", false, record.getRowLabel(), null));
    }

    /** @return true if the current user has permission to modify routing with a given owner, else false */
    private boolean hasRoutingPermission(String owner) {
        return sessionService.canAdminister()
                || (sessionService.canManageOwnedCables() && owner.equals(sessionService.getLoggedInName()));// TODO
                                                                                                             // change
                                                                                                             // this
    }

    /** Processes the record for routing update. */
    private void updateRouting(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial

        checkRoutingAttributeValidity(record);// validation

        if (report.isError())
            return;

        if (!checkRoutingName(record))
            return;

        final String routingName = Utility.formatWhitespaces(record.getField(RoutingColumn.NAME).toString());

        final Routing oldRouting = routingService.getRoutingByName(routingName);
        routingService.detachRouting(oldRouting);
        final Routing routing = routingService.getRoutingByName(routingName);

        final String owner = routing.getOwner();
        if (!hasRoutingPermission(owner)) {
            report.addMessage(getErrorMessage("You do not have permission to update routings you don't own" + " "
                    + getUserAndOwnerInfo(owner) + ".", record, RoutingColumn.OWNER));
        }

        if (!routing.isActive()) {
            report.addMessage(new ValidationMessage(
                    getRoutingNameInfo(routingName) + " cannot be updated as it is already deleted.", true,
                    record.getRowLabel(), null, routingName));
            return;
        }

        final String description = Utility.formatWhitespaces(record.getField(RoutingColumn.DESCRIPTION).toString());
        final String cableClasses = Utility.formatWhitespaces(record.getField(RoutingColumn.CLASSES).toString());
        final String location = Utility.formatWhitespaces(record.getField(RoutingColumn.LOCATION).toString());
        final Float length = toFloat(record.getField(RoutingColumn.LENGTH));

        if (report.isError())
            return;

        routing.setName(routingName);
        routing.setDescription(description);
        routing.setLength(length);
        routing.setOwner(owner);
        routing.setCableClasses(cableClasses);
        routing.setLocation(location);

        if (!test) {
            routingService.updateRouting(routing, oldRouting, sessionService.getLoggedInName());
            updateRows++;
            report.addAffected(routing);
        }
        report.addMessage(
                new ValidationMessage("Updating routing '" + routingName + "'.", false, record.getRowLabel(), null));
    }

    /** Processes the record for routing deletion. */
    private void deleteRouting(DSRecord record) {

        if (!checkRoutingName(record))
            return;

        final String routingName = Utility.formatWhitespaces(record.getField(RoutingColumn.NAME).toString());
        final Routing routing = routingService.getRoutingByName(routingName);

        final String owner = routing.getOwner();
        if (!hasRoutingPermission(owner)) {
            report.addMessage(getErrorMessage("You do not have permission to delete routings you don't own" + " "
                    + getUserAndOwnerInfo(owner) + ".", record, RoutingColumn.OWNER));
        }

        if (report.isError())
            return;

        if (!routing.isActive()) {
            report.addMessage(new ValidationMessage(getRoutingNameInfo(routingName) + " is already deleted.", true,
                    record.getRowLabel(), routingName));
        }

        if (report.isError())
            return;

        if (!test) {
            routingService.deleteRouting(routing, sessionService.getLoggedInName());
            deleteRows++;
            report.addAffected(routing);
        }
        report.addMessage(new ValidationMessage("Deleting routing with number " + routingName + ".", false,
                record.getRowLabel(), null));
    }

    /** Check that the record has all expected columns. */
    private void checkAllColumnsPresent(DSRecord record) {

        for (final RoutingColumn column : RoutingColumn.values()) {
            if (!column.isExcelColumn()) {
                continue;
            }
            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by importing an old template or importing a wrong file.", true));
                break;
            }
        }
    }

    /** Check that the routing attributes are valid. */
    private void checkRoutingAttributeValidity(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial
                                                                  // validation
        try {
            final String cableClasses = Utility.formatWhitespaces(record.getField(RoutingColumn.CLASSES).toString());
            if (isNullOrEmpty(cableClasses)) {
                report.addMessage(getErrorMessage("CLASS is not specified.", record, RoutingColumn.CLASSES));
            } else if (!Routing.areCableClassesValid(cableClasses)) {
                report.addMessage(getErrorMessage("CLASS field is not valid list of cable classes.", record,
                        RoutingColumn.CLASSES, cableClasses));
            }
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + RoutingColumn.CLASSES, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, RoutingColumn.CLASSES,
                    objectToString(record.getField(RoutingColumn.CLASSES))));
        }

        final String name = Utility.formatWhitespaces(record.getField(RoutingColumn.NAME).toString());
        try {
            if (isNullOrEmpty(name)) {
                report.addMessage(getErrorMessage("NAME is not specified.", record, RoutingColumn.NAME));
            }
            validateStringSize(name, RoutingColumn.NAME);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + RoutingColumn.NAME, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, RoutingColumn.NAME, objectToString(name)));
        }

        final String description = Utility.formatWhitespaces(record.getField(RoutingColumn.DESCRIPTION).toString());
        try {
            validateStringSize(description, RoutingColumn.DESCRIPTION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + RoutingColumn.DESCRIPTION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, RoutingColumn.DESCRIPTION, objectToString(description)));
        }

        final String location = Utility.formatWhitespaces(record.getField(RoutingColumn.LOCATION).toString());
        try {
            validateStringSize(location, RoutingColumn.LOCATION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + RoutingColumn.LOCATION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, RoutingColumn.LOCATION, objectToString(location)));
        }

        try {
            final String lengthLabel = Utility.formatWhitespaces(record.getField(RoutingColumn.LENGTH).toString());
            if (isNullOrEmpty(lengthLabel)) {
                report.addMessage(getErrorMessage("LENGTH is not specified.", record, RoutingColumn.LENGTH));
            } else {
                final Float length = toFloat(record.getField(RoutingColumn.LENGTH));
            }
        } catch (NumberFormatException e) {
            LOGGER.log(Level.FINEST, "Invalid LENGTH", e);
            report.addMessage(getErrorMessage("LENGTH field is not in the valid format.", record, CableColumn.LENGTH,
                    objectToString(record.getField(RoutingColumn.LENGTH))));
        }

    }

    /**
     * Checks the validity of routing name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkRoutingName(DSRecord record) {
        final String routingName = Utility.formatWhitespaces(record.getField(RoutingColumn.NAME).toString());

        final Routing routing = routingService.getRoutingByName(routingName);
        if (routing == null) {
            report.addMessage(getErrorMessage(getRoutingNameInfo(routingName) + " does not exist in the database.",
                    record, RoutingColumn.NAME, routingName));
            return false;
        }
        return true;
    }

    private String getUserAndOwnerInfo(String owner) {
        return "(logged in user: " + sessionService.getLoggedInName() + ", owner: " + owner + ")";
    }

    private String getRoutingNameInfo(String routingName) {
        return "Routing with name " + routingName;
    }

    private void validateStringSize(final String value, final RoutingColumn column) {
        super.validateStringSize(value, column, Routing.class);
    }
}
