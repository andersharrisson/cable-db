/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.CableArticle;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.services.dl.CableArticleColumn;

/**
 * <code>CableArticleService</code> is the service layer that handles cable article operations.
 *
 * @author Lars Johansson
 */
@Stateless
public class CableArticleService {

    private static final Logger LOGGER = Logger.getLogger(CableArticleService.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;

    /**
     * Return a list of all cable articles.
     *
     * @return a list of all cable articles
     */
    public List<CableArticle> getCableArticles() {
        try {
            return em.createQuery("SELECT ca FROM CableArticle ca", CableArticle.class).getResultList();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable articles not found", e);
            return new ArrayList<>();
        }
    }

    /**
     * Return a list of all active cable articles.
     *
     * @return a list of all active cable articles
     */
    public List<CableArticle> getActiveCableArticles() {
        try {
            return em.createQuery("SELECT ca FROM CableArticle ca WHERE ca.active = 'true'",
                    CableArticle.class).getResultList();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Active cable articles not found", e);
            return new ArrayList<>();
        }
    }

    /**
     * Returns a cable article with the specified cable article id.
     *
     * @param id
     *            the cable article id
     * @return the cable article, or null if the cable article with such id cannot be found
     */
    public CableArticle getCableArticleById(Long id) {
        try {
            return em.find(CableArticle.class, id);
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable article with id: " + id + " not found", e);
            return null;
        }
    }

    /**
     * Return cable article by manufacturer and externalId.
     *
     * @param shortNameExternalId short name external id
     * @return cable article by manufacturer and externalId
     */
    public CableArticle getCableArticleByShortNameExternalId(String shortNameExternalId) {
        try {
        return em.createQuery(
                "SELECT ca FROM CableArticle ca WHERE ca.shortNameExternalId = :shortNameExternalId",
                CableArticle.class)
                .setParameter("shortNameExternalId", shortNameExternalId)
                .getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable article with shortNameExternalId: " + shortNameExternalId + " not found", e);
            return null;
        }
    }

    /**
     * Return a list of all cable articles by manufacturer.
     *
     * @param manufacturer manufacturer
     * @return a list of all cable articles by manufacturer
     */
    public List<CableArticle> getCableArticlesByManufacturer(String manufacturer) {
        try {
            return em.createQuery(
                    "SELECT ca FROM CableArticle ca WHERE ca.manufacturer = :manufacturer",
                    CableArticle.class)
                    .setParameter("manufacturer", manufacturer)
                    .getResultList();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable article with manufacturer: " + manufacturer + " not found", e);
            return new ArrayList<>();
        }
    }

    /**
     * Return a list of all cable articles by externalId.
     *
     * @param externalId external id
     * @return a list of all cable articles by externalId
     */
    public List<CableArticle> getCableArticlesByExternalId(String externalId) {
        try {
            return em.createQuery(
                    "SELECT ca FROM CableArticle ca WHERE ca.externalId = :externalId",
                    CableArticle.class)
                    .setParameter("externalId", externalId)
                    .getResultList();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable article with external id: " + externalId + " not found", e);
            return new ArrayList<>();
        }
    }

    public CableArticle createCableArticle(String manufacturer, String externalId, String erpNumber, String isoClass,
            String description, String longDescription, String modelType,
            Float bendingRadius, Float outerDiameter, Float ratedVoltage, Float weightPerLength,
            String shortName, String shortNameExternalId, String urlChessPart,
            boolean active, Date created, Date modified,
            String userId) {

        final CableArticle cableArticle = new CableArticle(manufacturer, externalId, erpNumber, isoClass,
                description, longDescription, modelType,
                bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                shortName, shortNameExternalId, urlChessPart,
                active, created, modified);
        em.persist(cableArticle);

        historyService.createHistoryEntry(EntityTypeOperation.CREATE,
                cableArticle.getShortNameExternalId(), EntityType.CABLE_ARTICLE,
                cableArticle.getId(), "", "", userId);

        return cableArticle;
    }

    /**
     * Creates an cable article in the database and returns it.
     *
     * @param cableArticle
     *            the cable article to add
     * @param userId
     *            userId of user creating the cable article, for history record
     * @return the created cable article
     */
    public CableArticle createCableArticle(CableArticle cableArticle, String userId) {
        final Date created = new Date();
        final Date modified = created;
        cableArticle.setCreated(created);
        cableArticle.setModified(modified);

        em.persist(cableArticle);

        historyService.createHistoryEntry(EntityTypeOperation.CREATE,
                cableArticle.getShortNameExternalId(), EntityType.CABLE_ARTICLE,
                cableArticle.getId(), "", "", userId);

        return cableArticle;
    }

    /**
     * Updates the attributes on the given cable article.
     *
     * @param cableArticle the cable article with modified attributes to save to the database
     * @param oldCableArticle the cable article before modification
     * @param userId username of user updating the cable article, for history record
     * @return true if that cable article was updated, false if the cable article was not updated
     */
    public boolean updateCableArticle(CableArticle cableArticle, CableArticle oldCableArticle, String userId) {
        // not update if content is same
        if (cableArticle.equals(oldCableArticle)) {
            return false;
        }

        cableArticle.setModified(new Date());
        em.merge(cableArticle);

        // entity name - (cable article) shortNameExternalId
        historyService.createHistoryEntry(EntityTypeOperation.UPDATE,
                cableArticle.getShortNameExternalId(), EntityType.CABLE_ARTICLE,
                cableArticle.getId(), getChangeString(cableArticle, oldCableArticle), "", userId);
        return true;
    }

    /**
     * Marks the cable article as inactive in the database.
     *
     * @param cableArticle cable article
     * @param userId username of user deleting the cable article, for history record
     * @return true if the cable article was deleted, false if the cable article was already deleted
     */
    public boolean deleteCableArticle(CableArticle cableArticle, String userId) {
        if (!cableArticle.isActive()) {
            return false;
        }

        cableArticle.setActive(false);
        cableArticle.setModified(new Date());
        em.merge(cableArticle);

        historyService.createHistoryEntry(EntityTypeOperation.DELETE,
                cableArticle.getShortNameExternalId(), EntityType.CABLE_ARTICLE,
                cableArticle.getId(), "", "", userId);
        return true;
    }

    /**
     * Generates and returns string with all changed cable article attributes.
     *
     * @param cableArticle new cable article
     * @param oldCableArticle old cable article
     * @return string with all changed cable article attributes
     */
    private String getChangeString(CableArticle cableArticle, CableArticle oldCableArticle) {
        StringBuilder sb = new StringBuilder(900);

        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.MANUFACTURER.getColumnLabel(),
                cableArticle.getManufacturer(), oldCableArticle.getManufacturer()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.EXTERNAL_ID.getColumnLabel(),
                cableArticle.getExternalId(), oldCableArticle.getExternalId()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.ERP_NUMBER.getColumnLabel(),
                cableArticle.getErpNumber(), oldCableArticle.getErpNumber()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.ISO_CLASS.getColumnLabel(),
                cableArticle.getIsoClass(), oldCableArticle.getIsoClass()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.DESCRIPTION.getColumnLabel(),
                cableArticle.getDescription(), oldCableArticle.getDescription()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.LONG_DESCRIPTION.getColumnLabel(),
                cableArticle.getLongDescription(), oldCableArticle.getLongDescription()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.MODEL_TYPE.getColumnLabel(),
                cableArticle.getModelType(), oldCableArticle.getModelType()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.BENDING_RADIUS.getColumnLabel(),
                cableArticle.getBendingRadius(), oldCableArticle.getBendingRadius()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.OUTER_DIAMETER.getColumnLabel(),
                cableArticle.getOuterDiameter(), oldCableArticle.getOuterDiameter()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.RATED_VOLTAGE.getColumnLabel(),
                cableArticle.getRatedVoltage(), oldCableArticle.getRatedVoltage()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.WEIGHT_PER_LENGTH.getColumnLabel(),
                cableArticle.getWeightPerLength(), oldCableArticle.getWeightPerLength()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.SHORT_NAME.getColumnLabel(),
                cableArticle.getShortName(), oldCableArticle.getShortName()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.SHORT_NAME_EXTERNAL_ID.getColumnLabel(),
                cableArticle.getShortNameExternalId(), oldCableArticle.getShortNameExternalId()));
        sb.append(HistoryService.getDiffForAttributes(CableArticleColumn.URL_CHESS_PART.getColumnLabel(),
                cableArticle.getUrlChessPart(), oldCableArticle.getUrlChessPart()));

        return sb.toString();
    }

    /** Performs clearing of the persistance context, thus detaching all entities. */
    public void detachAllCableArticles() {
        em.clear();
    }

    /**
     * Detaches a single cable article from entity manager
     *
     * @param cableArticle
     *            the cable article to detach
     */
    public void detachCableArticle(CableArticle cableArticle) {
        em.detach(cableArticle);
    }

}
