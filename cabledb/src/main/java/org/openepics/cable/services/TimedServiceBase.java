package org.openepics.cable.services;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * This is base for timed services functionality that supports handling timed operations like validation and cache
 * update from concurrent requests.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Lock(LockType.READ)
public abstract class TimedServiceBase {
    private static final Logger LOGGER = Logger.getLogger(TimedServiceBase.class.getName());

    protected boolean initialized;
    private boolean valid;

    private ReentrantLock lock = new ReentrantLock();

    /**
     * Extend this function to provide service name used for logging.
     * 
     * @return the service name.
     */
    protected abstract String getServiceName();

    /**
     * Extend this function to provide operation used for logging.
     * 
     * @return the operation.
     */
    protected abstract String getOperation();

    /**
     * Extend this function to run timed operation.
     * 
     * @return true if the operation was successfully run, else false.
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    protected abstract boolean runTimed();

    protected void initialise() {
        LOGGER.log(Level.INFO, getServiceName() + "  is initializing.");
        runTimedWithLock();
    }

    /**
     * Runs timed operation and returns when finished. If operation is already in progress when the method is invoked,
     * it returns when that operation completes.
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void runTimedBlocking() {
        boolean completed = runTimedWithLock();
        if (completed) {
            return;
        }

        LOGGER.log(Level.INFO,
                getServiceName() + ": " + getOperation() + " is already running. Will wait until it is done.");
        // We try to acquire lock waiting for 20 seconds. If unsuccessful we try until successful.
        try {
            while (!lock.tryLock(20, TimeUnit.SECONDS))
                ;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        lock.unlock();
        LOGGER.log(Level.INFO, getServiceName() + ": " + getOperation() + " finished.");
    }

    /**
     * Runs operation and returns when finished. If operation is already in progress when the method is invoked, it
     * returns immediately.
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void runTimedNonBlocking() {
        boolean completed = runTimedWithLock();
        if (!completed) {
            LOGGER.log(Level.INFO, getServiceName() + ": " + getOperation() + " is alreday in progress. Will skip.");
        }
    }

    /**
     * Runs operation and returns finished. If operation is already in progress when the method is invoked, it returns
     * immediately and returns false.
     * 
     * @return true if the operation was run, else false
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private boolean runTimedWithLock() {
        if (lock.tryLock()) {
            try {
                LOGGER.log(Level.INFO, getServiceName() + ": Running " + getOperation() + ".");
                valid = runTimed();
                initialized = true;
                LOGGER.log(Level.INFO, getServiceName() + ": " + getOperation() + " finished.");
            } finally {
                lock.unlock();
            }
            return true;
        } else {
            return false;
        }
    }

    /** @return true if the cache contains valid data, else false */
    public boolean isValid() {
        return valid;
    }
}
