/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.google.common.base.Preconditions;
import org.openepics.cable.dto.NotificationDTO;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsService;
import org.openepics.cable.util.fbs.FbsUtil;

/**
 * This is a service that performs periodic validation of the cable database.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@DependsOn("DatabaseMigration")
@Lock(LockType.READ)
public class CableValidationService extends ConcurrentOperationServiceBase {

    private static final Logger LOGGER = Logger.getLogger(CableValidationService.class.getName());
    private static final String VALIDATION_USER = "CableValidationService";
    private static final int FBS_BUNCH = 500;
    private static final int VALIDATION_BUNCH = 1000;

    private static boolean startupValidation;

    @Resource
    private SessionContext ctx;
    @Inject
    private CableService cableService;
    @Inject
    private FbsService fbsService;
    @Inject
    private NotificationService notificationService;

    /** Creates an interval timer that periodically invokes validations. */
    @PostConstruct
    private void init() {}

    /**
     * Performs the validation once. This should be invoked when application is started and dependent services are up.
     */
    public synchronized void validateAtStartup() {

        if (startupValidation) {
            return;
        }
        // no validation
        startupValidation = true;
    }

    /**
     * Performs periodic cable validation.
     *
     */
    @Schedule(minute = "30", hour = "1", persistent = false)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void validate() {
        runOperationNonBlocking(VALIDATION_USER);
    }

    @Override
    protected String getServiceName() {
        return "Cable Validation Service";
    }

    @Override
    protected String getOperation() {
        return "Validation";
    }

    /**
     * Validation of cables including FBS tag update. Note <tt>@Lock(LockType.WRITE)</tt>
     * for update of shared resources.
     */
    @Lock(LockType.WRITE)
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    protected boolean runOperation(String... vargs) {
        Preconditions.checkArgument(vargs.length == 1);
        String userId = vargs[0];
        // validate cables after update of FBS tag mappings as the latter potentially updates names
        // mappings depend on names but validation that is visible is to be trusted
        updateFbsTagMappingsInBunches(userId);
        return validateCablesInBunches(userId);
    }

    /**
     * Updates the FBS tag mappings. Get all FBS tag mappings for batch job.
     *
     * @param userId
     *            id of the user invoking validation
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private boolean updateFbsTagMappingsInBunches(String userId) {
        long time = System.currentTimeMillis();

        // use interface for CHESS/ITIP to retrieve mappings - cable name, ESS name, id, tag (FBS)
        //     information is retrieved and handled (sorted, mappings prepared), and then used
        //         e.g. retrieve tag for name (or vice versa) and store name/tag for cable (cable, endpoint)
        //     to show better (more complete) set of information to user

        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();
        if (!fbsElements.isEmpty()) {
            Map<String, FbsElement> mappingsCableNameToFbsElement =
                    FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            Map<String, FbsElement> mappingsESSNameToFbsElement =
                    FbsUtil.readMappingsEssNameToFbsElement(fbsElements);
            Map<String, FbsElement> mappingsIdToFbsElement =
                    FbsUtil.readMappingsIdToFbsElement(fbsElements);
            Map<String, FbsElement> mappingsTagToFbsElement =
                    FbsUtil.readMappingsTagToFbsElement(fbsElements);

            LOGGER.log(Level.INFO, "##FBS tag update, # FBS mappings:            " + fbsElements.size());
            LOGGER.log(Level.INFO, "##FBS tag update, # FBS mappings cable name: " + mappingsCableNameToFbsElement.size());
            LOGGER.log(Level.INFO, "##FBS tag update, # FBS mappings ESS name:   " + mappingsESSNameToFbsElement.size());
            LOGGER.log(Level.INFO, "##FBS tag update, # FBS mappings id:         " + mappingsIdToFbsElement.size());
            LOGGER.log(Level.INFO, "##FBS tag update, # FBS mappings tag:        " + mappingsTagToFbsElement.size());

            // map with key (ownersstring) and value (list of cable names)
            // to track cables for which values changed unexpectedly in tag mapping update
            Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges = new HashMap<>();

            int start = 0;
            // We do update until we run out of cables to update
            while (cableService.getAndFbsTagUpdateCables(start, FBS_BUNCH, userId,
                    mappingsCableNameToFbsElement, mappingsESSNameToFbsElement,
                    mappingsIdToFbsElement, mappingsTagToFbsElement,
                    mapOwnersCablesUnexpectedChanges)) {
                start += FBS_BUNCH;
                LOGGER.log(Level.INFO, "##FBS tag update of a bunch completed.");
            }

            // log number of owners and number of unexpected changes
            int numberOwners = mapOwnersCablesUnexpectedChanges.size();
            int numberUnexpectedChanges = 0;
            HashSet<String> cablesUnexpectedChanges = new HashSet<>();
            for (Map.Entry<String, List<NotificationDTO>> entry : mapOwnersCablesUnexpectedChanges.entrySet()) {
                numberUnexpectedChanges += entry.getValue().size();
                for (NotificationDTO notification : entry.getValue()) {
                    cablesUnexpectedChanges.add(notification.getCableName());
                }
            }
            LOGGER.log(Level.INFO, "##FBS tags updated, "
                    + numberOwners                   + " owners with "
                    + numberUnexpectedChanges        + " unexpected changes in "
                    + cablesUnexpectedChanges.size() + " cables");

            // send notification according to mapOwnersCablesUnexpectedChanges
            //     email per/to owner constellation and/or relevant cable coordinators
            //     with links to app for cables that were changed unexpectedly
            //         subject: "Cable batch update notification"
            //         content:
            //             "In batch update, as result of CHESS integration, # cables have been unexpectedly updated."
            //             "Unexpected update is replace non-empty value."
            //             "For more information, see each cable."
            //             "In batch update, as result of CHESS integration, # cables have been unexpectedly updated.
            //              For more information, see each cable."
            //                 https://cable.esss.lu.se/cables.xhtml?cableName=35B035359
            //                 ...

            if (!mapOwnersCablesUnexpectedChanges.isEmpty()) {
                notificationService.notifyUserFromChange(mapOwnersCablesUnexpectedChanges);
            }
        }

        LOGGER.log(Level.INFO, "##FBS tags updated, runtime: " + (System.currentTimeMillis() - time) + " ms");
        return true;
    }

    /**
     * Validates the consistency of cable data and updates the data and/or status.
     *
     * @param userId
     *            id of the user invoking validation
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private boolean validateCablesInBunches(String userId) {
        long time = System.currentTimeMillis();

        int start = 0;
        // We do validation until we run out of cables to validate
        while (cableService.getAndValidateCables(start, VALIDATION_BUNCH, userId)) {
            start += VALIDATION_BUNCH;
            LOGGER.log(Level.INFO, "##Validation of a bunch completed.");
        }

        LOGGER.log(Level.INFO, "##Validation completed, runtime: " + (System.currentTimeMillis() - time) + " ms");
        return true;
    }

}
