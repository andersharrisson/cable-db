/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryCondition;

/**
 * <code>QueryService</code> is the service layer that handles individual query, query conditions operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class QueryService {

    private static final String OWNER = "owner";

    @PersistenceContext
    private EntityManager em;

    /**
     * Return a list of all queries which belongs to the currently sign in user.
     *
     * @param owner query owner
     * @return a list of all queries which belongs to the currently sign in user
     */
    public List<Query> getQueries(String owner) {
        return em.createQuery("SELECT q FROM Query q WHERE q.owner = :owner ORDER BY q.created DESC", Query.class)
                .setParameter(OWNER, owner).getResultList();
    }

    /**
     * Return last three executed queries which belongs to the currently sign in user.
     *
     * @param owner query owner
     * @param entityType of the current data
     * @return last three executed queries which belongs to the currently sign in user
     */
    public List<Query> getLastThreeQueries(String owner, EntityType entityType) {
        return em
                .createQuery("SELECT q FROM Query q WHERE q.owner = :owner AND" + "  q.entityType = :entityType AND"
                        + " q.executed IS NOT NULL ORDER BY q.executed DESC", Query.class)
                .setParameter(OWNER, owner).setParameter("entityType", entityType).setMaxResults(3).getResultList();
    }

    /**
     * Saves new query into database.
     *
     * @param description query description
     * @param owner query owner
     * @param entityType entity type
     * @param queryConditions query conditions
     * @return query
     */
    public Query saveQuery(String description, EntityType entityType, String owner,
            List<QueryCondition> queryConditions) {

        final Date created = new Date();
        final Query query = new Query(description, entityType, owner, created, queryConditions);
        em.persist(query);
        return query;
    }

    /**
     * Return if query description is unique for owner and entity type.
     *
     * @param description query description
     * @param owner query owner (user)
     * @param entityType entity type
     * @return true if query description is unique, false otherwise
     *
     * @see EntityType
     */
    public boolean isQueryDescriptionUnique(String description, String owner, EntityType entityType) {
        List<Query> queries = em
                .createQuery("SELECT q FROM Query q WHERE q.owner = :owner AND"
                        + " q.description = :description AND q.entityType = :entityType", Query.class)
                .setParameter(OWNER, owner).setParameter("description", description)
                .setParameter("entityType", entityType).getResultList();
        return queries.isEmpty();
    }

    /**
     * Update query.
     *
     * @param query new query
     * @return merged query
     */
    public Query updateQuery(Query query) {
        return em.merge(query);
    }

    /**
     * Delete query from database.
     *
     * @param query query to be deleted
     */
    public void deleteQuery(Query query) {
        Query queryFromDB = em.find(Query.class, query.getId());
        em.remove(queryFromDB);
    }

    /**
     * Return query given id.
     *
     * @param id query id
     * @return query given id
     */
    public Query getQueryById(long id) {
        return em.find(Query.class, id);
    }

    /**
     * Update query execution data given query.
     *
     * @param query query to be updated
     */
    public void updateQueryExecutionDate(Query query) {
        query.updateExecutionDate(new Date());
        updateQuery(query);
    }
}
