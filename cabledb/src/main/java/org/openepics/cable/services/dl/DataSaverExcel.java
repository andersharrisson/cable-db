/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.cable.model.Persistable;
import org.openepics.cable.services.DateUtil;

/**
 * This is an abstract base class that saves data objects to Excel spreadsheet. The extending classes have to implement
 * the save method that defines the values to save, and the template spreadsheet to use.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 *
 * @param <T>
 *            the class type of the objects that are saved
 */
public abstract class DataSaverExcel<T> { // NOSONAR squid:S1200 Coupled to over 20 classes due to using many POI
                                          // classes, no true complexity

    private static final String SLASH = "/";

    private Sheet sheet;
    private Workbook workbook;
    private int currentRow;
    private int endRowIndex;
    private DSHeader header;
    private CellStyle unlockedCellStyle;
    private CellStyle hyperlinkCellStyle;
    private CellStyle dateCellStyle;
    private DataValidationHelper dataValidationHelper;

    /**
     * Constructs this object and stores the data objects.
     *
     * @param objects
     *            the objects to save
     */
    protected DataSaverExcel(Iterable<T> objects) {

        try {
            workbook = new XSSFWorkbook(getTemplate());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // process the template with loader to obtain required template information
        final DataLoader<Persistable> dataLoader = new DataLoader<Persistable>();
        final LoaderResult<Persistable> result = dataLoader.load(getTemplate());
        header = result.getHeader();
        endRowIndex = result.getHeaderIndex() + 1;
        currentRow = endRowIndex;

        sheet = workbook.getSheetAt(0);
        unlockedCellStyle = getUnlockedCellStyle();
        hyperlinkCellStyle = getHyperlinkCellStyle();
        dateCellStyle = getDateCellStyle();
        dataValidationHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);

        setColumnsValidValues();

        for (T object : objects) {
            save(object);
            currentRow++;
        }

        // HSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
    }

    /**
     * Saves the stored data objects to spreadsheet.
     *
     * @return the input stream containing the spreadsheet
     */
    public InputStream save() {
        return workbookToInputStream(workbook);
    }

    /**
     * Stores the value in the spreadsheet at the specified row and the specified column.
     *
     * The {@link #save()} method performs all the required storing, this method is public only for the purpose of
     * tests.
     *
     * @param rowIndex
     *            the row index, 0 for the first object
     * @param columnName
     *            the column name
     * @param value
     *            the value to store
     */
    public void setValue(int rowIndex, Object columnName, Object value) {
        setValue(columnName, rowIndex + endRowIndex, header.getColumnIndex(columnName.toString()), value);
    }

    /**
     * Writes the command string in the spreadsheet at the specified row.
     *
     * The {@link #save()} method performs all the required command writing, this method is public only for the purpose
     * of tests.
     *
     * @param rowIndex
     *            the row index, 0 for the first object
     * @param command
     *            the command to write; if {@link DSCommand#NONE} or {@link DSCommand#OPERATION}, nothing is written
     */
    public void setCommand(int rowIndex, DSCommand command) {
        if (command != DSCommand.NONE || command != DSCommand.OPERATION) {
            setValue("Operation", rowIndex + endRowIndex, 0, command);
        }
    }

    private InputStream workbookToInputStream(Workbook workbook) {
        try {
            final InputStream inputStream;
            File temporaryFile = File.createTempFile("temp", "xlsx");
            FileOutputStream outputStream = new FileOutputStream(temporaryFile);
            workbook.write(outputStream);
            outputStream.close();
            inputStream = new FileInputStream(temporaryFile);
            temporaryFile.delete(); // NOSONAR ignore failure of deleting temporary file

            return inputStream;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Saves the object by calling setValue(...) and setCommand(...) methods.
     * 
     * @param object
     *            the object to be saved
     */
    protected abstract void save(T object);

    /**
     * Returns the template excel file to be used to save data to.
     *
     * @return the input stream from which to read the template
     */
    protected abstract InputStream getTemplate();

    /**
     * Override this method to set column valid values using {@link #setColumnValidValues(Object, String[])}. Base
     * implementation does nothing.
     */
    protected void setColumnsValidValues() {}

    /**
     * Stores the value in the spreadsheet at the current row and the specified column.
     *
     * @param name
     *            the column name
     * @param value
     *            the value to store
     */
    protected void setValue(Object name, Object value) {
        setValue(name, currentRow, header.getColumnIndex(name.toString()), value);
    }

    /**
     * Limits the cells of a column to a given list of valid values.
     *
     * @param name
     *            the column name
     * @param validValues
     *            the array of valid values
     */
    protected void setColumnValidValues(Object name, String[] validValues) {

        final int column = header.getColumnIndex(name.toString());
        /*
         * Excel doesn't seem to support that user could copy validations between cells when sheets are protected, so
         * validations are assigned to the whole column for user convenience. Since assigning them to the whole column
         * doesn't seem to be supported in poi, they are assigned to very long range.
         */
        CellRangeAddressList addressList = new CellRangeAddressList(currentRow, Short.MAX_VALUE - 1, column, column);
        DataValidationConstraint constraint = dataValidationHelper.createExplicitListConstraint(validValues);
        DataValidation dataValidation = dataValidationHelper.createValidation(constraint, addressList);
        sheet.addValidationData(dataValidation);
    }

    /**
     * Writes the command string in the spreadsheet at the current row.
     *
     * @param command
     *            the command to write; if {@link DSCommand#NONE}, nothing is written
     */
    protected void setCommand(DSCommand command) {
        if (command != DSCommand.NONE) {
            setValue("Command", currentRow, 0, command);
        }
    }

    /**
     * Writes the comment string in the spreadsheet at the current row instead of command.
     *
     * @param comment
     *            to write under the command column.
     */
    protected void setCommandComment(String comment) {
        setValue("Command", currentRow, 0, "#" + comment);
    }

    private void setValue(Object name, int row, int column, Object value) {

        if (sheet.getRow(row) == null) {
            sheet.createRow(row);
        }

        final Row rowObject = sheet.getRow(row);
        if (rowObject.getCell(column) == null) {
            final Cell cell = rowObject.createCell(column);
            cell.setCellStyle(unlockedCellStyle);
        }

        final Cell cell = rowObject.getCell(column);

        if (value instanceof String) {

            if (!((String) value).trim().isEmpty()) {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue((String) value);
            } else {
                cell.setCellType(Cell.CELL_TYPE_BLANK);
            }

        } else if (value instanceof Integer) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue(((Integer) value).doubleValue());

        } else if (value instanceof Double) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue((Double) value);

        } else if (value instanceof Date) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue((Date) value);
            cell.setCellStyle(dateCellStyle);
        } else if (value instanceof URL) {
            CreationHelper createHelper = workbook.getCreationHelper();
            Hyperlink link = createHelper.createHyperlink(Hyperlink.LINK_URL);
            link.setAddress(value.toString());
            cell.setHyperlink(link);
            cell.setCellStyle(hyperlinkCellStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(getURLLastSegment(value.toString()));
        } else if (value == null) {
            cell.setCellType(Cell.CELL_TYPE_BLANK);
        } else {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(value.toString());
        }
    }

    private CellStyle getHyperlinkCellStyle() {
        CellStyle cellStyle = getUnlockedCellStyle();
        Font font = workbook.createFont();
        font.setUnderline(Font.U_SINGLE);
        font.setColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFont(font);
        return cellStyle;
    }

    private CellStyle getDateCellStyle() {
        CellStyle cellStyle = getUnlockedCellStyle();
        DataFormat format = workbook.getCreationHelper().createDataFormat();
        cellStyle.setDataFormat(format.getFormat(DateUtil.DATE_FORMAT_STRING));
        return cellStyle;
    }

    private CellStyle getUnlockedCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setLocked(false);
        return cellStyle;
    }

    private String getURLLastSegment(String urlString) {
        // remove trailing slash
        final String cleanedUrlString = urlString.endsWith(SLASH) ? urlString.substring(0, urlString.length() - 1)
                : urlString;

        return cleanedUrlString.substring(cleanedUrlString.lastIndexOf(SLASH) + 1, cleanedUrlString.length());
    }
}
