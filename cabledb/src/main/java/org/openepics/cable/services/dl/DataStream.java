/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facilitty for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */

package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.List;

import org.apache.poi.ss.util.CellRangeAddress;

/**
 * This represents a data stream from which the data is loaded.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public interface DataStream {

    /**
     * Opens the given stream to read data from. After this call, this object owns the stream.
     *
     * @param stream
     *            the stream to read from
     */
    void open(InputStream stream);

    /**
     * Switches to the next row. Call this after opening the stream to get data for the first row.
     */
    void nextRow();

    /**
     * Returns the data in a cell from the current row.
     *
     * @param index
     *            the 0-based index of the cell column, should be less than row size.
     * @return the data in this cell
     */
    Object getCell(int index);

    /**
     * Returns the row size, that is the number of columns of this row.
     *
     * @return the row size
     */
    int getRowSize();

    /**
     * Returns true if the end of the stream has been reached, and there is no more data.
     *
     * @return true if the end was reached
     */
    boolean isEndOfStream();

    /** Closes the stream releasing all resources associated to it. */
    void close();

    /**
     * Returns the label of the column.
     *
     * @param index
     *            the 0-based index of the column
     * @return the column label (A, B, C...)
     */
    String getColumnLabel(int index);

    /**
     * Returns the 0-based index of the currently read row.
     *
     * @return the row index, or -1 if no current row
     */
    int getRowIndex();

    /**
     * Returns the label of the currently read row.
     *
     * @return the row label
     */
    String getRowLabel();

    /**
     * Returns true if current row contains merged cells.
     *
     * @return true if current row contains merged cells otherwise false.
     */
    boolean containsMergedCells();

    /**
     * Returns list of merged regions of the current row.
     *
     * @return list of merged regions.
     */
    List<CellRangeAddress> getMergedRegions();

    /**
     * Returns a data in a cell for given row.
     *
     * @param rowIndex
     *            row index
     * @param colIndex
     *            column index
     *
     * @return data in a cell.
     */
    Object getCell(int rowIndex, int colIndex);

    /**
     * @return the urlColumns
     */
    public List<Integer> getUrlColumns();

    /**
     * @param urlColumns
     *            the urlColumns to set
     */
    public void setUrlColumns(List<Integer> urlColumns);
}
