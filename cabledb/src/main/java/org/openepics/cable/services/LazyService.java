/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
package org.openepics.cable.services;

import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import org.openepics.cable.model.Query;
import org.primefaces.model.SortOrder;

/**
 * Common interface for services behind lazy models.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 *
 * @param <T> type
 */
public interface LazyService<T> {

    /**
     * Returns a list of data from database in a paged way.
     *
     * @param first index of first row
     * @param pageSize amount of rows to load
     * @param sortField field by which data should be sorted
     * @param sortOrder order by which data should be sorted
     * @param filters filters for data
     * @param customQuery query to add
     * @return a list of data from database in a paged way
     */
    List<T> findLazy(final int first, final int pageSize, final @Nullable String sortField,
            final @Nullable SortOrder sortOrder, final @Nullable Map<String, Object> filters, Query customQuery);

}
