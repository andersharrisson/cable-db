/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.model.InstallationPackage;

import com.google.common.base.Preconditions;
import org.openepics.cable.services.dl.InstallationPackageColumn;

/**
 * <code>InstallationPackageService</code> is the service layer that handles individual installation package operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class InstallationPackageService {

    private static final Logger LOGGER = Logger.getLogger(InstallationPackageService.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;

    /** @return a list of all installation packages */
    public List<InstallationPackage> getInstallationPackages() {
        return em.createQuery("SELECT ip FROM InstallationPackage ip", InstallationPackage.class).getResultList();
    }

    /** @return a list of all active installation packages */
    public List<InstallationPackage> getActiveInstallationPackages() {
        return em.createQuery(
                "SELECT ip FROM InstallationPackage ip WHERE ip.active = 'true'", InstallationPackage.class)
                .getResultList();
    }

    /**
     * Returns an installation package with the specified installation package name.
     *
     * @param name
     *            the installation package name
     * @return the installation package, or null if the installation package with such name cannot be found
     */
    public InstallationPackage getInstallationPackageByName(String name) {
        try {

            return em.createQuery(
                    "SELECT ip FROM InstallationPackage ip WHERE ip.name = :name", InstallationPackage.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "InstallationPackage with name: " + name + " not found", e);
            return null;
        }
    }

    /**
     * Returns an installation package with the specified installation package id.
     *
     * @param id
     *            the installation package id
     * @return the installation package, or null if the installation package with such id cannot be found
     */
    public InstallationPackage getInstallationPackageById(Long id) {
        return em.find(InstallationPackage.class, id);
    }

    /**
     * Creates an installation package in the database and returns it.
     * If a deleted installation package of the same name exists, it is reused.
     *
     * @param installationPackage
     *            the installation package to add
     * @param userId
     *            userId of user creating the installation package, for history record
     * @return the created installation package
     */
    public InstallationPackage createInstallationPackage(InstallationPackage installationPackage, String userId) {
        InstallationPackage existingInstallationPackage = getInstallationPackageByName(installationPackage.getName());
        Preconditions.checkArgument(existingInstallationPackage == null || !existingInstallationPackage.isActive());

        final Date created = new Date();
        final Date modified = created;
        installationPackage.setCreated(created);
        installationPackage.setModified(modified);

        if (existingInstallationPackage != null) {
            existingInstallationPackage.setName(installationPackage.getName());
            existingInstallationPackage.setDescription(installationPackage.getDescription());
            existingInstallationPackage.setCableCoordinator(installationPackage.getCableCoordinator());
            existingInstallationPackage.setLocation(installationPackage.getLocation());
            existingInstallationPackage.setCreated(installationPackage.getCreated());
            existingInstallationPackage.setModified(installationPackage.getModified());
            existingInstallationPackage.setRoutingDocumentation(installationPackage.getRoutingDocumentation());
            existingInstallationPackage.setInstallerCable(installationPackage.getInstallerCable());
            existingInstallationPackage.setInstallerConnectorA(installationPackage.getInstallerConnectorA());
            existingInstallationPackage.setInstallerConnectorB(installationPackage.getInstallerConnectorB());
            existingInstallationPackage.setInstallationPackageLeader(
                    installationPackage.getInstallationPackageLeader());
            existingInstallationPackage.setActive(installationPackage.isActive());
            em.merge(existingInstallationPackage);
            installationPackage = existingInstallationPackage;
        } else {
            em.persist(installationPackage);
        }

        historyService.createHistoryEntry(
                EntityTypeOperation.CREATE, installationPackage.getName(), EntityType.INSTALLATION_PACKAGE,
                installationPackage.getId(), "", "", userId);

        return installationPackage;
    }

    /**
     * Updates the attributes on the given installation package.
     *
     * @param installationPackage
     *            the installation package with modified attributes to save to the database
     * @param oldInstallationPackage
     *            the installation package before modification
     * @param userId
     *            userId of user updating the installation package, for history record
     * @return true if that installation package was updated, false if the installation package was not updated
     */
    public boolean updateInstallationPackage(InstallationPackage installationPackage,
            InstallationPackage oldInstallationPackage, String userId) {
        // not update if content is same
        if (installationPackage.equals(oldInstallationPackage)) {
            return false;
        }

        em.merge(installationPackage);

        historyService.createHistoryEntry(
                EntityTypeOperation.UPDATE, installationPackage.getName(), EntityType.INSTALLATION_PACKAGE,
                installationPackage.getId(), getChangeString(installationPackage, oldInstallationPackage), "", userId);
        return true;
    }

    /**
     * Marks the installation package deleted in the database.
     *
     * @param installationPackage
     *            the installation package to delete
     * @return true if the installation package was deleted, false if the installation package was already deleted
     * @param userId
     *            userid of user deleting the installation package, for history record
     */
    public boolean deleteInstallationPackage(InstallationPackage installationPackage, String userId) {
        if (!installationPackage.isActive()) {
            return false;
        }

        installationPackage.setActive(false);
        installationPackage.setModified(new Date());
        em.merge(installationPackage);

        historyService.createHistoryEntry(
                EntityTypeOperation.DELETE, installationPackage.getName(), EntityType.INSTALLATION_PACKAGE,
                installationPackage.getId(), "", "", userId);

        return true;
    }

    /**
     * Generates and returns string with all changed installation package attributes.
     *
     * @param installationPackage
     *            new installation package
     * @param oldInstallationPackage
     *            old installation package
     *
     * @return string with all changed installation package attributes.
     */
    private String getChangeString(
            InstallationPackage installationPackage, InstallationPackage oldInstallationPackage) {

        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.NAME.getColumnLabel(),
                installationPackage.getName(),
                oldInstallationPackage.getName()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.DESCRIPTION.getColumnLabel(),
                installationPackage.getDescription(),
                oldInstallationPackage.getDescription()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.LOCATION.getColumnLabel(),
                installationPackage.getLocation(),
                oldInstallationPackage.getLocation()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.CABLE_COORDINATOR.getColumnLabel(),
                installationPackage.getCableCoordinator(),
                oldInstallationPackage.getCableCoordinator()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.ROUTING_DOCUMENTATION.getColumnLabel(),
                installationPackage.getRoutingDocumentation(),
                oldInstallationPackage.getRoutingDocumentation()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.INSTALLER_CABLE.getColumnLabel(),
                installationPackage.getInstallerCable(),
                oldInstallationPackage.getInstallerCable()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.INSTALLER_CONNECTOR_A.getColumnLabel(),
                installationPackage.getInstallerConnectorA(),
                oldInstallationPackage.getInstallerConnectorA()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.INSTALLER_CONNECTOR_B.getColumnLabel(),
                installationPackage.getInstallerConnectorB(),
                oldInstallationPackage.getInstallerConnectorB()));
        sb.append(HistoryService.getDiffForAttributes(
                InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER.getColumnLabel(),
                installationPackage.getInstallationPackageLeader(),
                oldInstallationPackage.getInstallationPackageLeader()));
        return sb.toString();
    }

    /** Performs clearing of the persistance context, thus detaching all entities. */
    public void detachAllInstallationPackages() {
        em.clear();
    }

    /**
     * Detaches a single installation package from entity manager
     *
     * @param installationPackage
     *            the installation package to detach
     */
    public void detachInstallationPackage(InstallationPackage installationPackage) {
        em.detach(installationPackage);
    }
}
