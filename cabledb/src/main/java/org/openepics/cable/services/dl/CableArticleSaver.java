/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import java.io.InputStream;

import org.openepics.cable.model.CableArticle;

/**
 * This saves {@link CableArticle} instances to Excel spreadsheet.
 *
 * @author Lars Johansson
 */
public class CableArticleSaver extends DataSaverExcelStream<CableArticle> {

    /**
     * Constructs an instance to save the given {@link CableArticle} objects.
     *
     * @param objects
     *            the objects to save
     */
    public CableArticleSaver(Iterable<CableArticle> objects) {
        super(objects);

        saveAndValidate();
    }

    @Override
    protected void save(CableArticle cableArticle) {
        setCommand(DSCommand.UPDATE);

        setValue(CableArticleColumn.MANUFACTURER,
                cableArticle.getManufacturer());
        setValue(CableArticleColumn.EXTERNAL_ID,
                cableArticle.getExternalId());
        setValue(CableArticleColumn.ERP_NUMBER,
                cableArticle.getErpNumber());
        setValue(CableArticleColumn.ISO_CLASS,
                cableArticle.getIsoClass());
        setValue(CableArticleColumn.DESCRIPTION,
                cableArticle.getDescription());
        setValue(CableArticleColumn.LONG_DESCRIPTION,
                cableArticle.getLongDescription());
        setValue(CableArticleColumn.MODEL_TYPE,
                cableArticle.getModelType());
        setValue(CableArticleColumn.BENDING_RADIUS,
                cableArticle.getBendingRadius());
        setValue(CableArticleColumn.OUTER_DIAMETER,
                cableArticle.getOuterDiameter());
        setValue(CableArticleColumn.RATED_VOLTAGE,
                cableArticle.getRatedVoltage());
        setValue(CableArticleColumn.WEIGHT_PER_LENGTH,
                cableArticle.getWeightPerLength());
        setValue(CableArticleColumn.SHORT_NAME,
                cableArticle.getShortName());
        setValue(CableArticleColumn.SHORT_NAME_EXTERNAL_ID,
                cableArticle.getShortNameExternalId());
        setValue(CableArticleColumn.URL_CHESS_PART,
                cableArticle.getUrlChessPart());
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_cable_articles.xlsx");
    }

}
