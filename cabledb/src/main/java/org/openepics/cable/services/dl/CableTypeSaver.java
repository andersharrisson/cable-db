/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;

import org.openepics.cable.model.CableType;
import org.openepics.cable.ui.CableTypeUI;

/**
 * This saves {@link CableType} instances to Excel spreadsheet.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableTypeSaver extends DataSaverExcelStream<CableType> {

    /**
     * Constructs an instance to save the given {@link CableType} objects.
     *
     * @param objects
     *            the objects to save
     */
    public CableTypeSaver(Iterable<CableType> objects) {
        super(objects);

        saveAndValidate();
    }

    @Override
    protected void save(CableType cableType) {
        setCommand(cableType.isActive() ? DSCommand.UPDATE : DSCommand.NONE);

        setValue(CableTypeColumn.STATUS, cableType.isActive() ? CableTypeUI.STATUS_VALID : CableTypeUI.STATUS_OBSOLETE);
        setValue(CableTypeColumn.NAME, cableType.getName());
        setValue(CableTypeColumn.DESCRIPTION, cableType.getDescription());
        setValue(CableTypeColumn.SERVICE, cableType.getService());
        setValue(CableTypeColumn.VOLTAGE_RATING, cableType.getVoltage());
        setValue(CableTypeColumn.INSULATION, cableType.getInsulation());
        setValue(CableTypeColumn.JACKET, cableType.getJacket());
        setValue(CableTypeColumn.FLAMABILITY, cableType.getFlammability());
        setValue(CableTypeColumn.INSTALLATION_TYPE, cableType.getInstallationType().getDisplayName());
        setValue(CableTypeColumn.RADIATION_RESISTANCE, cableType.getTid());
        setValue(CableTypeColumn.WEIGHT, cableType.getWeight());
        setValue(CableTypeColumn.DIAMETER, cableType.getDiameter());
        setValue(CableTypeColumn.MANUFACTURERS, cableType.getManufacturersAsString(false));
        setValue(CableTypeColumn.COMMENTS, cableType.getComments());
        setValue(CableTypeColumn.REVISION, cableType.getRevision());
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_cable_types.xlsx");
    }
}
