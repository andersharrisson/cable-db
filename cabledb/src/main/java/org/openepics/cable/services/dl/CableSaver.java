/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;

import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.util.CableNumbering;

/**
 * This saves {@link Cable} instances to Excel spreadsheet.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableSaver extends DataSaverExcel<Cable> {

    /**
     * Constructs an instance to save the given {@link Cable} objects.
     *
     * @param objects
     *            the objects to save
     */
    public CableSaver(Iterable<Cable> objects) {
        super(objects);
    }

    @Override
    protected void save(Cable cable) {
        if (cable.getStatus() == CableStatus.DELETED) {
            setCommandComment("Deleted");
        } else {
            setCommand(DSCommand.UPDATE);
        }

        if (cable.getSeqNumber() != null) {
            setValue(CableColumn.NAME, cable.getName());
        }
        setValue(CableColumn.SYSTEM, CableNumbering.getSystemLabel(cable.getSystem()));
        setValue(CableColumn.SUBSYSTEM, CableNumbering.getSubsystemLabel(cable.getSystem(), cable.getSubsystem()));
        setValue(CableColumn.CLASS, CableNumbering.getClassLabel(cable.getCableClass()));
        setValue(CableColumn.OWNERS, cable.getOwnersString());
        setValue(CableColumn.CABLE_TYPE, cable.getCableType());
        setValue(CableColumn.CONTAINER, cable.getContainer());

        saveEndpointA(cable.getEndpointA());
        saveEndpointB(cable.getEndpointB());

        setValue(CableColumn.ROUTINGS, cable.getRoutingsString());
        setValue(CableColumn.STATUS, cable.getStatus().getDisplayName());
        setValue(CableColumn.INSTALLATION_DATE, cable.getInstallationBy());
        setValue(CableColumn.TERMINATION_DATE, cable.getTerminationBy());
        setValue(CableColumn.AUTOCALCULATEDLENGTH, cable.getAutoCalculatedLength().getDisplayName());
        setValue(CableColumn.BASELENGTH, cable.getBaseLength());
        setValue(CableColumn.LENGTH, cable.getLength());
    }

    private void saveEndpointA(Endpoint endpoint) {
        setValue(CableColumn.DEVICE_A_BUILDING, endpoint.getBuilding());
        setValue(CableColumn.DEVICE_A_RACK, endpoint.getRack());
        setValue(CableColumn.DEVICE_A_NAME, endpoint.getDevice());
        setValue(CableColumn.DEVICE_A_CONNECTOR, endpoint.getConnector());
        setValue(CableColumn.DEVICE_A_WIRING, endpoint.getDrawing());
        setValue(CableColumn.DEVICE_A_USER_LABEL, endpoint.getLabel());
    }

    private void saveEndpointB(Endpoint endpoint) {
        setValue(CableColumn.DEVICE_B_BUILDING, endpoint.getBuilding());
        setValue(CableColumn.DEVICE_B_RACK, endpoint.getRack());
        setValue(CableColumn.DEVICE_B_NAME, endpoint.getDevice());
        setValue(CableColumn.DEVICE_B_CONNECTOR, endpoint.getConnector());
        setValue(CableColumn.DEVICE_B_WIRING, endpoint.getDrawing());
        setValue(CableColumn.DEVICE_B_USER_LABEL, endpoint.getLabel());
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_cables.xlsx");
    }
}
