/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.DataValidation;
import org.openepics.cable.dto.NotificationDTO;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.util.CableNumbering;

/**
 * This saves {@link Cable} instances to Excel spreadsheet.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
public class CableSaver extends DataSaverExcelStream<Cable> {

    private List<NotificationDTO> notificationDTOs;

    /**
     * Constructs an instance to save the given {@link Cable} objects.
     *
     * @param objects the objects to save
     */
    public CableSaver(Iterable<Cable> objects) {
        this(objects, null);
    }
    /**
     * Constructs an instance to save the given {@link Cable} objects
     * and highlight information according to given list of {@link NotificationDTO}.
     *
     * @param objects the objects to save
     * @param notificationDTOs list of changes to highlight
     */
    public CableSaver(Iterable<Cable> objects, List<NotificationDTO> notificationDTOs) {
        super(objects);
        this.notificationDTOs = notificationDTOs;

        saveAndValidate();
    }

    @Override
    protected void save(Cable cable) {
        if (cable.getStatus() == CableStatus.DELETED) {
            setCommandComment("Deleted");
        } else {
            setCommand(DSCommand.UPDATE);
        }

        if (cable.getSeqNumber() != null) {
            setValue(CableColumn.NAME, cable.getName(),
                    checkNotification(cable, CableColumn.NAME),
                    checkNotificationComment(cable, CableColumn.NAME));
        }
        setValue(CableColumn.FBS_TAG, cable.getFbsTag(),
                checkNotification(cable, CableColumn.FBS_TAG),
                checkNotificationComment(cable, CableColumn.FBS_TAG));
        setValue(CableColumn.CABLE_ARTICLE, cable.getCableArticle(),
                checkNotification(cable, CableColumn.CABLE_ARTICLE),
                checkNotificationComment(cable, CableColumn.CABLE_ARTICLE));
        setValue(CableColumn.CABLE_TYPE, cable.getCableType(),
                checkNotification(cable, CableColumn.CABLE_TYPE),
                checkNotificationComment(cable, CableColumn.CABLE_TYPE));
        setValue(CableColumn.CONTAINER, cable.getContainer());

        saveEndpointA(cable);
        saveEndpointB(cable);

        setValue(CableColumn.SYSTEM, CableNumbering.getSystemLabel(cable.getSystem()));
        setValue(CableColumn.SUBSYSTEM, CableNumbering.getSubsystemLabel(cable.getSystem(), cable.getSubsystem()));
        setValue(CableColumn.CLASS, CableNumbering.getClassLabel(cable.getCableClass()));
        setValue(CableColumn.INSTALLATION_PACKAGE, cable.getInstallationPackage());
        setValue(CableColumn.OWNERS, cable.getOwnersString());
        setValue(CableColumn.STATUS, cable.getStatus().getDisplayName());
        setValue(CableColumn.INSTALLATION_DATE, cable.getInstallationBy());
        setValue(CableColumn.COMMENTS, cable.getComments());
        setValue(CableColumn.REVISION, cable.getRevision());
        setValue(CableColumn.ELECTRICAL_DOCUMENTATION, cable.getElectricalDocumentation());
    }

    private void saveEndpointA(Cable cable) {
        Endpoint endpoint = cable.getEndpointA();

        setValue(CableColumn.FROM_ESS_NAME, endpoint.getDevice(),
                checkNotification(cable, endpoint, CableColumn.FROM_ESS_NAME),
                checkNotificationComment(cable, endpoint, CableColumn.FROM_ESS_NAME));
        setValue(CableColumn.FROM_FBS_TAG, endpoint.getDeviceFbsTag(),
                checkNotification(cable, endpoint, CableColumn.FROM_FBS_TAG),
                checkNotificationComment(cable, endpoint, CableColumn.FROM_FBS_TAG));
        setValue(CableColumn.FROM_LBS_TAG, endpoint.getBuilding());
        setValue(CableColumn.FROM_ENCLOSURE_ESS_NAME, endpoint.getRack(),
                checkNotification(cable, endpoint, CableColumn.FROM_ENCLOSURE_ESS_NAME),
                checkNotificationComment(cable, endpoint, CableColumn.FROM_ENCLOSURE_ESS_NAME));
        setValue(CableColumn.FROM_ENCLOSURE_FBS_TAG, endpoint.getRackFbsTag(),
                checkNotification(cable, endpoint, CableColumn.FROM_ENCLOSURE_FBS_TAG),
                checkNotificationComment(cable, endpoint, CableColumn.FROM_ENCLOSURE_FBS_TAG));
        setValue(CableColumn.FROM_CONNECTOR, endpoint.getConnector(),
                checkNotification(cable, endpoint, CableColumn.FROM_CONNECTOR),
                checkNotificationComment(cable, endpoint, CableColumn.FROM_CONNECTOR));
        setValue(CableColumn.FROM_USER_LABEL, endpoint.getLabel());
    }

    private void saveEndpointB(Cable cable) {
        Endpoint endpoint = cable.getEndpointB();

        setValue(CableColumn.TO_ESS_NAME, endpoint.getDevice(),
                checkNotification(cable, endpoint, CableColumn.TO_ESS_NAME),
                checkNotificationComment(cable, endpoint, CableColumn.TO_ESS_NAME));
        setValue(CableColumn.TO_FBS_TAG, endpoint.getDeviceFbsTag(),
                checkNotification(cable, endpoint, CableColumn.TO_FBS_TAG),
                checkNotificationComment(cable, endpoint, CableColumn.TO_FBS_TAG));
        setValue(CableColumn.TO_LBS_TAG, endpoint.getBuilding());
        setValue(CableColumn.TO_ENCLOSURE_ESS_NAME, endpoint.getRack(),
                checkNotification(cable, endpoint, CableColumn.TO_ENCLOSURE_ESS_NAME),
                checkNotificationComment(cable, endpoint, CableColumn.TO_ENCLOSURE_ESS_NAME));
        setValue(CableColumn.TO_ENCLOSURE_FBS_TAG, endpoint.getRackFbsTag(),
                checkNotification(cable, endpoint, CableColumn.TO_ENCLOSURE_FBS_TAG),
                checkNotificationComment(cable, endpoint, CableColumn.TO_ENCLOSURE_FBS_TAG));
        setValue(CableColumn.TO_CONNECTOR, endpoint.getConnector(),
                checkNotification(cable, endpoint, CableColumn.TO_CONNECTOR),
                checkNotificationComment(cable, endpoint, CableColumn.TO_CONNECTOR));
        setValue(CableColumn.TO_USER_LABEL, endpoint.getLabel());
    }

    /**
     * Check for, and return boolean, if notification is available.
     *
     * @param cable cable
     * @param cableColumn cable column
     * @return boolean if notification is available
     */
    private boolean checkNotification(Cable cable, CableColumn cableColumn) {
        // consider (Cable, CableColumn) compared to NotificationDTO

        if (notificationDTOs != null && cable != null && cableColumn != null) {
            for (NotificationDTO notificationDTO : notificationDTOs) {
                if (cableColumn.equals(notificationDTO.getCableColumn())
                        && notificationDTO.getCable() != null
                        && cable.getId().equals(notificationDTO.getCable().getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check for, and return boolean, if notification is available.
     *
     * @param cable cable
     * @param endpoint endpoint
     * @param cableColumn cable column
     * @return boolean if notification is available
     */
    private boolean checkNotification(Cable cable, Endpoint endpoint, CableColumn cableColumn) {
        // consider (Endpoint, CableColumn) compared to NotificationDTO

        if (notificationDTOs != null && cable != null && endpoint != null && cableColumn != null) {
            for (NotificationDTO notificationDTO : notificationDTOs) {
                if (cableColumn.equals(notificationDTO.getCableColumn())
                        && notificationDTO.getEndpoint() != null
                        && endpoint.getId().equals(notificationDTO.getEndpoint().getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check for, and return string, if notification comment is available, null otherwise.
     *
     * @param cable cable
     * @param cableColumn cable column
     * @return string if notification comment is available, null otherwise
     */
    private String checkNotificationComment(Cable cable, CableColumn cableColumn) {
        if (!checkNotification(cable, cableColumn)) {
            return null;
        }

        if (notificationDTOs != null && cable != null && cableColumn != null) {
            for (NotificationDTO notificationDTO : notificationDTOs) {
                if (cableColumn.equals(notificationDTO.getCableColumn())
                        && notificationDTO.getCable() != null
                        && cable.getId().equals(notificationDTO.getCable().getId())) {
                    return notificationDTO.getComment();
                }
            }
        }
        return null;
    }

    /**
     * Check for, and return string, if notification comment is available, null otherwise.
     *
     * @param cable cable
     * @param endpoint endpoint
     * @param cableColumn cable column
     * @return string if notification comment is available, null otherwise
     */
    private String checkNotificationComment(Cable cable, Endpoint endpoint, CableColumn cableColumn) {
        if (!checkNotification(cable, endpoint, cableColumn)) {
            return null;
        }

        if (notificationDTOs != null && cable != null && endpoint != null && cableColumn != null) {
            for (NotificationDTO notificationDTO : notificationDTOs) {
                if (cableColumn.equals(notificationDTO.getCableColumn())
                        && notificationDTO.getEndpoint() != null
                        && endpoint.getId().equals(notificationDTO.getEndpoint().getId())) {
                    return notificationDTO.getComment();
                }
            }
        }
        return null;
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_cables.xlsx");
    }

    @Override
    protected void addCustomValidationModification(DataValidation dataValidation) {
        String formula = dataValidation.getValidationConstraint().getFormula1();
        String vlookupPattern = "(VLOOKUP\\([A-Z]+)([\\d]+),";
        Pattern pattern = Pattern.compile(vlookupPattern);
        Matcher matcher = pattern.matcher(formula.toUpperCase());
        while(matcher.find()) {
            String replaced = matcher.replaceAll(matcher.group(1) + (endRowIndex + 1) + ",");
            dataValidation.getValidationConstraint().setFormula1(replaced);
        }
    }

}
