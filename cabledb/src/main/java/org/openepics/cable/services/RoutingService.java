/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Routing Database.
 * Routing Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.model.Routing;

import com.google.common.base.Preconditions;

/**
 * <code>RoutingService</code> is the service layer that handles individual routing operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class RoutingService {

    private static final Logger LOGGER = Logger.getLogger(RoutingService.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;

    /** @return a list of all routings */
    public List<Routing> getRoutings() {
        return em.createQuery("SELECT c FROM Routing c", Routing.class).getResultList();
    }

    /** @return a list of all active routings */
    public List<Routing> getActiveRoutings() {
        return em.createQuery("SELECT c FROM Routing c WHERE c.active = 'true'", Routing.class).getResultList();
    }

    /**
     * Returns a Routing with the specified Routing name.
     *
     * @param name
     *            the Routing name
     * @return the Routing, or null if the Routing with such name cannot be found
     */
    public Routing getRoutingByName(String name) {
        try {

            return em.createQuery("SELECT c FROM Routing c WHERE c.name = :name", Routing.class)
                    .setParameter("name", name).getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Routing with name: " + name + " not found");
            return null;
        }
    }

    /**
     * Returns a Routing with the specified Routing id.
     *
     * @param id
     *            the Routing id
     * @return the Routing, or null if the Routing with such id cannot be found
     */
    public Routing getRoutingById(Long id) {
        return em.find(Routing.class, id);
    }

    /**
     * Creates a Routing in the database and returns it. If a deleted routing of the same name exists, it is reused.
     *
     * @param routing
     *            the routing to add
     * @return the created Routing
     */
    public Routing createRouting(Routing routing) {

        Routing exsistingRouting = getRoutingByName(routing.getName());
        Preconditions.checkArgument(exsistingRouting == null || !exsistingRouting.isActive());

        final Date created = new Date();
        final Date modified = created;
        routing.setCreated(created);
        routing.setModified(modified);

        if (exsistingRouting != null) {
            exsistingRouting.setName(routing.getName());
            exsistingRouting.setDescription(routing.getDescription());
            exsistingRouting.setLength(routing.getLength());
            exsistingRouting.setOwner(routing.getOwner());
            exsistingRouting.setCableClasses(routing.getCableClasses());
            exsistingRouting.setLocation(routing.getLocation());
            exsistingRouting.setCreated(routing.getCreated());
            exsistingRouting.setModified(routing.getModified());
            exsistingRouting.setActive(routing.isActive());
            em.merge(exsistingRouting);
            routing = exsistingRouting;
        } else {
            em.persist(routing);
        }

        historyService.createHistoryEntry(EntityTypeOperation.CREATE, routing.getName(), EntityType.ROUTING,
                routing.getId(), "", routing.getOwner());
        return routing;
    }

    /**
     * Updates the attributes on the given Routing.
     *
     * @param routing
     *            the Routing with modified attributes to save to the database
     * @param oldRouting
     *            the Routing before modification
     * @param userId
     *            userId of user updating the Routing, for history record
     */
    public void updateRouting(Routing routing, Routing oldRouting, String userId) {
        em.merge(routing);

        historyService.createHistoryEntry(EntityTypeOperation.UPDATE, routing.getName(), EntityType.ROUTING,
                routing.getId(), getChangeString(routing, oldRouting), userId);
    }

    /**
     * Marks the routing deleted in the database.
     *
     * @param routing
     *            the routing to delete
     * @return true if the routing was deleted, false if the routing was already deleted
     * @param userId
     *            userid of user deleting the routing, for history record
     */
    public boolean deleteRouting(Routing routing, String userId) {
        if (!routing.isActive()) {
            return false;
        }

        routing.setActive(false);
        routing.setModified(new Date());
        em.merge(routing);

        historyService.createHistoryEntry(EntityTypeOperation.DELETE, routing.getName(), EntityType.ROUTING,
                routing.getId(), "", userId);
        return true;
    }

    /**
     * Generates and returns string with all changed routing attributes.
     *
     * @param routing
     *            new routing
     * @param oldRouting
     *            old routing
     *
     * @return string with all changed routing attributes.
     */
    private String getChangeString(Routing routing, Routing oldRouting) {
        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes("Name", routing.getName(), oldRouting.getName()));
        sb.append(HistoryService.getDiffForAttributes("Description", routing.getDescription(), oldRouting.getDescription()));
        sb.append(HistoryService.getDiffForAttributes("Classes", routing.getCableClassesAsString(),
                oldRouting.getCableClassesAsString()));
        sb.append(HistoryService.getDiffForAttributes("Location", routing.getLocation(), oldRouting.getLocation()));
        sb.append(HistoryService.getDiffForAttributes("Length", routing.getLength(), oldRouting.getLength()));
        sb.append(HistoryService.getDiffForAttributes("Owner", routing.getOwner(), oldRouting.getOwner()));
        return sb.toString();
    }

    /** Performs clearing of the persistance context, thus detaching all entities. */
    public void detachAllRoutings() {
        em.clear();
    }

    /**
     * Detaches a single routing from entity manager
     * 
     * @param routing
     *            the routing to detach
     */
    public void detachRouting(Routing routing) {
        em.detach(routing);
    }
}
