package org.openepics.cable.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.openepics.cable.model.DeviceStatus;
import org.openepics.names.jaxb.DeviceNameElement;

/**
 * A session bean holding caching the data retrieved from {@link NamesService}. This class ensures that the possibly
 * slow read operations from underlying services are not performed more than once in a given session.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
public class NamesServiceCache extends TimedServiceBase implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(NamesServiceCache.class.getName());
    private static final long serialVersionUID = 1L;

    // the time to wait before updating name service cache (20 min )
    private static final String UPDATE_INTERVAL = "*/20";

    @Inject
    private transient NamesService namesService;

    private static boolean valid;

    private Map<String, String> namesStatus;
    private Set<DeviceNameElement> devices;

    @PostConstruct
    protected void initialise() {
        super.initialise();
    }

    /**
     * Updated name service cache.
     * 
     */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        runTimedNonBlocking();
    }

    @Override
    protected String getServiceName() {
        return "Names Service Cache";
    }

    @Override
    protected String getOperation() {
        return "Cache update";
    }

    /**
     * Helper method that invokes cache updating and returns true if updating is not already in progress, else false
     */
    protected boolean updateCache() {
        Set<DeviceNameElement> newDevices = updateDevices();
        Map<String, String> newNamesStatus = generateNamesStatusMap(newDevices);

        devices = newDevices;
        namesStatus = newNamesStatus;
        return true;
    }

    @Override
    protected boolean runTimed() {
        return updateCache();
    }

    private Set<DeviceNameElement> updateDevices() {
        Set<DeviceNameElement> newDevices;
        try {
            newDevices = namesService.getAllDevices();
            valid = true;
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "There was an exception retrieving devices from the naming service.", e);
            valid = false;
            newDevices = Collections.emptySet();
        }
        return newDevices;
    }

    /**
     * Returns a set of all devices currently registered in the naming service.
     *
     * @return the set
     */
    public Set<DeviceNameElement> getAllDevices() {
        if (devices != null && (!devices.isEmpty())) {
            return devices;
        }
        updateCache();
        return devices;
    }

    private Map<String, String> generateNamesStatusMap(Set<DeviceNameElement> newDevices) {
        namesStatus = new HashMap<String, String>();
        for (DeviceNameElement element : newDevices) {
            namesStatus.put(element.getName(), element.getStatus());
        }
        return namesStatus;
    }

    /**
     * Returns a set of all names currently registered in the naming service.
     *
     * @return the set
     */
    public Set<String> getAllNames() {
        if (namesStatus == null) {
            updateCache();
        }
        return namesStatus.keySet();
    }

    /**
     * Returns a set of all active names currently registered in the naming service.
     *
     * @return the set
     */
    public List<String> getAllActiveNamesList() {
        List<String> activeNames = new ArrayList<String>();
        for (String name : getAllNamesList()) {
            if (DeviceStatus.valueOf(namesStatus.get(name)) == DeviceStatus.ACTIVE) {
                activeNames.add(name);
            }
        }
        return activeNames;
    }

    /**
     * Returns a list of all names currently registered in the naming service.
     *
     * @return the list
     */
    public List<String> getAllNamesList() {
        if (namesStatus == null) {
            updateCache();
        }
        List<String> results = new ArrayList<String>(getAllNames());
        Collections.sort(results);
        return results;
    }

    /**
     * @return true if the cache contains valid data, false if the data retrieval from the names service failed
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Returns a status of device registered in the naming service.
     *
     * @param deviceName
     *            device name
     *
     * @return status of device registered in the naming service
     */
    public DeviceStatus getDeviceStatus(String deviceName) {
        if (namesStatus == null) {
            updateCache();
        }
        String status = namesStatus.get(deviceName);
        return (status == null || status.isEmpty()) ? DeviceStatus.NONE : DeviceStatus.valueOf(status);
    }
}
