/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 * 
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 * 
 * Contact Information:
 *   Facilitty for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */
package org.openepics.cable.services.dl;

import java.util.HashMap;
import java.util.Map;

/**
 * This represents a control or data record with context information.
 * 
 * The data is stored as field name-value pairs. The control record is specified by the command of the record.
 * 
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DSRecord {
    // field-name, value
    private Map<String, Object> fieldMap;
    // command associated with the record
    private DSCommand command = DSCommand.NONE;

    private int rowIndex;
    private String rowLabel;

    /** Constructs an empty record with no fields defined and command set to none. */
    public DSRecord() {
        fieldMap = new HashMap<String, Object>();
    }

    /**
     * Sets the field to a value.
     * 
     * @param name
     *            the field name
     * @param value
     *            Arbitrary object to represent the value. The value cannot be null.
     */
    public void setField(String name, Object value) {
        fieldMap.put(name, value);
    }

    /**
     * Returns the value of the field
     * 
     * @param name
     *            the field name; if not a string, it is converted to string
     * @return the value
     * 
     * @throws IllegalArgumentException
     *             if the field by this name was not set, or name is null
     */
    public Object getField(Object name) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }
        Object value = fieldMap.get(name.toString());
        if (value == null) {
            throw new IllegalArgumentException("Could not get field: " + name);
        }

        return value;
    }

    /** Clears all fields on this record. */
    public void clear() {
        fieldMap.clear();
    }

    /** @return the command of this record */
    public DSCommand getCommand() {
        return command;
    }

    /**
     * Sets the command on this record.
     * 
     * @param command
     *            the command to set
     */
    public void setCommand(DSCommand command) {
        this.command = command;
    }

    /** @return the -based row index of this record */
    public int getRowIndex() {
        return rowIndex;
    }

    /**
     * @param rowIndex
     *            the 0-based row index to set
     */
    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    /** @return the name of the row Name */
    public String getRowLabel() {
        return rowLabel;
    }

    /**
     * @param rowLabel
     *            the rowName to set
     */
    public void setRowLabel(String rowLabel) {
        this.rowLabel = rowLabel;
    }
}
