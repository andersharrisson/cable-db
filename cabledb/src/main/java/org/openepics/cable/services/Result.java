/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

/**
 * This represents a result of an action consisting of failure state and message.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class Result {

    private String message;
    private boolean failure;

    /** @return the message of this result */
    public String getMessage() {
        return message;
    }

    /** @return the result failure state */
    public boolean isFailure() {
        return failure;
    }

    /**
     * Constructs a non-failure result with message.
     * 
     * @param message
     *            the result message
     */
    public Result(String message) {
        this(message, false);
    }

    /**
     * Constructs a result with message and failure.
     * 
     * @param message
     *            the result message
     * @param failure
     *            the result failure flag
     */
    public Result(String message, boolean failure) {
        super();
        this.message = message;
        this.failure = failure;
    }
}
