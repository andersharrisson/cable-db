/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.CableTypeManufacturer;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.ManufacturerService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.util.Utility;

/**
 * <code>CableTypeLoader</code> is {@link DataLoader} that creates and persists cable types from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableTypeLoader extends DataLoader<CableType> {

    private static final Logger LOGGER = Logger.getLogger(CableTypeLoader.class.getName());

    private static final String INVALID = "Invalid ";

    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private ManufacturerService manufacturerService;

    @Inject
    private SessionService sessionService;

    // used for measuring time performance
    private long time;
    private long totalTime;

    // used for session information
    private boolean canAdminister;
    private String loggedInName;

    @Override
    public LoaderResult<CableType> load(InputStream stream, boolean test) {
        LOGGER.warning("Starting load");
        resetTime();

        // retrieve information before import
        //     session information
        // print import statistics and time

        canAdminister = sessionService.canAdminister();
        loggedInName = sessionService.getLoggedInName();

        LoaderResult<CableType> result = super.load(stream, test);

        totalTime += getPassedTime();
        if (test) {
            LOGGER.info("# cable types (data):         " + dataRows);
            LOGGER.info("# cable types (create test):  " + createRowsTest);
            LOGGER.info("# cable types (update test):  " + updateRowsTest);
            LOGGER.info("# cable types (delete test):  " + deleteRowsTest);
            LOGGER.info("# cable types (skipped test): " + result.getImportFileStatistic().getSkippedRowsTest());
        } else {
            LOGGER.info("# cable types (data):    " + dataRows);
            LOGGER.info("# cable types (create):  " + createRows);
            LOGGER.info("# cable types (update):  " + updateRows);
            LOGGER.info("# cable types (delete):  " + deleteRows);
            LOGGER.info("# cable types (skipped): " + result.getImportFileStatistic().getSkippedRows());
        }
        LOGGER.info("Import time: " + totalTime + " ms");

        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {
        checkAllColumnsPresent(record);

        if (report.isError()) {
            stopLoad = true;
            return;
        }

        switch (record.getCommand()) {
        case CREATE:
            createCableType(record);
            break;
        case UPDATE:
            updateCableType(record);
            break;
        case DELETE:
            deleteCableType(record);
            break;
        default:
            // do nothing
            break;
        }
    }

    /** Processes the record for cable type creation. */
    private void createCableType(DSRecord record) {
        checkCableTypeAttributeValidity(record);

        if (report.isError() && !test)
            return;

        final String name = getRecordValue(record, CableTypeColumn.NAME);

        if (cableTypeService.getCableTypeByName(name) != null) {
            report.addMessage(getErrorMessage("CableType with name " + name + " already exists in the database.",
                    record, CableTypeColumn.NAME, name));
            if (test) {
                createRowsTest++;
            }
            return;
        }

        final List<String> manufacturerNames = Utility.splitStringIntoList(getRecordValue(record, CableTypeColumn.MANUFACTURERS, false, false));
        final InstallationType installationType = InstallationType.convertToInstallationType(Utility.upperCase(getRecordValue(record, CableTypeColumn.INSTALLATION_TYPE)));
        final String description = getRecordValue(record, CableTypeColumn.DESCRIPTION, true, true);
        final String service = getRecordValue(record, CableTypeColumn.SERVICE);
        final Integer voltage = Utility.toInteger(getRecordValue(record, CableTypeColumn.VOLTAGE_RATING));
        final String insulation = getRecordValue(record, CableTypeColumn.INSULATION);
        final String jacket = getRecordValue(record, CableTypeColumn.JACKET);
        final String flammability = getRecordValue(record, CableTypeColumn.FLAMABILITY);
        final Float tid = Utility.toFloat(getRecordValue(record, CableTypeColumn.RADIATION_RESISTANCE));
        final Float weight = Utility.toFloat(getRecordValue(record, CableTypeColumn.WEIGHT));
        final Float diameter = Utility.toFloat(getRecordValue(record, CableTypeColumn.DIAMETER));
        final List<CableTypeManufacturer> manufacturers = new ArrayList<>();
        for (String manufacturerName : manufacturerNames) {
            Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerName);
            if (manufacturer != null) {
                manufacturers.add(new CableTypeManufacturer(null, manufacturer, null, manufacturers.size()));
            }
        }
        final String comments = getRecordValue(record, CableTypeColumn.COMMENTS, true, true);

        // revision is optional column/field
        //     administrator only
        //     no previous value to consider
        String revision = canAdminister ? getRecordValue(record, CableTypeColumn.REVISION) : null;

        if (test) {
            createRowsTest++;
        } else {
            final CableType cableType = cableTypeService.createCableType(name, installationType, description, service,
                    voltage, insulation, jacket, flammability, tid, weight, diameter, manufacturers, comments,
                    revision, loggedInName);
            manufacturers.stream().forEach(m -> m.setCableType(cableType));
            createRows++;
            report.addAffected(cableType);
        }
        report.addMessage(
                new ValidationMessage("Adding new cable type '" + name + "'", false, record.getRowLabel(), null));

    }

    /** Processes the record for cable type update. */
    private void updateCableType(DSRecord record) {
        checkCableTypeAttributeValidity(record);

        if (report.isError() && !test)
            return;

        if (!checkCableTypeName(record)) {
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final CableType oldCableType = getCableTypeFromRecord(record);
        cableTypeService.detachCableType(oldCableType);
        final CableType cableType = getCableTypeFromRecord(record);
        final String name = cableType.getName();

        if (!cableType.isActive()) {
            report.addMessage(new ValidationMessage("CableType '" + name + "' cannot be updated as it is obsolete.",
                    true, record.getRowLabel(), null, name));
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        if (report.isError() && !test)
            return;

        List<String> manufacturerNames = new ArrayList<>(Utility.splitStringIntoList(getRecordValue(record, CableTypeColumn.MANUFACTURERS, false, false)));
        final List<CableTypeManufacturer> existingManufacturers = cableType.getManufacturers();
        final List<CableTypeManufacturer> manufacturers = new ArrayList<>();
        for (String manufacturerName : manufacturerNames) {
            Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerName);
            if (manufacturer != null) {
                manufacturers.add(new CableTypeManufacturer(cableType, manufacturer, null, manufacturers.size()));
            }
        }
        // Iterate through manufacturers on the cable type and preserve ones that are specified in the import file
        for (CableTypeManufacturer existingCableTypeManufacturer : existingManufacturers) {
            final String existingCableTypeManufacturerName = existingCableTypeManufacturer.getManufacturer().getName();
            // Iterate through manufacturers specified in the import file to check if current existing manufacturer is
            // amongst them. It yes swap the new manufacturer with the existing one, preserving the imported order.
            for (int i = 0; i < manufacturers.size(); i++) {
                if (existingCableTypeManufacturerName.equals(manufacturers.get(i).getManufacturer().getName())) {
                    manufacturers.set(i, existingCableTypeManufacturer);
                    break;
                }
            }
        }

        cableType.setDescription(getRecordValue(record, CableTypeColumn.DESCRIPTION, true, true));
        cableType.setService(getRecordValue(record, CableTypeColumn.SERVICE));
        cableType.setVoltage(Utility.toInteger(getRecordValue(record, CableTypeColumn.VOLTAGE_RATING)));
        cableType.setInsulation(getRecordValue(record, CableTypeColumn.INSULATION));
        cableType.setJacket(getRecordValue(record, CableTypeColumn.JACKET));
        cableType.setFlammability(getRecordValue(record, CableTypeColumn.FLAMABILITY));
        cableType.setInstallationType(InstallationType.convertToInstallationType(Utility.upperCase(getRecordValue(record, CableTypeColumn.INSTALLATION_TYPE))));
        cableType.setTid(Utility.toFloat(getRecordValue(record, CableTypeColumn.RADIATION_RESISTANCE)));
        cableType.setWeight(Utility.toFloat(getRecordValue(record, CableTypeColumn.WEIGHT)));
        cableType.setDiameter(Utility.toFloat(getRecordValue(record, CableTypeColumn.DIAMETER)));
        cableType.setManufacturers(manufacturers);
        cableType.setComments(getRecordValue(record, CableTypeColumn.COMMENTS, true, true));

        // revision is optional column/field
        //     administrator only
        //     previous value to consider
        if (canAdminister) {
            cableType.setRevision(getRecordValue(record, CableTypeColumn.REVISION));
        }

        if (test) {
            updateRowsTest++;
        } else {
            if (cableTypeService.updateCableType(cableType, oldCableType, loggedInName)) {
                updateRows++;
            }
            report.addAffected(cableType);
        }
        report.addMessage(
                new ValidationMessage("Updating cable type '" + name + "'.", false, record.getRowLabel(), null));
    }

    /** Processes the record for cable type deletion. */
    private void deleteCableType(DSRecord record) {
        if (!checkCableTypeName(record)) {
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        final String name = getRecordValue(record, CableTypeColumn.NAME);
        if (StringUtils.isBlank(name)) {
            report.addMessage(getErrorMessage("Cable type NAME/CODE not specified.", record, CableTypeColumn.NAME));
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        final CableType cableType = getCableTypeFromRecord(record);
        if (cableType == null) {
            report.addMessage(getErrorMessage("Cable type '" + name + "' does not exist in the database.", record,
                    CableTypeColumn.NAME, name));
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        if (!cableType.isActive()) {
            report.addMessage(new ValidationMessage("Cable '" + name + "' is already obsolete.", true,
                    record.getRowLabel(), null, name));
        }

        if (report.isError() && !test)
            return;

        if (test) {
            deleteRowsTest++;
        } else {
            if (cableTypeService.changeCableTypeStatus(cableType, false, loggedInName)) {
                deleteRows++;
            }
            report.addAffected(cableType);
        }
        report.addMessage(
                new ValidationMessage("Obsoleting cable type '" + name + "'.", false, record.getRowLabel(), null));
    }

    /**
     * Return value for column in record.
     * Assumes empty value is to be replaced with null value.
     *
     * @param record record
     * @param column column
     * @return value for column in record
     */
    private String getRecordValue(DSRecord record, CableTypeColumn column) {
        return getRecordValue(record, column, true, false);
    }

    /**
     * Return value for column in record.
     *
     * @param record record
     * @param column column
     * @param nullIfEmpty if empty value is to be replaced with null value
     * @param trimFrom true if leading and trailing whitespaces are to be trimmed, false if all whitespaces are to be trimmed.
     *                 Only relevant when nullIfEmpty is true.
     * @return value for column in record
     */
    private String getRecordValue(DSRecord record, CableTypeColumn column, boolean nullIfEmpty, boolean trimFrom) {
        return nullIfEmpty
                ? trimFrom
                        ? Utility.formatRecordValueTrimFrom(record.getField(column).toString())
                        : Utility.formatRecordValue(record.getField(column).toString())
                : Utility.formatWhitespace(record.getField(column).toString());
    }

    /** Check that the record has all expected columns. */
    private void checkAllColumnsPresent(DSRecord record) {
        for (final CableTypeColumn column : CableTypeColumn.getColumns()) {
            if (column.isExcelVolatileColumn()) {
                // if volatile column/field, presence in Excel not guaranteed
                //     @see CableTypeColumn#REVISION
                continue;
            }

            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by using an old template or importing a wrong file.", true));
            }
        }
    }

    /** Check that the cable type attributes are valid. */
    private void checkCableTypeAttributeValidity(DSRecord record) {
        final String name = getRecordValue(record, CableTypeColumn.NAME);
        try {
            if (StringUtils.isBlank(name)) {
                report.addMessage(getErrorMessage("NAME/CODE is not specified.", record, CableTypeColumn.NAME));
            }
            validateStringSize(name, CableTypeColumn.NAME);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.NAME, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableTypeColumn.NAME, name));
        }

        final String installationType = getRecordValue(record, CableTypeColumn.INSTALLATION_TYPE);
        if (StringUtils.isBlank(installationType)) {
            report.addMessage(
                    getErrorMessage("INSTALLATION TYPE is not specified.", record, CableTypeColumn.INSTALLATION_TYPE));
        } else {
            if (InstallationType.convertToInstallationType(Utility.upperCase(installationType)) == null) {
                LOGGER.log(Level.FINEST, "Invalid INSTALLATION TYPE");
                report.addMessage(
                        getErrorMessage("INSTALLATION TYPE '" + installationType + "' is not of a known type.", record,
                                CableTypeColumn.INSTALLATION_TYPE, installationType));
            }
        }

        try {
            Utility.toInteger(getRecordValue(record, CableTypeColumn.VOLTAGE_RATING));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("VOLTAGE RATING must be a number.", record,
                    CableTypeColumn.VOLTAGE_RATING, getRecordValue(record, CableTypeColumn.VOLTAGE_RATING)));
        }

        try {
            Utility.toFloat(getRecordValue(record, CableTypeColumn.RADIATION_RESISTANCE));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("RADIATION RESISTANCE must be a number.", record,
                    CableTypeColumn.RADIATION_RESISTANCE,
                    getRecordValue(record, CableTypeColumn.RADIATION_RESISTANCE)));
        }

        try {
            Utility.toFloat(getRecordValue(record, CableTypeColumn.WEIGHT));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("WEIGHT must be a number.", record, CableTypeColumn.WEIGHT,
                    getRecordValue(record, CableTypeColumn.WEIGHT)));
        }

        try {
            Utility.toFloat(getRecordValue(record, CableTypeColumn.DIAMETER));
        } catch (NumberFormatException e) {
            report.addMessage(getErrorMessage("DIAMETER must be a number.", record, CableTypeColumn.DIAMETER,
                    getRecordValue(record, CableTypeColumn.DIAMETER)));
        }

        final List<String> manufacturerNames = Utility.splitStringIntoList(getRecordValue(record, CableTypeColumn.MANUFACTURERS, false, false));
        if (!Utility.isNullOrEmpty(manufacturerNames)) {
            for (String manufacturer : manufacturerNames) {
                if (!StringUtils.isEmpty(manufacturer) && manufacturerService.getManufacturer(manufacturer) == null) {
                    report.addMessage(getErrorMessage("Specified MANUFACTURER does not exit.", record,
                            CableTypeColumn.MANUFACTURERS, manufacturer));
                }
            }
            final HashSet<String> manufacturerNamesSet = new HashSet<String>(manufacturerNames);
            if (manufacturerNamesSet.size() != manufacturerNames.size()) {
                report.addMessage(getErrorMessage("Only one MANUFACTURER of each type can be added.", record,
                        CableTypeColumn.MANUFACTURERS));
            }
        }

        final String description = getRecordValue(record, CableTypeColumn.DESCRIPTION, true, true);
        try {
            validateStringSize(description, CableTypeColumn.DESCRIPTION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.DESCRIPTION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.DESCRIPTION, description));
        }

        final String service = getRecordValue(record, CableTypeColumn.SERVICE);
        try {
            validateStringSize(service, CableTypeColumn.SERVICE);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.SERVICE, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.SERVICE, service));
        }

        final String insulation = getRecordValue(record, CableTypeColumn.INSULATION);
        try {
            validateStringSize(insulation, CableTypeColumn.INSULATION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.INSULATION, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.INSULATION, insulation));
        }

        final String jacket = getRecordValue(record, CableTypeColumn.JACKET);
        try {
            validateStringSize(jacket, CableTypeColumn.JACKET);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.JACKET, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableTypeColumn.JACKET, jacket));
        }

        final String flamability = getRecordValue(record, CableTypeColumn.FLAMABILITY);
        try {
            validateStringSize(flamability, CableTypeColumn.FLAMABILITY);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.FLAMABILITY, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.FLAMABILITY, flamability));
        }

        final String comments = getRecordValue(record, CableTypeColumn.COMMENTS, true, true);
        try {
            validateStringSize(comments, CableTypeColumn.COMMENTS);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.COMMENTS, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableTypeColumn.COMMENTS, comments));
        }

        // revision is optional column/field
        //     administrator only
        if (canAdminister) {
            String revision = null;
            try {
                revision = getRecordValue(record, CableTypeColumn.REVISION);
                validateStringSize(revision, CableTypeColumn.REVISION);
            } catch (IllegalArgumentException e) {
                revision = null;
            } catch (DbFieldLengthViolationException e) {
                revision = null;
                LOGGER.log(Level.FINEST, INVALID + CableTypeColumn.REVISION, e);
                report.addMessage(
                        getErrorMessage(e.getMessage(), record, CableTypeColumn.REVISION, revision));
            }
        }
    }

    /**
     * Returns the cable type corresponding to the name of the given record.
     *
     * @param record
     *            the record
     * @return the corresponding cable type or null if none
     */
    private CableType getCableTypeFromRecord(DSRecord record) {
        return cableTypeService.getCableTypeByName(getRecordValue(record, CableTypeColumn.NAME));
    }

    private void validateStringSize(final String value, final CableTypeColumn column) {
        super.validateStringSize(value, column, CableType.class);
    }

    /**
     * Checks the validity of cableType name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkCableTypeName(DSRecord record) {
        final String name = getRecordValue(record, CableTypeColumn.NAME);
        if (StringUtils.isBlank(name)) {
            report.addMessage(getErrorMessage("CableType name must be specified", record, CableTypeColumn.NAME));
            return false;
        }
        final CableType cableType = cableTypeService.getCableTypeByName(name);
        if (cableType == null) {
            report.addMessage(getErrorMessage("CableType with name " + name + " does not exist in the database.",
                    record, CableTypeColumn.NAME, name));
            return false;
        }
        return true;
    }

}
