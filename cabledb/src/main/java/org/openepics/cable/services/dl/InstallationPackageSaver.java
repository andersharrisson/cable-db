/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;

import org.openepics.cable.model.InstallationPackage;

/**
 * This saves {@link InstallationPackage} instances to Excel spreadsheet.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class InstallationPackageSaver extends DataSaverExcelStream<InstallationPackage> {

    /**
     * Constructs an instance to save the given {@link InstallationPackage} objects.
     *
     * @param objects
     *            the objects to save
     */
    public InstallationPackageSaver(Iterable<InstallationPackage> objects) {
        super(objects);

        saveAndValidate();
    }

    @Override
    protected void save(InstallationPackage installationPackage) {
        setCommand(DSCommand.UPDATE);

        setValue(InstallationPackageColumn.NAME,
                installationPackage.getName());
        setValue(InstallationPackageColumn.DESCRIPTION,
                installationPackage.getDescription());
        setValue(InstallationPackageColumn.LOCATION,
                installationPackage.getLocation());
        setValue(InstallationPackageColumn.CABLE_COORDINATOR,
                installationPackage.getCableCoordinator());
        setValue(InstallationPackageColumn.ROUTING_DOCUMENTATION,
                installationPackage.getRoutingDocumentation());
        setValue(InstallationPackageColumn.INSTALLER_CABLE,
                installationPackage.getInstallerCable());
        setValue(InstallationPackageColumn.INSTALLER_CONNECTOR_A,
                installationPackage.getInstallerConnectorA());
        setValue(InstallationPackageColumn.INSTALLER_CONNECTOR_B,
                installationPackage.getInstallerConnectorB());
        setValue(InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER,
                installationPackage.getInstallationPackageLeader());
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_installation_packages.xlsx");
    }
}
