/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.ejb.Startup;

import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;

/**
 * This is a service that tests that authentication and authorization service can be reached on application start up. In
 * case of error an exception is thrown.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
public class AuthenticationServiceStartup {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationServiceStartup.class.getName());

    private static boolean validated;

    /** Tests that the authentication and authorization service is reachable, and throws an exception if it isn't. */
    public synchronized void validate() {

        if (validated) {
            return;
        }

        LOGGER.log(Level.INFO, "Checking for presence of authentication and authorization service.");

        try {
            final ISecurityFacade securityFacade = SecurityFacade.getDefaultInstance();
            final String rbacVersion = securityFacade.getRBACVersion();
            LOGGER.log(Level.INFO, "Using RBAC version " + rbacVersion + ".");

        } catch (SecurityFacadeException e) {
            throw new RuntimeException(e);
        }

        validated = true;
    }
}
