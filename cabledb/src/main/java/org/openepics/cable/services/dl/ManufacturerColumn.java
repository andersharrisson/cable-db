/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the Manufacturer table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum ManufacturerColumn implements FieldIntrospection {

    NAME("MANUFACTURER NAME", "name", "Name", true, true),
    ADDRESS("MANUFACTURER ADDRESS", "address", "Address", true, true),
    PHONE("MANUFACTURER PHONE NUMBER", "phoneNumber", "Phone Number", true, true),
    EMAIL("MANUFACTURER EMAIL", "email", "Email", true, true),
    COUNTRY("MANUFACTURER COUNTRY", "country", "Country", true, true),
    STATUS("STATUS", "status", "Status", true, true);

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isStringComparisonOperator;

    private ManufacturerColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Return manufacturer column given column label.
     *
     * @param columnLabel column label
     * @return manufacturer column
     */
    public static ManufacturerColumn convertColumnLabel(String columnLabel) {
        if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (ADDRESS.getColumnLabel().equals(columnLabel)) {
            return ADDRESS;
        } else if (PHONE.getColumnLabel().equals(columnLabel)) {
            return PHONE;
        } else if (EMAIL.getColumnLabel().equals(columnLabel)) {
            return EMAIL;
        } else if (COUNTRY.getColumnLabel().equals(columnLabel)) {
            return COUNTRY;
        }
        return null;
    }
}
