package org.openepics.cable.services.dl;

public class DbFieldLengthViolationException extends RuntimeException {

    private static final long serialVersionUID = -7367328797455776721L;

    public DbFieldLengthViolationException() {
        super();
    }

    public DbFieldLengthViolationException(String message) {
        super(message);
    }

    public DbFieldLengthViolationException(Throwable cause) {
        super(cause);
    }

    public DbFieldLengthViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
