/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.DisplayView;
import org.openepics.cable.model.DisplayViewColumn;
import org.openepics.cable.model.EntityType;

/**
 * <code>DisplayViewService</code> is the service layer that handles individual DisplayView, DisplayView column
 * operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class DisplayViewService {

    @PersistenceContext
    private EntityManager em;

    /**
     * @param owner
     *            DisplayView owner
     *
     * @return a list of all displayViews which belongs to the currently signed in user
     */
    public List<DisplayView> getDisplayViews(String owner) {
        return em.createQuery("SELECT q FROM DisplayView q WHERE q.owner = :owner ORDER BY q.created DESC",
                DisplayView.class).setParameter("owner", owner).getResultList();
    }

    /**
     * @param owner
     *            DisplayView owner
     * @param entityType
     *            entityType of the current data
     *
     * @return last three executed DisplayViews which belongs to the currently signed in user
     */
    public List<DisplayView> getLastThreeDisplayViews(String owner, EntityType entityType) {
        List<DisplayView> list = em
                .createQuery("SELECT q FROM DisplayView q WHERE q.owner = :owner AND"
                        + " q.entityType = :entityType AND" + " q.executed IS NOT NULL ORDER BY q.executed DESC",
                        DisplayView.class)
                .setParameter("owner", owner).setParameter("entityType", entityType).setMaxResults(3).getResultList();
        return list;
    }

    /**
     * Saves new DisplayView into database.
     *
     * @param description
     *            DisplayView description
     * @param owner
     *            DisplayView owner
     * @param entityType
     *            entityType
     * @param displayViewColumns
     *            displayViewColumns
     * @return DisplayView
     */
    public DisplayView saveDisplayView(String description, EntityType entityType, String owner,
            List<DisplayViewColumn> displayViewColumns) {

        final Date created = new Date();
        final DisplayView displayView = new DisplayView(description, entityType, owner, created, displayViewColumns);
        em.persist(displayView);
        return displayView;
    }

    public boolean isDescriptionUnique(String description, String owner, EntityType entityType) {
        List<DisplayView> queries = em
                .createQuery("SELECT q FROM DisplayView q WHERE q.owner = :owner AND q.description ="
                        + " :description AND q.entityType = :entityType", DisplayView.class)
                .setParameter("owner", owner).setParameter("description", description)
                .setParameter("entityType", entityType).setMaxResults(1).getResultList();
        return queries.isEmpty();
    }

    /**
     * Updates DisplayView.
     *
     * @param displayView
     *            new displayView
     *
     * @return merged DisplayView
     */
    public DisplayView updateDisplayView(DisplayView displayView) {
        return em.merge(displayView);
    }

    public void deleteDisplayView(DisplayView displayView) {
        DisplayView displayViewFromDB = em.find(DisplayView.class, displayView.getId());
        em.remove(displayViewFromDB);
    }

    public DisplayView getDisplayViewById(long id) {
        return em.find(DisplayView.class, id);
    }
}
