/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.services.dl;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openepics.cable.services.DateUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * Abstract super class for store and save data to Excel.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 *
 * @param <T> type
 */
public abstract class DataSaverExcelBase<T> {

    private static final String SLASH = "/";

    protected Sheet sheet;
    Workbook workbook;
    int currentRow;
    int endRowIndex;
    int maxRowIndex;
    protected DSHeader header;

    protected Drawing drawing;
    protected CreationHelper creationHelper;
    protected ClientAnchor anchor;

    CellStyle unlockedCellStyle;
    CellStyle hyperlinkCellStyle;
    CellStyle dateCellStyle;
    CellStyle unlockedHighlightCellStyle;
    CellStyle hyperlinkHighlightCellStyle;
    CellStyle dateHighlightCellStyle;

    /**
     * Saves the stored data objects to spreadsheet.
     *
     * @return the input stream containing the spreadsheet
     */
    public InputStream save() {
        return workbookToInputStream(workbook);
    }

    /**
     * Writes the command string in the spreadsheet at the specified row.
     *
     * The {@link #save()} method performs all the required command writing, this method is public only for the purpose
     * of tests.
     *
     * @param rowIndex
     *            the row index, 0 for the first object
     * @param command
     *            the command to write; if {@link DSCommand#NONE} or {@link DSCommand#OPERATION}, nothing is written
     */
    public void setCommand(int rowIndex, DSCommand command) {
        if (command != DSCommand.NONE || command != DSCommand.OPERATION) {
            setValue("Operation", rowIndex + endRowIndex, 0, command);
        }
    }

    private InputStream workbookToInputStream(Workbook workbook) {
        try {
            final InputStream inputStream;
            File temporaryFile = File.createTempFile("temp", "xlsx");
            FileOutputStream outputStream = new FileOutputStream(temporaryFile);
            workbook.write(outputStream);
            outputStream.close();
            inputStream = new FileInputStream(temporaryFile);
            temporaryFile.delete();

            return inputStream;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Saves the object by calling setValue(...) and setCommand(...) methods.
     *
     * @param object
     *            the object to be saved
     */
    protected abstract void save(T object);

    /**
     * Override this method to set column valid values. Base
     * implementation does nothing.
     */
    void setColumnsValidValues() {}

    /**
     * Writes the command string in the spreadsheet at the current row.
     *
     * @param command
     *            the command to write; if {@link DSCommand#NONE}, nothing is written
     */
    protected void setCommand(DSCommand command) {
        if (command != DSCommand.NONE) {
            setValue("Command", currentRow, 0, command);
        }
    }

    /**
     * Writes the comment string in the spreadsheet at the current row instead of command.
     *
     * @param comment
     *            to write under the command column.
     */
    void setCommandComment(String comment) {
        setValue("Command", currentRow, 0, "#" + comment);
    }

    /**
     * Stores the value in the spreadsheet at the specified row and the specified column.
     *
     * The {@link #save()} method performs all the required storing, this method is public only for the purpose of
     * tests.
     *
     * @param rowIndex
     *            the row index, 0 for the first object
     * @param columnName
     *            the column name
     * @param value
     *            the value to store
     */
    public void setValue(int rowIndex, Object columnName, Object value) {
        setValue(columnName, rowIndex + endRowIndex, header.getColumnIndex(columnName.toString()), value);
    }

    /**
     * Stores the value in the spreadsheet at the current row and the specified column.
     *
     * @param name column name
     * @param value value to store
     */
    void setValue(Object name, Object value) {
        setValue(name, currentRow, header.getColumnIndex(name.toString()), value);
    }

    /**
     * Stores the value in the spreadsheet at the current row and the specified column,
     * possibly with highlighted cell.
     *
     * @param name column name
     * @param value value to store
     * @param highlight if cell is to be highlighted, with adjusted foreground color and borders
     */
    void setValue(Object name, Object value, boolean highlight) {
        setValue(name, currentRow, header.getColumnIndex(name.toString()), value, highlight, null);
    }

    /**
     * Stores the value in the spreadsheet at the current row and the specified column,
     * possibly with highlighted cell and cell comment.
     *
     * @param name column name
     * @param value value to store
     * @param highlight if cell is to be highlighted, with adjusted foreground color and borders
     * @param comment cell comment
     */
    void setValue(Object name, Object value, boolean highlight, String comment) {
        setValue(name, currentRow, header.getColumnIndex(name.toString()), value, highlight, comment);
    }

    private void setValue(Object name, int row, int column, Object value) {
        setValue(name, row, column, value, false, null);
    }

    private void setValue(Object name, int row, int column, Object value, boolean highlight, String comment) {
        if (sheet.getRow(row) == null) {
            sheet.createRow(row);
        }

        final Row rowObject = sheet.getRow(row);
        if (rowObject.getCell(column) == null) {
            final Cell cell = rowObject.createCell(column);
            cell.setCellStyle(unlockedCellStyle);
        }

        final Cell cell = rowObject.getCell(column);
        cell.setCellStyle(highlight ? unlockedHighlightCellStyle : unlockedCellStyle);

        // cell comment
        if (!StringUtils.isEmpty(comment)) {
            anchor.setCol1(cell.getColumnIndex());
            anchor.setCol2(cell.getColumnIndex() + 1);
            anchor.setRow1(cell.getRowIndex());
            anchor.setRow2(cell.getRowIndex() + 3);
            Comment cellComment = drawing.createCellComment(anchor);
            RichTextString richTextString = creationHelper.createRichTextString(comment);
            cellComment.setString(richTextString);
            cellComment.setAuthor("Cable DB");
            cell.setCellComment(cellComment);
        }

        if (value instanceof String) {
            if (!((String) value).trim().isEmpty()) {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue((String) value);
            } else {
                cell.setCellType(Cell.CELL_TYPE_BLANK);
            }

        } else if (value instanceof Integer) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue(((Integer) value).doubleValue());

        } else if (value instanceof Double) {
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue((Double) value);

        } else if (value instanceof Date) {
            cell.setCellStyle(highlight ? dateHighlightCellStyle : dateCellStyle);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            cell.setCellValue((Date) value);
        } else if (value instanceof URL) {
            Hyperlink link = creationHelper.createHyperlink(Hyperlink.LINK_URL);
            link.setAddress(value.toString());
            cell.setHyperlink(link);
            cell.setCellStyle(highlight ? hyperlinkHighlightCellStyle : hyperlinkCellStyle);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(getURLLastSegment(value.toString()));
        } else if (value == null) {
            cell.setCellType(Cell.CELL_TYPE_BLANK);
        } else {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(value.toString());
        }
    }

    CellStyle getHyperlinkCellStyle() {
        return getHyperlinkCellStyle(false);
    }
    CellStyle getHyperlinkCellStyle(boolean highlight) {
        CellStyle cellStyle = getUnlockedCellStyle(highlight);
        Font font = workbook.createFont();
        font.setUnderline(Font.U_SINGLE);
        font.setColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFont(font);
        return cellStyle;
    }

    CellStyle getDateCellStyle() {
        return getDateCellStyle(false);
    }
    CellStyle getDateCellStyle(boolean highlight) {
        CellStyle cellStyle = getUnlockedCellStyle(highlight);
        DataFormat format = workbook.getCreationHelper().createDataFormat();
        cellStyle.setDataFormat(format.getFormat(DateUtil.DATE_FORMAT_STRING));
        return cellStyle;
    }

    CellStyle getUnlockedCellStyle() {
        return getUnlockedCellStyle(false);
    }
    CellStyle getUnlockedCellStyle(boolean highlight) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setLocked(false);
        if (highlight) {
            cellStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
            cellStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
            cellStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
            cellStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
            cellStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
            cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        return cellStyle;
    }

    private String getURLLastSegment(String urlString) {
        // remove trailing slash
        final String cleanedUrlString = urlString.endsWith(SLASH) ? urlString.substring(0, urlString.length() - 1)
                : urlString;

        return cleanedUrlString.substring(cleanedUrlString.lastIndexOf(SLASH) + 1, cleanedUrlString.length());
    }
}
