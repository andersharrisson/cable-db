/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.security.InvalidParameterException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.openepics.cable.model.Artifact;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableName;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.model.RoutingRow;
import org.openepics.cable.services.dl.CableColumn;
import org.openepics.cable.ui.CableColumnUI;
import org.openepics.cable.util.UiUtility;
import org.primefaces.model.SortOrder;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * <code>CableService</code> is the service layer that handles individual cable operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableService {

    private static final Logger LOGGER = Logger.getLogger(CableService.class.getName());
    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;
    @Inject
    private QueryService queryService;
    @Inject
    private NamesServiceCache namesServiceCache;
    @Inject
    private MailService mailService;
    @Inject
    private UserDirectoryServiceCache userDirectoryServiceCache;

    /** @return a list of all cables */
    public List<Cable> getCables() {
        LOGGER.log(Level.FINEST, "Getting all cables from database");
        return findLazy(0, Integer.MAX_VALUE, CableColumnUI.NAME.getValue(), SortOrder.ASCENDING,
                new HashMap<String, Object>(), null);
    }

    /**
     * Returns a list of cables filtered by regural expression.
     *
     * @param fields
     *            filter fields
     * @param regExp
     *            filter regular expression
     *
     * @return a list of cables filtered by regular expression.
     */
    public List<Cable> getFilteredCablesByRegExp(List<CableColumn> fields, String regExp) {
        LOGGER.log(Level.FINEST, "Retrieving cables by regular expression: " + regExp);
        LOGGER.log(Level.FINEST, "	Creating query from regular expression");
        final List<QueryCondition> conditions = new ArrayList<QueryCondition>();
        final Query query = new Query("regExprQuery", EntityType.CABLE, null, null, conditions);

        for (int i = 0; i < fields.size(); i++) {
            conditions.add(new QueryCondition(query, QueryParenthesis.NONE, fields.get(i).getColumnLabel(),
                    QueryComparisonOperator.LIKE,
                    Strings.isNullOrEmpty(regExp) ? "%" : regExp.replace('*', '%').replace('?', '_'),
                    QueryParenthesis.NONE,
                    i == (fields.size() - 1) ? QueryBooleanOperator.NONE : QueryBooleanOperator.AND, i));
        }

        return findLazy(0, Integer.MAX_VALUE, CableColumnUI.NAME.getValue(), SortOrder.ASCENDING,
                new HashMap<String, Object>(), query);
    }

    /**
     * Returns a cable with the specified cable name.
     *
     * @param name
     *            the cable name (example 12A012345)
     * @return the cable, or null if the cable with such name cannot be found
     */
    public Cable getCableByName(String name) {
        try {
            LOGGER.log(Level.FINEST, "Searching for cable with name:" + name);
            final CableName cableName = new CableName(name);

            return em
                    .createQuery("SELECT c FROM Cable c WHERE c.system = :system AND c.subsystem = :subsystem"
                            + " AND c.cableClass = :cableClass AND c.seqNumber = :seqNumber", Cable.class)
                    .setParameter("system", cableName.getSystem()).setParameter("subsystem", cableName.getSubsystem())
                    .setParameter("cableClass", cableName.getCableClass())
                    .setParameter("seqNumber", cableName.getSeqNumber()).getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable with name: " + name + " not found");
            return null;
        }
    }

    /**
     * Returns a list of cables with the specified names. Also gets all cable lazy fetched fields eagerly.
     *
     * @param names
     *            the cable names
     * @return the cables
     */
    public List<Cable> getCablesByName(Iterable<String> names) {

        try {
            LOGGER.log(Level.FINEST, "Searching for cables by name");
            List<Integer> seqNumbers = new ArrayList<>();
            for (String name : names) {
                final CableName cableName = new CableName(name);
                seqNumbers.add(cableName.getSeqNumber());
            }
            if (seqNumbers.size() <= 0) {
                return new ArrayList<>();
            }

            final CriteriaBuilder cb = em.getCriteriaBuilder();
            final CriteriaQuery<Cable> cq = cb.createQuery(Cable.class);
            final Root<Cable> cableRecord = cq.from(Cable.class);
            // Joins to force fetch all of the cable external fields
            cableRecord.fetch("cableType", JoinType.LEFT);
            cableRecord.fetch("endpointA", JoinType.LEFT);
            cableRecord.fetch("endpointB", JoinType.LEFT);
            cableRecord.fetch("qualityReport", JoinType.LEFT);
            cableRecord.fetch("routingRows", JoinType.LEFT);
            cableRecord.fetch("owners", JoinType.LEFT);
            cq.select(cableRecord);

            cq.where(cableRecord.get("seqNumber").in(seqNumbers));

            final TypedQuery<Cable> query = em.createQuery(cq);
            List<Cable> cables = query.getResultList();

            LOGGER.log(Level.FINEST, "Finished loading cables");
            return cables;

        } catch (IllegalArgumentException | NoResultException e) {
            return new ArrayList<>();
        }
    }

    /**
     * Refreshes the length of the cable based on auto-calculation values. TODO Make this happen automatically on the
     * model, remove all independent calculations
     *
     * @param cable
     *            the cable to refresh
     * @return the new length
     */
    public Float refreshCableLength(Cable cable) {
        Float length = cable.getLength();
        if (cable.getAutoCalculatedLength() == CableAutoCalculatedLength.YES) {
            length = cable.getBaseLength();
            for (RoutingRow routing : cable.getRoutingRows()) {
                length += routing.getRouting().getLength();
            }
            cable.setLength(length);
        }
        return length;
    }

    /**
     * Returns a cable with the specified cable id.
     *
     * @param id
     *            the cable id
     * @return the cable, or null if the cable with such id cannot be found
     */
    public Cable getCableById(Long id) {
        return em.find(Cable.class, id);
    }

    /**
     * Creates a bunch of cables in the database and returns them.
     * 
     * @param cables
     *            cables to create in database
     * @param userId
     *            user creating cables
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should be send
     * @return the list of cables retrieved from the database
     */
    public List<Cable> createCables(List<Cable> cables, String userId, boolean sendNotification) {
        List<Cable> createdCables = new ArrayList<Cable>();
        if (cables.size() != 0) {
            int seqNumber = getUnusedCableSeqNumber();
            for (Cable c : cables) {
                Cable cable = createCable(c.getSystem(), c.getSubsystem(), c.getCableClass(), seqNumber, c.getOwners(),
                        c.getStatus(), c.getCableType(), c.getContainer(), c.getEndpointA(), c.getEndpointB(),
                        c.getRoutingRows(), c.getInstallationBy(), c.getTerminationBy(), c.getQualityReport(),
                        c.getAutoCalculatedLength(), c.getBaseLength(), c.getLength(), userId, sendNotification);
                seqNumber++;
                createdCables.add(cable);
            }
        }
        return createdCables;
    }

    /**
     * Creates a cable in the database and returns it.
     *
     * @param system
     *            the cable system digit
     * @param subsystem
     *            the cable subsystem digit
     * @param cableClass
     *            the cable class letter
     * @param owners
     *            the cable owners
     * @param status
     *            cable's status
     * @param cableType
     *            the cable type
     * @param container
     *            the container cable name of this cable
     * @param endpointA
     *            endpoint a of the cable
     * @param endpointB
     *            endpoint b of the cable
     * @param routingRows
     *            the routing value for this cable instance
     * @param installationBy
     *            the date by which this cable is installed
     * @param terminationBy
     *            the date by which this cable is terminated
     * @param qualityReport
     *            quality report artifact
     * @param autoCalculatedLength
     *            is route calculated automatically
     * @param baseLength
     *            base length
     * @param length
     *            the user specified cable length
     * @param userId
     *            username of user creating the cable, for history record
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should be send
     *
     * @return the created cable
     */
    public Cable createCable(String system, String subsystem, String cableClass, List<String> owners,
            CableStatus status, CableType cableType, String container, Endpoint endpointA, Endpoint endpointB,
            List<RoutingRow> routingRows, Date installationBy, Date terminationBy, Artifact qualityReport,
            CableAutoCalculatedLength autoCalculatedLength, Float baseLength, Float length, String userId,
            boolean sendNotification) {

        return createCable(system, subsystem, cableClass, getUnusedCableSeqNumber(), owners, status, cableType,
                container, endpointA, endpointB, routingRows, installationBy, terminationBy, qualityReport,
                autoCalculatedLength, baseLength, length, userId, sendNotification);
    }

    private Cable createCable(String system, String subsystem, String cableClass, Integer seqNumber,
            List<String> owners, CableStatus status, CableType cableType, String container, Endpoint endpointA,
            Endpoint endpointB, List<RoutingRow> routingRows, Date installationBy, Date terminationBy,
            Artifact qualityReport, CableAutoCalculatedLength autoCalculatedLength, Float baseLength, Float length,
            String userId, boolean sendNotification) {

        final Date created = new Date();
        final Date modified = created;
        final Cable cable = new Cable(system, subsystem, cableClass, seqNumber, owners, status, cableType, container,
                endpointA, endpointB, created, modified, routingRows, installationBy, terminationBy, qualityReport,
                autoCalculatedLength, length);
        cable.setBaseLength(baseLength);
        if (routingRows != null) {
            for (RoutingRow routingRow : routingRows) {
                routingRow.setCable(cable);
            }
        }
        cable.updateRoutings();
        em.persist(endpointA);
        em.persist(endpointB);
        em.persist(qualityReport);
        em.persist(cable);
        if (sendNotification) {
            sendCableStatusNotification(cable, userId);
        }

        historyService.createHistoryEntry(EntityTypeOperation.CREATE, cable.getName(), EntityType.CABLE, cable.getId(),
                "", userId);

        return cable;
    }

    /**
     * Updates the attributes on the given cables.
     *
     * @param cables
     *            the cables with modified attributes to save to the database.
     * @param oldCables
     *            the cables before modification. Should be in the same order as in cables. Also needs to be of the same
     *            size as cables.
     * @param userId
     *            username of user updating the cables, for history record
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should be send
     */
    public void updateCables(List<Cable> cables, List<Cable> oldCables, String userId, boolean sendNotification) {
        if (cables.size() != oldCables.size()) {
            throw new InvalidParameterException("The cables and oldCables lists are not of the same length.");
        }
        Integer seqNumber = null;
        if (cables.size() != 0) {
            seqNumber = getUnusedCableSeqNumber();
        }
        for (int i = 0; i < cables.size(); i++) {
            Cable cable = cables.get(i);
            Cable oldCable = oldCables.get(i);
            if (!cable.getSystem().equals(oldCable.getSystem()) || !cable.getSubsystem().equals(oldCable.getSubsystem())
                    || !cable.getCableClass().equals(oldCable.getCableClass())) {
                updateCable(cable, oldCable, seqNumber, userId, sendNotification);
                seqNumber++;
                continue;
            }
            updateCable(cable, oldCable, cable.getSeqNumber(), userId, sendNotification);

        }
    }

    /**
     * Updates the attributes on the given cable.
     *
     * @param cable
     *            the cable with modified attributes to save to the database
     * @param oldCable
     *            the cable before modification
     * @param userId
     *            username of user updating the cable, for history record
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should be send
     */
    public void updateCable(Cable cable, Cable oldCable, String userId, boolean sendNotification) {
        if (!cable.getSystem().equals(oldCable.getSystem()) || !cable.getSubsystem().equals(oldCable.getSubsystem())
                || !cable.getCableClass().equals(oldCable.getCableClass())) {
            updateCable(cable, oldCable, getUnusedCableSeqNumber(), userId, sendNotification);
            return;

        }
        updateCable(cable, oldCable, cable.getSeqNumber(), userId, sendNotification);
    }

    private void updateCable(Cable cable, Cable oldCable, Integer seqNumber, String userId, boolean sendNotification) {
        cable.setSeqNumber(seqNumber);

        cable.setModified(new Date());
        cable.updateRoutings();

        if (cable.getQualityReport() != null) {
	        if (cable.getQualityReport().getId() != null) {
	            em.merge(cable.getQualityReport());
	        } else {
	            em.persist(cable.getQualityReport());
	        }
        }
        em.merge(cable.getEndpointA());
        em.merge(cable.getEndpointB());
        em.merge(cable);

        if (oldCable.getStatus() != cable.getStatus() && sendNotification) {
            sendCableStatusNotification(cable, userId);
        }

        // History logging
        historyService.createHistoryEntry(EntityTypeOperation.UPDATE, cable.getName(), EntityType.CABLE, cable.getId(),
                getChangeString(cable, oldCable), userId);
    }

    /**
     * Marks the cables deleted in the database.
     *
     * @param cables
     *            the cables to delete
     * @param userId
     *            username of user deleting the cables, for history record
     */
    public void deleteCables(Iterable<Cable> cables, String userId) {
        for (Cable cable : cables) {
            deleteCable(cable, userId);
        }
    }

    /**
     * Marks the cable deleted in the database.
     *
     * @param cable
     *            the cable to delete
     * @return true if the cable was deleted, false if the cable was already deleted
     * @param userId
     *            username of user deleting the cable, for history record
     */
    public boolean deleteCable(Cable cable, String userId) {
        if (cable.getStatus() == CableStatus.DELETED) {
            return false;
        }

        cable.setStatus(CableStatus.DELETED);
        cable.setModified(new Date());
        em.merge(cable);

        historyService.createHistoryEntry(EntityTypeOperation.DELETE, cable.getName(), EntityType.CABLE, cable.getId(),
                "", userId);
        return true;
    }

    /**
     * Retrieves cables and validates them in one transaction to ensure no other changes happen in the mean time.
     * 
     * @param start
     *            the number of the starting cable to validate
     * @param size
     *            amount of cables to validate
     * @param userId
     *            username of the user invoking validation
     * @return true if validation was performed, false if there were no more cables to validate
     */
    public boolean getAndValidateCables(int start, int size, String userId) {
        final List<Cable> cables = findLazy(start, size, CableColumnUI.MODIFIED.getValue(), SortOrder.ASCENDING,
                new HashMap<String, Object>(), null);
        if (cables == null || cables.isEmpty()) {
            return false;
        }
        validateCables(userId, cables, true);
        return true;
    }

    /**
     * Validates the consistency of cable data and updates the data and/or status.
     * 
     * @param userId
     *            id of the user invoking validation
     * @param cables
     *            the cables to validate
     * @param externalValidation
     *            whether perform validation against data loaded from external services such as naming service, might be
     *            slow
     */
    public void validateCables(String userId, Iterable<Cable> cables, boolean externalValidation) {
        LOGGER.log(Level.FINEST, "Validating  cables");
        final Set<String> validEndpointNames = externalValidation ? namesServiceCache.getAllNames()
                : Collections.emptySet();
        LOGGER.log(Level.FINEST, "	Valid endpoint names acquired");
        Set<String> validContainers = getValidContainers(cables);

        for (final Cable cable : cables) {
            if (cable.getStatus() != CableStatus.DELETED) {
                final boolean containerValid = cable.getContainer() == null || cable.getContainer().isEmpty()
                        || (cable.getContainer() != cable.getName() && validContainers.contains(cable.getContainer()));
                final boolean endpointAValid = externalValidation
                        ? validEndpointNames.contains(cable.getEndpointA().getDevice()) : true;
                final boolean endpointBValid = externalValidation
                        ? validEndpointNames.contains(cable.getEndpointB().getDevice()) : true;
                final boolean cableValid = endpointAValid && endpointBValid && containerValid;
                final Cable.Validity cableValidity = cableValid ? Cable.Validity.VALID : Cable.Validity.DANGLING;
                final Endpoint.Validity endpointAValidity = endpointAValid ? Endpoint.Validity.VALID
                        : Endpoint.Validity.DANGLING;
                final Endpoint.Validity endpointBValidity = endpointBValid ? Endpoint.Validity.VALID
                        : Endpoint.Validity.DANGLING;

                final Endpoint endpointA = cable.getEndpointA();
                final Endpoint endpointB = cable.getEndpointB();

                final Cable.Validity oldCableValidity = cable.getValidity();
                final Endpoint.Validity oldEndpointAValidity = endpointA.getValidity();
                final Endpoint.Validity oldEndpointBValidity = endpointB.getValidity();

                final String oldRoutings = cable.getRoutingsString();
                cable.updateRoutings();
                final String newRoutings = cable.getRoutingsString();

                Float newLength = refreshCableLength(cable);

                if (oldCableValidity == cableValidity && oldEndpointAValidity == endpointAValidity
                        && oldEndpointBValidity == endpointBValidity
                        && cable.getAutoCalculatedLength() == CableAutoCalculatedLength.NO
                        || newLength == cable.getLength()) {
                    continue;
                }

                StringBuilder historyEntry = new StringBuilder(300);
                historyEntry.append(HistoryService.getDiffForAttributes("Validity", cableValidity, oldCableValidity))
                        .append("\n")
                        .append(HistoryService.getDiffForAttributes("Endpoint A validity", endpointAValidity,
                                oldEndpointAValidity))
                        .append("\n").append(HistoryService.getDiffForAttributes("Endpoint B validity",
                                endpointBValidity, oldEndpointBValidity))
                        .append("\n");
                if (newLength != cable.getLength()) {
                    cable.setLength(newLength);
                    historyEntry.append("Cable length was outdated, recalculated it.");
                }
                if (oldRoutings != newRoutings) {
                    historyEntry.append("Cable routings were outdated, updated them.");
                }

                cable.setValidity(cableValidity);
                endpointA.setValidity(endpointAValidity);
                endpointB.setValidity(endpointBValidity);

                em.merge(endpointA);
                em.merge(endpointB);
                em.merge(cable);

                historyService.createHistoryEntry(EntityTypeOperation.VALIDATE, cable.getName(), EntityType.CABLE,
                        cable.getId(), historyEntry.toString(), userId);
            }
        }
        LOGGER.log(Level.FINEST, "	Validation completed");
    }

    private Set<String> getValidContainers(Iterable<Cable> cables) {
        List<String> containerNames = new ArrayList<String>();
        for (Cable cable : cables) {
            containerNames.add(cable.getContainer());
        }
        List<Cable> containers = getCablesByName(containerNames);

        Set<String> validContainerNames = new HashSet<String>();
        for (Cable container : containers) {
            validContainerNames.add(container.getName());
        }
        return validContainerNames;
    }

    /** @return a sequential number that is currently not yet used by the database */
    public Integer getUnusedCableSeqNumber() {
        Integer maxNumber = em.createQuery("SELECT MAX(c.seqNumber) FROM Cable c", Integer.class).getSingleResult();
        return maxNumber != null ? maxNumber + 1 : 1;
    }

    /**
     * Generates and returns string with all changed cable attributes.
     *
     * @param cable
     *            new cable
     * @param oldCable
     *            old cable
     *
     * @return string with all changed cable attributes.
     */
    private String getChangeString(Cable cable, Cable oldCable) {
        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes("Name", cable.getName(), oldCable.getName()));
        sb.append(HistoryService.getDiffForAttributes("Owner", cable.getOwners(), oldCable.getOwners()));
        if (cable.getCableType() != null && oldCable.getCableType() != null) {
            sb.append(HistoryService.getDiffForAttributes("Type Id", String.valueOf(cable.getCableType().getId()),
                    String.valueOf(oldCable.getCableType().getId())));
        }
        if (cable.getContainer() != null && oldCable.getContainer() != null) {
            sb.append(HistoryService.getDiffForAttributes("Container", String.valueOf(cable.getContainer()),
                    String.valueOf(oldCable.getContainer())));
        }
        if (cable.getEndpointA() != null && oldCable.getEndpointA() != null) {
            sb.append(HistoryService.getDiffForAttributes("Device A", cable.getEndpointA().getDevice(),
                    oldCable.getEndpointA().getDevice()));
            sb.append(HistoryService.getDiffForAttributes("Building A", cable.getEndpointA().getBuilding(),
                    oldCable.getEndpointA().getBuilding()));
            sb.append(HistoryService.getDiffForAttributes("Rack A", cable.getEndpointA().getRack(),
                    oldCable.getEndpointA().getRack()));
            sb.append(HistoryService.getDiffForAttributes("Connector A", cable.getEndpointA().getConnector(),
                    oldCable.getEndpointA().getConnector()));
            sb.append(HistoryService.getDiffForAttributes("Drawing A", cable.getEndpointA().getDrawing(),
                    oldCable.getEndpointA().getDrawing()));
            sb.append(HistoryService.getDiffForAttributes("User Label Text A", cable.getEndpointA().getLabel(),
                    oldCable.getEndpointA().getLabel()));
        }
        if (cable.getEndpointB() != null && oldCable.getEndpointB() != null) {
            sb.append(HistoryService.getDiffForAttributes("Device B", cable.getEndpointB().getDevice(),
                    oldCable.getEndpointB().getDevice()));
            sb.append(HistoryService.getDiffForAttributes("Building B", cable.getEndpointB().getBuilding(),
                    oldCable.getEndpointB().getBuilding()));
            sb.append(HistoryService.getDiffForAttributes("Rack B", cable.getEndpointB().getRack(),
                    oldCable.getEndpointB().getRack()));
            sb.append(HistoryService.getDiffForAttributes("Connector B", cable.getEndpointB().getConnector(),
                    oldCable.getEndpointB().getConnector()));
            sb.append(HistoryService.getDiffForAttributes("Drawing B", cable.getEndpointB().getDrawing(),
                    oldCable.getEndpointB().getDrawing()));
            sb.append(HistoryService.getDiffForAttributes("User Label Text B", cable.getEndpointB().getLabel(),
                    oldCable.getEndpointB().getLabel()));
        }
        sb.append(HistoryService.getDiffForAttributes("Status", cable.getStatus(), oldCable.getStatus()));
        sb.append(HistoryService.getDiffForAttributes("Installation By", DateUtil.format(cable.getInstallationBy()),
                DateUtil.format(oldCable.getInstallationBy())));
        sb.append(HistoryService.getDiffForAttributes("Termination By", DateUtil.format(cable.getTerminationBy()),
                DateUtil.format(oldCable.getTerminationBy())));
        String cableQualityReportName =
        		cable.getQualityReport() != null ? cable.getQualityReport().getName() : "<None>";
        String oldCableQualityReportName =
        		oldCable.getQualityReport() != null ? oldCable.getQualityReport().getName() : "<None>";
        sb.append(HistoryService.getDiffForAttributes("Quality report", cableQualityReportName,
        		oldCableQualityReportName));
        sb.append(HistoryService.getDiffForAttributes("Length", String.valueOf(cable.getLength()),
                String.valueOf(oldCable.getLength())));
        sb.append(HistoryService.getDiffForAttributes("Routing", cable.getRoutingsString(),
                oldCable.getRoutingsString()));
        return sb.toString();
    }

    /**
     * Returns minimal data on cables
     *
     * @param customQuery
     *            customQuery for filtering
     * @return cables with reduced data
     */
    public List<Cable> getCableEndpoints(Query customQuery) {// TODO refactor cableEndpoint REST
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        final Root<Cable> cableRecord = cq.from(Cable.class);
        cq.multiselect(cableRecord.get("system"), cableRecord.get("subsystem"), cableRecord.get("cableClass"),
                cableRecord.get("seqNumber"), cableRecord.get("endpointA"), cableRecord.get("endpointB"),
                cableRecord.get("modified"), cableRecord.get("status"), cableRecord.get("validity"));
        List<Predicate> predicates = new ArrayList<Predicate>();
        Predicate predicate = addCustomQuery(cb, cableRecord, customQuery);

        if (predicate != null) {
            predicates.add(predicate);
        }

        cq.where(predicates.toArray(new Predicate[] {}));

        final TypedQuery<Tuple> query = em.createQuery(cq);
        List<Tuple> result = query.getResultList();
        List<Cable> cables = new ArrayList<Cable>();
        for (Tuple tuple : result) {
            Cable cable = new Cable((String) tuple.get(0), (String) tuple.get(1), (String) tuple.get(2),
                    (Integer) tuple.get(3), Arrays.asList("REST"), (CableStatus) tuple.get(7), null, null,
                    (Endpoint) tuple.get(4), (Endpoint) tuple.get(5), (Date) tuple.get(6), (Date) tuple.get(6), null,
                    null, null, null, CableAutoCalculatedLength.NO, (float) 0);
            cable.setValidity((Cable.Validity) tuple.get(8));
            cables.add(cable);
        }
        return cables;
    }

    /**
     * Returns only a subset of data based on sort column, sort order and filtered by all the fields.
     *
     * @param first
     *            the index of the first result to return
     * @param pageSize
     *            the number of results
     * @param sortField
     *            the field by which to sort
     * @param sortOrder
     *            ascending/descending
     * @param filters
     *            filters to use
     * @param customQuery
     *            query to add
     * @return The required entities.
     */
    public List<Cable> findLazy(final int first, final int pageSize, final @Nullable String sortField,
            final @Nullable SortOrder sortOrder, final @Nullable Map<String, Object> filters, Query customQuery) {
        LOGGER.log(Level.FINEST, "Lazy loading cables");
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Cable> cq = cb.createQuery(Cable.class);
        final Root<Cable> cableRecord = cq.from(Cable.class);
        final Fetch<Cable, CableType> cableTypeFetch = cableRecord.fetch("cableType", JoinType.LEFT);
        cableRecord.fetch("endpointA", JoinType.LEFT);
        cableRecord.fetch("endpointB", JoinType.LEFT);
        cableRecord.fetch("qualityReport", JoinType.LEFT);
        cq.select(cableRecord);

        addSortingOrder(sortField, sortOrder, cb, cq, cableRecord);

        List<Predicate> predicates = buildPredicateList(cb, cableRecord, filters);

        Predicate predicate = addCustomQuery(cb, cableRecord, customQuery);
        if (predicate != null) {
            predicates.add(predicate);
        }

        cq.where(predicates.toArray(new Predicate[] {}));

        final TypedQuery<Cable> query = em.createQuery(cq);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        List<Cable> cables = query.getResultList();

        LOGGER.log(Level.FINEST, "Finished loading cables");
        return cables;
    }

    private void addSortingOrder(final String sortField, final SortOrder sortOrder, final CriteriaBuilder cb,
            final CriteriaQuery<Cable> cq, final Root<Cable> cableRecord) {
        if ((sortField != null) && (sortOrder != null) && (sortOrder != SortOrder.UNSORTED)) {
            for (CableColumnUI column : CableColumnUI.values()) {
                if (column.getValue().equals(sortField)) {
                    switch (column) {
                    case NAME:
                        List<Order> orderList = new ArrayList<Order>();
                        if (sortOrder == SortOrder.ASCENDING) {
                            orderList.add(cb.asc(cableRecord.get(CableColumnUI.SYSTEM.getValue())));
                            orderList.add(cb.asc(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue())));
                            orderList.add(cb.asc(cableRecord.get(CableColumnUI.CLASS.getValue())));
                            orderList.add(cb.asc(cableRecord.get("seqNumber")));
                        } else {
                            orderList.add(cb.desc(cableRecord.get(CableColumnUI.SYSTEM.getValue())));
                            orderList.add(cb.desc(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue())));
                            orderList.add(cb.desc(cableRecord.get(CableColumnUI.CLASS.getValue())));
                            orderList.add(cb.desc(cableRecord.get("seqNumber")));
                        }
                        cq.orderBy(orderList);
                        break;
                    case MODIFIED:
                    case CABLE_TYPE:
                    case CONTAINER:
                    case STATUS:
                    case INSTALLATION_DATE:
                    case TERMINATION_DATE:
                    case LENGTH:
                    case QUALITY_REPORT:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING ? cb.asc(cableRecord.get(column.getValue()))
                                : cb.desc(cableRecord.get(column.getValue())));
                        break;
                    case DEVICE_A_NAME:
                    case DEVICE_A_BUILDING:
                    case DEVICE_A_WIRING:
                    case DEVICE_A_RACK:
                    case DEVICE_A_USER_LABEL:
                    case DEVICE_B_NAME:
                    case DEVICE_B_BUILDING:
                    case DEVICE_B_WIRING:
                    case DEVICE_B_RACK:
                    case DEVICE_B_USER_LABEL:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())))
                                : cb.desc(cb
                                        .lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()))));
                        break;
                    case DEVICE_B_CONNECTOR:
                    case DEVICE_A_CONNECTOR:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())
                                        .get("name")))
                                : cb.desc(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())
                                        .get("name"))));
                        break;
                    default:
                        cq.orderBy(
                                sortOrder == SortOrder.ASCENDING ? cb.asc(cb.lower(cableRecord.get(column.getValue())))
                                        : cb.desc(cb.lower(cableRecord.get(column.getValue()))));
                        break;
                    }
                    break;
                }
            }
        }
    }

    private List<Predicate> buildPredicateList(final CriteriaBuilder cb, final Root<Cable> cableRecord,
            final @Nullable Map<String, Object> filters) {
        final List<Predicate> predicates = Lists.newArrayList();

        for (CableColumnUI column : CableColumnUI.values()) {
            if (filters.containsKey(column.getValue())) {
                switch (column) {
                case NAME:
                    final String filter = filters.get(column.getValue()).toString();// acquiring cable name filter
                    String[] separatedFilters = filter.split("[a-zA-Z]", 2);// separating filter on first occurrence of
                                                                            // letter (cable class)
                    // Concating system, subsystem and class
                    Expression<String> exp1 = cb.concat(
                            cb.lower(cableRecord.get(CableColumnUI.SYSTEM.getValue()).as(String.class)),
                            cb.lower(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue()).as(String.class)));
                    exp1 = cb.concat(exp1, cb.lower(cableRecord.get(CableColumnUI.CLASS.getValue()).as(String.class)));
                    Expression<String> exp2 = cb.lower(cableRecord.get("seqNumber").as(String.class));

                    if (separatedFilters.length == 2) {// Letter was found
                        separatedFilters[0] = separatedFilters[0] + filter.charAt(separatedFilters[0].length());// Adding
                                                                                                                // letter
                                                                                                                // to
                                                                                                                // first
                                                                                                                // filter
                                                                                                                // string

                        predicates.add(cb.and(
                                cb.like(exp1, "%" + escapeDbString(separatedFilters[0].toLowerCase()) + "%", '\\'),
                                cb.like(exp2, escapeDbString(separatedFilters[1]).replaceFirst("^0+", "") + "%",
                                        '\\')));
                    } else {// Letter was not found
                        predicates.add(cb.or(cb.like(exp1, "%" + escapeDbString(separatedFilters[0]) + "%", '\\'),
                                cb.like(exp2, "%" + escapeDbString(separatedFilters[0].replaceFirst("^0+", "")) + "%",
                                        '\\')));
                    }
                    break;

                case LENGTH:
                case STATUS:
                case MODIFIED:
                case INSTALLATION_DATE:
                case TERMINATION_DATE:
                case AUTOCALCULATEDLENGTH:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case CABLE_TYPE:
                case QUALITY_REPORT:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case DEVICE_A_NAME:
                case DEVICE_A_BUILDING:
                case DEVICE_A_WIRING:
                case DEVICE_A_RACK:
                case DEVICE_A_USER_LABEL:
                case DEVICE_B_NAME:
                case DEVICE_B_BUILDING:
                case DEVICE_B_WIRING:
                case DEVICE_B_RACK:
                case DEVICE_B_USER_LABEL:
                    predicates.add(cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case DEVICE_A_CONNECTOR:
                case DEVICE_B_CONNECTOR:
                    predicates.add(cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                default:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue())),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;

                }
            }
        }
        return predicates;
    }

    /**
     * Parses customQuery and adds its conditions to predicate, updates query executionDate.
     * 
     * @param cb
     *            criteriaBuilder
     * @param cableRecord
     *            cableRecord
     * @param customQuery
     *            custom Query to parse
     * @return predicate build from custom query conditions, null if there is no conditions or customQuery is null
     */
    private Predicate addCustomQuery(final CriteriaBuilder cb, final Root<Cable> cableRecord, Query customQuery) {
        if (customQuery == null || customQuery.getConditions().isEmpty()) {
            return null;
        }
        // make a copy list of conditions
        List<QueryCondition> queryConditions = new ArrayList<QueryCondition>(customQuery.getConditions());
        Predicate predicate;

        predicate = buildPredicate(cb, cableRecord, queryConditions);

        // Update query execution date only if it is already in database (it was not build in logic layer for support)
        if (customQuery.getId() != null) {
            customQuery.updateExecutionDate(new Date());
            queryService.updateQuery(customQuery);
        }

        return predicate;
    }

    /**
     * Creates a predicate out of given query conditions
     * 
     * @param cb
     *            criteria builder
     * @param cableRecord
     *            cable record
     * @param queryConditions
     *            query conditions
     * @return parsed predicate
     */
    private Predicate buildPredicate(final CriteriaBuilder cb, final Root<Cable> cableRecord,
            List<QueryCondition> queryConditions) {

        queryConditions.sort(new Comparator<QueryCondition>() {
            @Override
            public int compare(QueryCondition o1, QueryCondition o2) {
                return o1.getPosition().compareTo(o2.getPosition());
            }
        });

        // get 1st conditinon
        QueryCondition condition = queryConditions.get(0);
        queryConditions.remove(0);

        // parse 1st condition
        Predicate predicate = parseQueryCondition(cb, cableRecord, condition);
        Predicate predicate2;

        QueryBooleanOperator operator = condition.getBooleanOperator();

        if (QueryBooleanOperator.NOT.equals(operator)) {
            predicate = cb.not(predicate);
        }

        // we add queries until we run out of them or get to an open parenthesis
        while (!queryConditions.isEmpty()) {
            condition = queryConditions.get(0);
            if (!QueryParenthesis.OPEN.equals(condition.getParenthesisOpen())) {
                predicate2 = parseQueryCondition(cb, cableRecord, condition);
                queryConditions.remove(0);
                if (QueryBooleanOperator.NOT.equals(condition.getBooleanOperator())) {
                    predicate2 = cb.not(predicate2);
                }
            } else {
                // we found parenthesis
                break;
            }
            switch (operator) {
            case OR:
                predicate = cb.or(predicate, predicate2);
                break;
            default:
                predicate = cb.and(predicate, predicate2);
            }
            // get operator for next addition
            operator = condition.getBooleanOperator();
        }

        if (!queryConditions.isEmpty()) {
            predicate2 = buildPredicate(cb, cableRecord, queryConditions);
            switch (operator) {
            case OR:
                predicate = cb.or(predicate, predicate2);
                break;
            default:
                predicate = cb.and(predicate, predicate2);
            }
        }

        return predicate;
    }

    /**
     * Parses field, operator and value from query condition into predicate
     * 
     * @param cb
     *            Criteria builder
     * @param cableRecord
     *            cable record
     * @param condition
     *            condition to parse
     * @return parsed predicate
     */
    private Predicate parseQueryCondition(final CriteriaBuilder cb, final Root<Cable> cableRecord,
            QueryCondition condition) {
        QueryComparisonOperator operator = condition.getComparisonOperator();
        CableColumnUI column = CableColumnUI.convertColumnLabel(condition.getField());
        LOGGER.log(Level.FINE, "Parsing query: " + condition.toString());
        switch (column) {
        case NAME:
            Expression<String> exp = cb.concat(
                    cb.lower(cableRecord.get(CableColumnUI.SYSTEM.getValue()).as(String.class)),
                    cb.lower(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue()).as(String.class)));
            exp = cb.concat(exp, cb.lower(cableRecord.get(CableColumnUI.CLASS.getValue()).as(String.class)));
            exp = cb.concat(exp, cb.lower(cableRecord.get("seqNumber").as(String.class)));
            switch (operator) {
            case EQUAL:
                return cb.equal(exp, condition.getValue().toLowerCase());
            case NOT_EQUAL:
                return cb.notEqual(exp, condition.getValue().toLowerCase());
            case LIKE:
                return cb.like(exp, "%" + condition.getValue().toLowerCase() + "%", '\\');
            default:
                return cb.equal(exp, condition.getValue());
            }
        case LENGTH:
            switch (operator) {
            case EQUAL:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            case GREATER_THAN_OR_EQUAL_TO:
                return cb.greaterThanOrEqualTo(cableRecord.get(column.getValue()), condition.getValue());
            case GREATER_THAN:
                return cb.greaterThan(cableRecord.get(column.getValue()), condition.getValue());
            case LESS_THAN_OR_EQUAL_TO:
                return cb.lessThanOrEqualTo(cableRecord.get(column.getValue()), condition.getValue());
            case LESS_THAN:
                return cb.lessThan(cableRecord.get(column.getValue()), condition.getValue());
            case NOT_EQUAL:
                return cb.notEqual(cableRecord.get(column.getValue()), condition.getValue());
            case LIKE:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            }
        case STATUS:
            CableStatus status = UiUtility.parseIntoEnum(condition.getValue(), CableStatus.class);
            switch (operator) {
            case EQUAL:
                return cb.equal(cableRecord.get(column.getValue()), status);
            case NOT_EQUAL:
                return cb.notEqual(cableRecord.get(column.getValue()), status);
            default:
                return cb.equal(cableRecord.get(column.getValue()), status);
            }
        case CONTAINER:
            switch (operator) {
            case STARTS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getValue())),
                        escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case CONTAINS:
                return cb.like(cb.lower(cableRecord.get(column.getValue())),
                        "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case ENDS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getValue())),
                        "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
            case EQUAL:
                return cb.equal(cb.lower(cableRecord.get(column.getValue())), condition.getValue().toLowerCase());
            case LIKE:
                return cb.like(cableRecord.get(column.getValue()), condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            }
        case CABLE_TYPE:
        case QUALITY_REPORT:
            switch (operator) {
            case STARTS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                        escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case CONTAINS:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                        "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case ENDS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                        "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
            case EQUAL:
                return cb.equal(cb.lower(cableRecord.get(column.getValue()).get("name")),
                        condition.getValue().toLowerCase());
            case LIKE:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()).get("name"), condition.getValue());
            }
        case MODIFIED:
        case INSTALLATION_DATE:
        case TERMINATION_DATE:
            Date date = Date
                    .from(UiUtility.processUIDateTime(condition.getValue()).atZone(ZoneId.systemDefault()).toInstant());
            switch (operator) {
            case EQUAL:
                return cb.equal(cableRecord.get(column.getValue()), date);
            case GREATER_THAN_OR_EQUAL_TO:
                return cb.greaterThanOrEqualTo(cableRecord.get(column.getValue()), date);
            case GREATER_THAN:
                return cb.greaterThan(cableRecord.get(column.getValue()), date);
            case LESS_THAN_OR_EQUAL_TO:
                return cb.lessThanOrEqualTo(cableRecord.get(column.getValue()), date);
            case LESS_THAN:
                return cb.lessThan(cableRecord.get(column.getValue()), date);
            case NOT_EQUAL:
                return cb.notEqual(cableRecord.get(column.getValue()), date);
            case LIKE:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), date);
            }
        case DEVICE_A_NAME:
        case DEVICE_A_BUILDING:
        case DEVICE_A_WIRING:
        case DEVICE_A_RACK:
        case DEVICE_A_USER_LABEL:
        case DEVICE_B_NAME:
        case DEVICE_B_BUILDING:
        case DEVICE_B_WIRING:
        case DEVICE_B_RACK:
        case DEVICE_B_USER_LABEL:
            switch (operator) {
            case STARTS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                        escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case CONTAINS:
                return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                        "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case ENDS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                        "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
            case EQUAL:
                return cb.equal(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                        condition.getValue().toLowerCase());
            case LIKE:
                return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            }
        case DEVICE_A_CONNECTOR:
        case DEVICE_B_CONNECTOR:
            switch (operator) {
            case STARTS_WITH:
                return cb.like(
                        cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                        escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case CONTAINS:
                return cb.like(
                        cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                        "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
            case ENDS_WITH:
                return cb.like(
                        cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                        "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
            case EQUAL:
                return cb.equal(
                        cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                        condition.getValue().toLowerCase());
            case LIKE:
                return cb.like(
                        cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            }
        case OWNERS:
            switch (operator) {
            case STARTS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        condition.getValue().toLowerCase() + "%", '\\');
            case CONTAINS:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        "%" + condition.getValue().toLowerCase() + "%", '\\');
            case ENDS_WITH:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        "%" + condition.getValue().toLowerCase(), '\\');
            case EQUAL:
                return cb.equal(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        condition.getValue().toLowerCase());
            case LIKE:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            }
        default:
            switch (operator) {
            case LIKE:
                return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                        condition.getValue().toLowerCase(), '\\');
            default:
                return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
            }
        }
    }

    /**
     * Escapes the characters that have a special meaning in the database LIKE query, '%' and '_'.
     *
     * @param dbString
     *            the string to escape
     * @return the escaped string
     */
    protected String escapeDbString(final String dbString) {
        return dbString.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("\\[", "\\\\[").replaceAll("]",
                "\\\\]");
    }

    /**
     * Returns the number of elements to be included in the table.
     * 
     * @param filters
     *            filters to use
     * @param customQuery
     *            query to add
     * 
     * @return the number of elements in the table
     */
    public long getRowCount(final @Nullable Map<String, Object> filters, Query customQuery) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
        final Root<Cable> cableRecordCount = cqCount.from(Cable.class);

        List<Predicate> predicatesCount = buildPredicateList(cb, cableRecordCount, filters);

        Predicate predicateCount = addCustomQuery(cb, cableRecordCount, customQuery);
        if (predicateCount != null) {
            predicatesCount.add(predicateCount);
        }

        cqCount.where(predicatesCount.toArray(new Predicate[] {}));

        cqCount.select(cb.count(cableRecordCount));
        return em.createQuery(cqCount).getSingleResult();
    }

    private void sendCableStatusNotification(Cable cable, String userId) {
        final Set<String> notifiedUsers = new HashSet<>();
        for (final String userName : cable.getOwners()) {
            notifiedUsers.add(userName);
        }
        for (final String userName : userDirectoryServiceCache.getAllAdministratorUsernames()) {
            notifiedUsers.add(userName);
        }
        String subject = "Cable " + cable.getStatus().getDisplayName().toLowerCase() + " notification";
        String content = "Cable " + cable.getName() + " has been set to " + cable.getStatus().getDisplayName()
                + " in the Cable Database" + " (by " + userDirectoryServiceCache.getUserFullNameAndEmail(userId) + ")";
        mailService.sendMail(notifiedUsers, userId, subject, content, null, null, false, true);

    }

    /** Performs clearing of the persistance context, thus detaching all entities. */
    public void detachAllCables() {
        em.clear();
    }

    /**
     * Detaches a single cable from entity manager
     * 
     * @param cable
     *            the cable to detach
     */
    public void detachCable(Cable cable) {
        em.detach(cable);
    }
}
