/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.security.InvalidParameterException;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.dto.NotificationDTO;
import org.openepics.cable.dto.ReuseCableDetails;
import org.openepics.cable.model.*;
import org.openepics.cable.services.dl.CableColumn;
import org.openepics.cable.services.dl.CableParams;
import org.openepics.cable.ui.CableColumnUI;
import org.openepics.cable.ui.CableColumnUIConstants;
import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.ValidityUtil;
import org.openepics.cable.util.fbs.FbsElement;
import org.primefaces.model.SortOrder;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * <code>CableService</code> is the service layer that handles individual cable
 * operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Stateless
public class CableService {

    private static final Logger LOGGER = Logger.getLogger(CableService.class.getName());

    private static final String ENDPOINT_B = "endpointB";
    private static final String ENDPOINT_A = "endpointA";
    private static final String SEQ_NUMBER = "seqNumber";

    private static final String CABLE_ID_COLUMN = "id";

    private static final String CHESS_ID           = "CHESS Id";
    private static final String DEVICE_CHESS_ID    = "Device CHESS Id";
    private static final String ENCLOSURE_CHESS_ID = "Enclosure CHESS Id";

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;
    @Inject
    private Names names;
    @Inject
    private MailService mailService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;
    @Inject
    private NotificationService notificationService;
    @Inject
    private CableArticleService cableArticleService;
    @Inject
    private SessionService sessionService;

    private static class ExamineReport {
        private boolean isSame;
        private boolean isSameEnclosure;

        public ExamineReport(boolean isSame, boolean isSameEnclosure) {
            this.isSame = isSame;
            this.isSameEnclosure = isSameEnclosure;
        }

        public boolean isSame() {
            return isSame;
        }

        public boolean isSameEnclosure() {
            return isSameEnclosure;
        }

    }

    private final LazyService<Cable> lazyService = new LazyService<Cable>() {
        /**
         * Returns only a subset of data based on sort column, sort order and
         * filtered by all the fields.
         *
         * @param first the index of the first result to return
         * @param pageSize the number of results
         * @param sortField the field by which to sort
         * @param sortOrder ascending/descending
         * @param filters filters to use
         * @param customQuery query to add
         * @return The required entities.
         */
        @Override
        public List<Cable> findLazy(final int first, final int pageSize,
                                    final @Nullable String sortField, final @Nullable SortOrder sortOrder,
                                    final @Nullable Map<String, Object> filters, Query customQuery) {
            LOGGER.log(Level.FINEST, "CableService:findLazy offset: " + first + ", pageSize: " + pageSize);
            final CriteriaBuilder cb = em.getCriteriaBuilder();
            final CriteriaQuery<Cable> cq = cb.createQuery(Cable.class);
            final Root<Cable> cableRecord = cq.from(Cable.class);
            cableRecord.fetch(ENDPOINT_A, JoinType.LEFT);
            cableRecord.fetch(ENDPOINT_B, JoinType.LEFT);
            cq.select(cableRecord);

            List<Predicate> predicates;

            if(StringUtils.isNotEmpty(sortField)
                    && CableColumnUIConstants.INFORMATION_VALUE.equalsIgnoreCase(sortField)) {
                addSortingOrder(null, sortOrder, cb, cq, cableRecord);
                predicates = problemSortOrder(cb, cableRecord, sortOrder.equals(SortOrder.ASCENDING));
            } else {
                addSortingOrder(sortField, sortOrder, cb, cq, cableRecord);
                predicates = buildPredicateList(cb, cableRecord, filters);
            }

            Predicate predicate = addCustomQuery(cb, cableRecord, customQuery);
            if (predicate != null) {
                predicates.add(predicate);
            }

            cq.where(predicates.toArray(new Predicate[]{}));

            final TypedQuery<Cable> query = em.createQuery(cq);
            query.setFirstResult(first);
            query.setMaxResults(pageSize);
            List<Cable> cables = query.getResultList();

            // check and note if cables are valid
            //     no need to calculate or derive value, may check cable attribute
            if(cables != null) {
                for(Cable c: cables) {
                    c.setHasProblem(!c.isValid());
                }
            }

            LOGGER.log(Level.FINEST, "Finished loading cables");
            return cables;
        }
    };

    public LazyService<Cable> getLazyService() {
        return lazyService;
    }


    /** @return a list of all cables */
    public List<Cable> getCables() {
        LOGGER.log(Level.FINEST, "Getting all cables from database");
        return lazyService.findLazy(0, Integer.MAX_VALUE, CableColumnUI.NAME.getValue(), SortOrder.ASCENDING,
                new HashMap<String, Object>(), null);
    }

    /**
     * Returns a list of cables filtered by regural expression.
     *
     * @param fields
     *            filter fields
     * @param regExp
     *            filter regular expression
     *
     * @return a list of cables filtered by regular expression.
     */
    public List<Cable> getFilteredCablesByRegExp(List<CableColumn> fields, String regExp) {
        LOGGER.log(Level.FINEST, "Retrieving cables by regular expression: " + regExp);
        LOGGER.log(Level.FINEST, "  Creating query from regular expression");
        final List<QueryCondition> conditions = new ArrayList<QueryCondition>();
        final Query query = new Query("regExprQuery", EntityType.CABLE, null, null, conditions);

        for (int i = 0; i < fields.size(); i++) {
            conditions.add(new QueryCondition(query, QueryParenthesis.NONE, fields.get(i).getColumnLabel(),
                    QueryComparisonOperator.LIKE,
                    Strings.isNullOrEmpty(regExp) ? "%" : regExp.replace('*', '%').replace('?', '_'),
                    QueryParenthesis.NONE,
                    i == (fields.size() - 1) ? QueryBooleanOperator.NONE : QueryBooleanOperator.AND, i));
        }

        return lazyService.findLazy(0, Integer.MAX_VALUE, CableColumnUI.NAME.getValue(), SortOrder.ASCENDING,
                new HashMap<String, Object>(), query);
    }

    /**
     * Returns a cable with the specified cable name.
     *
     * @param name
     *            the cable name (example 12A012345)
     * @param detached
     *            boolean indicating if the cable should be detached from the
     *            database
     * @return the cable, or null if the cable with such name cannot be found
     */
    public Cable getCableByName(String name, Boolean detached) {
        Cable cable = getCableByName(name);
        if (detached) {
            detachCables(Arrays.asList(cable));
        }
        return cable;
    }

    /**
     * Returns a cable with the specified cable name.
     *
     * @param name
     *            the cable name (example 12A012345)
     * @return the cable, or null if the cable with such name cannot be found
     */
    public Cable getCableByName(String name) {
        try {
            LOGGER.log(Level.FINEST, "Searching for cable with name:" + name);
            final CableName cableName = new CableName(name);

            return em
                    .createQuery("SELECT c FROM Cable c WHERE c.system = :system AND c.subsystem = :subsystem"
                            + " AND c.cableClass = :cableClass AND c.seqNumber = :seqNumber", Cable.class)
                    .setParameter("system", cableName.getSystem()).setParameter("subsystem", cableName.getSubsystem())
                    .setParameter("cableClass", cableName.getCableClass())
                    .setParameter(SEQ_NUMBER, cableName.getSeqNumber()).getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable with name: " + name + " not found", e);
            return null;
        }
    }

    /**
     * Returns a list of cables associated with the specified parameters.
     * Method available for report purposes.
     * <p>
     * Note:
     * <ul>
     * <li>deleted cables not considered</li>
     * <li>exact match for parameters</li>
     * </ul>
     * </p>
     *
     * @param system system
     * @param subsystem subsystem
     * @return a list of cables
     */
    public List<Cable> getCables(String system, String subsystem) {
        return getCables(system, subsystem, null);
    }
    /**
     * Returns a list of cables associated with the specified parameters.
     * Method available for report purposes.
     * <p>
     * Note:
     * <ul>
     * <li>deleted cables not considered</li>
     * <li>exact match for parameters</li>
     * </ul>
     * </p>
     *
     * @param system system
     * @param subsystem subsystem
     * @param installationPackage installation package
     * @return a list of cables
     */
    @SuppressWarnings("unchecked")
    public List<Cable> getCables(String system, String subsystem, String installationPackage) {
        return em.createNativeQuery(
                "SELECT c.* FROM Cable c "
                + "WHERE c.status != 'DELETED'"
                + (StringUtils.isEmpty(system)
                        ? ""
                        : " AND c.\"system\" = '" + system + "'")
                + (StringUtils.isEmpty(subsystem)
                        ? ""
                        : " AND c.subsystem = '" + subsystem + "'")
                + (StringUtils.isEmpty(installationPackage)
                        ? ""
                        : " AND c.installation_package_id IN ("
                        + "    SELECT ip.id FROM installation_package ip "
                        + "    WHERE ip.active = 'true' AND ip.\"name\" = '" + installationPackage + "'"
                        + ")")
                , Cable.class)
                .getResultList();
    }

    /**
     * Returns a list of cables with the specified names. Cables with invalid names
     * are skipped. Also gets all cable lazy fetched fields eagerly. If getDeleted
     * is true also returns deleted cables else only non-deleted ones.
     *
     * @param names
     *            the cable names
     * @param detached
     *            a boolean indicating if the cables should be detached from the
     *            database
     * @param getDeleted
     *            if true returns all cables, if false only non-deleted
     *
     * @return the cables
     */
    public List<Cable> getCablesByName(Iterable<String> names, Boolean detached, boolean getDeleted) {

        try {
            LOGGER.log(Level.FINEST, "Searching for cables by name");
            List<Integer> seqNumbers = new ArrayList<>();
            for (String name : names) {
                try {
                    final CableName cableName = new CableName(name);
                    seqNumbers.add(cableName.getSeqNumber());
                } catch (IllegalArgumentException e) {
                    LOGGER.log(Level.FINEST, name + " is invalid and will be skipped.", e);
                }
            }
            if (seqNumbers.isEmpty()) {
                return new ArrayList<>();
            }

            final CriteriaBuilder cb = em.getCriteriaBuilder();
            final CriteriaQuery<Cable> cq = cb.createQuery(Cable.class);
            final Root<Cable> cableRecord = cq.from(Cable.class);
            // Joins to force fetch all of the cable external fields
            cableRecord.fetch("cableType", JoinType.LEFT);
            cableRecord.fetch(ENDPOINT_A, JoinType.LEFT);
            cableRecord.fetch(ENDPOINT_B, JoinType.LEFT);
            cq.select(cableRecord);

            cq.where(cableRecord.get(SEQ_NUMBER).in(seqNumbers));
            if (!getDeleted) {
                cq.where(cableRecord.get("status").in(CableStatus.DELETED).not());
            }

            final TypedQuery<Cable> query = em.createQuery(cq);
            List<Cable> cables = query.getResultList();

            LOGGER.log(Level.FINEST, "Finished loading cables");
            if (detached) {
                detachCables(cables);
            }
            return cables;

        } catch (NoResultException e) {
            LOGGER.log(Level.FINEST, e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    /**
     * Helps listing cable names for UI autocomplete method.
     * List size will be limited according to limit parameter value.
     * Names will be ordered ASC!
     * If namePart is empty then first few names will be listed
     *
     * @param namePart the part of the cable name that has to be looked for in the DB
     * @param limit size limit for the response list
     *
     * @return list of cable names according to parameter order by name ASC
     */
    public List<String> cablesByName(String namePart, int limit) {

        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Cable> cq = cb.createQuery(Cable.class);
        final Root<Cable> cableRecord = cq.from(Cable.class);
        cq.select(cableRecord);

        Map<String, Object> nameFilter = new HashMap<>();
        nameFilter.put(CableColumnUI.NAME.getValue(), namePart);

        List<Predicate> predicates = buildPredicateList(cb, cableRecord, nameFilter);

        cq.where(predicates.toArray(new Predicate[]{}));

        final TypedQuery<Cable> query = em.createQuery(cq);
        query.setMaxResults(limit);
        List<Cable> cables = query.getResultList();

        return cables.stream()
                .map(Cable::getName)
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * Validates parameter, and reuses cable. (Creates new cable with same parameters, just different seqNumber,
     * sets Status to NOT IN USE, and sets the original cable-parameters according to parameter)
     *
     * Algorithm behind reusing a cable:
     * 1. Check if ORIGINAL cable available in DB
     * 2. Create NEW cable with same parameters as ORIGINAL has (it will have different seqNumber)
     * 3. Set NEW cable Status to NOT IN USE
     * 4. Set the ORIGINAL cable CableType, and Status to what the user defined to be
     * 5. Save the NEW, and ORIGINAL cable to the DB, and generate History entry
     * If exception occurs between any of the steps, the DB will rollback,
     * which means there will be no "partially created" objects
     *
     * With this method there is no need to swap seqNumbers
     *
     * @param cableDetails new value for cable that has to be re-used
     * @throws RuntimeException when cable name is empty; cable not found, or DB error happened
     * @return the newly created cable
     */
    @Transactional
    public Cable reuseCable(ReuseCableDetails cableDetails) {
        LOGGER.log(Level.FINE, "Trying to reuse cable");

        if (StringUtils.isEmpty(cableDetails.getOldCableName())) {
            LOGGER.log(Level.WARNING, "Old cable name is empty, can not call cable reuse function!");
            throw new RuntimeException("Old cable nam empty");
        }

        //getting ORIGINAL cable
        Cable originalCable = getCableByName(cableDetails.getOldCableName());

        if (originalCable == null) {
            LOGGER.log(Level.WARNING, "Cable not found to be reused!");
            throw new RuntimeException("Cable not found to reuse");
        }

        //setting parameters
        CableParams cp = new CableParams.Builder()
                .withSystem(originalCable.getSystem())
                .withSubSystem(originalCable.getSubsystem())
                .withCableClass(originalCable.getCableClass())
                .withFbsTag(originalCable.getFbsTag())
                .withCableArticle(originalCable.getCableArticle())
                .withCableType(originalCable.getCableType())
                .withContainer(originalCable.getContainer())
                .withElectricalDocumentation(originalCable.getElectricalDocumentation())
                .withEndpointA(originalCable.getEndpointA())
                .withEndpointB(originalCable.getEndpointB())
                .withInstallationPackage(originalCable.getInstallationPackage())
                .withOwners(originalCable.getOwners())
                .withCableStatus(CableStatus.NOT_IN_USE)
                .withInstallationBy(originalCable.getInstallationBy())
                .withComments(originalCable.getComments())
                .withRevision(originalCable.getRevision())
                .withUserId(sessionService.getLoggedInName())
                .withSendNotification(false)
                .build();

        Cable cable = createCable(cp);

        //setting new values for reused cable
        originalCable.setCableArticle(cableArticleService.getCableArticleByShortNameExternalId(cableDetails.getCableArticle()));
        originalCable.setStatus(cableDetails.getNewStatus());

        em.merge(originalCable);

        historyService.createHistoryEntry(EntityTypeOperation.REUSE, originalCable.getName(), EntityType.CABLE,
                originalCable.getId(),
                originalCable.getName() + " reused, old cable stored under " + cable.getName(), "",
                sessionService.getLoggedInName());

        LOGGER.log(Level.INFO, "Cable reuse finished successfully");

        return cable;
    }

    /**
     * Returns a cable with the specified cable id.
     *
     * @param id
     *            the cable id
     * @return the cable, or null if the cable with such id cannot be found
     */
    public Cable getCableById(Long id) {
        return em.find(Cable.class, id);
    }

    /**
     * Creates a bunch of cables in the database and returns them.
     *
     * @param cables
     *            cables to create in database
     * @param userId
     *            user creating cables
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should
     *            be send
     * @return the list of cables retrieved from the database
     */
    public List<Cable> createCables(Iterable<Cable> cables, String userId, boolean sendNotification) {
        List<Cable> createdCables = new ArrayList<Cable>();
        if (cables.iterator().hasNext()) {
            int seqNumber = getUnusedCableSeqNumber();
            for (Cable c : cables) {

                CableParams newCable = new CableParams.Builder()
                        .withSystem(c.getSystem())
                        .withSubSystem(c.getSubsystem())
                        .withCableClass(c.getCableClass())
                        .withFbsTag(c.getFbsTag())
                        .withCableArticle(c.getCableArticle())
                        .withCableType(c.getCableType())
                        .withContainer(c.getContainer())
                        .withElectricalDocumentation(c.getElectricalDocumentation())
                        .withEndpointA(c.getEndpointA())
                        .withEndpointB(c.getEndpointB())
                        .withInstallationPackage(c.getInstallationPackage())
                        .withOwners(c.getOwners())
                        .withCableStatus(c.getStatus())
                        .withInstallationBy(c.getInstallationBy())
                        .withComments(c.getComments())
                        .withRevision(c.getRevision())
                        .withUserId(userId)
                        .withSendNotification(sendNotification)
                        .build();
                Cable cable = createCable(newCable, seqNumber);
                seqNumber++;
                createdCables.add(cable);
            }
        }
        return createdCables;
    }

    /**
     * Creates a cable in the database and returns it.
     *
     * @param newCable the parameters of the cable that has to be stored
     *
     * @return the created cable
     */
    public Cable createCable(CableParams newCable) {
        return createCable(newCable, getUnusedCableSeqNumber());
    }

    private Cable createCable(CableParams newCable, Integer seqNumber) {
        // Assign endpoint uuid
        //     uuid to be empty if fbs tag is non-empty
        if (StringUtils.isEmpty(newCable.getEndpointA().getDeviceFbsTag())) {
            newCable.getEndpointA().setUuid(names.getUuidByName(newCable.getEndpointA().getDevice()));
        } else {
            newCable.getEndpointA().setUuid(null);
        }
        if (StringUtils.isEmpty(newCable.getEndpointB().getDeviceFbsTag())) {
            newCable.getEndpointB().setUuid(names.getUuidByName(newCable.getEndpointB().getDevice()));
        } else {
            newCable.getEndpointB().setUuid(null);
        }

        final Date created = new Date();
        final Date modified = created;
        final Cable cable = new Cable(newCable.getSystem(), newCable.getSubsystem(), newCable.getCableClass(),
                seqNumber, newCable.getOwners(), newCable.getStatus(), newCable.getCableArticle(), newCable.getCableType(), newCable.getContainer(),
                newCable.getEndpointA(), newCable.getEndpointB(), created, modified, newCable.getInstallationPackage(),
                newCable.getInstallationBy(),
                newCable.getComments(),
                newCable.getRevision(), StringUtils.trimToNull(newCable.getElectricalDocumentation()));
        if (!StringUtils.isEmpty(newCable.getFbsTag())) {
            cable.setFbsTag(newCable.getFbsTag());
        }

        em.persist(newCable.getEndpointA());
        em.persist(newCable.getEndpointB());
        em.persist(cable);

        if (newCable.isSendNotification()) {
            sendCableStatusNotification(cable, newCable.getUserId());
        }

        historyService.createHistoryEntry(EntityTypeOperation.CREATE, cable.getName(), EntityType.CABLE, cable.getId(),
                "", "", newCable.getUserId());

        updateValidity(newCable.getUserId(), cable);

        return cable;
    }

    /**
     * Updates the attributes on the given cables.
     *
     * @param cables
     *            the cables with modified attributes to save to the database.
     * @param oldCables
     *            the cables before modification. Should be in the same order as in
     *            cables. Also needs to be of the same size as cables.
     * @param userId
     *            username of user updating the cables, for history record
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should
     *            be send
     * @return number of updated cables
     */
    public int updateCables(Iterable<Cable> cables, Iterable<Cable> oldCables, String userId,
            boolean sendNotification) {
        int count = 0;
        final Iterator<Cable> cablesIterator = cables.iterator();
        final Iterator<Cable> oldCablesIterator = oldCables.iterator();
        Integer seqNumber = null;
        seqNumber = getUnusedCableSeqNumber();
        while (cablesIterator.hasNext()) {
            Preconditions.checkArgument(cablesIterator.hasNext() && oldCablesIterator.hasNext());
            Cable cable = cablesIterator.next();
            Cable oldCable = oldCablesIterator.next();
            if (!cable.getSystem().equals(oldCable.getSystem())
                    || !cable.getSubsystem().equals(oldCable.getSubsystem())
                    || !cable.getCableClass().equals(oldCable.getCableClass())) {
                if (updateCable(cable, oldCable, seqNumber, userId, sendNotification)) {
                    count++;
                }
                seqNumber++;
                continue;
            }
            if (updateCable(cable, oldCable, cable.getSeqNumber(), userId, sendNotification)) {
                count++;
            }
        }
        Preconditions.checkArgument(!(cablesIterator.hasNext() || oldCablesIterator.hasNext()));
        return count;
    }

    /**
     * Updates the attributes on the given cable.
     *
     * @param cable
     *            the cable with modified attributes to save to the database
     * @param oldCable
     *            the cable before modification
     * @param userId
     *            username of user updating the cable, for history record
     * @param sendNotification
     *            boolean indicating if notification regarding cable status should
     *            be send
     * @return true if that cable was updated, false if the cable was not updated
     */
    public boolean updateCable(Cable cable, Cable oldCable, String userId, boolean sendNotification) {
        if (!cable.getSystem().equals(oldCable.getSystem())
                || !cable.getSubsystem().equals(oldCable.getSubsystem())
                || !cable.getCableClass().equals(oldCable.getCableClass())) {
            return updateCable(cable, oldCable, getUnusedCableSeqNumber(), userId, sendNotification);

        }
        return updateCable(cable, oldCable, cable.getSeqNumber(), userId, sendNotification);
    }

    private boolean updateCable(Cable cable, Cable oldCable, Integer seqNumber, String userId, boolean sendNotification) {
        // not update if content is same
        if (cable.equals(oldCable)) {
            return false;
        }

        cable.setSeqNumber(seqNumber);
        cable.setModified(new Date());

        // Assign endpoint uuid
        //     uuid to be empty if fbs tag is non-empty
        if (StringUtils.isEmpty(cable.getEndpointA().getDeviceFbsTag())) {
            cable.getEndpointA().setUuid(names.getUuidByName(cable.getEndpointA().getDevice()));
        } else {
            cable.getEndpointA().setUuid(null);
        }
        if (StringUtils.isEmpty(cable.getEndpointB().getDeviceFbsTag())) {
            cable.getEndpointB().setUuid(names.getUuidByName(cable.getEndpointB().getDevice()));
        } else {
            cable.getEndpointB().setUuid(null);
        }

        em.merge(cable.getEndpointA());
        em.merge(cable.getEndpointB());
        em.merge(cable);
        if (oldCable.getStatus() != cable.getStatus() && sendNotification) {
            sendCableStatusNotification(cable, userId);
        }

        // History logging
        historyService.createHistoryEntry(
                EntityTypeOperation.UPDATE,
                cable.getName(), EntityType.CABLE, cable.getId(), getChangeString(cable, oldCable), "", userId);

        updateValidity(userId, cable);
        return true;
    }

    /**
     * Marks the cables deleted in the database.
     *
     * @param cables
     *            the cables to delete
     * @param userId
     *            username of user deleting the cables, for history record
     * @return number of deleted cables
     */
    public int deleteCables(Iterable<Cable> cables, String userId) {
        int count = 0;
        for (Cable cable : cables) {
            if (deleteCable(cable, userId)) {
                count++;
            }
        }
        return count;
    }

    /**
     * Marks the cable deleted in the database.
     *
     * @param cable
     *            the cable to delete
     * @param userId
     *            username of user deleting the cable, for history record
     * @return true if the cable was deleted, false if the cable was already deleted
     */
    public boolean deleteCable(Cable cable, String userId) {
        if (cable.getStatus() == CableStatus.DELETED) {
            return false;
        }

        cable.setStatus(CableStatus.DELETED);
        cable.setModified(new Date());
        em.merge(cable);

        historyService.createHistoryEntry(EntityTypeOperation.DELETE, cable.getName(), EntityType.CABLE, cable.getId(),
                "", "", userId);
        return true;
    }

    /**
     * Retrieves cables and updates FBS tag mappings in one transaction to ensure no other
     * changes happen in the mean time.
     *
     * @param start
     *            the number of the starting cable to update
     * @param size
     *            amount of cables to update
     * @param userId
     *            id of the user invoking update
     * @param mappingsCableNameToFbsElement
     *            mappings for cable name to FbsElement
     * @param mappingsEssNameToFbsElement
     *            mappings for ESS name to FbsElement
     * @param mappingsIdToFbsElement
     *            mappings for id to FbsElement
     * @param mappingsTagToFbsElement
     *            mappings for tag to FbsElement
     * @param mapOwnersCablesUnexpectedChanges
     *            map with key (ownersstring) and value (list of cable names)
     *            to track cables for which values changed unexpectedly in tag mapping update
     * @return if validation was performed, false if there were no more cables to update
     *
     * @see CableService#getAndValidateCables(int, int, String)
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean getAndFbsTagUpdateCables(
            int start, int size, String userId,
            Map<String, FbsElement> mappingsCableNameToFbsElement,
            Map<String, FbsElement> mappingsEssNameToFbsElement,
            Map<String, FbsElement> mappingsIdToFbsElement,
            Map<String, FbsElement> mappingsTagToFbsElement,
            Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges) {

        final List<Cable> cables = lazyService.findLazy(start, size, CableColumnUI.MODIFIED.getValue(),
                SortOrder.ASCENDING, new HashMap<String, Object>(), null);
        if (cables == null || cables.isEmpty()) {
            return false;
        }
        updateFbsTagMappings(userId, cables,
                mappingsCableNameToFbsElement, mappingsEssNameToFbsElement,
                mappingsIdToFbsElement, mappingsTagToFbsElement,
                mapOwnersCablesUnexpectedChanges);
        return cables.size() == size;
    }

    /**
     * Update FBS tag mappings for cables.
     *
     * @param userId
     *            id of the user invoking validation
     * @param cables
     *            cables to validate
     * @param mappingsCableNameToFbsElement
     *            mappings for cable name to FbsElement
     * @param mappingsEssNameToFbsElement
     *            mappings for ESS name to FbsElement
     * @param mappingsIdToFbsElement
     *            mappings for id to FbsElement
     * @param mappingsTagToFbsElement
     *            mappings for tag to FbsElement
     * @param mapOwnersCablesUnexpectedChanges
     *            map with key (ownersstring) and value (list of cable names)
     *            to track cables for which values changed unexpectedly in tag mapping update
     */
    public void updateFbsTagMappings(
            String userId, Iterable<Cable> cables,
            Map<String, FbsElement> mappingsCableNameToFbsElement,
            Map<String, FbsElement> mappingsEssNameToFbsElement,
            Map<String, FbsElement> mappingsIdToFbsElement,
            Map<String, FbsElement> mappingsTagToFbsElement,
            Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges) {

        for (final Cable cable : cables) {
            if (cable.getStatus() != CableStatus.DELETED) {
                // use interface for CHESS/ITIP to retrieve mappings - cable name, ESS name, id, tag (FBS)
                //     information is retrieved and handled (sorted, mappings prepared), and then used
                //         e.g. retrieve tag for name (or vice versa) and store name/tag for cable (cable, endpoint)
                //     to show better (more complete) set of information to user
                //     --------------------------------------------------------------------------------
                //     fields
                //         cable name              - fbs tag                - chess id             (cable)
                //         from ess name           - from fbs tag           - chess id             (endpoint)
                //         from enclosure ess name - from enclosure fbs tag - enclosure chess id   (endpoint)
                //         to ess name             - to fbs tag             - chess id             (endpoint)
                //         to enclosure ess name   - to enclosure fbs tag   - enclosure chess id   (endpoint)
                //     --------------------------------------------------------------------------------
                //     before
                //         cable name
                //             Cable Name to have precedence over FBS Tag
                //             Cable Name to be used to look up value for FBS Tag
                //         ess name / fbs tag
                //             FBS Tag to have predence over ESS Name
                //             FBS Tag, if present, to be used to look up value for ESS Name
                //                 otherwise, ESS Name, if present, to be used to look up value for FBS Tag
                //             uuid to be empty if fbs tag is non-empty
                //     now
                //         similar to before but adjustment for for ess name / fbs tag
                //         chess id has precedence over ess name / fbs tag
                //     --------------------------------------------------------------------------------
                //     possible side effect (before)
                //         if FBS Tag used to look up ESS Name and cable updated with ESS Name,
                //         then new value trumps previous value, thus in effect cuts any connection
                //         to Naming, i.e. uuid is no longer valid and is to be deleted.
                //         Consequence is that Cable becomes dependent of FBS Tag / ESS Name mapping,
                //         i.e. CHESS - Naming integration, for keeping ESS Name up to date.
                //         Option to query Naming for uuid if functionality available. However not
                //         recommended as subsequent fbs tag mapping update might clear.
                //         - handle with chess id
                //     --------------------------------------------------------------------------------
                //     track cables for which values changed unexpectedly in tag mapping update
                //         definition unexpected change
                //             replace non-empty value
                //     --------------------------------------------------------------------------------
                //     different options to get and use values -  cable name, ESS name, id, tag (FBS)
                //     considerable difference in performance depending on how names and tags are used
                //     to look up one another
                //         - time vs memory
                //               FBSUtil
                //                   not keep mappings in memory, order of magnitude - hour
                //               Map<String, String> tagMappings
                //                   keep mappings in memory, order of magnitude - minute
                //     --------------------------------------------------------------------------------

                StringBuilder historyEntry = new StringBuilder(300);

                // fields
                //     cable name         - fbs tag                    - chess id        (cable)
                //     --------------------------------------------------------------------------------
                //     find out if same as before, if not same then - set new value, prepare log
                //     necessary to keep track of chess id for fbs tag for cable name

                String oldChessId = cable.getChessId();
                String oldFbsTag = cable.getFbsTag();
                FbsElement fbsElement = mappingsCableNameToFbsElement.get(cable.getName());
                String chessId = fbsElement != null ? fbsElement.id  : null;
                String fbsTag  = fbsElement != null ? fbsElement.tag : null;
                boolean isSameFbsTag = StringUtils.equalsIgnoreCase(oldFbsTag, fbsTag);
                boolean setChessId = isSameFbsTag
                        && !StringUtils.isEmpty(fbsTag)
                        && StringUtils.isEmpty(oldChessId)
                        && !StringUtils.isEmpty(chessId);
                boolean isSameChessInfo = true;

                // consider chess mapping for cable name
                if (!isSameFbsTag) {
                    // ability to handle cable with
                    //     chess mapping available
                    //         cable mapping not available - create cable mapping
                    //         cable mapping     available - change cable mapping
                    //
                    //     chess mapping not available
                    //         cable mapping not available - wait, not clear fbs tag
                    //         cable mapping     available - clear cable mapping
                    //
                    // create, change - same action

                    if (StringUtils.isEmpty(chessId)) {
                        // chess mapping not available
                        //     wait
                        //         if no mapping found for cable name then leave chess id and fbs tag fields unchanged
                        //             e.g. if cable name and fbs tag set for a cable but cable name not in chess
                        //                  then leave fbs tag unchanged
                        //     clear - clear

                        if (!StringUtils.isEmpty(oldChessId)) {
                            // clear
                            isSameChessInfo = false;

                            cable.setChessId(null);
                            cable.setFbsTag(null);

                            historyEntry.append(HistoryService.getDiffForAttributes(CHESS_ID , chessId, oldChessId));
                            historyEntry.append(HistoryService.getDiffForAttributes(CableColumn.FBS_TAG.getColumnLabel() , fbsTag, oldFbsTag));
                            remarkChangeUnexpectedCable(mapOwnersCablesUnexpectedChanges, cable, oldFbsTag, fbsTag);
                        }
                    } else {
                        // chess mapping available
                        //     create, change - same action
                        isSameChessInfo = false;

                        cable.setChessId(chessId);
                        cable.setFbsTag(fbsTag);

                        historyEntry.append(HistoryService.getDiffForAttributes(CHESS_ID , chessId, oldChessId));
                        historyEntry.append(HistoryService.getDiffForAttributes(CableColumn.FBS_TAG.getColumnLabel(), fbsTag, oldFbsTag));
                        remarkChangeUnexpectedCable(mapOwnersCablesUnexpectedChanges, cable, oldFbsTag, fbsTag);
                    }
                }
                if (setChessId) {
                    // chess mapping available and used but chess id not set
                    isSameChessInfo = false;

                    cable.setChessId(chessId);

                    historyEntry.append(HistoryService.getDiffForAttributes(CHESS_ID , chessId, oldChessId));
                }

                // endpoint a
                //     from ess name           - from fbs tag           - chess id             (endpoint)
                //     from enclosure ess name - from enclosure fbs tag - enclosure chess id   (endpoint)
                ExamineReport reportA = examineEndpoint(
                        cable.getEndpointA(), historyEntry, cable, true,
                        mappingsEssNameToFbsElement, mappingsIdToFbsElement, mappingsTagToFbsElement,
                        mapOwnersCablesUnexpectedChanges);

                // endpoint b
                //     to ess name             - to fbs tag             - chess id             (endpoint)
                //     to enclosure ess name   - to enclosure fbs tag   - enclosure chess id   (endpoint)
                ExamineReport reportB = examineEndpoint(
                        cable.getEndpointB(), historyEntry, cable, false,
                        mappingsEssNameToFbsElement, mappingsIdToFbsElement, mappingsTagToFbsElement,
                        mapOwnersCablesUnexpectedChanges);

                // if all same (no changes), continue
                if (isSameChessInfo
                        && reportA.isSame()
                        && reportA.isSameEnclosure()
                        && reportB.isSame()
                        && reportB.isSameEnclosure()) {
                    continue;
                }

                // persist changes
                if (!reportA.isSame() || !reportA.isSameEnclosure()) {
                    em.merge(cable.getEndpointA());
                }
                if (!reportB.isSame || !reportB.isSameEnclosure()) {
                    em.merge(cable.getEndpointB());
                }
                if (!isSameChessInfo) {
                    cable.setModified(new Date());
                    em.merge(cable);
                }

                // history logging
                historyService.createHistoryEntry(EntityTypeOperation.UPDATE, cable.getName(),
                        EntityType.CABLE, cable.getId(), historyEntry.toString(), "", userId);
            }
        }
    }

    /**
     * Examines the cable endpoint to see if there were changes regarding ESS Name and FBS Tag.
     *
     * @param endpoint the cable endpoint that has to be examined (endpointA, or endpoint B)
     * @param historyEntry if changes happened, the log entry message should be written in this variable.
     *                     Should not be NULL!
     * @param cable cable for endpoint
     * @param isEndpointA if endpoint is endpoint A of cable, otherwise endpoint B
     * @param mappingsEssNameToFbsElement map of ESS name -> to FbsElement; read only property
     * @param mappingsIdToFbsElement map of id -> to FbsElement; read only property
     * @param mappingsTagToFbsElement map of tag -> to FbsElement; read only property
     * @param mapOwnersCablesUnexpectedChanges map of owners, and list of changes in cables; read-write property
     * @return statistics of endpoint examination
     */
    private ExamineReport examineEndpoint(
                Endpoint endpoint,
                StringBuilder historyEntry,
                Cable cable,
                boolean isEndpointA,
                Map<String, FbsElement> mappingsEssNameToFbsElement,
                Map<String, FbsElement> mappingsIdToFbsElement,
                Map<String, FbsElement> mappingsTagToFbsElement,
                Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges) {

        // fields
        //     from ess name           - from fbs tag           - chess id             (endpoint)
        //     from enclosure ess name - from enclosure fbs tag - enclosure chess id   (endpoint)
        //     to ess name             - to fbs tag             - chess id             (endpoint)
        //     to enclosure ess name   - to enclosure fbs tag   - enclosure chess id   (endpoint)
        //     --------------------------------------------------------------------------------
        //     order of precedence
        //         chess id, fbs tag, ess name
        //     --------------------------------------------------------------------------------
        //     uuid to be empty if fbs tag is non-empty
        //     no history entry for uuid
        //     ------------------------------------------------------------
        //     if not empty - existing fbs tag
        //         get ess name for fbs tag
        //         find out if same
        //         if not same
        //             set new value
        //             prepare log
        //     else if not empty - existing ess name
        //         get fbs tag for ess name
        //         find out if same
        //         if not same
        //             set new value
        //             prepare log

        // endpoint a or b
        // device or enclosure
        // fbs tag or ess name

        // endpoint device
        //     device, deviceFbsTag, deviceChessId
        String oldChessId = endpoint.getDeviceChessId();
        String oldESSName = endpoint.getDevice();
        String oldFbsTag  = endpoint.getDeviceFbsTag();
        boolean isSameEndpoint = true;

        if (!StringUtils.isEmpty(oldChessId)) {
            // get ess name, fbs tag for chess id
            FbsElement fbsElement = mappingsIdToFbsElement.get(oldChessId);
            String chessId = fbsElement != null ? oldChessId : null;
            String essName = fbsElement != null ? fbsElement.essName : null;
            String fbsTag  = fbsElement != null ? fbsElement.tag     : null;

            boolean isSameESSName = StringUtils.equalsIgnoreCase(oldESSName, essName);
            boolean isSameFbsTag = StringUtils.equalsIgnoreCase(oldFbsTag, fbsTag);
            isSameEndpoint = isSameESSName && isSameFbsTag;

            if (StringUtils.isEmpty(chessId)) {
                // clear - chess mapping not available
                endpoint.setDeviceChessId(null);
                endpoint.setDevice(null);
                endpoint.setDeviceFbsTag(null);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, true, null, oldChessId));
                historyEntry.append(historyDiffEndpoint(isEndpointA, true, false, null, oldESSName));
                historyEntry.append(historyDiffEndpoint(isEndpointA, true, true, null, oldFbsTag));
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, true, false, oldESSName, null);
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, true, true, oldFbsTag, null);
            } else if (!isSameEndpoint) {
                if (!isSameESSName) {
                    endpoint.setDevice(essName);

                    historyEntry.append(historyDiffEndpoint(isEndpointA, true, false, essName, oldESSName));
                    remarkChangeUnexpectedEndpoint(
                            mapOwnersCablesUnexpectedChanges, cable, endpoint,
                            isEndpointA, true, false, oldESSName, essName);
                }
                if (!isSameFbsTag) {
                    endpoint.setDeviceFbsTag(fbsTag);

                    historyEntry.append(historyDiffEndpoint(isEndpointA, true, true, fbsTag, oldFbsTag));
                    remarkChangeUnexpectedEndpoint(
                            mapOwnersCablesUnexpectedChanges, cable, endpoint,
                            isEndpointA, true, true, oldFbsTag, fbsTag);
                }
            }
        } else if (!StringUtils.isEmpty(oldFbsTag)) {
            // get chess id, ess name for fbs tag
            FbsElement fbsElement = mappingsTagToFbsElement.get(oldFbsTag);
            String chessId = fbsElement != null ? fbsElement.id      : null;
            String essName = fbsElement != null ? fbsElement.essName : null;

            isSameEndpoint = StringUtils.equalsIgnoreCase(oldESSName, essName);

            if (StringUtils.isEmpty(endpoint.getDeviceChessId())) {
                endpoint.setDeviceChessId(chessId);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, true, chessId, oldChessId));
            }
            if (!isSameEndpoint) {
                endpoint.setDeviceChessId(chessId);
                endpoint.setDevice(essName);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, true, chessId, oldChessId));
                historyEntry.append(historyDiffEndpoint(isEndpointA, true, false, essName, oldESSName));
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, true, false, oldESSName, essName);
            }
        } else if (!StringUtils.isEmpty(oldESSName)) {
            // get chess id, fbs tag for ess name
            FbsElement fbsElement = mappingsEssNameToFbsElement.get(oldESSName);
            String chessId = fbsElement != null ? fbsElement.id  : null;
            String fbsTag  = fbsElement != null ? fbsElement.tag : null;

            isSameEndpoint = StringUtils.equalsIgnoreCase(oldFbsTag, fbsTag);

            if (StringUtils.isEmpty(endpoint.getDeviceChessId())) {
                endpoint.setDeviceChessId(chessId);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, true, chessId, oldChessId));
            }
            if (!isSameEndpoint) {
                endpoint.setDeviceChessId(chessId);
                endpoint.setDeviceFbsTag(fbsTag);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, true, chessId, oldChessId));
                historyEntry.append(historyDiffEndpoint(isEndpointA, true, true, fbsTag, oldFbsTag));
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, true, true, oldFbsTag, fbsTag);
            }
        }
        // uuid to be empty if fbs tag is non-empty
        if (!isSameEndpoint && !StringUtils.isEmpty(endpoint.getDeviceFbsTag())) {
            endpoint.setUuid("");
        }

        // endpoint enclosure
        //     rack, rackFbsTag, rackChessId
        oldChessId = endpoint.getRackChessId();
        oldESSName = endpoint.getRack();
        oldFbsTag = endpoint.getRackFbsTag();
        boolean isSameEndpointEnclosure = true;

        if (!StringUtils.isEmpty(oldChessId)) {
            // get ess name, fbs tag for chess id
            FbsElement fbsElement = mappingsIdToFbsElement.get(oldChessId);
            String chessId = fbsElement != null ? oldChessId : null;
            String essName = fbsElement != null ? fbsElement.essName : null;
            String fbsTag  = fbsElement != null ? fbsElement.tag     : null;

            boolean isSameESSName = StringUtils.equalsIgnoreCase(oldESSName, essName);
            boolean isSameFbsTag = StringUtils.equalsIgnoreCase(oldFbsTag, fbsTag);
            isSameEndpointEnclosure = isSameESSName && isSameFbsTag;

            if (StringUtils.isEmpty(chessId)) {
                // clear - chess mapping not available
                endpoint.setRackChessId(null);
                endpoint.setRack(null);
                endpoint.setRackFbsTag(null);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, false, null, oldChessId));
                historyEntry.append(historyDiffEndpoint(isEndpointA, false, false, null, oldESSName));
                historyEntry.append(historyDiffEndpoint(isEndpointA, false, true, null, oldFbsTag));
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, false, false, oldESSName, null);
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, false, true, oldFbsTag, null);

            } else if (!isSameEndpointEnclosure) {
                if (!isSameESSName) {
                    endpoint.setRack(essName);

                    historyEntry.append(historyDiffEndpoint(isEndpointA, false, false, essName, oldESSName));
                    remarkChangeUnexpectedEndpoint(
                            mapOwnersCablesUnexpectedChanges, cable, endpoint,
                            isEndpointA, false, false, oldESSName, essName);
                }
                if (!isSameFbsTag) {
                    endpoint.setRackFbsTag(fbsTag);

                    historyEntry.append(historyDiffEndpoint(isEndpointA, false, true, fbsTag, oldFbsTag));
                    remarkChangeUnexpectedEndpoint(
                            mapOwnersCablesUnexpectedChanges, cable, endpoint,
                            isEndpointA, false, true, oldFbsTag, fbsTag);
                }
            }

        } else if (!StringUtils.isEmpty(oldFbsTag)) {
            // get chess id, ess name for fbs tag
            FbsElement fbsElement = mappingsTagToFbsElement.get(oldFbsTag);
            String chessId = fbsElement != null ? fbsElement.id      : null;
            String essName = fbsElement != null ? fbsElement.essName : null;

            isSameEndpointEnclosure = StringUtils.equalsIgnoreCase(oldESSName, essName);

            if (StringUtils.isEmpty(endpoint.getRackChessId())) {
                endpoint.setRackChessId(chessId);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, false, chessId, oldChessId));
            }
            if (!isSameEndpointEnclosure) {
                endpoint.setRackChessId(chessId);
                endpoint.setRack(essName);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, false, chessId, oldChessId));
                historyEntry.append(historyDiffEndpoint(isEndpointA, false, false, essName, oldESSName));
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, false, false, oldESSName, essName);
            }
        } else if (!StringUtils.isEmpty(oldESSName)) {
            // get chess id, fbs tag for ess name
            FbsElement fbsElement = mappingsEssNameToFbsElement.get(oldESSName);
            String chessId = fbsElement != null ? fbsElement.id  : null;
            String fbsTag  = fbsElement != null ? fbsElement.tag : null;

            isSameEndpointEnclosure = StringUtils.equalsIgnoreCase(oldFbsTag, fbsTag);

            if (StringUtils.isEmpty(endpoint.getRackChessId())) {
                endpoint.setRackChessId(chessId);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, false, chessId, oldChessId));
            }
            if (!isSameEndpointEnclosure) {
                endpoint.setRackChessId(chessId);
                endpoint.setRackFbsTag(fbsTag);

                historyEntry.append(historyDiffEndpointChessId(isEndpointA, false, chessId, oldChessId));
                historyEntry.append(historyDiffEndpoint(isEndpointA, false, true, fbsTag, oldFbsTag));
                remarkChangeUnexpectedEndpoint(
                        mapOwnersCablesUnexpectedChanges, cable, endpoint,
                        isEndpointA, false, true, oldFbsTag, fbsTag);
            }
        }

        return new ExamineReport(isSameEndpoint, isSameEndpointEnclosure);
    }

    private String historyDiffEndpoint(
            boolean isEndpointA,
            boolean isDevice,
            boolean isFbsTag,
            Object value,
            Object oldValue) {

        // utility method to get diff for attributes for purpose of writing to history
        return HistoryService.getDiffForAttributes(
                getCableColumn(isEndpointA, isDevice, isFbsTag).getColumnLabel(),
                value,
                oldValue);
    }
    private String historyDiffEndpointChessId(
            boolean isEndpointA,
            boolean isDevice,
            Object value,
            Object oldValue) {

        // utility method to get diff for attributes for purpose of writing to history
        return HistoryService.getDiffForAttributes(
                getCableColumnChessId(isEndpointA, isDevice),
                value,
                oldValue);
    }

    private void remarkChange(
            Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges,
            String ownersString,
            NotificationDTO notification) {

        // utility method to remark change
        List<NotificationDTO> cablesUnexpectedChanges =
                mapOwnersCablesUnexpectedChanges.get(ownersString);
        if (cablesUnexpectedChanges == null) {
            cablesUnexpectedChanges = new ArrayList<>();
        }
        cablesUnexpectedChanges.add(notification);
        mapOwnersCablesUnexpectedChanges.put(ownersString, cablesUnexpectedChanges);

    }
    private void remarkChangeUnexpectedCable(
            Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges,
            Cable cable,
            String oldValue,
            String newValue) {

        // utility method to remark unexpected change for cable
        //     track unexpected change - if replace non-empty value
        if (!StringUtils.isEmpty(oldValue)) {
            remarkChange(mapOwnersCablesUnexpectedChanges,
                    cable.getOwnersString(),
                    new NotificationDTO(
                            cable,
                            null,
                            cable.getName(),
                            CableColumn.FBS_TAG,
                            null,
                            oldValue,
                            newValue));
        }
    }
    private void remarkChangeUnexpectedEndpoint(
            Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges,
            Cable cable,
            Endpoint endpoint,
            boolean isEndpointA,
            boolean isDevice,
            boolean isFbsTag,
            String oldValue,
            String newValue) {

        // utility method to remark unexpected change for endpoint
        //     track unexpected change - if replace non-empty value
        if (!StringUtils.isEmpty(oldValue)) {
            remarkChange(
                    mapOwnersCablesUnexpectedChanges,
                    cable.getOwnersString(),
                    new NotificationDTO(
                            null,
                            endpoint,
                            cable.getName(),
                            getCableColumn(isEndpointA, isDevice, isFbsTag),
                            null,
                            oldValue,
                            newValue));
        }
    }
    private CableColumn getCableColumn(boolean isEndpointA, boolean isDevice, boolean isFbsTag) {
        // utility method to get cable column.
        //     endpoint a or b
        //     device or enclosure
        //     fbs tag or ess name

        return isEndpointA
                ? isDevice
                    ? isFbsTag
                        ? CableColumn.FROM_FBS_TAG
                        : CableColumn.FROM_ESS_NAME
                    : isFbsTag
                        ? CableColumn.FROM_ENCLOSURE_FBS_TAG
                        : CableColumn.FROM_ENCLOSURE_ESS_NAME
                : isDevice
                    ? isFbsTag
                        ? CableColumn.TO_FBS_TAG
                        : CableColumn.TO_ESS_NAME
                    : isFbsTag
                        ? CableColumn.TO_ENCLOSURE_FBS_TAG
                        : CableColumn.TO_ENCLOSURE_ESS_NAME;
    }
    private String getCableColumnChessId(boolean isEndpointA, boolean isDevice) {
        // utility method to get cable column.
        //     endpoint a or b
        //     device or enclosure
        //     fbs tag or ess name

        return isEndpointA
                ? isDevice
                    ? "From " + DEVICE_CHESS_ID
                    : "From " + ENCLOSURE_CHESS_ID
                : isDevice
                    ? "To " + DEVICE_CHESS_ID
                    : "To " + ENCLOSURE_CHESS_ID;
    }

    /**
     * Retrieves cables and validates them in one transaction to ensure no other
     * changes happen in the mean time.
     *
     * @param start
     *            the number of the starting cable to validate
     * @param size
     *            amount of cables to validate
     * @param userId
     *            username of the user invoking validation
     * @return true if validation was performed, false if there were no more cables
     *         to validate
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean getAndValidateCables(int start, int size, String userId) {
        final List<Cable> cables = lazyService.findLazy(start, size, CableColumnUI.MODIFIED.getValue(),
                SortOrder.ASCENDING, new HashMap<String, Object>(), null);
        if (cables == null || cables.isEmpty()) {
            return false;
        }
        validateCables(userId, cables);
        return true;
    }

    /**
     * Validates the consistency of cable data and updates the data and/or status.
     *
     * @param userId
     *            id of the user invoking validation
     * @param cables
     *            the cables to validate
     */
    public void validateCables(String userId, Iterable<Cable> cables) {
        LOGGER.log(Level.FINEST, "Validating  cables");

        for (final Cable cable : cables) {
            if (cable.getStatus() != CableStatus.DELETED) {
                updateValidity(userId, cable);
            }
        }
        LOGGER.log(Level.FINEST, "  Validation completed");
    }

    /**
     * Updates the validity of the cable.
     *
     * @param userId
     *            id of the user invoking validation
     * @param cable
     *            the cable to validate
     */
    public void updateValidity(String userId, Cable cable) {
        // update validity
        //     retrieve old (before update) validity
        //     check validity
        //         cable
        //             cable article
        //             cable type
        //             cable article or cable type
        //          -> no check for container
        //             endpoint A
        //             endpoint B
        //     compare & set validity
        //         put together validity
        //         compare (before and after)
        //     prepare & persist
        //         endpoint A
        //         endpoint B
        //         cable
        //         history

        // retrieve old (before update) validity
        //     used for comparison
        final Cable.Validity oldCableValidity = cable.getValidity();
        final NameStatus oldEndpointANameStatus = cable.getEndpointA().getNameStatus();
        final NameStatus oldEndpointBNameStatus = cable.getEndpointB().getNameStatus();
        final Endpoint.Validity oldEndpointAValidity = cable.getEndpointA().getValidity();
        final Endpoint.Validity oldEndpointBValidity = cable.getEndpointB().getValidity();

        // history
        String endpointHistoryMessage = "";

        // cable
        //     cable article
        //     cable type
        //     cable article or cable type
        boolean isCableArticleInvalid         = ValidityUtil.isCableArticleInvalid(cable);
        boolean isCableTypeInvalid            = ValidityUtil.isCableTypeInvalid(cable);
        boolean isCableArticleCableTypeActive = ValidityUtil.isCableArticleCableTypeActive(cable);

        // cable
        //     endpoint A
        //         device, device fbs tag
        //         name status
        //         uuid
        //         name status
        //         connector
        //     order of checking things are important, since ess name, uuid may be changed
        //         consider content in naming
        //         possibly update - if uuid available and name out of sync
        boolean isCableEndpointAEssNameFbsTagAvailable = ValidityUtil.isCableEndpointEssNameFbsTagAvailable(cable, true);
        boolean isCableEndpointAESSNameDeletedPre      = ValidityUtil.isCableEndpointEssNameDeleted(cable, true, names);
        boolean isCableEndpointAESSNameUuidInvalid     = ValidityUtil.isCableEndpointEssNameUuidInvalid(cable, true, names);
        String  messageUuidUpdateCableEndpointAEssName = ValidityUtil.uuidUpdateCableEndpointEssName(cable, true, names);
        boolean isCableEndpointAESSNameDeletedPost     = ValidityUtil.isCableEndpointEssNameDeleted(cable, true, names);
        boolean isCableEndpointAConnectorInvalid       = ValidityUtil.isCableEndpointConnectorInvalid(cable, true);

        if (StringUtils.isNotEmpty(messageUuidUpdateCableEndpointAEssName)) {
            endpointHistoryMessage += messageUuidUpdateCableEndpointAEssName;
        }

        // cable
        //     endpoint B
        //         device, device fbs tag
        //         name status
        //         uuid
        //         name status
        //         connector
        //     order of checking things are important, since ess name, uuid may be changed
        //         consider content in naming
        //         possibly update - if uuid available and name out of sync
        boolean isCableEndpointBEssNameFbsTagAvailable = ValidityUtil.isCableEndpointEssNameFbsTagAvailable(cable, false);
        boolean isCableEndpointBESSNameDeletedPre      = ValidityUtil.isCableEndpointEssNameDeleted(cable, false, names);
        boolean isCableEndpointBESSNameUuidInvalid     = ValidityUtil.isCableEndpointEssNameUuidInvalid(cable, false, names);
        String  messageUuidUpdateCableEndpointBEssName = ValidityUtil.uuidUpdateCableEndpointEssName(cable, false, names);
        boolean isCableEndpointBESSNameDeletedPost     = ValidityUtil.isCableEndpointEssNameDeleted(cable, false, names);
        boolean isCableEndpointBConnectorInvalid       = ValidityUtil.isCableEndpointConnectorInvalid(cable, false);

        if (StringUtils.isNotEmpty(messageUuidUpdateCableEndpointBEssName)) {
            endpointHistoryMessage += messageUuidUpdateCableEndpointBEssName;
        }

        // compare & set validity
        //     put together validity
        //     compare (before and after)
        final boolean isEndpointAValid = isCableEndpointAEssNameFbsTagAvailable
                && !isCableEndpointAESSNameDeletedPre
                && !isCableEndpointAESSNameUuidInvalid
                && !isCableEndpointAESSNameDeletedPost
                && !isCableEndpointAConnectorInvalid;
        final boolean isEndpointBValid = isCableEndpointBEssNameFbsTagAvailable
                && !isCableEndpointBESSNameDeletedPre
                && !isCableEndpointBESSNameUuidInvalid
                && !isCableEndpointBESSNameDeletedPost
                && !isCableEndpointBConnectorInvalid;
        final boolean cableValid =
                !isCableArticleInvalid
                && !isCableTypeInvalid
                && isCableArticleCableTypeActive
                && isEndpointAValid
                && isEndpointBValid;
        final boolean endpointUpdated = StringUtils.isNotEmpty(endpointHistoryMessage);

        final Cable.Validity cableValidity = cableValid
                ? Cable.Validity.VALID
                : Cable.Validity.DANGLING;
        final NameStatus endpointANameStatus = cable.getEndpointA().getNameStatus();
        final NameStatus endpointBNameStatus = cable.getEndpointB().getNameStatus();
        final Endpoint.Validity endpointAValidity = isEndpointAValid
                ? Endpoint.Validity.VALID
                : Endpoint.Validity.DANGLING;
        final Endpoint.Validity endpointBValidity = isEndpointBValid
                ? Endpoint.Validity.VALID
                : Endpoint.Validity.DANGLING;

        if ((oldCableValidity == cableValidity
                && oldEndpointAValidity == endpointAValidity
                && oldEndpointBValidity == endpointBValidity
                && oldEndpointANameStatus == endpointANameStatus
                && oldEndpointBNameStatus == endpointBNameStatus)
                && !endpointUpdated) {
            return;
        }

        // prepare & persist
        StringBuilder historyEntry = new StringBuilder(300);
        historyEntry
            .append(HistoryService.getDiffForAttributes("Validity", cableValidity, oldCableValidity))
            .append(HistoryService.getDiffForAttributes("Endpoint A validity", endpointAValidity, oldEndpointAValidity))
            .append(HistoryService.getDiffForAttributes("Endpoint B validity", endpointBValidity, oldEndpointBValidity))
            .append(HistoryService.getDiffForAttributes("Endpoint A NameStatus", endpointANameStatus, oldEndpointANameStatus))
            .append(HistoryService.getDiffForAttributes("Endpoint B NameStatus", endpointBNameStatus, oldEndpointBNameStatus));
        historyEntry.append(endpointHistoryMessage);

        cable.setValidity(cableValidity);
        cable.getEndpointA().setValidity(endpointAValidity);
        cable.getEndpointB().setValidity(endpointBValidity);
        cable.getEndpointA().setNameStatus(endpointANameStatus);
        cable.getEndpointB().setNameStatus(endpointBNameStatus);
        em.merge(cable.getEndpointA());
        em.merge(cable.getEndpointB());
        em.merge(cable);

        StringBuilder validityEntry = new StringBuilder(300);
        validityEntry.append("Cable validity: " + (cable.isValid() ? "valid" : "invalid") + "\n");
        if (!cableValid) {
            validityEntry.append(ValidityUtil.getReasonsForProblem(cable, names, ".\n"));
        }
        historyService.createHistoryEntry(EntityTypeOperation.VALIDATE, cable.getName(), EntityType.CABLE,
                cable.getId(), historyEntry.toString(), validityEntry.toString(), userId);
    }

    /**
     * @return a sequential number that is currently not yet used by the database
     */
    private Integer getUnusedCableSeqNumber() {
        Integer maxNumber = em.createQuery("SELECT MAX(c.seqNumber) FROM Cable c", Integer.class).getSingleResult();
        return maxNumber != null ? maxNumber + 1 : 1;
    }

    /**
     * Generates and returns string with all changed cable attributes.
     *
     * @param cable
     *            new cable
     * @param oldCable
     *            old cable
     *
     * @return string with all changed cable attributes.
     */
    private String getChangeString(Cable cable, Cable oldCable) {
        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.NAME.getColumnLabel(), cable.getName(), oldCable.getName()));
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.FBS_TAG.getColumnLabel(), cable.getFbsTag(), oldCable.getFbsTag()));
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.OWNERS.getColumnLabel(), cable.getOwners(), oldCable.getOwners()));
        if (!cable.getSystem().equals(oldCable.getSystem())) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.SYSTEM.getColumnLabel(), cable.getSystem(), oldCable.getSystem()));
        }
        if (!cable.getSubsystem().equals(oldCable.getSubsystem())) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.SUBSYSTEM.getColumnLabel(), cable.getSubsystem(), oldCable.getSubsystem()));
        }
        if (!cable.getCableClass().equals(oldCable.getCableClass())) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.CLASS.getColumnLabel(), cable.getCableClass(), oldCable.getCableClass()));
        }
        String nameCableArticle = cable.getCableArticle() != null ? cable.getCableArticle().getName() : null;
        String nameoldCableArticle = oldCable.getCableArticle() != null ? oldCable.getCableArticle().getName() : null;
        if (!StringUtils.equals(nameCableArticle, nameoldCableArticle)) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.CABLE_ARTICLE.getColumnLabel(),
                    nameCableArticle,
                    nameoldCableArticle));
        }
        String nameCableType = cable.getCableType() != null ? cable.getCableType().getName() : null;
        String nameoldCableType = oldCable.getCableType() != null ? oldCable.getCableType().getName() : null;
        if (!StringUtils.equals(nameCableType, nameoldCableType)) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.CABLE_TYPE.getColumnLabel(),
                    nameCableType,
                    nameoldCableType));
        }
        if (cable.getContainer() != null && oldCable.getContainer() != null) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.CONTAINER.getColumnLabel(), cable.getContainer(), oldCable.getContainer()));
        }
        if (cable.getComments() != null && oldCable.getComments() != null) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.COMMENTS.getColumnLabel(), cable.getComments(), oldCable.getComments()));
        }
        if (cable.getEndpointA() != null && oldCable.getEndpointA() != null) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_ESS_NAME.getColumnLabel(),
                    cable.getEndpointA().getDevice(),
                    oldCable.getEndpointA().getDevice()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_FBS_TAG.getColumnLabel(),
                    cable.getEndpointA().getDeviceFbsTag(),
                    oldCable.getEndpointA().getDeviceFbsTag()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_LBS_TAG.getColumnLabel(),
                    cable.getEndpointA().getBuilding(),
                    oldCable.getEndpointA().getBuilding()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_ENCLOSURE_ESS_NAME.getColumnLabel(),
                    cable.getEndpointA().getRack(),
                    oldCable.getEndpointA().getRack()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_ENCLOSURE_FBS_TAG.getColumnLabel(),
                    cable.getEndpointA().getRackFbsTag(),
                    oldCable.getEndpointA().getRackFbsTag()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_CONNECTOR.getColumnLabel(),
                    cable.getEndpointA().getConnector(),
                    oldCable.getEndpointA().getConnector()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.FROM_USER_LABEL.getColumnLabel(),
                    cable.getEndpointA().getLabel(),
                    oldCable.getEndpointA().getLabel()));
        }
        if (cable.getEndpointB() != null && oldCable.getEndpointB() != null) {
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_ESS_NAME.getColumnLabel(),
                    cable.getEndpointB().getDevice(),
                    oldCable.getEndpointB().getDevice()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_FBS_TAG.getColumnLabel(),
                    cable.getEndpointB().getDeviceFbsTag(),
                    oldCable.getEndpointB().getDeviceFbsTag()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_LBS_TAG.getColumnLabel(),
                    cable.getEndpointB().getBuilding(),
                    oldCable.getEndpointB().getBuilding()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_ENCLOSURE_ESS_NAME.getColumnLabel(),
                    cable.getEndpointB().getRack(),
                    oldCable.getEndpointB().getRack()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_ENCLOSURE_FBS_TAG.getColumnLabel(),
                    cable.getEndpointB().getRackFbsTag(),
                    oldCable.getEndpointB().getRackFbsTag()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_CONNECTOR.getColumnLabel(),
                    cable.getEndpointB().getConnector(),
                    oldCable.getEndpointB().getConnector()));
            sb.append(HistoryService.getDiffForAttributes(
                    CableColumn.TO_USER_LABEL.getColumnLabel(),
                    cable.getEndpointB().getLabel(),
                    oldCable.getEndpointB().getLabel()));
        }
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.STATUS.getColumnLabel(),
                cable.getStatus(),
                oldCable.getStatus()));
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.INSTALLATION_DATE.getColumnLabel(),
                DateUtil.format(cable.getInstallationBy()),
                DateUtil.format(oldCable.getInstallationBy())));
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.INSTALLATION_PACKAGE.getColumnLabel(),
                cable.getInstallationPackage(),
                oldCable.getInstallationPackage()));
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.REVISION.getColumnLabel(),
                cable.getRevision(),
                oldCable.getRevision()));
        sb.append(HistoryService.getDiffForAttributes(
                CableColumn.ELECTRICAL_DOCUMENTATION.getColumnLabel(),
                cable.getElectricalDocumentation(),
                oldCable.getElectricalDocumentation()));
        return sb.toString();
    }

    /**
     * Returns minimal data on cables
     *
     * @param customQuery
     *            customQuery for filtering
     * @return cables with reduced data
     */
    public List<Cable> getCableEndpoints(Query customQuery) {
        // TODO refactor cableEndpoint REST
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        final Root<Cable> cableRecord = cq.from(Cable.class);
        cq.multiselect(cableRecord.get("system"), cableRecord.get("subsystem"), cableRecord.get("cableClass"),
                cableRecord.get(SEQ_NUMBER), cableRecord.get(ENDPOINT_A), cableRecord.get(ENDPOINT_B),
                cableRecord.get("modified"), cableRecord.get("status"), cableRecord.get("validity"));
        List<Predicate> predicates = new ArrayList<Predicate>();
        Predicate predicate = addCustomQuery(cb, cableRecord, customQuery);

        if (predicate != null) {
            predicates.add(predicate);
        }

        cq.where(predicates.toArray(new Predicate[] {}));

        final TypedQuery<Tuple> query = em.createQuery(cq);
        List<Tuple> result = query.getResultList();
        List<Cable> cables = new ArrayList<Cable>();
        for (Tuple tuple : result) {
            Cable cable = new Cable((String) tuple.get(0), (String) tuple.get(1), (String) tuple.get(2),
                    (Integer) tuple.get(3), Arrays.asList("REST"), (CableStatus) tuple.get(7), null, null, null,
                    (Endpoint) tuple.get(4), (Endpoint) tuple.get(5), (Date) tuple.get(6), (Date) tuple.get(6), null,
                    null, null, null, null);
            cable.setValidity((Cable.Validity) tuple.get(8));
            cables.add(cable);
        }
        return cables;
    }

    private void addSortingOrder(final String sortField, final SortOrder sortOrder, final CriteriaBuilder cb,
            final CriteriaQuery<Cable> cq, final Root<Cable> cableRecord) {
        if ((sortField != null) && (sortOrder != null) && (sortOrder != SortOrder.UNSORTED)) {
            for (CableColumnUI column : CableColumnUI.values()) {
                if (column.getValue().equals(sortField)) {
                    switch (column) {
                    case NAME:
                        List<Order> orderList = new ArrayList<Order>();
                        if (sortOrder == SortOrder.ASCENDING) {
                            orderList.add(cb.asc(cableRecord.get(CableColumnUI.SYSTEM.getValue())));
                            orderList.add(cb.asc(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue())));
                            orderList.add(cb.asc(cableRecord.get(CableColumnUI.CLASS.getValue())));
                            orderList.add(cb.asc(cableRecord.get(SEQ_NUMBER)));
                        } else {
                            orderList.add(cb.desc(cableRecord.get(CableColumnUI.SYSTEM.getValue())));
                            orderList.add(cb.desc(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue())));
                            orderList.add(cb.desc(cableRecord.get(CableColumnUI.CLASS.getValue())));
                            orderList.add(cb.desc(cableRecord.get(SEQ_NUMBER)));
                        }
                        cq.orderBy(orderList);
                        break;
                    case FBS_TAG:
                    case MODIFIED:
                    case STATUS:
                    case INSTALLATION_DATE:
                    case COMMENTS:
                    case CONTAINER:
                    case ELECTRICAL_DOCUMENTATION:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING ? cb.asc(cableRecord.get(column.getValue()))
                                : cb.desc(cableRecord.get(column.getValue())));
                        break;
                    case CABLE_ARTICLE:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc(cb.lower(cableRecord.join(column.getValue(), JoinType.LEFT).get("shortNameExternalId")))
                                : cb.desc(cb.lower(cableRecord.join(column.getValue(), JoinType.LEFT).get("shortNameExternalId"))));
                        break;
                    case CABLE_TYPE:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc(cb.lower(cableRecord.join(column.getValue(), JoinType.LEFT).get("name")))
                                : cb.desc(cb.lower(cableRecord.join(column.getValue(), JoinType.LEFT).get("name"))));
                        break;
                    case INSTALLATION_PACKAGE:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc (cb.lower(cableRecord.join(column.getValue(), JoinType.LEFT).get("name")))
                                : cb.desc(cb.lower(cableRecord.join(column.getValue(), JoinType.LEFT).get("name"))));
                        break;
                    case FROM_ESS_NAME:
                    case FROM_FBS_TAG:
                    case FROM_LBS_TAG:
                    case FROM_ENCLOSURE:
                    case FROM_ENCLOSURE_FBS_TAG:
                    case FROM_USER_LABEL:
                    case TO_ESS_NAME:
                    case TO_FBS_TAG:
                    case TO_LBS_TAG:
                    case TO_ENCLOSURE:
                    case TO_ENCLOSURE_FBS_TAG:
                    case TO_USER_LABEL:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())))
                                : cb.desc(cb
                                        .lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()))));
                        break;
                    case FROM_CONNECTOR:
                    case TO_CONNECTOR:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING
                                ? cb.asc(cb.lower(cableRecord.join(column.getEndpoint(), JoinType.LEFT)
                                        .join(column.getEndpointValue(), JoinType.LEFT).get("name")))
                                : cb.desc(cb.lower(cableRecord.join(column.getEndpoint(), JoinType.LEFT)
                                        .join(column.getEndpointValue(), JoinType.LEFT).get("name"))));
                        break;
                    default:
                        cq.orderBy(
                                sortOrder == SortOrder.ASCENDING ? cb.asc(cb.lower(cableRecord.get(column.getValue())))
                                        : cb.desc(cb.lower(cableRecord.get(column.getValue()))));
                        break;
                    }
                    break;
                }
            }
        } else {
            // Add default order for paging:
            cq.orderBy(cb.desc(cableRecord.get(CABLE_ID_COLUMN)));
        }
    }

    private List<Predicate> buildPredicateList(final CriteriaBuilder cb, final Root<Cable> cableRecord,
            final @Nullable Map<String, Object> filters) {
        final List<Predicate> predicates = Lists.newArrayList();

        for (CableColumnUI column : CableColumnUI.values()) {
            if (filters.containsKey(column.getValue())) {
                switch (column) {
                case NAME:
                    //NAME is transient, consist of SYSTEM + SUBSYSTEM + CLASS + PADDED(SEQNUMBER)

                    // acquiring cable name filter
                    final String filter = filters.get(column.getValue()).toString().toLowerCase();
                    //filter with padding (SEQNUMBER < NAME number_length limit)
                    Expression<String> exp1 = cb.concat(
                            cb.lower(cableRecord.<String>get(CableColumnUI.SYSTEM.getValue())),
                            cb.lower(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue())));
                    exp1 = cb.concat(
                            exp1,
                            cb.trim(CriteriaBuilder.Trimspec.BOTH,
                                    cb.lower(cableRecord.<String>get(CableColumnUI.CLASS.getValue()))));
                    //add PADDING using concatenation, and then TRIM SQL function
                    exp1 = cb.concat(
                            exp1,
                            cb.trim(CriteriaBuilder.Trimspec.BOTH,
                                    cb.lower(
                                            cb.function("RIGHT", String.class,
                                                    cb.concat(
                                                            sqlZeroPadding(),
                                                            cableRecord.get(SEQ_NUMBER).as(String.class)),
                                                    cb.literal(CableName.NAME_NUMBER_MIN_LENGTH).as(Integer.class)))));

                    //add LIKE search to NAME field
                    Predicate padLike = cb.like(exp1, "%" + escapeDbString(filter) + "%", '\\');

                    //add NAME number_length limit
                    Predicate padWithLimit = cb.and(cb.le(cableRecord.get(SEQ_NUMBER), nameNumberLimit()));
                    Predicate paddedExpression = cb.and(padWithLimit, padLike);

                    //filter WITHOUT padding (SEQNUMBER >= NAME number_length limit)
                    Expression<String> exp2 = cb.concat(
                            cb.lower(cableRecord.<String>get(CableColumnUI.SYSTEM.getValue())),
                            cb.lower(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue())));
                    exp2 = cb.concat(
                            exp2,
                            cb.trim(CriteriaBuilder.Trimspec.BOTH,
                                    cb.lower(cableRecord.<String>get(CableColumnUI.CLASS.getValue()))));
                    //just add SEQNUMBER as String without padding
                    exp2 = cb.concat(
                            exp2,
                            cb.trim(CriteriaBuilder.Trimspec.BOTH,
                                    cb.lower(cableRecord.get(SEQ_NUMBER).as(String.class))));

                    //add NAME number_length limit
                    Predicate unPadWithLimit = cb.and(cb.gt(cableRecord.get(SEQ_NUMBER), nameNumberLimit()));
                    Predicate unPadLike = cb.like(exp2, "%" + escapeDbString(filter) + "%", '\\');
                    Predicate unPaddedExpression = cb.and(unPadWithLimit, unPadLike);

                    //final expression
                    predicates.add(cb.or((unPaddedExpression), (paddedExpression)));

                    break;

                case STATUS:
                    if (filters.get(column.getValue()).toString()
                            .equals(CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE)) {
                        predicates.add(cb.notLike(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                                "%" + escapeDbString(CableStatus.DELETED.getDisplayName()).toLowerCase() + "%", '\\'));
                    } else {
                        CableStatus cableStatus =
                                CableStatus.convertToCableStatus((filters.get(column.getValue()).toString()));
                        String filterValue = cableStatus == null ? "" : cableStatus.name().toLowerCase();

                        predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                                "%" + escapeDbString(filterValue) + "%",
                                '\\'));
                    }
                    break;
                case MODIFIED:
                case INSTALLATION_DATE:
                case ELECTRICAL_DOCUMENTATION:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case CABLE_ARTICLE:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).get("shortNameExternalId")),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case CABLE_TYPE:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case INSTALLATION_PACKAGE:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case FBS_TAG:
                    predicates.add(
                            cb.like(
                                    cb.lower(cableRecord.get(column.getValue())),
                                    "" + escapeDbStringRegexlike(
                                            filters.get(column.getValue()).toString()).toLowerCase() + "",
                                    '\\')
                            );
                    break;
                case FROM_ESS_NAME:
                case FROM_FBS_TAG:
                case FROM_ENCLOSURE:
                case FROM_ENCLOSURE_FBS_TAG:
                case TO_ESS_NAME:
                case TO_FBS_TAG:
                case TO_ENCLOSURE:
                case TO_ENCLOSURE_FBS_TAG:
                    predicates.add(
                            cb.like(
                                    cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                                    "" + escapeDbStringRegexlike(
                                            filters.get(column.getValue()).toString()).toLowerCase() + "",
                                    '\\')
                            );
                    break;
                case FROM_LBS_TAG:
                case FROM_USER_LABEL:
                case TO_LBS_TAG:
                case TO_USER_LABEL:
                    predicates.add(cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                case FROM_CONNECTOR:
                case TO_CONNECTOR:
                    predicates.add(cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;
                default:
                    predicates.add(cb.like(cb.lower(cableRecord.get(column.getValue())),
                            "%" + escapeDbString(filters.get(column.getValue()).toString()).toLowerCase() + "%", '\\'));
                    break;

                }
            }
        }
        return predicates;
    }

    /**
     * Parses customQuery and adds its conditions to predicate, updates query
     * executionDate.
     *
     * @param cb
     *            criteriaBuilder
     * @param cableRecord
     *            cableRecord
     * @param customQuery
     *            custom Query to parse
     * @return predicate build from custom query conditions, null if there is no
     *         conditions or customQuery is null
     */
    private Predicate addCustomQuery(final CriteriaBuilder cb, final Root<Cable> cableRecord, Query customQuery) {
        if (customQuery == null || customQuery.getConditions().isEmpty()) {
            return null;
        }
        // make a copy list of conditions
        List<QueryCondition> queryConditions = new ArrayList<QueryCondition>(customQuery.getConditions());
        Predicate predicate;

        predicate = buildPredicate(cb, cableRecord, queryConditions);

        return predicate;
    }

    /**
     * Creates a predicate out of given query conditions
     *
     * @param cb
     *            criteria builder
     * @param cableRecord
     *            cable record
     * @param queryConditions
     *            query conditions
     * @return parsed predicate
     */
    private Predicate buildPredicate(final CriteriaBuilder cb, final Root<Cable> cableRecord,
            List<QueryCondition> queryConditions) {

        queryConditions.sort(new Comparator<QueryCondition>() {
            @Override
            public int compare(QueryCondition o1, QueryCondition o2) {
                return o1.getPosition().compareTo(o2.getPosition());
            }
        });

        // get 1st conditinon
        QueryCondition condition = queryConditions.get(0);
        queryConditions.remove(0);

        // parse 1st condition
        Predicate predicate = parseQueryCondition(cb, cableRecord, condition);
        Predicate predicate2;

        QueryBooleanOperator operator = condition.getBooleanOperator();

        if (QueryBooleanOperator.NOT.equals(operator)) {
            predicate = cb.not(predicate);
        }

        // we add queries until we run out of them or get to an open parenthesis
        while (!queryConditions.isEmpty()) {
            condition = queryConditions.get(0);
            if (!QueryParenthesis.OPEN.equals(condition.getParenthesisOpen())) {
                predicate2 = parseQueryCondition(cb, cableRecord, condition);
                queryConditions.remove(0);
                if (QueryBooleanOperator.NOT.equals(condition.getBooleanOperator())) {
                    predicate2 = cb.not(predicate2);
                }
            } else {
                // we found parenthesis
                break;
            }
            switch (operator) {
            case OR:
                predicate = cb.or(predicate, predicate2);
                break;
            default:
                predicate = cb.and(predicate, predicate2);
            }
            // get operator for next addition
            operator = condition.getBooleanOperator();
        }

        if (!queryConditions.isEmpty()) {
            predicate2 = buildPredicate(cb, cableRecord, queryConditions);
            switch (operator) {
            case OR:
                predicate = cb.or(predicate, predicate2);
                break;
            default:
                predicate = cb.and(predicate, predicate2);
            }
        }

        return predicate;
    }

    /**
     * Parses field, operator and value from query condition into predicate
     *
     * @param cb
     *            Criteria builder
     * @param cableRecord
     *            cable record
     * @param condition
     *            condition to parse
     * @return parsed predicate
     */
    private Predicate parseQueryCondition(final CriteriaBuilder cb, final Root<Cable> cableRecord,
            QueryCondition condition) {
        QueryComparisonOperator operator = condition.getComparisonOperator();
        CableColumnUI column = CableColumnUI.convertColumnLabel(condition.getField());
        LOGGER.log(Level.FINE, "Parsing query: " + condition.toString());
        if (column != null) {
            switch (column) {
            case NAME:
                Expression<String> exp = cb.concat(
                        cb.lower(cableRecord.get(CableColumnUI.SYSTEM.getValue()).as(String.class)),
                        cb.lower(cableRecord.get(CableColumnUI.SUBSYSTEM.getValue()).as(String.class)));
                exp = cb.concat(exp, cb.lower(cableRecord.get(CableColumnUI.CLASS.getValue()).as(String.class)));
                exp = cb.concat(exp, cb.lower(cableRecord.get(SEQ_NUMBER).as(String.class)));
                switch (operator) {
                case EQUAL:
                    return cb.equal(exp, condition.getValue().toLowerCase());
                case NOT_EQUAL:
                    return cb.notEqual(exp, condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(exp, "%" + condition.getValue().toLowerCase() + "%", '\\');
                default:
                    return cb.equal(exp, condition.getValue());
                }
            case STATUS:
                CableStatus status = UiUtility.parseIntoEnum(condition.getValue(), CableStatus.class);
                switch (operator) {
                case EQUAL:
                    return cb.equal(cableRecord.get(column.getValue()), status);
                case NOT_EQUAL:
                    return cb.notEqual(cableRecord.get(column.getValue()), status);
                default:
                    return cb.equal(cableRecord.get(column.getValue()), status);
                }
            case CONTAINER:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue())),
                            escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(cb.lower(cableRecord.get(column.getValue())),
                            "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue())),
                            "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(cb.lower(cableRecord.get(column.getValue())), condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(cableRecord.get(column.getValue()), condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
                }
            case CABLE_ARTICLE:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("shortNameExternalId")),
                            escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("shortNameExternalId")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("shortNameExternalId")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(cb.lower(cableRecord.get(column.getValue()).get("shortNameExternalId")),
                            condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("shortNameExternalId")),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()).get("shortNameExternalId"), condition.getValue());
                }
            case CABLE_TYPE:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()).get("name"), condition.getValue());
                }
            case INSTALLATION_PACKAGE:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).get("name")),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()).get("name"), condition.getValue());
                }
            case MODIFIED:
            case INSTALLATION_DATE:
                Date date = Date.from(
                        UiUtility.processUIDateTime(condition.getValue()).atZone(ZoneId.systemDefault()).toInstant());
                switch (operator) {
                case EQUAL:
                    return cb.equal(cableRecord.get(column.getValue()), date);
                case GREATER_THAN_OR_EQUAL_TO:
                    return cb.greaterThanOrEqualTo(cableRecord.get(column.getValue()), date);
                case GREATER_THAN:
                    return cb.greaterThan(cableRecord.get(column.getValue()), date);
                case LESS_THAN_OR_EQUAL_TO:
                    return cb.lessThanOrEqualTo(cableRecord.get(column.getValue()), date);
                case LESS_THAN:
                    return cb.lessThan(cableRecord.get(column.getValue()), date);
                case NOT_EQUAL:
                    return cb.notEqual(cableRecord.get(column.getValue()), date);
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()), date);
                }
            case FROM_ESS_NAME:
            case FROM_FBS_TAG:
            case FROM_LBS_TAG:
            case FROM_ENCLOSURE:
            case FROM_ENCLOSURE_FBS_TAG:
            case FROM_USER_LABEL:
            case TO_ESS_NAME:
            case TO_FBS_TAG:
            case TO_LBS_TAG:
            case TO_ENCLOSURE:
            case TO_ENCLOSURE_FBS_TAG:
            case TO_USER_LABEL:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue())),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
                }
            case FROM_CONNECTOR:
            case TO_CONNECTOR:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            "%" + escapeDbString(condition.getValue()).toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(
                            cb.lower(cableRecord.get(column.getEndpoint()).get(column.getEndpointValue()).get("name")),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
                }
            case OWNERS:
                switch (operator) {
                case STARTS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            condition.getValue().toLowerCase() + "%", '\\');
                case CONTAINS:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            "%" + condition.getValue().toLowerCase() + "%", '\\');
                case ENDS_WITH:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            "%" + condition.getValue().toLowerCase(), '\\');
                case EQUAL:
                    return cb.equal(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            condition.getValue().toLowerCase());
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
                }
            default:

                switch (operator) {
                case LIKE:
                    return cb.like(cb.lower(cableRecord.get(column.getValue()).as(String.class)),
                            condition.getValue().toLowerCase(), '\\');
                default:
                    return cb.equal(cableRecord.get(column.getValue()), condition.getValue());
                }
            }
        }
        throw new InvalidParameterException("Query filtering field: " + condition.getField() + " is invalid");

    }

    /**
     * Escapes the characters that have a special meaning in the database LIKE
     * query, '%' and '_'.
     *
     * @param dbString
     *            the string to escape
     * @return the escaped string
     */
    protected String escapeDbString(final String dbString) {
        return dbString
                .replaceAll("%",   "\\\\%")
                .replaceAll("_",   "\\\\_")
                .replaceAll("\\[", "\\\\[")
                .replaceAll("]",   "\\\\]");
    }

    /**
     * Escapes the characters that have a special meaning in the database LIKE
     * query, '%' and '_'.
     *
     * Note that it is not true regex since not working with lazy load.
     * Instead characters '%' and '_' are used.
     * '%' denotes zero to multiple number of any character.
     * '_' denotes zero or one of any character.
     *
     * @param dbString
     *            the string to escape
     * @return the escaped string
     */
    protected String escapeDbStringRegexlike(final String dbString) {
        return dbString
                .replaceAll("%",   "\\%")
                .replaceAll("_",   "\\_")
                .replaceAll("\\[", "\\\\[")
                .replaceAll("]",   "\\\\]");
    }

    /**
     * Returns the number of elements to be included in the table.
     *
     * @param filters
     *            filters to use
     * @param customQuery
     *            query to add
     *
     * @return the number of elements in the table
     */
    public long getRowCount(final @Nullable Map<String, Object> filters, Query customQuery) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
        final Root<Cable> cableRecordCount = cqCount.from(Cable.class);

        List<Predicate> predicatesCount = buildPredicateList(cb, cableRecordCount, filters);

        Predicate predicateCount = addCustomQuery(cb, cableRecordCount, customQuery);
        if (predicateCount != null) {
            predicatesCount.add(predicateCount);
        }

        cqCount.where(predicatesCount.toArray(new Predicate[] {}));

        cqCount.select(cb.count(cableRecordCount));
        return em.createQuery(cqCount).getSingleResult();
    }

    private void sendCableStatusNotification(Cable cable, String userId) {
        final Set<String> notifiedUsers = new HashSet<>();
        for (final String userName : cable.getOwners()) {
            notifiedUsers.add(userName);
        }
        for (final String userName : userDirectoryServiceFacade.getAllAdministratorUsernames()) {
            notifiedUsers.add(userName);
        }
        String subject = "Cable " + cable.getStatus().getDisplayName().toLowerCase() + " notification";
        String content = "Cable " + cable.getName() + " has been set to " + cable.getStatus().getDisplayName()
                + " in the Cable Database" + " (by " + userDirectoryServiceFacade.getUserFullNameAndEmail(userId) + ")";
        mailService.sendMail(notifiedUsers, userId, subject, content, null, null, false, true);

    }

    /** Detaches all entities. */
    private void detachCables(Iterable<Cable> cables) {
        for (Cable cable : cables) {
            em.detach(cable);
            em.detach(cable.getEndpointA());
            em.detach(cable.getEndpointB());
        }
    }

    private List<Predicate> problemSortOrder(final CriteriaBuilder cb, final Root<Cable> cableRecord,
                                             final boolean ascending) {
        final List<Predicate> predicates = Lists.newArrayList();

        Cable.Validity valid;

        if(ascending) {
            valid = Cable.Validity.VALID;
        } else {
            valid = Cable.Validity.DANGLING;
        }



        predicates.add(cb.equal(cableRecord.get("validity"), valid));

        return predicates;
    }

    /**
     * Calculates the NAME SEQNUMBER limit for deciding to which number should be padding used
     *
     * @return the maximum SEQNUMBER to decide if number has to be padded, or can be used without padding
     */
    private int nameNumberLimit() {
        return (int)Math.pow(10, CableName.NAME_NUMBER_MIN_LENGTH) - 1;
    }

    /**
     * Generates the '0' padding for SQL query for the NAME filtering
     *
     * @return a string filled with '0's, and the length is the maximum length of SEQNUMBER in NAME
     */
    private String sqlZeroPadding() {
        return StringUtils.repeat("0", CableName.NAME_NUMBER_MIN_LENGTH);
    }
}
