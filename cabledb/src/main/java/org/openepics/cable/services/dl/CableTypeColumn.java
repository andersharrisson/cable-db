/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This represents the column names that are present in the header of the spreadsheet cable type template.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableTypeColumn implements FieldIntrospection {

    NAME("NAME", "name", "Name", true),
    DESCRIPTION("DESCRIPTION", "description", "Description", true),
    SERVICE("SERVICE/FUNCTION", "service", "Service/Function", true),
    DIAMETER("DIAMETER (mm)", "diameter", "Diameter (mm)", false),
    WEIGHT("WEIGHT (kg/m)", "weight", "Weight (kg/m)", false),
    INSULATION("INSULATION", "insulation", "Insulation", true),
    JACKET("JACKET", "jacket", "Jacket", true),
    VOLTAGE_RATING("VOLTAGE RATING (V)", "voltage", "Voltage Rating (V)", false),
    FLAMABILITY("FLAMMABLE CLASS", "flammability", "Flammable Class", true),
    INSTALLATION_TYPE("INSTALLATION TYPE", "installationtype", "Installation Type", true),
    RADIATION_RESISTANCE("RADIATION RESISTANCE (mrad)", "tid", "Radiation Resistance (mrad)", false),
    MANUFACTURERS("MANUFACTURERS", "manufacturers", "Manufacturers", true),
    STATUS("STATUS", "active", "Status", true),
    COMMENTS("COMMENTS", "comments", "Comments", true);

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isStringComparisonOperator;

    private CableTypeColumn(String stringValue, String fieldName, String columnLabel,
            boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    public static CableTypeColumn convertColumnLabel(String columnLabel) {
        if (STATUS.getColumnLabel().equals(columnLabel)) {
            return STATUS;
        } else if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return DESCRIPTION;
        } else if (SERVICE.getColumnLabel().equals(columnLabel)) {
            return SERVICE;
        } else if (DIAMETER.getColumnLabel().equals(columnLabel)) {
            return DIAMETER;
        } else if (WEIGHT.getColumnLabel().equals(columnLabel)) {
            return WEIGHT;
        } else if (INSULATION.getColumnLabel().equals(columnLabel)) {
            return INSULATION;
        } else if (JACKET.getColumnLabel().equals(columnLabel)) {
            return JACKET;
        } else if (VOLTAGE_RATING.getColumnLabel().equals(columnLabel)) {
            return VOLTAGE_RATING;
        } else if (FLAMABILITY.getColumnLabel().equals(columnLabel)) {
            return FLAMABILITY;
        } else if (INSTALLATION_TYPE.getColumnLabel().equals(columnLabel)) {
            return INSTALLATION_TYPE;
        } else if (RADIATION_RESISTANCE.getColumnLabel().equals(columnLabel)) {
            return RADIATION_RESISTANCE;
        } else if (MANUFACTURERS.getColumnLabel().equals(columnLabel)) {
            return MANUFACTURERS;
        } else if (COMMENTS.getColumnLabel().equals(columnLabel)) {
            return COMMENTS;
        }
        return null;
    }

    /** @return columns list of columns that need to be excel file */
    public static CableTypeColumn[] getColumns() {
        List<CableTypeColumn> columns = new ArrayList<CableTypeColumn>(Arrays.asList(values()));
        return columns.toArray(new CableTypeColumn[columns.size()]);

    }
}
