/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.openepics.cable.CableProperties;

import com.google.common.base.Joiner;

/**
 * This is a service to send mails through configuration specified in JBOSS.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
public class MailService {
    private static final Logger LOGGER = Logger.getLogger(MailService.class.getName());

    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;

    @Resource(name = "java:jboss/mail/Default")
    private Session mailSession;

    /**
     * Sends a mail with the specified content.
     *
     * @param recipientNames
     *            the set of user names to set the mail to; the names should be in the same format as for authentication
     *            and authorization
     * @param subject
     *            the subject of the mail
     * @param content
     *            the mail content
     * @param attachments
     *            attachment
     * @param filenames
     *            file name
     * @param withAttachment
     *            if true attachment is added to the message
     * @param footer
     *            whether to include footer with the URL to the application
     * @param loggedInName The name of the user logged in to the application
     */
    public void sendMail(Iterable<String> recipientNames, String loggedInName,
            String subject, String content,
            List<InputStream> attachments, List<String> filenames,
            boolean withAttachment, boolean footer) {

        final List<String> toAddresses = new ArrayList<>();
        for (final String recipientName : recipientNames) {
            try {
                final String email = userDirectoryServiceFacade.getEmail(recipientName);

                if (email == null || email.trim().length() == 0) {
                    LOGGER.warning("User '" + recipientName + "' has no email registered in LDAP. Skipping.");
                    continue;
                }

                toAddresses.add(email);
            } catch (Exception e) {
                LOGGER.log(
                        Level.SEVERE,
                        "Could not obtain valid email for user '" + recipientName + "' from LDAP. Skipping.",
                        e);
            }
        }

        if (toAddresses.isEmpty()) {
            return;
        }

        sendMailToAddresses(toAddresses, loggedInName, subject, content, attachments, filenames, withAttachment,
                footer);
    }

    /**
     * Sends a mail with the specified content.
     *
     * @param recipientAddresses
     *            the set of user addresses to set the mail to;
     *            the names should be in the same format as for authentication and authorization
     * @param loggedInName
     *            the name of the user invoking the notification
     * @param subject
     *            the subject of the mail
     * @param content
     *            the mail content
     * @param attachments
     *            attachment
     * @param filenames
     *            file name
     * @param withAttachment
     *            if true attachment is added to the message
     * @param footer
     *            whether to include footer with the URL to the application
     */
    public void sendMailToAddresses(Iterable<String> recipientAddresses, String loggedInName,
            String subject, String content,
            List<InputStream> attachments, List<String> filenames,
            boolean withAttachment, boolean footer) {

        // Note usage of mail session that has been injected

        final StringBuilder text = new StringBuilder(content);

        if (footer) {
            text.append("\n");
            text.append("---\n");
            text.append("Cable Database URL: ");
            text.append(CableProperties.getInstance().getApplicationBaseURL());
            text.append("\n");
        }

        try {
            final Message message = new MimeMessage(mailSession);

            final List<Address> toAddresses = new ArrayList<>();
            final List<Address> ccAddresses = new ArrayList<>();
            for (final String recipientAddress : recipientAddresses) {
                toAddresses.add(new InternetAddress(recipientAddress));
            }
            message.setRecipients(Message.RecipientType.TO, toAddresses.toArray(new InternetAddress[0]));

            if (toAddresses.isEmpty()) {
                return;
            }

            message.setSubject("[CDB] " + subject);
            message.setFrom(new InternetAddress(mailSession.getProperty("mail.from"), "Cable Database"));

            if (loggedInName != null) {
                String replyToEmail = userDirectoryServiceFacade.getEmail(loggedInName);
                if (replyToEmail != null && replyToEmail.trim().length() > 0) {
                    InternetAddress userAddress = new InternetAddress(replyToEmail, loggedInName);
                    ccAddresses.add(userAddress);
                    message.setReplyTo(new InternetAddress[] { userAddress });
                    message.setRecipients(Message.RecipientType.CC, ccAddresses.toArray(new InternetAddress[0]));
                }
            }

            message.setSentDate(new Date());

            if (withAttachment
                    && attachments != null && !attachments.isEmpty()
                    && filenames != null && !filenames.isEmpty()
                    && attachments.size() == filenames.size()) {
                MimeMultipart multipart = new MimeMultipart();
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(text.toString(), "text/plain");
                multipart.addBodyPart(messagePart);
                for (int i = 0; i < attachments.size(); i++) {
                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(attachments.get(i), "application/octet-stream");
                    attachmentPart.setDataHandler(new DataHandler(dataSource));
                    attachmentPart.setFileName(filenames.get(i));
                    multipart.addBodyPart(attachmentPart);
                }
                message.setContent(multipart);
            } else {
                message.setText(text.toString());
            }

            LOGGER.fine("Assembled a message with subject '" + subject + "' to:" + Joiner.on(", ").join(toAddresses)
                    + " cc:" + Joiner.on(", ").join(ccAddresses));
            LOGGER.finest("Message text:\n" + text.toString());

            if (CableProperties.getInstance().isMailNotificationEnabled()) {
                Transport.send(message);
            }
        } catch (MessagingException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    /***
     * Sends plain text email to users with given parameters.
     * The mail properties are stored in standalone.xml, and the proper JNDI name has to be set
     * for mailSession variable.
     *
     * @param toAddressList list of recipient mail addresses
     * @param ccAddressList list of mail addresses that has to be set for CC
     * @param subject the subject of the mail
     * @param content the email content (text)
     * @param attachments list of email-attachments (streams)
     * @param filenames attachment file-names
     * @param withAttachment should the email contain the attached files, or skip them?
     */
    public void sendMail(List<String> toAddressList, List<String> ccAddressList,
                         String subject, String content,
                         List<InputStream> attachments, List<String> filenames, boolean withAttachment) {

        if (!CableProperties.getInstance().isMailNotificationEnabled()) {
            LOGGER.log(Level.FINE, "Email sending is disabled, skipping sending mail");
            return;
        }

        try {
            final MimeMessage message = new MimeMessage(mailSession);

            //set TO list
            if (toAddressList != null) {
                InternetAddress[] toAddress = new InternetAddress[toAddressList.size()];

                // To get the array of toAddresses
                for (int i = 0; i < toAddressList.size(); i++) {
                    toAddress[i] = new InternetAddress(toAddressList.get(i));
                    message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                }

            } else {
                LOGGER.log(Level.WARNING, "Empty TO list when sending mail");
                return;
            }

            //set CC list
            if (ccAddressList != null) {
                InternetAddress[] ccAddress = new InternetAddress[ccAddressList.size()];

                // To get the array of ccAddresses
                for (int i = 0; i < ccAddressList.size(); i++) {
                    ccAddress[i] = new InternetAddress(ccAddressList.get(i));
                    message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
                }
            }

            //set subject
            message.setSubject("[CDB] " + subject, StandardCharsets.UTF_8.name());
            message.setFrom(new InternetAddress(mailSession.getProperty("mail.from"), "Cable Database"));

            message.setSentDate(new Date());

            //set attachments
            if (withAttachment
                    && attachments != null && !attachments.isEmpty()
                    && filenames != null && !filenames.isEmpty()
                    && attachments.size() == filenames.size()) {
                MimeMultipart multipart = new MimeMultipart();
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(content, "text/html; charset=utf-8");
                multipart.addBodyPart(messagePart);
                for (int i = 0; i < attachments.size(); i++) {
                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(attachments.get(i), "application/octet-stream");
                    attachmentPart.setDataHandler(new DataHandler(dataSource));
                    attachmentPart.setFileName(filenames.get(i));
                    multipart.addBodyPart(attachmentPart);
                }
                message.setContent(multipart);
            } else {
                message.setContent(content, "text/html; charset=utf-8");
            }

            LOGGER.log(Level.FINE, "Trying to send email to recipients");
            Transport.send(message);

        } catch (MessagingException | IOException e) {
            LOGGER.log(Level.WARNING, "Error while trying to sending email: " + e);
            throw new RuntimeException(e);
        }
    }
}
