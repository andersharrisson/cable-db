/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import org.openepics.cable.model.*;

import java.util.Date;
import java.util.List;

/**
 * Utility class to handle cable and its parameters. Note close resemblance to Cable, CableUI.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
public class CableParams {

    private String system;
    private String subsystem;
    private String cableClass;
    private List<String> owners;
    private CableStatus status;
    private CableArticle cableArticle;
    private CableType cableType;
    private String container;
    private Endpoint endpointA;
    private Endpoint endpointB;
    private InstallationPackage installationPackage;
    private Date installationBy;
    private String comments;
    private String revision;
    private String userId;
    private boolean sendNotification;
    private String electricalDocumentation;
    private String fbsTag;

    private CableParams(){}

    public String getSystem() {
        return system;
    }

    public String getSubsystem() {
        return subsystem;
    }

    public String getCableClass() {
        return cableClass;
    }

    public List<String> getOwners() {
        return owners;
    }

    public CableStatus getStatus() {
        return status;
    }

    public CableArticle getCableArticle() {
        return cableArticle;
    }

    public CableType getCableType() {
        return cableType;
    }

    public String getContainer() {
        return container;
    }

    public Endpoint getEndpointA() {
        return endpointA;
    }

    public Endpoint getEndpointB() {
        return endpointB;
    }

    public InstallationPackage getInstallationPackage() {
        return installationPackage;
    }

    public Date getInstallationBy() {
        return installationBy != null ? new Date(installationBy.getTime()) : null;
    }

    public String getComments() {
        return comments;
    }

    public String getRevision() {
        return revision;
    }

    public String getUserId() {
        return userId;
    }

    public boolean isSendNotification() {
        return sendNotification;
    }

    public String getElectricalDocumentation() {
        return electricalDocumentation;
    }

    public String getFbsTag() {
        return fbsTag;
    }

    /**
     * Utility class to collect cable content.
     */
    public static class Builder{

        private String system;
        private String subsystem;
        private String cableClass;
        private List<String> owners;
        private CableStatus status;
        private CableArticle cableArticle;
        private CableType cableType;
        private String container;
        private Endpoint endpointA;
        private Endpoint endpointB;
        private InstallationPackage installationPackage;
        private Date installationBy;
        private String comments;
        private String revision;
        private String userId;
        private boolean sendNotification;
        private String electricalDocumentation;
        private String fbsTag;

        /**
         * Return builder with cable system set.
         *
         * @param system cable system
         * @return builder with cable system set
         */
        public Builder withSystem(String system) {
            this.system = system;

            return this;
        }

        /**
         * Return builder with cable subsystem set.
         *
         * @param subSystem cable subsystem
         * @return builder with cable subsystem set
         */
        public Builder withSubSystem(String subSystem) {
            this.subsystem = subSystem;

            return this;
        }

        /**
         * Return builder with cable class set.
         *
         * @param cableClass cable class
         * @return builder with cable class set
         */
        public Builder withCableClass(String cableClass) {
            this.cableClass = cableClass;

            return this;
        }

        /**
         * Return builder with cable owners set.
         *
         * @param owners cable owners
         * @return builder with cable owners set
         */
        public Builder withOwners(List<String> owners) {
            this.owners = owners;

            return this;
        }

        /**
         * Return builder with cable status set.
         *
         * @param cableStatus cable status
         * @return builder with cable status set
         */
        public Builder withCableStatus(CableStatus cableStatus) {
            this.status = cableStatus;

            return this;
        }

        /**
         * Return builder with cable article set.
         *
         * @param cableArticle cable article
         * @return builder with cable article set
         */
        public Builder withCableArticle(CableArticle cableArticle) {
            this.cableArticle = cableArticle;

            return this;
        }

        /**
         * Return builder with cable type set.
         *
         * @param cableType cable type
         * @return builder with cable type set
         */
        public Builder withCableType(CableType cableType) {
            this.cableType = cableType;

            return this;
        }

        /**
         * Return builder with cable container set.
         *
         * @param container cable container
         * @return builder with cable container set
         */
        public Builder withContainer(String container) {
            this.container = container;

            return this;
        }

        /**
         * Return builder with endpoint A of cable set.
         *
         * @param endpointA endpoint A of cable
         * @return builder with endpoing A of cable set
         */
        public Builder withEndpointA(Endpoint endpointA) {
            this.endpointA = endpointA;

            return this;
        }

        /**
         * Return builder with endpoint B of cable set.
         *
         * @param endpointB endpoint B of cable
         * @return builder with endpoint B of cable
         */
        public Builder withEndpointB(Endpoint endpointB) {
            this.endpointB = endpointB;

            return this;
        }

        /**
         * Return builder with installation package for cable set.
         *
         * @param installationPackage installation package for cable
         * @return builder with installation package for cable set
         */
        public Builder withInstallationPackage(InstallationPackage installationPackage) {
            this.installationPackage = installationPackage;

            return this;
        }

        /**
         * Return builder with installation by (date) for cable set.
         *
         * @param installationBy installation by (date) for cable
         * @return builder with installation by (date) for cable set
         */

        public Builder withInstallationBy(Date installationBy) {
            this.installationBy = installationBy != null ? new Date(installationBy.getTime()) : null;

            return this;
        }

        /**
         * Return builder with comments for cable set.
         *
         * @param comments comments for cable
         * @return builder with comments for cable set
         */
        public Builder withComments(String comments) {
            this.comments = comments;

            return this;
        }

        /**
         * Return builder with revision for cable set.
         *
         * @param revision revision for cable
         * @return builder with revision for cable set
         */
        public Builder withRevision(String revision) {
            this.revision = revision;

            return this;
        }

        /**
         * Return builder with user id for cable set.
         *
         * @param userId user id for cable
         * @return builder with user id for cable set
         */
        public Builder withUserId(String userId) {
            this.userId = userId;

            return this;
        }

        /**
         * Return builder with send notification flag for cable set.
         *
         * @param sendNotification send notification flag for cable
         * @return builder with send notification flag for cable set
         */
        public Builder withSendNotification(boolean sendNotification) {
            this.sendNotification = sendNotification;

            return this;
        }

        /**
         * Return builder with electrical documentation for cable set.
         *
         * @param electricalDocumentation electrical documentation for cable
         * @return builder with electrical documentation for cable
         */
        public Builder withElectricalDocumentation(String electricalDocumentation) {
            this.electricalDocumentation = electricalDocumentation;

            return this;
        }

        /**
         * Return builder with fbs tag for cable set.
         *
         * @param fbsTag the fbs tag for the cable
         * @return builder with fbs tag for cable
         */
        public Builder withFbsTag(String fbsTag) {
            this.fbsTag = fbsTag;

            return this;
        }

        /**
         * Collect and set values, and return CableParams object.
         *
         * @return CableParams object
         */
        public CableParams build(){
            CableParams result = new CableParams();

            result.system = this.system;
            result.subsystem = this.subsystem;
            result.cableClass = this.cableClass;
            result.owners = this.owners;
            result.status = this.status;
            result.cableArticle = this.cableArticle;
            result.cableType = this.cableType;
            result.container = this.container;
            result.endpointA = this.endpointA;
            result.endpointB = this.endpointB;
            result.installationPackage = this.installationPackage;
            result.installationBy = this.installationBy;
            result.comments = this.comments;
            result.revision = this.revision;
            result.userId = this.userId;
            result.sendNotification = this.sendNotification;
            result.electricalDocumentation = this.electricalDocumentation;
            result.fbsTag = this.fbsTag;

            return result;
        }
    }
}
