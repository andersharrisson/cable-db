/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the service layer that handles installation package import and export operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class InstallationPackageImportExportService {

    private static final Logger LOGGER = Logger.getLogger(InstallationPackageImportExportService.class.getName());

    private static final String NO_IMPORT_PERMISSION = "You do not have permission to import installation packages.";
    private static final String NO_EXPORT_PERMISSION = "You do not have permission to export installation packages.";

    @Resource
    private EJBContext context;
    @Inject
    private InstallationPackageLoader installationPackageLoader;

    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;

    @Inject
    private MailService mailService;

    /**
     * Imports installation packages from Excel spreadsheet.
     *
     * @param inputStream
     *            the input stream representing the Excel file
     *
     * @return the report with validation state and messages
     *
     * @throws IllegalStateException
     *             if current user does not have permission for this action
     */
    public LoaderResult<InstallationPackage> importInstallationPackages(InputStream inputStream) {
        return importInstallationPackages(inputStream, false);
    }

    /**
     * Imports installation packages from Excel spreadsheet.
     *
     * @param inputStream
     *            the input stream representing the Excel file
     * @param test
     *            if true, only a test whether the import would succeed is performed,
     *            and no installation packages are imported
     *
     * @return the report with validation state and messages
     *
     * @throws IllegalStateException
     *             if current user does not have permission for this action
     */
    public LoaderResult<InstallationPackage> importInstallationPackages(InputStream inputStream, boolean test) {
        // We read inputStream into byte as we need to use it twice.
        byte[] bytes;
        try {
            bytes = ByteStreams.toByteArray(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ByteArrayInputStream byteInputstream = new ByteArrayInputStream(bytes);

        if (!canImportInstallationPackages()) {
            throw new IllegalStateException(NO_IMPORT_PERMISSION);
        }

        LoaderResult<InstallationPackage> report = installationPackageLoader.load(byteInputstream, test);

        if (!test && !report.isError()) {
            // Send mail
            final Set<String> notifiedUsers = new HashSet<>();
            for (final InstallationPackage installationPackage : report.getAffected()) {
                notifiedUsers.addAll(installationPackage.getCableCoordinators());
            }
            for (final String userName : userDirectoryServiceFacade.getAllAdministratorUsernames()) {
                notifiedUsers.add(userName);
            }

            if (!Lists.newArrayList(report.getAffected()).isEmpty()) {
                String content = "Installation packages have been imported in the Cable Database" + " (by "
                        + userDirectoryServiceFacade.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")";
                byteInputstream.reset();
                mailService.sendMail(notifiedUsers,
                        sessionService.getLoggedInName(),
                        "Installation packages imported notification",
                        content,
                        Arrays.asList(byteInputstream),
                        Arrays.asList("cdb_installation_packages_imported.xlsx"),
                        true,
                        true);
            }
        } else {
            context.setRollbackOnly();
        }

        LOGGER.fine("Returning result: " + report);

        return report;
    }

    /**
     * Exports installation packages cables to an Excel spreadsheet.
     *
     * @param installationPackages
     *            the installation packages to export
     * @return the input stream representing the Excel file
     *
     * @throws IllegalStateException
     *             if current user does not have permission for this action
     */
    public InputStream exportInstallationPackages(Iterable<InstallationPackage> installationPackages) {

        if (!sessionService.isLoggedIn()) {
            throw new IllegalStateException(NO_EXPORT_PERMISSION);
        }

        final InstallationPackageSaver installationPackageSaver = new InstallationPackageSaver(installationPackages);
        return installationPackageSaver.save();
    }

    /** @return true if the current user can import installation packages, else false */
    public boolean canImportInstallationPackages() {
        return sessionService.canAdminister();
    }
}
