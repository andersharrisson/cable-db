/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.io.Serializable;

import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.faces.bean.RequestScoped;
import javax.interceptor.Interceptor;

import se.esss.ics.rbac.loginmodules.service.RBACSSOSessionService;

/**
 * A session bean holding the logged in user information.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@RequestScoped
@Alternative
@Priority(Interceptor.Priority.APPLICATION + 10)
public class SessionService extends RBACSSOSessionService implements Serializable {

    private static final long serialVersionUID = 887338652473840125L;

    /** @return true if the user can administer database, else false; if no user logged in, false is returned */
    public boolean canAdminister() {
        return hasPermission(CableRBACDefinitions.ADMINISTER_CABLE_DB_PERMISSION);
    }

    /**
     * @return true if the user can manage cables that they own, else false; if no user logged in, false is returned
     */
    public boolean canManageOwnedCables() {
        return hasPermission(CableRBACDefinitions.MANAGE_OWNED_CABLES_PERMISSION);
    }
}
