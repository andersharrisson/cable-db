/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the spreadsheet cable template.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableColumn implements FieldIntrospection {

    SYSTEM("SYSTEM", "system", "Sy", true, false),
    SUBSYSTEM("SUBSYSTEM", "subsystem", "Su", true, false),
    CLASS("CLASS", "cableClass", "Cl", true, true),
    NAME("CABLE NAME", "name", "Name", true, false),
    MODIFIED("MODIFIED", "modified", "Modified", false, false),
    CABLE_TYPE("CABLE TYPE", "cableType", "Type", true, true),
    CONTAINER("CONTAINER (bundle)", "container", "Container (bundle)", true, true),
    DEVICE_A_NAME("DEVICE A (FROM):NAME", "device", "From Device A", true, true),
    DEVICE_A_BUILDING("DEVICE A (FROM):BUILDING", "building", "Location Device A (building)", true, true),
    DEVICE_A_RACK("DEVICE A (FROM):RACK", "rack", "Location Device A (rack)", true, true),
    DEVICE_A_CONNECTOR("DEVICE A (FROM):CONNECTOR", "connector", "Connector A", true, true),
    DEVICE_A_WIRING("DEVICE A (FROM):WIRING", "drawing", "Connector A (wiring)", true, true),
    DEVICE_A_USER_LABEL("DEVICE A (FROM):USER LABEL", "label", "User Label A", true, true),
    DEVICE_B_NAME("DEVICE B (TO):NAME", "device", "To Device B", true, true),
    DEVICE_B_BUILDING("DEVICE B (TO):BUILDING", "building", "Location Device B (building)", true, true),
    DEVICE_B_RACK("DEVICE B (TO):RACK", "rack", "Location Device B (rack)", true, true),
    DEVICE_B_CONNECTOR("DEVICE B (TO):CONNECTOR", "connector", "Connector B", true, true),
    DEVICE_B_WIRING("DEVICE B (TO):WIRING", "drawing", "Connector B (wiring)", true, true),
    DEVICE_B_USER_LABEL("DEVICE B (TO):USER LABEL", "label", "User Label B", true, true),
    ROUTINGS("ROUTINGS", "routingsString", "Routing", true, true),
    OWNERS("OWNERS", "ownersString", "Owners", true, true),
    STATUS("STATUS", "status", "Status", true, true),
    INSTALLATION_DATE("INSTALLATION DATE", "installationBy", "Installation Date", true, false),
    TERMINATION_DATE("TERMINATION DATE", "terminationBy", "Termination Date", true, false),
    QUALITY_REPORT("QUALITY REPORT", "qualityReport", "Quality Report", false, true),
    AUTOCALCULATEDLENGTH("AUTOCALCULATED LENGTH", "autocalculated", "Autocalculated (length)", true, true),
    BASELENGTH("BASE LENGTH (m)", "baselength", "Base Length (m)", true, false),
    LENGTH("LENGTH (m)", "length", "Length (m)", true, false);

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isStringComparisonOperator;

    private CableColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public static CableColumn convertColumnLabel(String columnLabel) {
        if (MODIFIED.getColumnLabel().equals(columnLabel)) {
            return MODIFIED;
        } else if (CABLE_TYPE.getColumnLabel().equals(columnLabel)) {
            return CABLE_TYPE;
        } else if (CONTAINER.getColumnLabel().equals(columnLabel)) {
            return CONTAINER;
        } else if (DEVICE_A_NAME.getColumnLabel().equals(columnLabel)) {
            return DEVICE_A_NAME;
        } else if (DEVICE_A_BUILDING.getColumnLabel().equals(columnLabel)) {
            return DEVICE_A_BUILDING;
        } else if (DEVICE_A_RACK.getColumnLabel().equals(columnLabel)) {
            return DEVICE_A_RACK;
        } else if (DEVICE_A_CONNECTOR.getColumnLabel().equals(columnLabel)) {
            return DEVICE_A_CONNECTOR;
        } else if (DEVICE_A_USER_LABEL.getColumnLabel().equals(columnLabel)) {
            return DEVICE_A_USER_LABEL;
        } else if (DEVICE_B_NAME.getColumnLabel().equals(columnLabel)) {
            return DEVICE_B_NAME;
        } else if (DEVICE_B_BUILDING.getColumnLabel().equals(columnLabel)) {
            return DEVICE_B_BUILDING;
        } else if (DEVICE_B_RACK.getColumnLabel().equals(columnLabel)) {
            return DEVICE_B_RACK;
        } else if (DEVICE_B_CONNECTOR.getColumnLabel().equals(columnLabel)) {
            return DEVICE_B_CONNECTOR;
        } else if (DEVICE_B_USER_LABEL.getColumnLabel().equals(columnLabel)) {
            return DEVICE_B_USER_LABEL;
        } else if (SYSTEM.getColumnLabel().equals(columnLabel)) {
            return SYSTEM;
        } else if (SUBSYSTEM.getColumnLabel().equals(columnLabel)) {
            return SUBSYSTEM;
        } else if (CLASS.getColumnLabel().equals(columnLabel)) {
            return CLASS;
        } else if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (ROUTINGS.getColumnLabel().equals(columnLabel)) {
            return ROUTINGS;
        } else if (OWNERS.getColumnLabel().equals(columnLabel)) {
            return OWNERS;
        } else if (STATUS.getColumnLabel().equals(columnLabel)) {
            return STATUS;
        } else if (INSTALLATION_DATE.getColumnLabel().equals(columnLabel)) {
            return INSTALLATION_DATE;
        } else if (TERMINATION_DATE.getColumnLabel().equals(columnLabel)) {
            return TERMINATION_DATE;
        } else if (QUALITY_REPORT.getColumnLabel().equals(columnLabel)) {
            return QUALITY_REPORT;
        } else if (AUTOCALCULATEDLENGTH.getColumnLabel().equals(columnLabel)) {
            return AUTOCALCULATEDLENGTH;
        } else if (BASELENGTH.getColumnLabel().equals(columnLabel)) {
            return BASELENGTH;
        } else if (LENGTH.getColumnLabel().equals(columnLabel)) {
            return LENGTH;
        }
        return null;
    }
}
