/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the spreadsheet Routing template.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum RoutingColumn implements FieldIntrospection {

    NAME("NAME", "name", "Name", true, true),
    DESCRIPTION("DESCRIPTION", "description", "Description", true, true),
    CLASSES("CLASSES", "cableClasses", "Classes", true, true),
    LOCATION("LOCATION", "location", "Location", true, true),
    LENGTH("LENGTH (m)", "length", "Length (m)", true, false),
    MODIFIED("MODIFIED", "modified", "Modified", false, false),
    OWNER("OWNER", "owner", "Owner", false, true);

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isStringComparisonOperator;

    private RoutingColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public static RoutingColumn convertColumnLabel(String columnLabel) {
        if (MODIFIED.getColumnLabel().equals(columnLabel)) {
            return MODIFIED;
        } else if (DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return DESCRIPTION;
        } else if (CLASSES.getColumnLabel().equals(columnLabel)) {
            return CLASSES;
        } else if (LOCATION.getColumnLabel().equals(columnLabel)) {
            return LOCATION;
        } else if (LENGTH.getColumnLabel().equals(columnLabel)) {
            return LENGTH;
        } else if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (OWNER.getColumnLabel().equals(columnLabel)) {
            return OWNER;
        }
        return null;
    }
}
