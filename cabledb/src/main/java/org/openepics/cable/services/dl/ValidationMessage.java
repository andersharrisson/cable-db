/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.Serializable;

/**
 * This holds one piece of validation information tied to a location.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class ValidationMessage implements Serializable {

    private static final long serialVersionUID = 959553320078004358L;

    private String message;

    private boolean error;

    private String row;
    private String column;
    private String value;

    /**
     * Constructs the message.
     * 
     * @param message
     *            the message text
     * @param error
     *            true if this message represents an error
     */
    public ValidationMessage(String message, boolean error) {
        this(message, error, null, null, null);
    }

    /**
     * Constructs the message with location information.
     * 
     * @param message
     *            the message text
     * @param error
     *            true if this message represents an error
     * @param row
     *            the row location description, or null for no information
     * @param column
     *            the column location description, or null for no information
     */
    public ValidationMessage(String message, boolean error, String row, String column) {
        this(message, error, row, column, null);
    }

    /**
     * Constructs the message with location information.
     * 
     * @param message
     *            the message text
     * @param error
     *            true if this message represents an error
     * @param row
     *            the row location description, or null for no information
     * @param column
     *            the column location description, or null for no information
     * @param value
     *            the value of the field, or null for no information
     */
    public ValidationMessage(String message, boolean error, String row, String column, String value) {
        super();
        this.message = message;
        this.error = error;
        this.row = row;
        this.column = column;
        this.value = value;
    }

    /** @return the message text */
    public String getMessage() {
        return message;
    }

    /** @return true if this message represents an error */
    public boolean isError() {
        return error;
    }

    /** @return the row label, or null if not specified */
    public String getRow() {
        return row;
    }

    /** @return the column label, or null if not specified */
    public String getColumn() {
        return column;
    }

    /** @return the value , or null if not specified */
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        if (getRow() != null) {
            builder.append("Row ");
            builder.append(getRow());
        }
        if (getRow() != null && getColumn() != null) {
            builder.append(", ");
        }
        if (getColumn() != null) {
            builder.append("Column ");
            builder.append(getColumn());
        }
        if (getValue() != null) {
            builder.append(", Value ");
            builder.append(getValue());
        }
        if (getRow() != null || getColumn() != null) {
            builder.append(": ");
        }
        if (isError()) {
            builder.append("ERROR: ");
        }
        builder.append(getMessage());

        return builder.toString();
    }
}
