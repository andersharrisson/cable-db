/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.Artifact;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableName;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.Routing;
import org.openepics.cable.model.RoutingRow;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.NamesServiceCache;
import org.openepics.cable.services.RoutingService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceCache;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.Utility;

/**
 * <code>CableLoader</code> is {@link DataLoader} that creates and persists cables from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableLoader extends DataLoader<Cable> {

    private static final Logger LOGGER = Logger.getLogger(CableLoader.class.getName());
    private static final List<String> urlColumns = Arrays.asList(CableColumn.DEVICE_A_WIRING.toString(),
            CableColumn.DEVICE_B_WIRING.toString());

    @Inject
    private CableService cableService;
    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private ConnectorService connectorService;
    @Inject
    private RoutingService routingService;
    @Inject
    private NamesServiceCache namesServiceCache;
    @Inject
    private UserDirectoryServiceCache userDirectoryServiceCache;

    @Inject
    private SessionService sessionService;

    private Set<String> validEndpointNames;
    private Set<String> validUsernames;
    private Map<String, CableType> cableTypes = new HashMap<>();
    private Map<String, Connector> connectors = new HashMap<>();
    private Map<String, Routing> routings = new HashMap<>();
    private Map<String, Cable> originalCables = new HashMap<>();
    private Map<String, Cable> cables = new HashMap<>();

    private List<Cable> cablesToCreate;
    private List<Cable> cablesToDelete;
    private List<Cable> cablesToUpdate;
    private List<Cable> oldCablesToUpdate;

    // used for measuring time performance
    private long time;
    private long totalTime;

    /**
     * Default constructor.
     */
    public CableLoader() {
        super(urlColumns);
    }

    @Override
    public LoaderResult<Cable> load(InputStream stream, boolean test) {

        resetTime();
        validEndpointNames = namesServiceCache.getAllNames();
        validUsernames = userDirectoryServiceCache.getAllUsernames();
        cableTypes.clear();
        for (CableType cableType : cableTypeService.getAllCableTypes()) {
            cableTypes.put(cableType.getName(), cableType);
        }
        connectors.clear();
        for (Connector connector : connectorService.getConnectors()) {
            connectors.put(connector.getName(), connector);
        }
        routings.clear();
        for (Routing routing : routingService.getActiveRoutings()) {
            routings.put(routing.getName(), routing);
        }

        LoaderResult<Cable> result = super.load(stream, test);

        totalTime += getPassedTime();

        LOGGER.info("Import time: " + totalTime + " ms");

        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {

        switch (record.getCommand()) {
        case CREATE:
            createCable(record);
            break;
        case UPDATE:
            updateCable(record);
            break;
        case DELETE:
            deleteCable(record);
            break;
        default:
            // do nothing
            break;
        }
    }

    @Override
    public void updateRecordList(Iterable<DSRecord> records) {

        List<String> cableNames = new ArrayList<>();
        for (DSRecord record : records) {
            checkAllColumnsPresent(record);

            if (report.isError()) {
                stopLoad = true;
                return;
            }

            if (DSCommand.UPDATE.equals(record.getCommand()) || DSCommand.DELETE.equals(record.getCommand())) {
                String name = Utility.formatWhitespaces(record.getField(CableColumn.NAME).toString());
                cableNames.add(name);
            }
        }

        List<Cable> oldCableList = cableService.getCablesByName(cableNames);
        originalCables.clear();
        for (Cable cable : oldCableList) {
            originalCables.put(cable.getName(), cable);
        }
        cableService.detachAllCables();
        List<Cable> cableList = cableService.getCablesByName(cableNames);
        cables.clear();
        for (Cable cable : cableList) {
            cables.put(cable.getName(), cable);
        }
        cablesToCreate = new ArrayList<Cable>();
        cablesToUpdate = new ArrayList<Cable>();
        oldCablesToUpdate = new ArrayList<Cable>();
        cablesToDelete = new ArrayList<Cable>();

        super.updateRecordList(records);

        List<Cable> newCables = cableService.createCables(cablesToCreate, sessionService.getLoggedInName(), false);
        cableService.updateCables(cablesToUpdate, oldCablesToUpdate, sessionService.getLoggedInName(), false);
        cableService.deleteCables(cablesToDelete, sessionService.getLoggedInName());

        for (Cable cable : newCables) {
            report.addAffected(cable);
        }
    }

    /** Processes the record for cable creation. */
    private void createCable(DSRecord record) {

        final List<String> owners = Utility
                .splitStringIntoList(Utility.formatWhitespaces(record.getField(CableColumn.OWNERS).toString()));
        if (!hasCablePermission(owners)) {
            report.addMessage(getErrorMessage(
                    "You do not have permission to add cables you don't own" + " " + getUserAndOwnersInfo(owners) + ".",
                    record, CableColumn.OWNERS));
        }

        if (report.isError())
            return;

        checkCableAttributeValidity(record);

        if (report.isError())
            return;

        final String system = CableNumbering
                .getSystemNumber(Utility.formatWhitespaces(record.getField(CableColumn.SYSTEM).toString()));
        final String subsystem = CableNumbering
                .getSubsystemNumber(Utility.formatWhitespaces(record.getField(CableColumn.SUBSYSTEM).toString()));
        final String cableClass = CableNumbering
                .getCableClassLetter(Utility.formatWhitespaces(record.getField(CableColumn.CLASS).toString()));

        final String cableTypeString = Utility
                .formatWhitespaces(objectToString(record.getField(CableColumn.CABLE_TYPE)));
        CableType cableType = null;
        if (cableTypeString != null && !cableTypeString.isEmpty()) {
            cableType = cableTypes.get(Utility.formatWhitespaces(record.getField(CableColumn.CABLE_TYPE).toString()));
        }
        final String connectorAString = Utility
                .formatWhitespaces(objectToString(record.getField(CableColumn.DEVICE_A_CONNECTOR)));
        final String connectorBString = Utility
                .formatWhitespaces(objectToString(record.getField(CableColumn.DEVICE_B_CONNECTOR)));
        Connector connectorA = null;
        Connector connectorB = null;
        if (connectorAString != null && !connectorAString.isEmpty()) {
            connectorA = connectors
                    .get(Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_CONNECTOR).toString()));
        }
        if (connectorBString != null && !connectorBString.isEmpty()) {
            connectorB = connectors
                    .get(Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_CONNECTOR).toString()));
        }
        final String container = Utility.formatWhitespaces(record.getField(CableColumn.CONTAINER).toString());

        final Endpoint endpointA = new Endpoint(
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_NAME).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_BUILDING).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_RACK).toString()), connectorA,
                toURL(record.getField(CableColumn.DEVICE_A_WIRING)),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_USER_LABEL).toString()));

        final Endpoint endpointB = new Endpoint(
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_NAME).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_BUILDING).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_RACK).toString()), connectorB,
                toURL(record.getField(CableColumn.DEVICE_B_WIRING)),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_USER_LABEL).toString()));

        final Date installationBy = toDate(record.getField(CableColumn.INSTALLATION_DATE));
        final Date terminationBy = toDate(record.getField(CableColumn.TERMINATION_DATE));
        final CableAutoCalculatedLength autoCalculatedLength = CableAutoCalculatedLength
                .convertToCableAutoCalculatedLength(record.getField(CableColumn.AUTOCALCULATEDLENGTH).toString());
        Float baseLength = toFloat(record.getField(CableColumn.BASELENGTH));
        Float length = toFloat(record.getField(CableColumn.LENGTH));

        List<RoutingRow> routingRows = new ArrayList<>();
        if (!isNullOrEmpty(objectToString(record.getField(CableColumn.ROUTINGS)))) {
            if (!sessionService.canAdminister()) {
                report.addMessage(new ValidationMessage(
                        "ROUTING points can only be added by administrator and will remain unchanged", false,
                        record.getRowLabel(), CableColumn.ROUTINGS.getColumnLabel()));

            } else {
                final List<String> routingList = Arrays
                        .asList(record.getField(CableColumn.ROUTINGS).toString().split("\\s*,\\s*"));
                routingRows = new ArrayList<RoutingRow>();
                for (int i = 0; i < routingList.size(); i++) {
                    routingRows.add(new RoutingRow(routings.get(routingList.get(i)), null, i));
                }
            }
        }

        if (autoCalculatedLength == CableAutoCalculatedLength.YES) {
            float calculatedLength = baseLength;
            for (RoutingRow routingRow : routingRows) {
                calculatedLength += routingRow.getRouting().getLength();
            }
            if (length != null && !length.equals(calculatedLength)) {
                report.addMessage(new ValidationMessage("Entered Length will be overriden by auto calculated length",
                        false, record.getRowLabel(), CableColumn.LENGTH.getColumnLabel()));
            }
            length = calculatedLength;
        }

        CableStatus newStatus = CableStatus.INSERTED;
        final String status = Utility.formatWhitespaces(record.getField(CableColumn.STATUS).toString());
        if (!isNullOrEmpty(status)) {
            newStatus = CableStatus.convertToCableStatus(status);
            if (newStatus != CableStatus.INSERTED && !sessionService.canAdminister()) {
                report.addMessage(new ValidationMessage(
                        "New cable STATUS can be set to other than INSERTED only by administrator.", true,
                        record.getRowLabel(), null));
            }
        }

        if (!test) {
            Cable cable = new Cable(system, subsystem, cableClass, null, owners, newStatus, cableType, container,
                    endpointA, endpointB, new Date(), new Date(), routingRows, installationBy, terminationBy,
                    new Artifact(), autoCalculatedLength, length);
            cable.setBaseLength(baseLength);
            cablesToCreate.add(cable);
            createRows++;
        }

        report.addMessage(new ValidationMessage("Adding new cable.", false, record.getRowLabel(), null));
    }

    /** @return true if the current user has permission to modify cable with a all of the given owners, else false */
    private boolean hasCablePermission(List<String> owners) {
        return sessionService.canAdminister()
                || (sessionService.canManageOwnedCables() && owners.contains(sessionService.getLoggedInName()));
    }

    /** Processes the record for cable update. */
    private void updateCable(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial
                                                // validation

        final List<String> owners = Utility
                .splitStringIntoList(Utility.formatWhitespaces(record.getField(CableColumn.OWNERS).toString()));
        if (!hasCablePermission(owners)) {
            report.addMessage(getErrorMessage("You do not have permission to update cables you don't own" + " "
                    + getUserAndOwnersInfo(owners) + ".", record, CableColumn.OWNERS));
        }

        if (report.isError())
            return;

        checkCableAttributeValidity(record);

        if (!checkCableName(record))
            return;

        final String cableName = Utility.formatWhitespaces(record.getField(CableColumn.NAME).toString());
        final Cable cable = cables.get(cableName);
        final Cable oldCable = originalCables.get(cableName);

        if (cable.getStatus() == CableStatus.DELETED) {
            report.addMessage(
                    new ValidationMessage(getCableNameInfo(cableName) + " cannot be updated as it is already deleted.",
                            true, record.getRowLabel(), null, cableName));
            return;
        }

        final String system = CableNumbering
                .getSystemNumber(Utility.formatWhitespaces(record.getField(CableColumn.SYSTEM).toString()));
        final String subsystem = CableNumbering
                .getSubsystemNumber(Utility.formatWhitespaces(record.getField(CableColumn.SUBSYSTEM).toString()));
        final String cableClass = CableNumbering
                .getCableClassLetter(Utility.formatWhitespaces(record.getField(CableColumn.CLASS).toString()));

        boolean cableNameChange = false;
        if (!cable.getSystem().equals(system) || !cable.getSubsystem().equals(subsystem)
                || !cable.getCableClass().equals(cableClass)) {
            cableNameChange = true;
        }

        if (report.isError())
            return;

        final Integer seqNumber = cable.getSeqNumber();

        final String cableTypeString = Utility
                .formatWhitespaces(objectToString(record.getField(CableColumn.CABLE_TYPE)));
        CableType cableType = null;
        if (cableTypeString != null && !cableTypeString.isEmpty()) {
            cableType = cableTypes.get(Utility.formatWhitespaces(record.getField(CableColumn.CABLE_TYPE).toString()));
        }
        final String connectorAString = Utility
                .formatWhitespaces(objectToString(record.getField(CableColumn.DEVICE_A_CONNECTOR)));
        final String connectorBString = Utility
                .formatWhitespaces(objectToString(record.getField(CableColumn.DEVICE_B_CONNECTOR)));
        Connector connectorA = null;
        Connector connectorB = null;
        if (connectorAString != null && !connectorAString.isEmpty()) {
            connectorA = connectors
                    .get(Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_CONNECTOR).toString()));
        }
        if (connectorBString != null && !connectorBString.isEmpty()) {
            connectorB = connectors
                    .get(Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_CONNECTOR).toString()));
        }
        final String container = Utility.formatWhitespaces(record.getField(CableColumn.CONTAINER).toString());
        final List<String> newOwners = Utility
                .splitStringIntoList(Utility.formatWhitespaces(record.getField(CableColumn.OWNERS).toString()));

        List<RoutingRow> routingRows = new ArrayList<RoutingRow>();
        if (!isNullOrEmpty(objectToString(record.getField(CableColumn.ROUTINGS)))) {
            List<String> routingList = Arrays
                    .asList(record.getField(CableColumn.ROUTINGS).toString().split("\\s*,\\s*"));
            for (int i = 0; i < routingList.size(); i++) {
                routingRows.add(new RoutingRow(routings.get(routingList.get(i)), cable, i));
            }
            if (!oldCable.getRoutingRows().equals(routingRows) && !sessionService.canAdminister()) {
                report.addMessage(new ValidationMessage(
                        "ROUTING points can only be modified by administrator and will remain unchanged.", false,
                        record.getRowLabel(), CableColumn.ROUTINGS.getColumnLabel()));
                routingRows = oldCable.getRoutingRows();
                for (RoutingRow row : routingRows) {
                    row.setCable(cable);
                }
            }
        }
        final CableAutoCalculatedLength autoCalculatedLength = CableAutoCalculatedLength
                .convertToCableAutoCalculatedLength(record.getField(CableColumn.AUTOCALCULATEDLENGTH).toString());
        Float baseLength = toFloat(record.getField(CableColumn.BASELENGTH));
        Float length = toFloat(record.getField(CableColumn.LENGTH));

        if (autoCalculatedLength == CableAutoCalculatedLength.YES) {
            float calculatedLength = baseLength;
            for (RoutingRow routingRow : routingRows) {
                calculatedLength += routingRow.getRouting().getLength();
            }
            if (length != null && !length.equals(calculatedLength)) {
                report.addMessage(new ValidationMessage("Entered Length will be overriden by auto calculated length",
                        false, record.getRowLabel(), CableColumn.LENGTH.getColumnLabel()));
            }
            length = calculatedLength;
        }

        CableStatus status = CableStatus.convertToCableStatus(record.getField(CableColumn.STATUS).toString());

        if (status != cable.getStatus()) {
            if (status == CableStatus.INSERTED) {
                report.addMessage(getErrorMessage("Cable status cannot be changed back to INSERTED.", record,
                        CableColumn.STATUS));
            } else if (status == CableStatus.DELETED) {
                report.addMessage(
                        getErrorMessage("Cable status cannot be changed to DELETED.", record, CableColumn.STATUS));
            } else if (cable.getStatus() == CableStatus.INSERTED) {
                if (!sessionService.canAdminister())
                    report.addMessage(getErrorMessage("Only admin can change INSERTED cable status.", record,
                            CableColumn.STATUS));
            } else if (cable.getStatus() == CableStatus.ROUTED) {
                if (status != CableStatus.APPROVED) {
                    report.addMessage(getErrorMessage("This cable status can only be changed to APPROVED.", record,
                            CableColumn.STATUS));
                } else if (!(cable.getOwners().contains(sessionService.getLoggedInName())
                        || sessionService.canAdminister())) {
                    report.addMessage(getErrorMessage("This cable status can only be changed by owner or admin.",
                            record, CableColumn.STATUS));
                }
            } else if (cable.getStatus() == CableStatus.APPROVED
                    && (!sessionService.canAdminister() || status != CableStatus.ROUTED)) {
                report.addMessage(getErrorMessage("This cable status can only be changed to ROUTED by admin.", record,
                        CableColumn.STATUS));
            } else if (status == null) {
                status = cable.getStatus();
                report.addMessage(new ValidationMessage("Cable status not recognized. Will remain unchanged", false,
                        record.getRowLabel(), CableColumn.STATUS.getColumnLabel()));
            }
        }

        if (report.isError())
            return;

        cable.setSystem(system);
        cable.setSubsystem(subsystem);
        cable.setCableClass(cableClass);
        cable.setSeqNumber(seqNumber);
        cable.setOwners(newOwners);
        cable.setStatus(status);
        cable.setCableType(cableType);
        cable.setContainer(container);
        cable.setRoutingRows(routingRows);
        cable.setInstallationBy(toDate(record.getField(CableColumn.INSTALLATION_DATE)));
        cable.setTerminationBy(toDate(record.getField(CableColumn.TERMINATION_DATE)));
        cable.setAutoCalculatedLength(autoCalculatedLength);
        cable.setLength(length);
        cable.setBaseLength(baseLength);

        cable.getEndpointA().update(Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_NAME).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_BUILDING).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_RACK).toString()), connectorA,
                toURL(record.getField(CableColumn.DEVICE_A_WIRING)),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_A_USER_LABEL).toString()));
        cable.getEndpointB().update(Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_NAME).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_BUILDING).toString()),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_RACK).toString()), connectorB,
                toURL(record.getField(CableColumn.DEVICE_B_WIRING)),
                Utility.formatWhitespaces(record.getField(CableColumn.DEVICE_B_USER_LABEL).toString()));

        if (!test) {
            cablesToUpdate.add(cable);
            oldCablesToUpdate.add(oldCable);
            updateRows++;
            report.addAffected(cable);
        }
        if (!cableNameChange) {
            report.addMessage(
                    new ValidationMessage("Updating cable '" + cableName + "'.", false, record.getRowLabel(), null));
        } else {
            report.addMessage(new ValidationMessage(
                    "Warning: updating cable '" + cableName + "' will change its current cable name.", false,
                    record.getRowLabel(), null));
        }
    }

    /** Processes the record for cable deletion. */
    private void deleteCable(DSRecord record) {

        if (!checkCableName(record))
            return;

        final String cableName = Utility.formatWhitespaces(record.getField(CableColumn.NAME).toString());
        final Cable cable = originalCables.get(cableName);

        final List<String> owners = new ArrayList<String>(cable.getOwners());
        if (!hasCablePermission(owners)) {
            report.addMessage(getErrorMessage("You do not have permission to delete cables you don't own" + " "
                    + getUserAndOwnersInfo(owners) + ".", record, CableColumn.OWNERS));
        }

        if (report.isError())
            return;

        if (cable.getStatus() == CableStatus.DELETED) {
            report.addMessage(new ValidationMessage(getCableNameInfo(cableName) + " is already deleted.", true,
                    record.getRowLabel(), cableName));
        }

        if (report.isError())
            return;

        if (!test) {
            cablesToDelete.add(cable);
            deleteRows++;
            report.addAffected(cable);
        }
        report.addMessage(new ValidationMessage("Deleting cable with number " + cableName + ".", false,
                record.getRowLabel(), null));
    }

    /**
     * Check that the record has all expected columns.
     * 
     * @param record
     *            record to check.
     */
    protected void checkAllColumnsPresent(DSRecord record) {

        for (final CableColumn column : CableColumn.values()) {
            if (!column.isExcelColumn()) {
                continue;
            }
            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by importing an old template or importing a wrong file.", true));
                break;
            }
        }
    }

    /** Check that the cable attributes are valid. */
    private void checkCableAttributeValidity(DSRecord record) { // NOSONAR Cyclomatic complexity due to serial
                                                                // validation

        try {
            final String systemLabel = objectToString(record.getField(CableColumn.SYSTEM));
            final String system = CableNumbering.getSystemNumber(systemLabel);
            if (!CableName.isValidSystem(system)) {
                report.addMessage(getErrorMessage("SYSTEM field is not in the valid format.", record,
                        CableColumn.SYSTEM, system));
            } else if (!CableNumbering.isValidSystem(systemLabel)) {
                report.addMessage(
                        getErrorMessage("SYSTEM field is not valid system.", record, CableColumn.SYSTEM, systemLabel));
            }
            validateStringSize(system, CableColumn.SYSTEM);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.SYSTEM, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.SYSTEM,
                    objectToString(record.getField(CableColumn.SYSTEM))));
        }

        try {
            final String systemLabel = objectToString(record.getField(CableColumn.SYSTEM));
            final String subsystemLabel = objectToString(record.getField(CableColumn.SUBSYSTEM));
            final String subsystem = CableNumbering.getSubsystemNumber(subsystemLabel);
            if (!CableName.isValidSubsystem(subsystem)) {
                report.addMessage(getErrorMessage("SUBSYSTEM field is not in the valid format.", record,
                        CableColumn.SUBSYSTEM, subsystem));
            } else if (!CableNumbering.isValidSubsystem(systemLabel, subsystemLabel)) {
                report.addMessage(getErrorMessage("SUBSYSTEM field is not valid subsystem.", record,
                        CableColumn.SUBSYSTEM, subsystemLabel));
            }
            validateStringSize(subsystem, CableColumn.SUBSYSTEM);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.SUBSYSTEM, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.SUBSYSTEM,
                    objectToString(record.getField(CableColumn.SUBSYSTEM))));
        }

        final String cableClassLabel = objectToString(record.getField(CableColumn.CLASS));
        final String cableClass = CableNumbering.getCableClassLetter(cableClassLabel);
        try {
            if (!CableName.isValidCableClass(cableClass)) {
                report.addMessage(getErrorMessage("CABLE CLASS is not in the valid format.", record, CableColumn.CLASS,
                        cableClass));
            } else if (!CableNumbering.isValidCableClass(cableClassLabel)) {
                report.addMessage(getErrorMessage("CABLE CLASS is not valid cable class.", record, CableColumn.CLASS,
                        cableClassLabel));
            }
            validateStringSize(cableClass, CableColumn.CLASS);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.CLASS, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.CLASS, objectToString(cableClass)));
        }

        final List<String> owners = Utility
                .splitStringIntoList(Utility.formatWhitespaces(record.getField(CableColumn.OWNERS).toString()));
        try {
            if (isNullOrEmpty(owners)) {
                report.addMessage(getErrorMessage("Cable OWNERS are not set.", record, CableColumn.OWNERS));
            } else {
                if (!validOwners(owners)) {
                    report.addMessage(getErrorMessage("Cable OWNERS are not present in the user directory.", record,
                            CableColumn.OWNERS, String.join(" , ", owners)));
                }
            }
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.OWNERS, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.OWNERS, objectToString(owners)));
        }

        final String status = Utility.formatWhitespaces(record.getField(CableColumn.STATUS).toString());
        if (!isNullOrEmpty(status)) {
            if (CableStatus.convertToCableStatus(status) == null) {
                report.addMessage(getErrorMessage("Cable STATUS is invalid.", record, CableColumn.STATUS, status));
            }
        } else {
            report.addMessage(getErrorMessage("Cable STATUS is not set.", record, CableColumn.STATUS));
        }

        final String cableType = objectToString(record.getField(CableColumn.CABLE_TYPE));
        if (!isNullOrEmpty(cableType)) {
            if (cableTypes.get(record.getField(CableColumn.CABLE_TYPE).toString()) == null) {
                report.addMessage(getErrorMessage("Specified CABLE TYPE does not exit.", record, CableColumn.CABLE_TYPE,
                        cableType));
            }
        }

        final String endpointADeviceName = objectToString(record.getField(CableColumn.DEVICE_A_NAME));
        try {
            if (isNullOrEmpty(endpointADeviceName)) {
                report.addMessage(
                        getErrorMessage("ENDPOINT A DEVICE NAME is not specified.", record, CableColumn.DEVICE_A_NAME));
            } else {
                if (!validEndpointNames.contains(endpointADeviceName)) {
                    report.addMessage(getErrorMessage("ENDPOINT A DEVICE NAME is not registered in the Naming System.",
                            record, CableColumn.DEVICE_A_NAME, endpointADeviceName));
                }
                validateEndpointStringSize(endpointADeviceName, CableColumn.DEVICE_A_NAME);
            }
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.DEVICE_A_NAME, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.DEVICE_A_NAME,
                    objectToString(endpointADeviceName)));
        }

        final String connectorAname = objectToString(record.getField(CableColumn.DEVICE_A_CONNECTOR));
        if (!isNullOrEmpty(connectorAname)) {
            if (!connectors.containsKey(connectorAname)) {
                report.addMessage(getErrorMessage("CONNECTOR A is not registered present in the cable database.",
                        record, CableColumn.DEVICE_A_CONNECTOR, connectorAname));
            }
        }

        final String endpointBDeviceName = objectToString(record.getField(CableColumn.DEVICE_B_NAME));
        try {
            if (isNullOrEmpty(endpointBDeviceName)) {
                report.addMessage(
                        getErrorMessage("ENDPOINT B DEVICE NAME is not specified.", record, CableColumn.DEVICE_B_NAME));
            } else {
                if (!validEndpointNames.contains(endpointBDeviceName)) {
                    report.addMessage(getErrorMessage("ENDPOINT B DEVICE NAME is not registered in the Naming System.",
                            record, CableColumn.DEVICE_B_NAME, endpointBDeviceName));
                }
                validateEndpointStringSize(endpointBDeviceName, CableColumn.DEVICE_B_NAME);
            }
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.DEVICE_B_NAME, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.DEVICE_B_NAME,
                    objectToString(endpointBDeviceName)));
        }

        final String connectorBname = objectToString(record.getField(CableColumn.DEVICE_B_CONNECTOR));
        if (!isNullOrEmpty(connectorBname)) {
            if (!connectors.containsKey(connectorBname)) {
                report.addMessage(getErrorMessage("CONNECTOR B is not registered present in the cable database.",
                        record, CableColumn.DEVICE_B_CONNECTOR, connectorBname));
            }
        }

        final Object endpointAWiringDrawing = record.getField(CableColumn.DEVICE_A_WIRING);
        try {
            toURL(endpointAWiringDrawing);
            validateEndpointStringSize(objectToString(endpointAWiringDrawing), CableColumn.DEVICE_A_WIRING);
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid ENDPOINT A WIRING DRAWING", e);
            report.addMessage(getErrorMessage("ENDPOINT A WIRING DRAWING field is not in the valid URL link format.",
                    record, CableColumn.DEVICE_A_WIRING, objectToString(endpointAWiringDrawing)));
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.DEVICE_A_WIRING, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.DEVICE_A_WIRING,
                    objectToString(endpointAWiringDrawing)));
        }

        final Object endpointBWiringDrawing = record.getField(CableColumn.DEVICE_B_WIRING);
        try {
            toURL(endpointBWiringDrawing);
            validateEndpointStringSize(objectToString(endpointBWiringDrawing), CableColumn.DEVICE_B_WIRING);
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid ENDPOINT B WIRING DRAWING", e);
            report.addMessage(getErrorMessage("ENDPOINT B WIRING DRAWING field is not in the valid URL link format.",
                    record, CableColumn.DEVICE_B_WIRING, objectToString(endpointBWiringDrawing)));
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, "Invalid " + CableColumn.DEVICE_B_WIRING, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.DEVICE_B_WIRING,
                    objectToString(endpointBWiringDrawing)));
        }

        final String endpointALabel = objectToString(record.getField(CableColumn.DEVICE_A_USER_LABEL));
        if (endpointALabel.length() > Endpoint.MAX_LABEL_SIZE) {
            report.addMessage(
                    getErrorMessage("ENDPOINT A LABEL is over maximum allowed length " + Endpoint.MAX_LABEL_SIZE + ".",
                            record, CableColumn.DEVICE_A_USER_LABEL, endpointALabel));
        }
        final String endpointBLabel = objectToString(record.getField(CableColumn.DEVICE_B_USER_LABEL));
        if (endpointBLabel.length() > Endpoint.MAX_LABEL_SIZE) {
            report.addMessage(
                    getErrorMessage("ENDPOINT B LABEL is over maximum allowed length " + Endpoint.MAX_LABEL_SIZE + ".",
                            record, CableColumn.DEVICE_B_USER_LABEL, endpointBLabel));
        }

        try {
            toDate(record.getField(CableColumn.INSTALLATION_DATE));
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid INSTALLATION_BY", e);
            report.addMessage(getErrorMessage("INSTALLATION BY field is not in the valid format.", record,
                    CableColumn.INSTALLATION_DATE, objectToString(record.getField(CableColumn.INSTALLATION_DATE))));
        }
        try {
            toDate(record.getField(CableColumn.TERMINATION_DATE));
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid TERMINATION BY", e);
            report.addMessage(getErrorMessage("TERMINATION BY field is not in the valid format.", record,
                    CableColumn.TERMINATION_DATE, objectToString(record.getField(CableColumn.TERMINATION_DATE))));
        }
        try {
            toFloat(record.getField(CableColumn.BASELENGTH));
        } catch (NumberFormatException e) {
            LOGGER.log(Level.FINEST, "Invalid BASELENGTH", e);
            report.addMessage(getErrorMessage("BASELENGTH field is not in the valid format.", record,
                    CableColumn.BASELENGTH, objectToString(record.getField(CableColumn.BASELENGTH))));
        }
        try {
            toFloat(record.getField(CableColumn.LENGTH));
        } catch (NumberFormatException e) {
            LOGGER.log(Level.FINEST, "Invalid LENGTH", e);
            report.addMessage(getErrorMessage("LENGTH field is not in the valid format.", record, CableColumn.LENGTH,
                    objectToString(record.getField(CableColumn.LENGTH))));
        }

        if (!isNullOrEmpty(objectToString(record.getField(CableColumn.ROUTINGS)))) {
            final List<String> routingList = Arrays
                    .asList(record.getField(CableColumn.ROUTINGS).toString().split("\\s*,\\s*"));
            for (String routing : routingList) {
                if (routings.get(routing) == null) {
                    report.addMessage(getErrorMessage("ROUTING is not registered in the database.", record,
                            CableColumn.ROUTINGS, routing));
                }
            }
        }

        CableAutoCalculatedLength isAutoCalc = CableAutoCalculatedLength
                .convertToCableAutoCalculatedLength(objectToString(record.getField(CableColumn.AUTOCALCULATEDLENGTH)));
        if (isAutoCalc == null) {
            report.addMessage(getErrorMessage("AUTOCALCULATED field is not in the valid format.", record,
                    CableColumn.AUTOCALCULATEDLENGTH));

        } else if (isAutoCalc == CableAutoCalculatedLength.YES) {
            try {
                if (toFloat(record.getField(CableColumn.BASELENGTH)) == null) {
                    // base length cannot be empty if autocalculated == YES
                    report.addMessage(
                            getErrorMessage("BASELENGTH field is not in the valid format based on AUTOCALCULATED.",
                                    record, CableColumn.BASELENGTH));
                }
                ;
            } catch (NumberFormatException e) {
                report.addMessage(getErrorMessage("BASELENGTH field is not in the valid format.", record,
                        CableColumn.BASELENGTH));
            }
        }

    }

    /**
     * Checks the validity of cable name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkCableName(DSRecord record) {
        final String cableName = Utility.formatWhitespaces(record.getField(CableColumn.NAME).toString());
        if (!CableName.isValidName(cableName)) {
            report.addMessage(
                    getErrorMessage("Cable NAME is not in the valid format.", record, CableColumn.NAME, cableName));
            return false;
        }
        final Cable cable = originalCables.get(cableName);
        if (cable == null) {
            report.addMessage(getErrorMessage(getCableNameInfo(cableName) + " does not exist in the database.", record,
                    CableColumn.NAME, cableName));
            return false;
        }
        return true;
    }

    private String getUserAndOwnersInfo(List<String> owners) {
        return "(logged in user: " + sessionService.getLoggedInName() + ", owners: " + String.join(" , ", owners) + ")";
    }

    private String getCableNameInfo(String cableName) {
        return "Cable with name " + cableName;
    }

    private void validateStringSize(final String value, final CableColumn column) {
        super.validateStringSize(value, column, Cable.class);
    }

    private void validateEndpointStringSize(final String value, final CableColumn column) {
        super.validateStringSize(value, column, Endpoint.class);
    }

    /**
     * Check validity of owners
     * 
     * @param owners
     *            usernames of the owners to check
     * @return true if all usernames of the owners are correct else false
     */
    private boolean validOwners(List<String> owners) {
        for (String user : owners) {
            if (!validUsernames.contains(user)) {
                return false;
            }
        }
        return true;
    }
}
