package org.openepics.cable.services.dl;

public interface FieldIntrospection {
    public String getFieldName();
}
