// shows status dialog
function start() {
  waitUntilLoaded('statusDialog',500, function(){PF('statusDialog').show();})
}

// hide status dialog
function stop() {
  PF('statusDialog').hide();
}

// Returns the global top position the given element
function getTop(element) {
    return element.offsetParent ? element.offsetTop + getTop(element.offsetParent) : element.offsetTop;
}

// attaches style and events to make Primefaces h:commandButton behave like Primefaces p:commandbutton.
function behaveAsPCommandButton(hCommandButtonId) {

    $('#' + hCommandButtonId)
    .addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")
    .css("padding","5px 10px 4px 10px")
    .mouseover(function() {
        var el = $(this);
        if(!el.hasClass('ui-state-disabled')) {
            el.addClass('ui-state-hover');
        }
    }).mouseout(function() {
        $(this).removeClass('ui-state-active ui-state-hover');
    }).mousedown(function() {
        var el = $(this);
        if(!el.hasClass('ui-state-disabled')) {
            el.addClass('ui-state-active');
        }
    }).mouseup(function() {
        $(this).removeClass('ui-state-active');
    }).focus(function() {
        $(this).addClass('ui-state-focus');
    }).blur(function() {
        $(this).removeClass('ui-state-focus');
    });
}

function adjustFooterPosition() {
    var footerWidth = $('.footer-message').outerWidth(true);
    $('.footer-message').css({"left":(window.innerWidth-footerWidth)/2});
}


function scrollSelectedIntoView(tableWidget) {
    var selectionTable;
    if (tableWidget.selection === undefined) {
        selectionTable = tableWidget.selections;
    } else {
        selectionTable = tableWidget.selection;
    }
    if (selectionTable.length > 0) {
        var scrollableBodyHeight = tableWidget.scrollBody[0].clientHeight;
        var selectedNodeSelector = "tr[data-rk='" + selectionTable[0] + "']";
        var selectedNode = $(selectedNodeSelector);
        var selectedNodePosition = selectedNode[0].offsetTop;
        var lowerLimit = Math.max(0, selectedNodePosition - scrollableBodyHeight + selectedNode.outerHeight());
        if (tableWidget.scrollBody.scrollTop() < lowerLimit || tableWidget.scrollBody.scrollTop() > selectedNodePosition) {
            tableWidget.scrollBody.scrollTop(Math.max(0,selectedNodePosition-scrollableBodyHeight/2));
        }
    }
}

/**
 * selectEntityInTable() only works on page load, so it can be used for navigating to an entity form another screen.
 * @param globalIndex the global index of the entity
 * @param tableVarName the name of the PrimeFaces table variable
 */
function selectEntityInTable(globalIndex, tableVarName, pageSize) {

  // shows status dialog
  start();

    // selectEntityInTable() only works on page load, so it can be used for navigating to
    //     an entity form another screen.
    var tableWidget = PF(tableVarName);

    if (tableWidget.cfg.scrollLimit < 1 || tableWidget.cfg.scrollLimit < globalIndex) return;

    if (tableWidget.scrollOffset + pageSize < globalIndex) {
        tableWidget.loadLiveRows();
        setTimeout(function(){ selectEntityInTable(globalIndex, tableVarName, pageSize); }, 50);
        return;
    }
    tableWidget.unselectAllRows();
    tableWidget.selectRow(globalIndex);
    if (!tableWidget.selection[0]){
        setTimeout(function(){ selectEntityInTable(globalIndex, tableVarName, pageSize); }, 50);
        return;
    }
    scrollSelectedIntoView(tableWidget);

    // hides status dialog
    stop();
}


function removeSessionIdAndParametersFromUrl() {
    var modification = false;
    var href = window.location.href;

    if (window.location.search !== "") {
        href = href.replace(window.location.search, "");
        modification = true;
    }

    if (href.indexOf(";jsessionid") >= 0) {
        href = href.substring(0, href.indexOf( ";jsessionid" ));
        modification = true;
    }

    if (modification) {
        window.history.pushState("", "", href);
    }
}


function disableButtons(formId, panelName){
  $("#" + formId + "\\:" + panelName).children().children().attr('disabled',true)
}


function toggleMaximizeInstallationPackage(maximized, form) {
    if (!maximized) {
        $("#" + form + "\\:r1")[0].style.display = "none";
        $("#" + form + "\\:r3")[0].style.display = "none";
        $("#" + form + "\\:r2 td")[0].style.display = "block";
        $("#" + form + "\\:r2 td")[1].style.display = "block";
        $("#" + form + "\\:r2 td")[1].style.width = "100%";
    } else {
        $("#" + form + "\\:r1")[0].style.display = "";
        $("#" + form + "\\:r3")[0].style.display = "";
        $("#" + form + "\\:r2 td")[0].style.display = "";
        $("#" + form + "\\:r2 td")[1].style.display = "";
        $("#" + form + "\\:r2 td")[1].style.width = "";
    }
}

//Helper functions that check if widgets are loadet
function isLoaded(elementName){
  element = PF(elementName)
  return element != undefined
}

function waitUntilLoaded(elementName,time, callback){
  if (!isLoaded(elementName)){
    setTimeout(function(){
      waitUntilLoaded(elementName,time, callback);
    }, time);
  }else{
    if (callback != null){
      callback();
    }
  }
}
