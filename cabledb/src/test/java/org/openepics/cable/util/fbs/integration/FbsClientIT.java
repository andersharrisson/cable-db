/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.util.fbs.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openepics.cable.category.WebserviceIntegrationCategory;
import org.openepics.cable.util.fbs.FbsClient;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsUtil;
import org.openepics.cable.util.webservice.ResponseException;

/**
 * Integration tests for {@link FbsClient} class.
 *
 * @author Lars Johansson
 *
 * @see FbsClient
 *
 * @see FbsClientTest
 */
@Category(WebserviceIntegrationCategory.class)
public class FbsClientIT {

    // Note
    //     see also FbsClient
    //
    //     notes to be updated as ITIP evolves
    //
    //     seems both https and http access to FBS url
    //     querying database with null or empty string may give all content in return (many rows)
    //     querying database with wrong lower or upper case string may give all content in return (many rows)
    //     wildcard search only for tag parameter
    //     max_results parameter only with tag parameter
    //     query resulting in all content returned takes time, content increasing over time
    //     encoding of parameter handled in getFbsListing method
    //
    //     > 40000 entries for test url

    private static final String TEST_FBS_URL = "https://itip.esss.lu.se/chess/fbs.json";

    private static FbsClient client = null;

    /**
     * One time setup before tests are run.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        // one-time initialization code
        client = new FbsClient(TEST_FBS_URL);
    }

    /**
     * One time teardown after tests are run.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        client = null;
    }

    @Test
    public void getFbsListing_url_noMatch() {
        // expected ResponseException
        try(FbsClient fbsClient = new FbsClient("noMatch")) {
            fbsClient.getFbsListing();
            fail();
        } catch (ResponseException e) {
            assertNotNull(e);
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void getFbsListing_url_wrong() {
        // expected ResponseException
        try (FbsClient fbsClient = new FbsClient("htp://itip.esss.lu.se/chess/fbs.xml")) {
            fbsClient.getFbsListing();
            fail();
        } catch (ResponseException e) {
            assertNotNull(e);
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test(expected=ResponseException.class)
    public void getFbsListing_url_notFound() throws Exception {
        try (FbsClient fbsClient = new FbsClient("https://itip.esss.lu.se/chess/fbs.json123")) {
            final List<FbsElement> fbsElements = fbsClient.getFbsListing();
            assertNull(fbsElements);
        }
    }

    @Test
    public void getFbsListing_url_empty() {
        try (FbsClient fbsClient = new FbsClient("")) {
            final List<FbsElement> fbsElements = fbsClient.getFbsListing();

            assertNull(fbsElements);
        } catch (Exception e) {
            assertNotNull(e);
            fail(e.getMessage());
        }
    }

    @Test
    public void getFbsListing_url_empty2() {
        try (FbsClient fbsClient = new FbsClient("   ")) {
            final List<FbsElement> fbsElements = fbsClient.getFbsListing();

            assertNull(fbsElements);
        } catch (Exception e) {
            assertNotNull(e);
            fail(e.getMessage());
        }
    }

    @Test
    public void getFbsListing_url_null() {
        try (FbsClient fbsClient = new FbsClient(null)) {
            final List<FbsElement> fbsElements = fbsClient.getFbsListing();

            assertNull(fbsElements);
        } catch (Exception e) {
            assertNotNull(e);
            fail(e.getMessage());
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListing_readMappingsCableNameToFbsTag() {
        // might take a few seconds
        // potentially lot of entries in fbsElements
        // potentially lot of entries in tagMappings

        final List<FbsElement> fbsElements = client.getFbsListing();
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListing_readMappingsESSNameToFbsTag() {
        // might take a few seconds
        // potentially lot of entries in fbsElements
        // potentially lot of entries in tagMappings

        final List<FbsElement> fbsElements = client.getFbsListing();
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListing_readMappingsFbsTagToEssName() {
        // might take a few seconds
        // potentially lot of entries in fbsElements
        // potentially lot of entries in tagMappings

        final List<FbsElement> fbsElements = client.getFbsListing();
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tagMappings.isEmpty());
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag() {
        // encoding of parameter handled in getFbsListing method
        String cableName = "24B035866";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("=ESS.ACC.A07.U01.U31.E01.G01.WF01", tagMappings.get(cableName));
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag2() {
        // encoding of parameter handled in getFbsListing method
        String cableName = "85C034390";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_noMatch() {
        String cableName = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_empty() {
        String cableName = "";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_empty2() {
        String cableName = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_null() {
        String cableName = null;

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag() {
        // encoding of parameter handled in getFbsListing method
        String essName = "HBL-110RFC:RFS-ADR-11002";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag2() {
        // encoding of parameter handled in getFbsListing method
        String essName = "TS2-010RFC:RFS-Mod-110";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("=ESS.ACC.A06.T01", tagMappings.get(essName));
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag3() {
        // encoding of parameter handled in getFbsListing method
        String essName = "ISrc-010:ISS-Magtr";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_noMatch() {
        String essName = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_empty() {
        String essName = "";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_empty2() {
        String essName = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_null() {
        String essName = null;

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.B01.B06.B01.R01.M02";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("MEBT-010:PBI-COLLA-002", tagMappings.get(tag));
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName2() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A08.E04.T01.KF01";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("HBL-070RFC:RFS-CCU-420", tagMappings.get(tag));
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName3() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A01.E01.K01";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertNull(tagMappings.get(tag));
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_noMatch() {
        String tag = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertTrue(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertTrue(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_empty() {
        String tag = "";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_empty2() {
        String tag = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_null() {
        String tag = null;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_cableName() {
        // encoding of parameter handled in getFbsListing method
        String id = "ESS-0415608";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(id, key);
            assertEquals("21F009765", value.cableName);
            assertEquals(null, value.essName);
            assertEquals(id, value.id);
            assertEquals("=ESS.ACC.E01.W03.WB02", value.tag);
        }
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_essName() {
        // encoding of parameter handled in getFbsListing method
        String id = "ESS-0401755";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(id, key);
            assertEquals(null, value.cableName);
            assertEquals("TS2-010Row:CnPw-U-006", value.essName);
            assertEquals(id, value.id);
            assertEquals("=ESS.ACC.A06.E02.UH02", value.tag);
        }
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_noMatch() {
        String id = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertTrue(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertTrue(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_empty() {
        String id = "";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_empty2() {
        String id = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_null() {
        String id = null;

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_cableName() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A07.U01.U31.E01.G01.WF01";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertEquals("24B035866", value.cableName);
            assertEquals(null, value.essName);
            assertEquals("ESS-1485237", value.id);
            assertEquals(tag, value.tag);
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_essName() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.INFR.K02.U01.U01";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertEquals(null, value.cableName);
            assertEquals("ISrc-010Row:CnPw-U-005", value.essName);
            assertEquals("ESS-0189769", value.id);
            assertEquals(tag, value.tag);
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_noMatch() {
        String tag = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertTrue(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertTrue(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_empty() {
        String tag = "";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_empty2() {
        String tag = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_null() {
        String tag = null;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_essName2() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03.T01.GF01";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertNull(value.cableName);
            assertEquals("HBL-180RFC:RFS-Circ-310", value.essName);
            assertEquals("ESS-0453597", value.id);
            assertEquals(tag, value.tag);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcardEndsWith_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag;
        int maxResults = 25;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertTrue(key.contains(tag));
            assertTrue(value.tag.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcardStartsWith_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = tag + "%";
        int maxResults = 25;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertTrue(key.contains(tag));
            assertTrue(value.tag.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcards() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03.T01.GF01";
        String tagSearch = "%" + tag + "%";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertEquals(null, value.cableName);
            assertEquals("HBL-180RFC:RFS-Circ-310", value.essName);
            assertEquals("ESS-0453597", value.id);
            assertEquals(tag, value.tag);
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcards_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 25;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertTrue(key.contains(tag));
            assertTrue(value.tag.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcards_maxResults2() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 5;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertTrue(key.contains(tag));
            assertTrue(value.tag.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcards_maxResults3() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 35;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertTrue(key.contains(tag));
            assertTrue(value.tag.contains(tag));
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcardEndsWith_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcardStartsWith_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = tag + "%";
        int maxResults = 25;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcards() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03.T01.GF01";
        String tagSearch = "%" + tag + "%";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tags.size());
        assertNotNull(tags.get(0));
        assertEquals(tag, tags.get(0));
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcards_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 25;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcards_maxResults2() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 5;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcards_maxResults3() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 35;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

}
