/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.util.fbs.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.openepics.cable.CableProperties;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsService;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link FbsService} class (using MockServer, details: {@link FbsServiceMock}).
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsService
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({CableProperties.class})
public class FbsServiceTest extends FbsServiceMock {
    private FbsService fbsService;

    @Mock
    private CableProperties cableProperties;

    @Before
    public void setup() {
        super.setup();
        Whitebox.setInternalState(CableProperties.class, "instance", cableProperties);
        when(cableProperties.getFbsApplicationURL()).thenReturn(TEST_FBS_URL);
        fbsService = new FbsService();
    }

    @Test
    public void getFbsListing() {
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();
        assertNotNull(fbsElements);
        assertEquals(32, fbsElements.size());
    }

    @Test
    public void getTagForEssName_match() throws Exception {
        final String essName = "HBL-110RFC:RFS-ADR-11002";
        final String fbsTag = fbsService.getFbsTagForEssName(essName);
        assertEquals(fbsTag, "=ESS.ACC.A05.A12.E01.K01.K01.KF01.K02");
    }

    @Test
    public void getTagForEssName_noMatch() throws Exception {
        final String essName = "no_match";
        final String fbsTag = fbsService.getFbsTagForEssName(essName);
        assertNull(fbsTag);
    }

    @Test
    public void getEssNameForTag_match() throws Exception {
        final String fbsTag = "=ESS.ACC.A03.A08.E02.E01.K01.KF06";
        final String essName = fbsService.getEssNameForFbsTag(fbsTag);
        assertEquals(essName, "Spk-070RFC:RFS-MTCA-2113MC");
    }

    @Test
    public void getEssNameForTag_noMatch() throws Exception {
        final String fbsTag = "no_match";
        final String essName = fbsService.getEssNameForFbsTag(fbsTag);
        assertNull(essName);
    }

    @Test
    public void getFbsTagsForFbsTagStartsWith_match() throws Exception {
        String tag = "=ESS.ACC.A05.A19.E03";
        int maxResults = 5;

        List<String> tags = fbsService.getFbsTagsForFbsTagStartsWith(tag, maxResults);

        assertNotNull(tags);
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }
}
