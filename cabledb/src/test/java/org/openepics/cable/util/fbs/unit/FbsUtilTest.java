/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.util.fbs.unit;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsUtil;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Map;

/**
 * Unit tests for {@link FbsUtil} class.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 * @author Lars Johansson
 *
 * @see FbsUtil
 **/
public class FbsUtilTest {

    /**
     * Test for reading tags.
     */
    @Test
    public void readTags(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));
        List<String> tags = FbsUtil.readTags(fbsElements);
        assertEquals(tags.size(), 2);
        assertEquals(tags.get(0), "tag1");
        assertEquals(tags.get(1), "tag2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsCableNameToFbsTag(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));
        Map<String, String> map = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);
        assertEquals(map.size(), 2);
        assertEquals(map.get("cable1"), "tag1");
        assertEquals(map.get("cable2"), "tag2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsCableNameToFbsTag_ambiguous(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable1", "ess2", "id2", "tag2"));
        Map<String, String> map = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);
        assertEquals(map.size(), 1);
        assertEquals(map.get("cable1"), "Ambiguous name: tag1, tag2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsEssNameToFbsTag(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));
        Map<String, String> map = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);
        assertEquals(map.size(), 2);
        assertEquals(map.get("ess1"), "tag1");
        assertEquals(map.get("ess2"), "tag2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsEssNameToFbsTag_ambiguous(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess1", "id2", "tag2"));
        Map<String, String> map = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);
        assertEquals(map.size(), 1);
        assertEquals(map.get("ess1"), "Ambiguous name: tag1, tag2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsFbsTagToEssName(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));
        Map<String, String> map = FbsUtil.readMappingsFbsTagToEssName(fbsElements);
        assertEquals(map.size(), 2);
        assertEquals(map.get("tag1"), "ess1");
        assertEquals(map.get("tag2"), "ess2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsFbsTagToEssName_ambiguous(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag1"));
        Map<String, String> map = FbsUtil.readMappingsFbsTagToEssName(fbsElements);
        assertEquals(map.size(), 1);
        assertEquals(map.get("tag1"), "Ambiguous name: ess1, ess2");
    }

    /**
     * Test for reading mappings.
     */
    @Test
    public void readMappingsIdToFbsElement(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));
        Map<String, FbsElement> map = FbsUtil.readMappingsIdToFbsElement(fbsElements);
        assertEquals(map.size(), 2);
        assertEquals(map.get("id1").cableName, "cable1");
        assertEquals(map.get("id1").essName, "ess1");
        assertEquals(map.get("id1").tag, "tag1");
        assertEquals(map.get("id2").cableName, "cable2");
        assertEquals(map.get("id2").essName, "ess2");
        assertEquals(map.get("id2").tag, "tag2");
    }

    /**
     * Test for reading tag mappings.
     */
    @Test
    public void readMappingsIdToFbsElement_ambiguous(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id1", "tag2"));
        Map<String, FbsElement> map = FbsUtil.readMappingsIdToFbsElement(fbsElements);
        assertEquals(map.size(), 1);
        assertNull(map.get("tag1"));
    }

    /**
     * Test for reading mappings.
     */
    @Test
    public void readMappingsFbsTagToFbsElement(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));
        Map<String, FbsElement> map = FbsUtil.readMappingsTagToFbsElement(fbsElements);
        assertEquals(map.size(), 2);
        assertEquals(map.get("tag1").cableName, "cable1");
        assertEquals(map.get("tag1").essName, "ess1");
        assertEquals(map.get("tag2").cableName, "cable2");
        assertEquals(map.get("tag2").essName, "ess2");
    }

    /**
     * Test for reading mappings.
     */
    @Test
    public void readMappingsFbsTagToFbsElement_ambiguous(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag1"));
        Map<String, FbsElement> map = FbsUtil.readMappingsTagToFbsElement(fbsElements);
        assertEquals(map.size(), 1);
        assertNull(map.get("tag1"));
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForCableName(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForCableName(fbsElements, "cable1");
        assertNotNull(element);
        assertEquals(element.id, "id1");
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForCableName2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForCableName(fbsElements, "zxcv");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForCableName_null(){
        FbsElement element = FbsUtil.getFbsElementForCableName(null, "cable1");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForCableName_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForCableName(fbsElements, null);
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForCableName_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForCableName(fbsElements, "");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForCableName_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForCableName(fbsElements, "   ");
        assertNull(element);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForCableName(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(fbsElements, "cable1");
        assertNotNull(elements);
        assertEquals(1, elements.size());
        assertEquals(elements.get(0).id, "id1");
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForCableName2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(fbsElements, "zxcv");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForCableName_null(){
        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(null, "cable1");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForCableName_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(fbsElements, null);
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForCableName_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(fbsElements, "");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForCableName_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(fbsElements, "   ");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForEssName(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForEssName(fbsElements, "ess1");
        assertNotNull(element);
        assertEquals(element.id, "id1");
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForEssName2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForEssName(fbsElements, "zxcv");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForEssName_null(){
        FbsElement element = FbsUtil.getFbsElementForEssName(null, "ess1");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForEssName_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForEssName(fbsElements, null);
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForEssName_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForEssName(fbsElements, "");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForEssName_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForEssName(fbsElements, "   ");
        assertNull(element);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForEssName(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(fbsElements, "ess1");
        assertNotNull(elements);
        assertEquals(1, elements.size());
        assertEquals(elements.get(0).id, "id1");
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForEssName2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(fbsElements, "zxcv");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForEssName_null(){
        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(null, "ess1");
        assertNotNull(elements);
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForEssName_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(fbsElements, null);
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForEssName_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(fbsElements, "");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForEssName_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(fbsElements, "   ");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForId(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForId(fbsElements, "id1");
        assertNotNull(element);
        assertEquals(element.id, "id1");
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForId2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForId(fbsElements, "zxcv");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForId_null(){
        FbsElement element = FbsUtil.getFbsElementForId(null, "id1");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForId_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element  = FbsUtil.getFbsElementForId(fbsElements, null);
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForId_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForId(fbsElements, "");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForId_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForId(fbsElements, "   ");
        assertNull(element);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForId(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForId(fbsElements, "id1");
        assertNotNull(elements);
        assertEquals(1, elements.size());
        assertEquals(elements.get(0).id, "id1");
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForId2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForId(fbsElements, "zxcv");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForId_null(){
        List<FbsElement> elements = FbsUtil.getFbsElementsForId(null, "id1");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForId_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForId(fbsElements, null);
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForId_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForId(fbsElements, "");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForId_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForId(fbsElements, "   ");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForTag(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForTag(fbsElements, "tag1");
        assertNotNull(element);
        assertEquals(element.id, "id1");
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForTag2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForTag(fbsElements, "zxcv");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForTag_null(){
        FbsElement element = FbsUtil.getFbsElementForTag(null, "tag1");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForTag_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForTag(fbsElements, null);
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForTag_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForTag(fbsElements, "");
        assertNull(element);
    }

    /**
     * Test for reading FbsElement entry.
     */
    @Test
    public void getFbsElementForTag_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        FbsElement element = FbsUtil.getFbsElementForTag(fbsElements, "   ");
        assertNull(element);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForTag(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(fbsElements, "tag1");
        assertNotNull(elements);
        assertEquals(1, elements.size());
        assertEquals(elements.get(0).id, "id1");
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForTag2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(fbsElements, "zxcv");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForTag_null(){
        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(null, "tag1");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForTag_null2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(fbsElements, null);
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForTag_empty(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(fbsElements, "");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    /**
     * Test for reading list of FbsElement entries.
     */
    @Test
    public void getFbsElementsForTag_empty2(){
        List<FbsElement> fbsElements = ImmutableList.of(
                createElement("cable1", "ess1", "id1", "tag1"),
                createElement("cable2", "ess2", "id2", "tag2"));

        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(fbsElements, "   ");
        assertNotNull(elements);
        assertEquals(0, elements.size());
    }

    // ----------------------------------------------------------------------------------------------------

    private FbsElement createElement(final String cableName, final String essName, final String id,
            final String fbsTag) {
        FbsElement fbsElement = new FbsElement();
        fbsElement.cableName = cableName;
        fbsElement.essName = essName;
        fbsElement.id = id;
        fbsElement.tag = fbsTag;
        return fbsElement;
    }
}
