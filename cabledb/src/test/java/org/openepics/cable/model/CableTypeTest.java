package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

/**
 * Tests the {@link CableType}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableTypeTest {

    /**
     * Tests
     * {@link CableType#CableType(String, InstallationType, String, String, String, String, String, String, String, Double, Double, URL, String)}.
     */
    @Test
    public void createCableTypeTest() throws MalformedURLException {

        final String name = "name";
        final InstallationType installationType = InstallationType.OUTDOOR;
        final String description = "description";
        final String service = "service";
        final Integer voltage = 24;
        final String insulation = "insulation";
        final String jacket = "jacket";
        final String flammability = "flammability";
        final Float tid = 30f;
        final Float weight = 3.21f;
        final Float diameter = 1.23f;
        final Manufacturer manufacturer = new Manufacturer("CABLEMAN", "street", "00854", "CABLEMAN@example.org",
                "NEVERLAND", new Date(), new Date());
        final CableTypeManufacturer cableTypeManufacturer = new CableTypeManufacturer(null, manufacturer, null, 0);
        final String comments = "comments";

        final CableType cableType = new CableType(name, installationType, description, service, voltage, insulation,
                jacket, flammability, tid, weight, diameter, Arrays.asList(cableTypeManufacturer), comments);

        assertEquals(name, cableType.getName());
        assertEquals(installationType, cableType.getInstallationType());
        assertEquals(description, cableType.getDescription());
        assertEquals(service, cableType.getService());
        assertEquals(voltage, cableType.getVoltage());
        assertEquals(insulation, cableType.getInsulation());
        assertEquals(jacket, cableType.getJacket());
        assertEquals(flammability, cableType.getFlammability());
        assertEquals(tid, cableType.getTid());
        assertEquals(weight, cableType.getWeight());
        assertEquals(diameter, cableType.getDiameter());
        assertEquals(manufacturer, cableType.getManufacturers().get(0).getManufacturer());
        assertEquals(comments, cableType.getComments());
    }

    /** Tests {@link CableType#CableType(String, InstallationType)}. */
    @Test
    public void createCableTypeTest_noData() {
        final String name = "name";
        final InstallationType installationType = InstallationType.OUTDOOR;

        final CableType cableType = new CableType(name, installationType);

        assertEquals(name, cableType.getName());
        assertEquals(installationType, cableType.getInstallationType());
        assertNull(cableType.getDescription());
        assertNull(cableType.getService());
        assertNull(cableType.getVoltage());
        assertNull(cableType.getInsulation());
        assertNull(cableType.getJacket());
        assertNull(cableType.getFlammability());
        assertNull(cableType.getTid());
        assertNull(cableType.getWeight());
        assertNull(cableType.getDiameter());
        assertEquals(0, cableType.getManufacturers().size());
        assertNull(cableType.getComments());
    }

    /** Tests that {@link CableType#CableType(String)} fails with no required data. */
    @Test(expected = IllegalArgumentException.class)
    public void createCableTypeTest_noRequiredData() {
        new CableType(null, null);
    }

    /** Tests {@link CableType#update(String, String, String, URL, String)} by passing null for optional data. */
    @Test
    public void updateCableTypeTest() {
        final CableType cableType = new CableType("name", InstallationType.OUTDOOR);
        cableType.setInstallationType(InstallationType.UNKNOWN);
    }

    /**
     * Tests that {@link CableType#update(String, String, String, URL, String)} fails when installation type is null.
     */
    @Test(expected = NullPointerException.class)
    public void updateCableTypeTest_noInstallationType() {
        final CableType cableType = new CableType("name", InstallationType.OUTDOOR);
        cableType.setInstallationType(null);
    }
}
