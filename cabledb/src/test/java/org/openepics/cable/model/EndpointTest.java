package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

/**
 * Tests the {@link Endpoint}.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class EndpointTest {

    /** Tests {@link Endpoint#Endpoint(String, String, String, String, URL, String)}. */
    @Test
    public void createEndpoint() throws MalformedURLException {
        final String device = "device";
        final String building = "building";
        final String rack = "rack";
        final String type = "type";
        final URL drawing = new URL("http://www.wxample.org");
        final String label = StringUtil.getString(Endpoint.MAX_LABEL_SIZE);

        final Endpoint endpoint = new Endpoint(device, building, rack, null, drawing, label);

        assertEquals(device, endpoint.getDevice());
        assertEquals(building, endpoint.getBuilding());
        assertEquals(rack, endpoint.getRack());
        assertNull(endpoint.getConnector());
        assertEquals(label, endpoint.getLabel());
    }

    /**
     * Tests {@link Endpoint#Endpoint(String, String, String, String, URL, String)} by passing null for optional data.
     */
    @Test
    public void createEndpoint_noData() {
        final String device = "device";
        final Endpoint endpoint = new Endpoint(device, null, null, null, null, null);
        assertEquals(device, endpoint.getDevice());
        assertNull(endpoint.getBuilding());
        assertNull(endpoint.getRack());
        assertNull(endpoint.getConnector());
        assertNull(endpoint.getLabel());
    }

    /** Tests {@link Endpoint#Endpoint(String)} fails with no required data. */
    @Test(expected = IllegalArgumentException.class)
    public void createEndpoint_noRequiredData() {
        new Endpoint(null);
    }

    /**
     * Tests that {@link Endpoint#Endpoint(String, String, String, String, URL, String)} fails when creating with label
     * over the limit.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createEndpoint_labelExceedLimit() {
        // label exceeding max size
        new Endpoint("device", null, null, null, null, StringUtil.getString(Endpoint.MAX_LABEL_SIZE + 1));
    }

    /** Tests {@link Endpoint#update(String, String, String, URL, String)} by passing null for optional data. */
    @Test
    public void updateEndpoint() {
        final Endpoint endpoint = new Endpoint("device");
        endpoint.update("new device", null, null, null, null, null);
    }

    /**
     * Tests that {@link Endpoint#update(String, String, String, URL, String)} fails when creating with label over the
     * limit.
     */
    @Test(expected = IllegalArgumentException.class)
    public void updateEndpoint_labelExceedLimit() {
        final Endpoint endpoint = new Endpoint("device");
        // label exceeding max size
        endpoint.update("new device", null, null, null, null, StringUtil.getString(Endpoint.MAX_LABEL_SIZE + 1));
    }
}
