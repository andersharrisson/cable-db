/**
 * 
 */
package org.openepics.cable.model;

import java.util.Arrays;

/**
 * This contains some utility methods that deal with strings.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class StringUtil {

    /**
     * Returns a string with the given length.
     * 
     * @param length
     *            the string length
     * @return the string
     */
    public static String getString(int length) {
        char[] charArray = new char[length];
        Arrays.fill(charArray, 'a');
        return new String(charArray);
    }
}
