package org.openepics.cable.model;

import org.junit.Test;

/**
 * Tests the {@link CableName}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableNameTest {

    /** Tests constructing a valid number. */
    @Test
    public void createValidNumber() {
        new CableName("12c000001");
    }

    /** Tests that constructing a number with invalid system fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidSystem() {
        new CableName("a2c000001");
    }

    /** Tests that constructing a number with invalid subsystem fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidSubsystem() {
        new CableName("1-c000001");
    }

    /** Tests that constructing a number with invalid class fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidClass() {
        new CableName("121000001");
    }

    /** Tests that constructing a number with invalid sequential number length fails. */
    @Test(expected = IllegalArgumentException.class)
    public void createNumber_invalidSeqNumber() {
        new CableName("12c00001");
    }
}
