/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.webservice;

import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openepics.cable.category.DockerCategory;

/**
 * Purpose of class to have integration test suite for REST interface of Cable application.
 *
 * @author Lars Johansson
 *
 * @see DockerCategory
 *
 * @see CableEndpointsResourceIT
 * @see CableResourceIT
 * @see CableTypeResourceIT
 * @see ConnectorResourceIT
 * @see HealthcheckResourceIT
 * @see ManufacturerResourceIT
 * @see InstallationPackageResourceIT
 */
@Category(DockerCategory.class)
@RunWith(Suite.class)
@SuiteClasses({
        CableEndpointsResourceIT.class,
        CableResourceIT.class,
        CableTypeResourceIT.class,
        ConnectorResourceIT.class,
        HealthcheckResourceIT.class,
        ManufacturerResourceIT.class,
        InstallationPackageResourceIT.class })
public class CableRestServiceTestSuiteIT {

    /*
        --------------------------------------------------------------------------------
        Note
            - war file to be assembled before integration tests are run
            - guide running of tests through pom
                    DockerCategory
                    skipTests property
                    maven-surefire-plugin
                    maven-failsafe-plugin
            - tests of REST API per endpoint
        --------------------------------------------------------------------------------
        REST API paths

            /cable
            /cable/{name}
            /cableEndpoints
            /cableEndpoints/{number}
            /cableType
            /cableType/{name}
            /connector
            /connector/{name}
            /healthcheck
            /manufacturer
            /manufacturer/{name}
            /installationPackage
            /installationPackage/{name}
        --------------------------------------------------------------------------------
     */

}
