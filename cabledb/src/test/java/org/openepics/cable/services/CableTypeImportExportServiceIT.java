package org.openepics.cable.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.services.dl.CableTypeColumn;
import org.openepics.cable.services.dl.CableTypeImportExportService;
import org.openepics.cable.services.dl.CableTypeSaver;
import org.openepics.cable.services.dl.DSCommand;
import org.openepics.cable.services.dl.LoaderResult;

/**
 * Tests the {@link CableTypeImportExportService} using arquillian framework integration test. This class tests the
 * import and export functionality for valid and invalid datasets.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Ignore
@RunWith(Arquillian.class)
@ApplyScriptAfter(value = "truncate_database.sql")
public class CableTypeImportExportServiceIT extends CableTypeImportExportServiceBase {

    @Inject
    private CableTypeService cableTypeService;

    @Deployment
    public static WebArchive createDeployment() {
        return CablePackager.createWebArchive();
    }

    @Before
    public void setUp() {
        sessionService.login(CableProperties.getInstance().getTestAdminUser(),
                CableProperties.getInstance().getTestAdminPassword());
    }

    @After
    public void tearDown() {
        sessionService.logout();
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Allow adding an instance.
     */
    @Test
    public void importCableTypesTest_addInstance() {
        final InputStream inputStream = convertToExcel(getCreateCommands(1), getExampleCableType(true));
        LoaderResult<CableType> result = cableTypeImportExportService.importCableTypes(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Allow adding an instance
     * with all optional data omitted.
     */
    @Test
    public void importCableTypesTest_addInstanceNoOptionalData() throws ParseException {
        final InputStream inputStream = getExampleExcel();
        LoaderResult<CableType> result = cableTypeImportExportService.importCableTypes(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Allow adding an instance
     * where some numeric property is specified with string in the spreadsheet.
     */
    @Test
    public void importCableTypesTest_addInstanceNumericAsString() throws ParseException {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getCreateCommands(1), getExampleCableType());
        cableTypeSaver.setValue(0, CableTypeColumn.WEIGHT, "12.3");
        final InputStream inputStream = cableTypeSaver.save();
        LoaderResult<CableType> result = cableTypeImportExportService.importCableTypes(inputStream);
        assertFalse(result.toString(), result.isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow adding an instance
     * with undefined name.
     */
    @Test
    public void importCableTypesTest_noName() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getExampleCableType());
        cableTypeSaver.setValue(0, CableTypeColumn.NAME, "");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow adding an instance
     * with undefined installation type.
     */
    @Test
    public void importCableTypesTest_noInstallationType() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getExampleCableType());
        cableTypeSaver.setValue(0, CableTypeColumn.INSTALLATION_TYPE, "");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow adding an instance
     * with malformed installationType.
     */
    @Test
    public void importCableTypesTest_malformedInstallationType() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getExampleCableType());
        cableTypeSaver.setValue(0, CableTypeColumn.INSTALLATION_TYPE, "tray");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow adding an instance
     * with non-numeric weight.
     */
    @Test
    public void importCableTypesTest_nonnumericWeight() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getExampleCableType());
        cableTypeSaver.setValue(0, CableTypeColumn.WEIGHT, "abc");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow adding an instance
     * with non-numeric diameter.
     */
    @Test
    public void importCableTypesTest_nonnumericDiameter() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getExampleCableType());
        cableTypeSaver.setValue(0, CableTypeColumn.DIAMETER, "abc");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow adding an instance
     * with a malformed manufacturer and model URL.
     */
    @Test
    public void importCableTypesTest_malformedManufacturer() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(getExampleCableType());
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Allow updating an instance.
     */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable.yml" })
    public void importCableTypesTest_updateInstance() throws MalformedURLException {
        final CableType cableType = cableTypeService.getCableType("cabletype1");
        final String description = "updated description";
        final String service = "updated service";
        final Integer voltage = 24;
        final String insulation = "updated insulation";
        final String jacket = "updated jacket";
        final String flammability = "updated flammability";
        final InstallationType installationType = InstallationType.INDOOR;
        final Float tid = 100f;
        final Float weight = 12.3f;
        final Float diameter = 1.23f;
        // final Manufacturer manufacturer = new Manufacturer("CABLEMAN","street","00854","CABLEMAN@example.org",
        // "NEVERLAND",new Date(),new Date());
        final Manufacturer manufacturer = null;
        final String comments = "updated comments";
        cableType.setDescription(description);
        cableType.setService(service);
        cableType.setVoltage(voltage);
        cableType.setInsulation(insulation);
        cableType.setJacket(jacket);
        cableType.setFlammability(flammability);
        cableType.setInstallationType(installationType);
        cableType.setTid(tid);
        cableType.setWeight(weight);
        cableType.setDiameter(diameter);
        cableType.setComments(comments);
        final InputStream inputStream = convertToExcel(cableType);

        LoaderResult<CableType> result = cableTypeImportExportService.importCableTypes(inputStream);
        assertFalse(result.toString(), result.isError());

        final CableType updatedCableType = cableTypeService.getCableType("cabletype1");
        assertEquals(description, updatedCableType.getDescription());
        assertEquals(service, updatedCableType.getService());
        assertEquals(voltage, updatedCableType.getVoltage());
        assertEquals(insulation, updatedCableType.getInsulation());
        assertEquals(jacket, updatedCableType.getJacket());
        assertEquals(flammability, updatedCableType.getFlammability());
        assertEquals(installationType, updatedCableType.getInstallationType());
        assertEquals(tid, updatedCableType.getTid());
        assertEquals(weight, updatedCableType.getWeight());
        assertEquals(diameter, updatedCableType.getDiameter());
        assertEquals(comments, updatedCableType.getComments());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow updating an
     * instance with undefined installation type.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noUpdateUndefinedInstallationType() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setValue(0, CableTypeColumn.INSTALLATION_TYPE, "");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow updating an
     * instance with illegal installation type.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noUpdateIllegalInstallationType() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setValue(0, CableTypeColumn.INSTALLATION_TYPE, "tray");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow updating an
     * instance with non-numeric weight.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noUpdateNonnumericWeight() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setValue(0, CableTypeColumn.WEIGHT, "abc");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow updating an
     * instance with non-numeric diameter.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noUpdateNonnumericDiameter() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setValue(0, CableTypeColumn.DIAMETER, "abc");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow updating an
     * instance with a malformed manufacturer and model URL.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noUpdateMalformedModel() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Allow deleting an instance.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_deleteInstance() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setCommand(0, DSCommand.DELETE);
        final InputStream inputStream = cableTypeSaver.save();

        final LoaderResult<CableType> result = cableTypeImportExportService.importCableTypes(inputStream);
        assertFalse(result.toString(), result.isError());

        assertFalse(cableTypeService.getCableType("cabletype1").isActive());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow deleting an
     * instance with name/code not specified.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noDeleteNameUndefined() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setCommand(0, DSCommand.DELETE);
        cableTypeSaver.setValue(0, CableTypeColumn.NAME, "");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }

    /**
     * Test for {@link CableTypeImportExportService#importCableTypes(java.io.InputStream)}. Disallow deleting when
     * instance with name/code cannot be found.
     */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCableTypesTest_noDeleteNonexistingName() {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypeService.getCableType("cabletype1"));
        cableTypeSaver.setCommand(0, DSCommand.DELETE);
        cableTypeSaver.setValue(0, CableTypeColumn.NAME, "nonexisting");
        final InputStream inputStream = cableTypeSaver.save();

        assertTrue(cableTypeImportExportService.importCableTypes(inputStream).isError());
    }
}
