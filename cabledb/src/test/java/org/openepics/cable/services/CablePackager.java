package org.openepics.cable.services;

import java.io.File;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

/**
 * This is a static helper class that packages cable application into {@link WebArchive} appropriate for running
 * integration tests.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CablePackager {

    /** @return An archive packaging the whole cable application. */
    public static WebArchive createWebArchive() {
        File[] libraries = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
                .withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "cable.war").addAsLibraries(libraries)
                .addPackages(true, "org.openepics.cable.model", "org.openepics.cable.services",
                        "org.openepics.cable.services.dl", "org.openepics.cable.util")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("templates/cdb_cable_types.xlsx").addAsResource("templates/cdb_cables.xlsx")
                .addAsWebInfResource("jboss-all.xml").addAsWebInfResource("jboss-web.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        return war;
    }
}
