package org.openepics.cable.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsInstanceOf;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.Cable;
import org.openepics.cable.services.dl.CableImportExportService;
import org.openepics.cable.services.dl.LoaderResult;

/**
 * Tests the {@link CableImportExportService} using arquillian framework integration test. This class tests that the
 * permissions for importing and exporting are correct.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Ignore
@RunWith(Arquillian.class)
@ApplyScriptAfter(value = "truncate_database.sql")
public class CableImportExportService_PermissionsIT extends CableImportExportServiceBase {

    private static final List<Cable> EMPTY_LIST = new ArrayList<Cable>();

    @Deployment
    public static WebArchive createDeployment() {
        return CablePackager.createWebArchive();
    }

    @After
    public void tearDown() {
        sessionService.logout();
    }

    /** Test that administrator can perform import of not owned cables. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCablesTest() {
        sessionService.login(CableProperties.getInstance().getTestAdminUser(),
                CableProperties.getInstance().getTestAdminPassword());
        final LoaderResult<Cable> result = cableImportExportService
                .importCables(getExampleExcel(CableProperties.getInstance().getTestCableUser()));
        assertFalse(result.toString(), result.isError());
    }

    /** Test that standard cable user can import their own cables. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCablesTest_allowOwnedCables() {
        sessionService.login(CableProperties.getInstance().getTestCableUser(),
                CableProperties.getInstance().getTestCableUserPassword());
        final LoaderResult<Cable> result = cableImportExportService
                .importCables(getExampleExcel(CableProperties.getInstance().getTestCableUser()));
        assertFalse(result.toString(), result.isError());
    }

    /** Test that standard cable user cannot perform import of not owned cables. */
    @Test
    public void importCablesTest_forbidCableUser() {
        sessionService.login(CableProperties.getInstance().getTestCableUser(),
                CableProperties.getInstance().getTestCableUserPassword());
        assertTrue(cableImportExportService
                .importCables(getExampleExcel(CableProperties.getInstance().getTestAdminUser())).isError());
    }

    /** Test that not logged in user cannot perform import. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void importCablesTest_forbidNotLoggedIn() {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalStateException.class));

        cableImportExportService.importCables(getExampleExcel(CableProperties.getInstance().getTestCableUser()));
    }

    /** Test that logged in user can perform export. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void exportCablesTest() {
        sessionService.login(CableProperties.getInstance().getTestCableUser(),
                CableProperties.getInstance().getTestCableUserPassword());
        cableImportExportService.exportCables(EMPTY_LIST);
    }

    /** Test that not logged in user cannot perform export. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void exportCablesTest_forbidNotLoggedIn() {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalStateException.class));

        cableImportExportService.exportCables(EMPTY_LIST);
    }
}
