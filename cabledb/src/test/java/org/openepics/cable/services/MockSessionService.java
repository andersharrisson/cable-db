package org.openepics.cable.services;

import javax.annotation.Priority;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.interceptor.Interceptor;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import org.openepics.cable.CableProperties;
import se.esss.ics.rbac.loginmodules.service.Message;

@RequestScoped
@Alternative
@Priority(Interceptor.Priority.APPLICATION + 20)
public class MockSessionService extends SessionService {
    private static final long serialVersionUID = 1L;

    private String username;
    private String password;
    private boolean loggedIn;
    private boolean admin;

    public MockSessionService() {
        username = null;
        password = null;
        loggedIn = false;
        admin = false;
    }

    @Override
    public Message login(String username, String password) {
        this.username = username;
        this.password = password;
        loggedIn = true;
        if (CableProperties.getInstance().getTestAdminUser().equals(username)) {
            admin = true;
        }
        return new Message("Sign in successful.", true);
    }

    @Override
    public Message logout() {
        username = null;
        password = null;
        loggedIn = false;
        admin = false;
        return new Message("Sign out successful.", true);
    }

    @Override
    public boolean isLoggedIn() {
        return loggedIn;
    }

    @Override
    public String getVisibleName() {
        return username;
    }

    @Override
    public String getLoggedInName() {
        return username;
    }

    @Override
    public boolean canAdminister() {
        return admin && loggedIn;
    }

    @Override
    public boolean canManageOwnedCables() {
        return loggedIn;
    }

    @Override
    public HttpServletRequest getRequest() {
        return null;
    }

    @Override
    public boolean hasPermission(String permission) {
        return loggedIn;
    }

    @Override
    public boolean hasRuntimePermission(String permission) throws LoginException {
        return loggedIn;
    }

}
