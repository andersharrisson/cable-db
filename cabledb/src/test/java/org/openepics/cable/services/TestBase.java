package org.openepics.cable.services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.openepics.cable.services.dl.DSCommand;

/**
 * This is a base class for integration tests using arquillian framework. It contains common functionality used in
 * tests.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public abstract class TestBase {

    /** Initialization for testing expected exceptions that are wrapped. */
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Creates a list of CREATE DSCommands
     * 
     * @param length
     *            the number of commands required
     * @return a list of length length
     */
    protected static List<DSCommand> getCreateCommands(int length) {
        List<DSCommand> commands = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            commands.add(DSCommand.CREATE);
        }
        return commands;
    }
}
