package org.openepics.cable.services;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsInstanceOf;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.CableType;
import org.openepics.cable.services.dl.CableTypeImportExportService;
import org.openepics.cable.services.dl.LoaderResult;

/**
 * Tests the {@link CableTypeImportExportService} using arquillian framework integration test. This class tests that the
 * permissions for importing and exporting are correct.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Ignore
@RunWith(Arquillian.class)
@ApplyScriptAfter(value = "truncate_database.sql")
public class CableTypeImportExportService_PermissionsIT extends CableTypeImportExportServiceBase {

    private static final List<CableType> EMPTY_LIST = new ArrayList<CableType>();

    @Deployment
    public static WebArchive createDeployment() {
        return CablePackager.createWebArchive();
    }

    @After
    public void tearDown() {
        sessionService.logout();
    }

    /** Test that administrator can perform import. */
    @Test
    public void createCableTypesTest() {
        sessionService.login(CableProperties.getInstance().getTestAdminUser(),
                CableProperties.getInstance().getTestAdminPassword());
        final LoaderResult<CableType> result = cableTypeImportExportService.importCableTypes(getExampleExcel());
        assertFalse(result.toString(), result.isError());
    }

    /** Test that standard cable user cannot perform import. */
    @Test
    public void importCableTypesTest_forbidCableUser() {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalStateException.class));

        sessionService.login(CableProperties.getInstance().getTestCableUser(),
                CableProperties.getInstance().getTestCableUserPassword());
        cableTypeImportExportService.importCableTypes(getExampleExcel());
    }

    /** Test that not logged in user cannot perform import. */
    @Test
    public void importCableTypesTest_forbidNotLoggedIn() {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalStateException.class));

        cableTypeImportExportService.importCableTypes(getExampleExcel());
    }

    /** Test that logged in user can perform export. */
    @Test
    public void exportCableTypesTest() {
        sessionService.login(CableProperties.getInstance().getTestCableUser(),
                CableProperties.getInstance().getTestCableUserPassword());
        cableTypeImportExportService.exportCableTypes(EMPTY_LIST);
    }

    /** Test that not logged in user cannot perform export. */
    @Test
    public void exportCableTypesTest_forbidNotLoggedIn() {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalStateException.class));

        cableTypeImportExportService.exportCableTypes(EMPTY_LIST);
    }
}
