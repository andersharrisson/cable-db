/*
 *  Copyright (c) 2018 European Spallation Source ERIC.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import java.util.Set;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test class for InstallationPackageLoader.
 *
 * @author Georg Weiss
 */
public class InstallationPackageLoaderTest {

    /**
     * Test of conversion of comma separated list of string values to a set of string values.
     */
    @Test
    public void testGetCableClassesAsSet(){
        InstallationPackageLoader installationPackageLoader = new InstallationPackageLoader();

        Set<String> classes = installationPackageLoader.getCableClassesAsSet("A,B,C");

        assertEquals(3, classes.size());
        assertTrue(classes.contains("A"));
        assertTrue(classes.contains("B"));
        assertTrue(classes.contains("C"));
    }

    /**
     * Test if comma separated list of string values for cable class are valid values.
     */
    @Test
    public void testAreCableClassesValid(){
        InstallationPackageLoader installationPackageLoader = new InstallationPackageLoader();

        assertTrue(installationPackageLoader.areCableClassesValid("A, B, C, E, F, G"));
        assertFalse(installationPackageLoader.areCableClassesValid("-1"));
        assertFalse(installationPackageLoader.areCableClassesValid("Z"));
    }

}
