package org.openepics.cable.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.core.IsInstanceOf;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableAutoCalculatedLength;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Endpoint;

/**
 * Tests the {@link CableService} using arquillian framework integration test. Data sets for the tests are stored in
 * resources package.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Ignore
@RunWith(Arquillian.class)
@ApplyScriptAfter(value = "truncate_database.sql")
public class CableServiceIT extends TestBase {

    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private CableService cableService;

    @Deployment
    public static WebArchive createDeployment() {
        return CablePackager.createWebArchive();
    }

    /** Tests {@link CableService#getCables} by checking the number of retrieved cables and the attribute values. */
    @Test
    @UsingDataSet({ "cabletype.yml", "cables.yml" })
    public void getCablesTest() {

        final List<Cable> cables = cableService.getCables();

        assertEquals(3, cables.size());

        final Cable cable = cables.get(0);
        assertNotNull(cable);

        assertEquals("1", cable.getSystem());
        assertEquals("2", cable.getSubsystem());
        assertEquals("C", cable.getCableClass());
        assertEquals(1, cable.getSeqNumber().intValue());
        assertNotNull(cable.getCableType());
        assertEquals(new HashSet<String>(Arrays.asList("owner")), cable.getOwners());
        assertNull(cable.getRoutingRows());
        assertNotNull(cable.getInstallationBy());
        assertNotNull(cable.getTerminationBy());
        assertEquals(1.23, cable.getLength().doubleValue(), 0.00001);
        assertNotNull(cable.getCreated());
        assertEquals(CableStatus.APPROVED, cable.getStatus());

        final Endpoint endpointA = cable.getEndpointA();
        assertNotNull(endpointA);

        assertEquals("deviceA", endpointA.getDevice());
        assertEquals("buildingA", endpointA.getBuilding());
        assertEquals("rackA", endpointA.getRack());
        assertEquals("http://www.example.org/drawingA", endpointA.getDrawing().toString());
        assertEquals("labelA", endpointA.getLabel());

        final Endpoint endpointB = cable.getEndpointB();
        assertNotNull(endpointB);

        assertEquals("deviceB", endpointB.getDevice());
        assertEquals("buildingB", endpointB.getBuilding());
        assertEquals("rackB", endpointB.getRack());
        assertEquals("http://www.example.org/drawingB", endpointB.getDrawing().toString());
        assertEquals("labelB", endpointB.getLabel());
    }

    /** Tests {@link CableService#getCableByName(String)} by retrieving a cable from database. */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable.yml" })
    public void getCableByNumberTest() {
        assertNotNull(cableService.getCableByName("12C000001"));
    }

    /** Tests that {@link CableService#getCableByName(String)} fails when retrieving a nonexisting cable. */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable.yml" })
    public void getCableByNumberTest_nonexisting() {
        assertNull(cableService.getCableByName("12C000002"));
    }

    /** Tests {@link CableService#createCable} by creating a cable and checking data stored in the database. */
    @Test
    @UsingDataSet("cabletype.yml")
    // @ShouldMatchDataSet("CableServiceTest#createCableTest.yml")
    public void createCableTest() throws ParseException, MalformedURLException {
        final String system = "1";
        final String subSystem = "2";
        final String cableClass = "C";
        final Integer seqNumber = 1;
        final List<String> owners = Arrays.asList("owner");
        final Date installationBy = DateUtil.parse("2015-01-01 10:00:00");
        final Date terminationBy = DateUtil.parse("2015-02-01 10:00:00");
        final Float length = 1.23f;
        final CableStatus status = CableStatus.APPROVED;

        final CableType cableType = cableTypeService.getCableType("cabletype1");

        final String deviceA = "deviceA";
        final String buildingA = "buildingA";
        final String rackA = "rackA";
        final String typeA = "typeA";
        final URL drawingA = new URL("http://www.example.org/drawingA");
        final String labelA = "labelA";
        final String deviceB = "deviceB";
        final String buildingB = "buildingB";
        final String rackB = "rackB";
        final String typeB = "typeB";
        final URL drawingB = new URL("http://www.example.org/drawingB");
        final String labelB = "labelB";
        final String userId = "test";

        final Endpoint endpointA = new Endpoint(deviceA, buildingA, rackA, null, drawingA, labelA);
        final Endpoint endpointB = new Endpoint(deviceB, buildingB, rackB, null, drawingB, labelB);

        cableService.createCable(system, subSystem, cableClass, owners, CableStatus.APPROVED, cableType, null,
                endpointA, endpointB, null, installationBy, terminationBy, null, CableAutoCalculatedLength.NO, 0f,
                length, userId, false);
        // cable
        final Cable updatedCable = cableService.getCables().get(0);
        assertEquals(system, updatedCable.getSystem());
        assertEquals(subSystem, updatedCable.getSubsystem());
        assertEquals(cableClass, updatedCable.getCableClass());
        assertEquals(seqNumber, updatedCable.getSeqNumber());
        assertEquals(installationBy, updatedCable.getInstallationBy());
        assertEquals(terminationBy, updatedCable.getTerminationBy());
        assertEquals(length, updatedCable.getLength());
        assertEquals(status, updatedCable.getStatus());
        // endpoints
        assertEquals(endpointA, updatedCable.getEndpointA());
        assertEquals(endpointB, updatedCable.getEndpointB());

    }

    /** Tests that {@link CableService#createCable} fails when creating a cable with an illegal attribute. */
    @Test
    @UsingDataSet("cabletype.yml")
    public void createCableTest_illegalAttribute() throws ParseException {
        expectedException.expectCause(IsInstanceOf.<Throwable> instanceOf(IllegalArgumentException.class));

        final CableType cableType = cableTypeService.getCableType("cabletype1");

        cableService.createCable("A", "2", "C", Arrays.asList("owner"), CableStatus.APPROVED, cableType, null,
                new Endpoint("deviceA"), new Endpoint("deviceB"), null, DateUtil.parse("2015-01-01 10:00:00"),
                DateUtil.parse("2015-02-01 10:00:00"), null, CableAutoCalculatedLength.NO, 0f, 1.23f, "test", false);
    }

    /** Tests {@link CableService#updateCable} by updating it and checking that attributes changed in the database. */
    @Test
    @UsingDataSet({ "cabletypes.yml", "cable.yml" })
    @ShouldMatchDataSet("CableServiceTest#updateCableTest.yml")
    public void updateCableTest() throws ParseException, MalformedURLException {

        final Cable oldCable = cableService.getCables().get(0);
        cableService.detachCable(oldCable);
        final Cable cable = cableService.getCables().get(0);

        final CableType cableType2 = cableTypeService.getCableType("cabletype2");

        cable.setSystem("2");
        cable.setSubsystem("3");
        cable.setCableClass("D");
        cable.setSeqNumber(2);
        cable.setOwners(Arrays.asList("rbactester1"));
        cable.setStatus(CableStatus.APPROVED);
        cable.setCableType(cableType2);
        cable.setInstallationBy(DateUtil.parse("2015-02-01 10:00:00"));
        cable.setTerminationBy(DateUtil.parse("2015-03-01 10:00:00"));
        cable.setAutoCalculatedLength(CableAutoCalculatedLength.NO);
        cable.setLength(2.23f);
        cable.getEndpointA().update(CableProperties.getInstance().getTestDeviceName2(), "buildingC", "rackC", null,
                new URL("http://www.example.org/drawingC"), "labelC");
        cable.getEndpointB().update(CableProperties.getInstance().getTestDeviceName2(), "buildingD", "rackD", null,
                new URL("http://www.example.org/drawingC"), "labelD");

        cableService.updateCable(cable, oldCable, "test", false);
    }

    /** Tests {@link CableService#deleteCable} by deleting a cable and checking that its state is deleted. */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable.yml" })
    // @ShouldMatchDataSet("CableServiceTest#deleteCableTest.yml")
    public void deleteCableTest() {
        final Cable cable = cableService.getCables().get(0);
        assertTrue(cableService.deleteCable(cable, "test"));
        assertEquals(CableStatus.DELETED, cableService.getCables().get(0).getStatus());
    }

    /** Tests that {@link CableService#deleteCable} returns false when deleting an already deleted cable. */
    @Test
    @UsingDataSet({ "cabletype.yml", "cable.yml" })
    public void deleteCableTest_alreadyDeleted() {
        final Cable cable = cableService.getCables().get(0);
        assertTrue(cableService.deleteCable(cable, "test"));
        assertFalse(cableService.deleteCable(cable, "test"));
    }
}
