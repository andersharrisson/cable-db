package org.openepics.cable.services;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.openepics.cable.model.CableType;
import org.openepics.cable.model.CableTypeManufacturer;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.services.dl.CableTypeImportExportService;
import org.openepics.cable.services.dl.CableTypeSaver;
import org.openepics.cable.services.dl.DSCommand;

/**
 * This is a base class for integration tests for {@link CableTypeImportExportService}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public abstract class CableTypeImportExportServiceBase extends TestBase {

    /** The session service to use for authentication during tests. */
    @Inject
    protected SessionService sessionService;
    /** The service to test. */
    @Inject
    protected CableTypeImportExportService cableTypeImportExportService;

    /**
     * Returns an example of a cable type
     *
     * @param fullData
     *            if true, all fields are populated, else the fields are minimally populated.
     * @return the cable type
     */
    protected static CableType getExampleCableType(boolean fullData) {
        if (fullData) {
            return new CableType("10C22OSTC", InstallationType.OUTDOOR,
                    "10 Conductor, 22 AWG, Stranded,  Non Paired, overall shield, tray cable", "RS-232 Applications",
                    24, "Semi-Rigid PVC", "PVC", "Flammability", 100f,
                    0.1f, 5.0f, Arrays.asList(new CableTypeManufacturer(null, new Manufacturer("CABLEMAN", "street",
                            "00854", "CABLEMAN@example.org", "NEVERLAND", new Date(), new Date()), null, 0)),
                    "new comments");
        } else {
            return new CableType("10C22OSTC", InstallationType.OUTDOOR);
        }
    }

    /**
     * Returns an example of a cable type
     * 
     * @return the cable type
     */
    protected static CableType getExampleCableType() {
        return getExampleCableType(false);
    }

    /**
     * Helper method that creates a {@link CableTypeSaver} that can save a set of cable types.
     *
     * @param cableTypes
     *            the cable types to save
     * @return the saver
     */
    protected static CableTypeSaver getCableTypeSaver(CableType... cableTypes) {
        return new CableTypeSaver(Arrays.asList(cableTypes));
    }

    /**
     * Helper method that creates a {@link CableTypeSaver} that can save a set of cables.
     * 
     * @param commands
     *            the commands to set for handling cables
     * @param cableTypes
     *            the cablTeypes to save
     * @return the saver
     */
    protected static CableTypeSaver getCableTypeSaver(List<DSCommand> commands, CableType... cableTypes) {
        final CableTypeSaver cableTypeSaver = getCableTypeSaver(cableTypes);
        if (commands != null) {
            for (int i = 0; i < commands.size(); i++) {
                cableTypeSaver.setCommand(i, commands.get(i));
            }
        }
        return cableTypeSaver;
    }

    /**
     * Helper method that an Excel file containing a set of cable types.
     *
     * @param cableTypes
     *            the cable types
     * @param commands
     *            the commands wanted in excel file
     * @return a stream representing the Excel file
     */
    protected static InputStream convertToExcel(List<DSCommand> commands, CableType... cableTypes) {
        return getCableTypeSaver(commands, cableTypes).save();
    }

    /**
     * Helper method that an Excel file containing a set of cable types.
     *
     * @param cableTypes
     *            the cable types
     * @return a stream representing the Excel file
     */
    protected static InputStream convertToExcel(CableType... cableTypes) {
        return getCableTypeSaver(cableTypes).save();
    }

    /** @return a stream representing the Excel file with an example cable type */
    protected static InputStream getExampleExcel() {
        return convertToExcel(getCreateCommands(1), getExampleCableType());
    }
}
