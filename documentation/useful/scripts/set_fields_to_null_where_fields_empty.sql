-- set fields for tables to null where fields are empty
--
-- tables
--     manufacturer
--     connector
--     installationpackage
--     cabletype
--     cablearticle
--     cable
--     endpoint
--
-- vacuum database
--
-- note
--     select count may be useful for understanding scope
--     database name
--         cabledb
--         cabledb_dev
-- ----------------------------------------------------------------------------------------------------

select count(*) from manufacturer m;
select count(*) from manufacturer where address = '';
select count(*) from manufacturer where country = '';
select count(*) from manufacturer where email = '';
select count(*) from manufacturer where "name" = '';
select count(*) from manufacturer where phonenumber = '';
select count(*) from manufacturer where status = '';

select count(*) from connector c;
select count(*) from connector where description = '';
select count(*) from connector where connectortype = '';
select count(*) from connector where "name" = '';
select count(*) from connector where assembly_instructions = '';
select count(*) from connector where link_to_datasheet = '';

select count(*) from installation_package ip;
select count(*) from installation_package where "name" = '';
select count(*) from installation_package where description = '';
select count(*) from installation_package where cable_coordinator = '';
select count(*) from installation_package where active = '';
select count(*) from installation_package where "location" = '';
select count(*) from installation_package where revision = '';
select count(*) from installation_package where installer_cable = '';
select count(*) from installation_package where installer_connector_a = '';
select count(*) from installation_package where installer_connector_b = '';
select count(*) from installation_package where installation_package_leader = '';
select count(*) from installation_package where routing_documentation = '';

select count(*) from cabletype c;
select count(*) from cabletype where description = '';
select count(*) from cabletype where "comments" = '';
select count(*) from cabletype where flammability = '';
select count(*) from cabletype where insulation = '';
select count(*) from cabletype where jacket = '';
select count(*) from cabletype where model = '';
select count(*) from cabletype where "name" = '';
select count(*) from cabletype where installationtype = '';
select count(*) from cabletype where service = '';
select count(*) from cabletype where revision = '';

select count(*) from cablearticle c;
select count(*) from cablearticle where manufacturer = '';
select count(*) from cablearticle where external_id = '';
select count(*) from cablearticle where erp_number = '';
select count(*) from cablearticle where iso_class = '';
select count(*) from cablearticle where description = '';
select count(*) from cablearticle where long_description = '';
select count(*) from cablearticle where model_type = '';
select count(*) from cablearticle where short_name = '';
select count(*) from cablearticle where short_name_external_id = '';
select count(*) from cablearticle where url_chess_part = '';

select count(*) from cable c;
select count(*) from cable c where cableclass = '';
select count(*) from cable c where ownersstring = '';
select count(*) from cable c where status = '';
select count(*) from cable c where subsystem = '';
select count(*) from cable c where "system" = '';
select count(*) from cable c where validity = '';
select count(*) from cable c where container = '';
select count(*) from cable c where autocalculatedlength = '';
select count(*) from cable c where routingsstring = '';
select count(*) from cable c where "comments" = '';
select count(*) from cable c where revision = '';
select count(*) from cable c where fbs_tag = '';
select count(*) from cable c where electrical_documentation = '';
select count(*) from cable c where chess_id = '';

select count(*) from endpoint e;
select count(*) from endpoint e where building = '';
select count(*) from endpoint e where device = '';
select count(*) from endpoint e where "label" = '';
select count(*) from endpoint e where rack = '';
select count(*) from endpoint e where validity = '';
select count(*) from endpoint e where uuid = '';
select count(*) from endpoint e where namestatus = '';
select count(*) from endpoint e where device_fbs_tag = '';
select count(*) from endpoint e where rack_fbs_tag = '';
select count(*) from endpoint e where device_chess_id = '';
select count(*) from endpoint e where rack_chess_id = '';

-- ----------------------------------------------------------------------------------------------------

select pg_size_pretty( pg_database_size('cabledb_dev') );

update manufacturer set address = null where address = '';
update manufacturer set country = null where country = '';
update manufacturer set email = null where email = '';
update manufacturer set "name" = null where "name" = '';
update manufacturer set phonenumber = null where phonenumber = '';
update manufacturer set status = null where status = '';

update connector set description = null where description = '';
update connector set connectortype = null where connectortype = '';
update connector set "name" = null where "name" = '';
update connector set assembly_instructions = null where assembly_instructions = '';
update connector set link_to_datasheet = null where link_to_datasheet = '';

update installation_package set "name" = null where "name" = '';
update installation_package set description = null where description = '';
update installation_package set cable_coordinator = null where cable_coordinator = '';
update installation_package set active = null where active = '';
update installation_package set "location" = null where "location" = '';
update installation_package set revision = null where revision = '';
update installation_package set installer_cable = null where installer_cable = '';
update installation_package set installer_connector_a = null where installer_connector_a = '';
update installation_package set installer_connector_b = null where installer_connector_b = '';
update installation_package set installation_package_leader = null where installation_package_leader = '';
update installation_package set routing_documentation = null where routing_documentation = '';

update cabletype set description = null where description = '';
update cabletype set "comments" = null where "comments" = '';
update cabletype set flammability = null where flammability = '';
update cabletype set insulation = null where insulation = '';
update cabletype set jacket = null where jacket = '';
update cabletype set model = null where model = '';
update cabletype set "name" = null where "name" = '';
update cabletype set installationtype = null where installationtype = '';
update cabletype set service = null where service = '';
update cabletype set revision = null where revision = '';

update cablearticle set manufacturer = null where manufacturer = '';
update cablearticle set external_id = null where external_id = '';
update cablearticle set erp_number = null where erp_number = '';
update cablearticle set iso_class = null where iso_class = '';
update cablearticle set description = null where description = '';
update cablearticle set long_description = null where long_description = '';
--update cablearticle set model_type = null where model_type = '';
update cablearticle set short_name = null where short_name = '';
update cablearticle set short_name_external_id = null where short_name_external_id = '';
update cablearticle set url_chess_part = null where url_chess_part = '';

update cable set cableclass = null where cableclass = '';
update cable set ownersstring = null where ownersstring = '';
update cable set status = null where status = '';
update cable set subsystem = null where subsystem = '';
update cable set "system" = null where "system" = '';
update cable set validity = null where validity = '';
update cable set container = null where container = '';
update cable set autocalculatedlength = null where autocalculatedlength = '';
update cable set routingsstring = null where routingsstring = '';
update cable set "comments" = null where "comments" = '';
update cable set revision = null where revision = '';
update cable set fbs_tag = null where fbs_tag = '';
update cable set electrical_documentation = null where electrical_documentation = '';
update cable set chess_id = null where chess_id = '';

update endpoint set building = null where building = '';
update endpoint set device = null where device = '';
update endpoint set "label" = null where "label" = '';
update endpoint set rack = null where rack = '';
update endpoint set validity = null where validity = '';
update endpoint set uuid = null where uuid = '';
update endpoint set namestatus = null where namestatus = '';
update endpoint set device_fbs_tag = null where device_fbs_tag = '';
update endpoint set rack_fbs_tag = null where rack_fbs_tag = '';
update endpoint set device_chess_id = null where device_chess_id = '';
update endpoint set rack_chess_id = null where rack_chess_id = '';

-- vacuum
vacuum full;

select pg_size_pretty( pg_database_size('cabledb_dev') );

