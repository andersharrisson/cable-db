-- deleted orphaned and unused artifacts
--
-- tables
--     cable
--     endpoint
--     cabletypemanufacturer
--     connectormanufacturer
--     connector
--
-- vacuum database
--
-- note
--     select count may be useful for understanding scope
--     database name
--         cabledb
--         cabledb_dev
-- ----------------------------------------------------------------------------------------------------

-- =====================================================================================================
-- content
--     check
--     delete orphan artifacts
--     delete artifacts for cable
--     delete artifacts for endpoint
--     delete artifacts for cabletypemanufacturer
--     delete artifacts for connectormanufacturer
--     delete artifacts for connector
--     check
-- =====================================================================================================
-- check
select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('binary_data') );
select pg_size_pretty( pg_total_relation_size('generic_artifact') );
-- --------------------------------------------------
select count(*) from generic_artifact ga ;
select count(*) from binary_data bd ;

select count(*) from cable c where c.artifact_id is not null;
select count(*) from endpoint e where e.artifact_id is not null;
select count(*) from cabletypemanufacturer ctm where ctm.artifact_id is not null;
select count(*) from connectormanufacturer cm where cm.artifact_id is not null;
select count(*) from connector c where c.artifact_id is not null;

select count(*) from generic_artifact ga 
where ga.id not in (select distinct c.artifact_id   from cable c                   where c.artifact_id   is not null)
and   ga.id not in (select distinct e.artifact_id   from endpoint e                where e.artifact_id   is not null)
and   ga.id not in (select distinct ctm.artifact_id from cabletypemanufacturer ctm where ctm.artifact_id is not null)
and   ga.id not in (select distinct cm.artifact_id  from connectormanufacturer cm  where cm.artifact_id  is not null)
and   ga.id not in (select distinct c.artifact_id   from connector c               where c.artifact_id   is not null); 

--tables with column(s) (artifact_id) referring to generic_artifact
--cable
--endpoint
--cabletypemanufacturer
--connectormanufacturer
--connector

-- --------------------------------------------------
select count(*) from cable c where c.artifact_id is not null;
-- --------------------------------------------------
select count(*) from endpoint e where e.artifact_id is not null;
--columns not shown, artifacts not reachable
-- --------------------------------------------------
select count(*) from cabletypemanufacturer ctm where ctm.artifact_id is not null;

select count(*) from cabletypemanufacturer ctm, cabletype ct, manufacturer m 
where ctm.artifact_id is not null and ct.id = ctm.cabletype_id and m.id = ctm.manufacturer_id and (ct.active is false or m.status = 'DELETED');;

select count(*) from cabletypemanufacturer ctm, cabletype ct 
where ctm.artifact_id is not null and ct.id = ctm.cabletype_id and (ct.active is false);

select count(*) from cabletypemanufacturer ctm, manufacturer m 
where ctm.artifact_id is not null and m.id = ctm.manufacturer_id and (m.status = 'DELETED');;
-- --------------------------------------------------
select count(*) from connectormanufacturer cm where cm.artifact_id is not null;

select count(*) from connectormanufacturer cm, connector c, manufacturer m 
where cm.artifact_id is not null and c.id = cm.connector_id and m.id = cm.manufacturer_id and (c.active is false or m.status = 'DELETED');;

select count(*) from connectormanufacturer cm, connector c 
where cm.artifact_id is not null and c.id = cm.connector_id and (c.active is false);

select count(*) from connectormanufacturer cm, manufacturer m 
where cm.artifact_id is not null and m.id = cm.manufacturer_id and (m.status = 'DELETED');;
-- --------------------------------------------------
select count(*) from connector c where c.artifact_id is not null;

select count(*) from connector c where c.artifact_id is not null and c.active = false;
-- --------------------------------------------------
select count(*) from generic_artifact ga;
select count(distinct ga.id) from generic_artifact ga;
select count(distinct ga.binary_data_id) from generic_artifact ga;
select count(*) from binary_data bd;
select count(distinct bd.id) from binary_data bd;
-- =====================================================================================================
-- delete orphan artifacts

select ga.id, ga.binary_data_id into tmp_generic_artifact from generic_artifact ga 
where ga.id not in (select distinct c.artifact_id   from cable c                   where c.artifact_id   is not null)
and   ga.id not in (select distinct e.artifact_id   from endpoint e                where e.artifact_id   is not null)
and   ga.id not in (select distinct ctm.artifact_id from cabletypemanufacturer ctm where ctm.artifact_id is not null)
and   ga.id not in (select distinct cm.artifact_id  from connectormanufacturer cm  where cm.artifact_id  is not null)
and   ga.id not in (select distinct c.artifact_id   from connector c               where c.artifact_id   is not null)
order by ga.id asc; 

select count(*) from cable c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from endpoint e where e.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from cabletypemanufacturer ctm where ctm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connectormanufacturer cm where cm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connector c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);

--order important, foreign keys
update generic_artifact set binary_data_id = null where binary_data_id in (select binary_data_id from tmp_generic_artifact); 
delete from binary_data where id in (select binary_data_id from tmp_generic_artifact);
delete from generic_artifact where id in (select id from tmp_generic_artifact);

--vacuum full binary_data;
--vacuum full generic_artifact;

select count(*) from generic_artifact ga;
select count(*) from binary_data bd;
select count(*) from generic_artifact ga where ga.id in (select id from tmp_generic_artifact);;
select count(*) from binary_data bd where bd.id in (select binary_data_id from tmp_generic_artifact);;

drop table tmp_generic_artifact;

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('binary_data') );
select pg_size_pretty( pg_total_relation_size('generic_artifact') );
-- =====================================================================================================
-- delete artifacts for cable
-- =====================================================================================================
-- delete artifacts for endpoint

select * from cable c 
where c.endpointa_id in (select distinct e.id from endpoint e where e.artifact_id is not null) 
or    c.endpointb_id in (select distinct e.id from endpoint e where e.artifact_id is not null);

select ga.id, ga.binary_data_id into tmp_generic_artifact from generic_artifact ga 
where ga.id in (select distinct e.artifact_id from endpoint e where e.artifact_id is not null);

select count(*) from cable c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from endpoint e where e.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from cabletypemanufacturer ctm where ctm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connectormanufacturer cm where cm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connector c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);

--order important, foreign keys
update endpoint set artifact_id = null where artifact_id in (select id from tmp_generic_artifact);
update generic_artifact set binary_data_id = null where binary_data_id in (select binary_data_id from tmp_generic_artifact); 
delete from binary_data where id in (select binary_data_id from tmp_generic_artifact);
delete from generic_artifact where id in (select id from tmp_generic_artifact);

--vacuum full binary_data;
--vacuum full generic_artifact;

select count(*) from generic_artifact ga;
select count(*) from binary_data bd;
select count(*) from generic_artifact ga where ga.id in (select id from tmp_generic_artifact);;
select count(*) from binary_data bd where bd.id in (select binary_data_id from tmp_generic_artifact);;

drop table tmp_generic_artifact;

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('binary_data') );
select pg_size_pretty( pg_total_relation_size('generic_artifact') );
-- =====================================================================================================
-- delete artifacts for cabletypemanufacturer
-- for obsolete cable types

select ga.id, ga.binary_data_id into tmp_generic_artifact from generic_artifact ga 
where ga.id in (
    select ctm.artifact_id from cabletypemanufacturer ctm, cabletype ct 
    where ctm.artifact_id is not null and ct.id = ctm.cabletype_id and (ct.active is false)
);
'
select count(*) from cable c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from endpoint e where e.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from cabletypemanufacturer ctm where ctm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connectormanufacturer cm where cm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connector c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);

--order important, foreign keys
update cabletypemanufacturer set artifact_id = null where artifact_id in (select id from tmp_generic_artifact);
update generic_artifact set binary_data_id = null where binary_data_id in (select binary_data_id from tmp_generic_artifact); 
delete from binary_data where id in (select binary_data_id from tmp_generic_artifact);
delete from generic_artifact where id in (select id from tmp_generic_artifact);

--vacuum full binary_data;
--vacuum full generic_artifact;

select count(*) from generic_artifact ga;
select count(*) from binary_data bd;
select count(*) from generic_artifact ga where ga.id in (select id from tmp_generic_artifact);;
select count(*) from binary_data bd where bd.id in (select binary_data_id from tmp_generic_artifact);;

drop table tmp_generic_artifact;

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('binary_data') );
select pg_size_pretty( pg_total_relation_size('generic_artifact') );
-- =====================================================================================================
-- delete artifacts for connectormanufacturer
-- =====================================================================================================
-- delete artifacts for connector
-- for obsolete connectors

select ga.id, ga.binary_data_id into tmp_generic_artifact from generic_artifact ga 
where ga.id in (
    select c.artifact_id from connector c 
    where c.artifact_id is not null and c.active = false
);

select count(*) from cable c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from endpoint e where e.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from cabletypemanufacturer ctm where ctm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connectormanufacturer cm where cm.artifact_id in (select tga.id from tmp_generic_artifact tga);
select count(*) from connector c where c.artifact_id in (select tga.id from tmp_generic_artifact tga);

--order important, foreign keys
update connector set artifact_id = null where artifact_id in (select id from tmp_generic_artifact);
update generic_artifact set binary_data_id = null where binary_data_id in (select binary_data_id from tmp_generic_artifact); 
delete from binary_data where id in (select binary_data_id from tmp_generic_artifact);
delete from generic_artifact where id in (select id from tmp_generic_artifact);

--vacuum full binary_data;
--vacuum full generic_artifact;

select count(*) from generic_artifact ga;
select count(*) from binary_data bd;
select count(*) from generic_artifact ga where ga.id in (select id from tmp_generic_artifact);;
select count(*) from binary_data bd where bd.id in (select binary_data_id from tmp_generic_artifact);;

drop table tmp_generic_artifact;

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('binary_data') );
select pg_size_pretty( pg_total_relation_size('generic_artifact') );
-- =====================================================================================================
-- check
vacuum full;

--skipping "pg_authid" --- only superuser can vacuum it
--skipping "pg_database" --- only superuser can vacuum it
--skipping "pg_db_role_setting" --- only superuser can vacuum it
--skipping "pg_tablespace" --- only superuser can vacuum it
--skipping "pg_pltemplate" --- only superuser can vacuum it
--skipping "pg_auth_members" --- only superuser can vacuum it
--skipping "pg_shdepend" --- only superuser can vacuum it
--skipping "pg_shdescription" --- only superuser can vacuum it
--skipping "pg_replication_origin" --- only superuser can vacuum it
--skipping "pg_shseclabel" --- only superuser can vacuum it

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('binary_data') );
select pg_size_pretty( pg_total_relation_size('generic_artifact') );
-- =====================================================================================================

