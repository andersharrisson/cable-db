-- remove rows from history table which has no change content
--
-- tables
--     history
--
-- vacuum database
--
-- note
--     select count may be useful for understanding scope
--     database name
--         cabledb
--         cabledb_dev
-- ----------------------------------------------------------------------------------------------------

select count(*) from history h;
select count(*) from history h where h.entry like '%Changes:: (no change)%';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%' and h.entitytype = 'CABLE_ARTICLE';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%' and h.entitytype = 'CABLE_TYPE';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%' and h.entitytype = 'CABLE';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%' and h.entitytype = 'CONNECTOR';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%' and h.entitytype = 'MANUFACTURER';
select count(*) from history h where h.operation = 'UPDATE' and h.entry like '%Changes:: (no change)%' and h.entitytype = 'INSTALLATION_PACKAGE';

-- ----------------------------------------------------------------------------------------------------

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('history') );

delete from history where entry like '%Changes:: (no change)%';

vacuum full;

select pg_size_pretty( pg_database_size('cabledb_dev') );
select pg_size_pretty( pg_total_relation_size('history') );

