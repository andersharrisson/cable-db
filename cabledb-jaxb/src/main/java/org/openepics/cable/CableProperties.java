/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This contains properties of the cable application, such as URL of the UI and services.
 * Properties are loaded from the <code>cable.properties</code> file which needs to be
 * present on the classpath. All values can be overridden by setting the system properties.
 * System properties are not cached and are reflected immediately.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public final class CableProperties {

    private static final Logger LOGGER = Logger.getLogger(CableProperties.class.getName());

    /** Base web UI application URL property name. */
    public static final String APPLICATION_BASE_URL_PROPERTY_NAME = "cable.applicationBaseURL";
    /** Base web services URL property name. */
    public static final String SERVICES_BASE_URL_PROPERTY_NAME = "cable.servicesBaseURL";
    /** Cable numbering document URL property name. */
    public static final String NUMBERING_DOCUMENT_URL_PROPERTY_NAME = "cable.cableNumberingDocumentURL";
    /** User manual URL property name. */
    public static final String USER_MANUAL_URL_PROPERTY_NAME = "cable.cableUserManualURL";
    /** Mail notifications flag property name. */
    public static final String MAIL_NOTIFICATIONS_ENABLED = "cable.mailNotificationsEnabled";
    /** CCDB URL property name. */
    public static final String CCDB_URL = "cable.ccdbURL";
    /** Naming service URL property name. */
    public static final String NAMING_URL = "org.openepics.discs.conf.props.namingAppURL";

    /** User name of an administrator user. */
    public static final String TEST_ADMIN_USER_PROPERTY_NAME = "cable.test.adminUser";
    /** Password of an administrator user. */
    public static final String TEST_ADMIN_PASSWORD_PROPERTY_NAME = "cable.test.adminPassword";
    /** User name of an user which is cable user. */
    public static final String TEST_CABLE_USER_PROPERTY_NAME = "cable.test.cableUser";
    /** Password of an user which is cable user. */
    public static final String TEST_CABLE_USER_PASSWORD_PROPERTY_NAME = "cable.test.cableUserPassword";
    /** The name of an existing device name in the naming service. */
    public static final String TEST_DEVICE_NAME_PROPERTY_NAME = "cable.test.deviceName";
    /** The name of an existing device name 2 in the naming service. */
    public static final String TEST_DEVICE_NAME_PROPERTY_NAME_2 = "cable.test.deviceName2";
    /** The path to blob store. */
    public static final String BLOB_STORE_PROPERTY_NAME = "cable.BlobStoreRoot";


    private static final String FILE_CABLE_PROPERTIES = "cable.properties";

    private static final String LOADING_FAILED = "Loading properties from file " + FILE_CABLE_PROPERTIES + " failed."
            + " Using default.";

    private static CableProperties instance;

    private final Properties properties;

    private CableProperties() {
        properties = new Properties();
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(FILE_CABLE_PROPERTIES)) {
            properties.load(stream);
        } catch (IOException | NullPointerException e) {
            LOGGER.log(Level.FINEST, LOADING_FAILED, e);
            LOGGER.info(LOADING_FAILED);
            properties.setProperty(APPLICATION_BASE_URL_PROPERTY_NAME, "https://localhost:8080/cable");
            properties.setProperty(SERVICES_BASE_URL_PROPERTY_NAME, "https://localhost:8080/cable/rest");
            properties.setProperty(NUMBERING_DOCUMENT_URL_PROPERTY_NAME,
                    "https://localhost:8080/documents/cablenumberingdocument");
            properties.setProperty(USER_MANUAL_URL_PROPERTY_NAME,
                    "https://localhost:8080/documents/cabledatabaseusermanual");
            properties.setProperty(MAIL_NOTIFICATIONS_ENABLED, Boolean.FALSE.toString().toLowerCase());
            properties.setProperty(CCDB_URL, "https://localhost:8080/ccdb");
            properties.setProperty(NAMING_URL, "https://localhost:8080/naming");

            properties.setProperty(TEST_ADMIN_USER_PROPERTY_NAME, "rbactester1");
            properties.setProperty(TEST_ADMIN_PASSWORD_PROPERTY_NAME, "Changeit!");
            properties.setProperty(TEST_CABLE_USER_PROPERTY_NAME, "rbactester2");
            properties.setProperty(TEST_CABLE_USER_PASSWORD_PROPERTY_NAME, "Changeit!");
            properties.setProperty(TEST_DEVICE_NAME_PROPERTY_NAME, "Sec-Sub:Dis-Dev-0");
            properties.setProperty(TEST_DEVICE_NAME_PROPERTY_NAME_2, "Sec-Sub:Dis-Dev-1");
            properties.setProperty(BLOB_STORE_PROPERTY_NAME, "/blobStore");
        }
    }

    /**
     * Returns the singleton instance of this class.
     * Properties are read from the properties file when the instance is first created. If
     * properties could not be read, default values are used instead. Properties can be
     * overridden by setting them as system properties.
     * <p>
     * The default values are:
     * <ul>
     * <li>{@value #APPLICATION_BASE_URL_PROPERTY_NAME} -&gt; https://localhost:8080/cable</li>
     * <li>{@value #SERVICES_BASE_URL_PROPERTY_NAME} -&gt; https://localhost:8080/cable/rest</li>
     * <li>{@value #NUMBERING_DOCUMENT_URL_PROPERTY_NAME} -&gt;
     * https://localhost:8080/documents/cablenumberingdocument</li>
     * <li>{@value #USER_MANUAL_URL_PROPERTY_NAME} -&gt;
     * https://localhost:8080/documents/cabledatabaseusermanual</li>
     * <li>{@value #MAIL_NOTIFICATIONS_ENABLED} -&gt; false</li>
     * <li>{@value #CCDB_URL} -&gt; https://localhost:8080/ccdb</li>
     * <li>{@value #NAMING_URL} -&gt; https://localhost:8080/naming</li>
     * <li>{@value #TEST_ADMIN_USER_PROPERTY_NAME} -&gt; rbactester1</li>
     * <li>{@value #TEST_ADMIN_PASSWORD_PROPERTY_NAME} -&gt; Changeit!</li>
     * <li>{@value #TEST_CABLE_USER_PROPERTY_NAME} -&gt; rbactester2</li>
     * <li>{@value #TEST_CABLE_USER_PASSWORD_PROPERTY_NAME} -&gt; Changeit!</li>
     * <li>{@value #TEST_DEVICE_NAME_PROPERTY_NAME} -&gt; Sec-Sub:Dis-Dev-0</li>
     * <li>{@value #TEST_DEVICE_NAME_PROPERTY_NAME_2} -&gt; Sec-Sub:Dis-Dev-1</li>
     * <li>{@value #BLOB_STORE_PROPERTY_NAME} -&gt; /blobStore</li>
     * </ul>
     *
     * @return the singleton instance
     */
    public static synchronized CableProperties getInstance() {
        if (instance == null) {
            instance = new CableProperties();
        }
        return instance;
    }

    /**
     * Searches for the property with the specified name amongst system properties and loaded properties.
     *
     * @param name the property name
     *
     * @return the value of the property or null if not found
     */
    public String getProperty(String name) {
        final String systemProperty = System.getProperties().getProperty(name);
        return systemProperty != null ? systemProperty : properties.getProperty(name);
    }

    /** @return the value of the {@value #APPLICATION_BASE_URL_PROPERTY_NAME} property. */
    public String getApplicationBaseURL() {
        return getProperty(APPLICATION_BASE_URL_PROPERTY_NAME);
    }

    /** @return the value of the {@value #SERVICES_BASE_URL_PROPERTY_NAME} property. */
    public String getPrimarySSLHostname() {
        return getProperty(SERVICES_BASE_URL_PROPERTY_NAME);
    }

    /** @return the value of the {@value #NUMBERING_DOCUMENT_URL_PROPERTY_NAME} property. */
    public String getCableNumberingDocumentURL() {
        return getProperty(NUMBERING_DOCUMENT_URL_PROPERTY_NAME);
    }

    /** @return the value of the {@value #USER_MANUAL_URL_PROPERTY_NAME} property. */
    public String getUserManualURL() {
        return getProperty(USER_MANUAL_URL_PROPERTY_NAME);
    }

    /** @return the value of the {@value #MAIL_NOTIFICATIONS_ENABLED} property. */
    public boolean isMailNotificationEnabled() {
        return getProperty(MAIL_NOTIFICATIONS_ENABLED).equalsIgnoreCase(Boolean.TRUE.toString());
    }

    /** @return the value of the {@value #TEST_ADMIN_USER_PROPERTY_NAME} property. */
    public String getTestAdminUser() {
        return getProperty(TEST_ADMIN_USER_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_ADMIN_USER_PROPERTY_NAME} property. */
    public String getTestAdminPassword() {
        return getProperty(TEST_ADMIN_PASSWORD_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_CABLE_USER_PROPERTY_NAME} property. */
    public String getTestCableUser() {
        return getProperty(TEST_CABLE_USER_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_CABLE_USER_PASSWORD_PROPERTY_NAME} property. */
    public String getTestCableUserPassword() {
        return getProperty(TEST_CABLE_USER_PASSWORD_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_DEVICE_NAME_PROPERTY_NAME} property. */
    public String getTestDeviceName() {
        return getProperty(TEST_DEVICE_NAME_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_DEVICE_NAME_PROPERTY_NAME_2} property. */
    public String getTestDeviceName2() {
        return getProperty(TEST_DEVICE_NAME_PROPERTY_NAME_2);
    }

    /** @return the value of the {@value #CCDB_URL} property. */
    public String getCCDBURL() {
        return getProperty(CCDB_URL);
    }

    /** @return the value of the {@value #NAMING_URL} property. */
    public String getNamingURL() {
        return getProperty(NAMING_URL);
    }
    
    /** @return the value of the {@value #BLOB_STORE_PROPERTY_NAME} property. */
    public String getBlobStore() {
        return getProperty(BLOB_STORE_PROPERTY_NAME);
    }
}