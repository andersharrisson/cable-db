/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import io.swagger.annotations.*;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides bulk cables and specific cable data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("cable")
@Api(value = "/cable")
@Produces({"application/json"})
@SwaggerDefinition(
        info = @Info(
                description = "\n"
                        + "    This is a documentation for all REST interfaces of CableDB service. \n"
                        + "    You can find out more about CableDB service in Cable application,   \n"
                        + "        https://cable.esss.lu.se",
                version = "1.0.0",
                title = "CableDB service API documentation",
                termsOfService = "http://swagger.io/terms/",
                contact = @Contact(
                        name = "Support",
                        email = "Icsscontrolsystemsupport@esss.se")
                ),
        externalDocs = @ExternalDocs(
                value = "Cable Database",
                url = "https://confluence.esss.lu.se/display/SW/Cable+Database"))
public interface CableResource {

    /**
     * Returns non-deleted cables in the database. The cables are optionally filtered by matching cable properties using
     * regular expression.
     *
     * @param field a list of string cable fields to search using regular expression; if empty, all fields are searched
     * @param regExp the regular expression to use to search the cables; if null or empty, all cables are returned
     *
     * @return the cables
     */
    @GET
    @ApiOperation(value = "Finds all cables",
            notes = "Returns a list of cableElements",
            response = CableElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CableElement> getAllCables(@QueryParam("field") List<String> field,
            @QueryParam("regexp") String regExp);

    /**
     * Returns a specific cable instance.
     *
     * @param name the name of the cable to retrieve
     * @return the cable instance data
     */
    @GET
    @ApiOperation(value = "Finds cable by name",
            notes = "Returns the details of the specific cable",
            response = CableElement.class)
    @Path("{name}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CableElement getCable(@PathParam("name") String name);

    @GET
    @Path("essName/{essName}")
    @ApiOperation(value = "Finds all cables that use given ESS Name",
            notes = "Returns a list of cableElements",
            response = CableElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CableElement> getAllCablesWithESSName(@PathParam("essName") String essName);

    @GET
    @Path("fbsTag/{fbsTag}")
    @ApiOperation(value = "Finds all cables that use given FBS Tag",
            notes = "Returns a list of cableElements",
            response = CableElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CableElement> getAllCablesWithFBSTag(@PathParam("fbsTag") String fbsTag);

    @GET
    @Path("lbsTag/{lbsTag}")
    @ApiOperation(value = "Finds all cables that use given LBS Tag",
            notes = "Returns a list of cableElements",
            response = CableElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CableElement> getAllCablesWithLBSTag(@PathParam("lbsTag") String lbsTag);

}
