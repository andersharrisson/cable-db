/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides bulk approved and specific connector data.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("connector")
@Api(value = "/connector")
public interface ConnectorResource {

    /** @return returns all non-obsoleted approved connector in the database. */
    @GET
    @ApiOperation(value = "Finds all non-obsolete, approved connectors",
            notes = "Returns a list of approved, non-obsolete connector",
            response = ConnectorElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<ConnectorElement> getAllConnectors();
    
    /**
     * Returns a specific approved connector instance.
     * 
     * @param name the name of the connector to retrieve
     * @return the connector data
     */
    @GET
    @ApiOperation(value = "Finds the connector with specific name",
            notes = "Returns connector that has a specific name from DB",
            response = ConnectorElement.class)
    @Path("{name}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public ConnectorElement getConnector(@PathParam("name") String name);
}
