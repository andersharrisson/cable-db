/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * This is data transfer object representing a cable type for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name="cableType")
@XmlAccessorType(XmlAccessType.FIELD)
public class CableTypeElement {

    private String name;
    private String description;
    private String service;
    private Integer voltage;
    private String insulation;
    private String jacket;
    private String flammability;
    private String installationType;
    private Float tid;
    private Float weight;
    private Float diameter;
    private String manufacturer;
    private String datasheet;
    private String comments;
    private boolean active;

    /** Default constructor used by JAXB. */
    public CableTypeElement() {}

    /** @return the name */
    public String getName() {
        return name;
    }

    /** @return the service */
    public String getService() {
        return service;
    }

    /** @return the voltage */
    public Integer getVoltage() {
        return voltage;
    }

    /** @return the insulation */
    public String getInsulation() {
        return insulation;
    }

    /** @return the jacket */
    public String getJacket() {
        return jacket;
    }

    /** @return the flammability */
    public String getFlammability() {
        return flammability;
    }

    /** @return the installation type */
    public String getInstallationType() {
        return installationType;
    }

    /** @return the tid */
    public Float getTid() {
        return tid;
    }

    /** @return the weight */
    public Float getWeight() {
        return weight;
    }

    /** @return the diameter */
    public Float getDiameter() {
        return diameter;
    }

    /** @return the Manufacturer */
    public String getManufacturer() {
        return manufacturer;
    }
    
    /** @return the Datasheet*/
    public String getDatasheet() {
        return datasheet;
    }

    /** @return the comments */
    public String getComments() {
        return comments;
    }

    /** @return the active */
    public boolean isActive() {
        return active;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @param voltage the voltage to set
     */
    public void setVoltage(Integer voltage) {
        this.voltage = voltage;
    }

    /**
     * @param insulation the insulation to set
     */
    public void setInsulation(String insulation) {
        this.insulation = insulation;
    }

    /**
     * @param jacket the jacket to set
     */
    public void setJacket(String jacket) {
        this.jacket = jacket;
    }

    /**
     * @param flammability the flammability to set
     */
    public void setFlammability(String flammability) {
        this.flammability = flammability;
    }

    /**
     * @param installationType the installationType to set
     */
    public void setInstallationType(String installationType) {
        this.installationType = installationType;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(Float tid) {
        this.tid = tid;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Float weight) {
        this.weight = weight;
    }

    /**
     * @param diameter the diameter to set
     */
    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }

    /**
     * @param manufacturer  the manufacturer to set
     */
    public void setManufacturer (String manufacturer ) {
        this.manufacturer  = manufacturer ;
    }
    
    /**
     * @param datasheet the datasheet to set
     */
    public void setDatasheet(String datasheet) {
        this.datasheet  = datasheet ;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
