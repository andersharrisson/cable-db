/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * This is data transfer object representing a cable endpoint for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name="endpoint")
@XmlAccessorType(XmlAccessType.FIELD)
public class EndpointElement {

    private String device;
    private String building;
    private String rack; 
    private String connector;
    private String drawing;
    private String label;
    private String validity;

    /** Default constructor used by JAXB. */
    public EndpointElement() {}

    /** @return the device */
    public String getDevice() {
        return device;
    }

    /** @return the building */
    public String getBuilding() {
        return building;
    }

    /** @return the rack */
    public String getRack() {
        return rack;
    }

    /** @return the connector*/
    public String getConnector() {
        return connector;
    }
    
    /** @return the drawing */
    public String getDrawing() {
        return drawing;
    }

    /** @return the label */
    public String getLabel() {
        return label;
    }

    /** @return the validity */
    public String getValidity() {
        return validity;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @param building the building to set
     */
    public void setBuilding(String building) {
        this.building = building;
    }

    /**
     * @param rack the rack to set
     */
    public void setRack(String rack) {
        this.rack = rack;
    }

    /**
     * @param connector the connector to set
     */
    public void setConnector(String connector) {
        this.connector = connector;
    }

    public void setDrawing(String drawing) {
        this.drawing = drawing;
    }
    
    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }
}
