/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.jaxb;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * This resource provides bulk approved and specific cable article data.
 *
 * @author Lars Johansson
 */
@Path("cableArticle")
@Api(value = "/cableArticle")
public interface CableArticleResource {

    /** @return returns all active approved cable articles in the database. */
    @GET
    @ApiOperation(value = "Finds all active, approved cable articles",
            notes = "Returns a list of approved, active cable article from DB",
            response = CableArticleElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CableArticleElement> getAllCableArticles();

    /**
     * Returns a specific approved cable article instance.
     *
     * @param shortNameExternalId the short name (of the manufacturer) and dot and the external id
     * of the cable article to retrieve
     * @return the cable article data
     */
    @GET
    @ApiOperation(value = "Finds a cable article with specific short name and and external id",
            notes = "Returns an installation package with specific short name and and external id from DB",
            response = CableArticleElement.class)
    @Path("name")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CableArticleElement getCableArticle(@QueryParam("shortNameExternalId") String shortNameExternalId);

}
