/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a cable for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name = "cable")
@XmlAccessorType(XmlAccessType.FIELD)
public class CableElement {

    private long id;
    private String name;
    private String fbsTag;
    private String system;
    private String subsystem;
    private String cableClass;
    private Integer seqNumber;
    private String owners;

    @XmlElement
    private CableArticleElement cableArticle;
    @XmlElement
    private CableTypeElement cableType;
    private String container;
    private String electricalDocumentation;
    @XmlElement
    private EndpointElement endpointA;
    @XmlElement
    private EndpointElement endpointB;
    @XmlElement
    private InstallationPackageElement installationPackage;
    private Date installationBy;
    private Date created;
    private Date modified;
    private String status;
    private String validity;
    private String comments;
    private String revision;

    /** Default constructor used by JAXB. */
    public CableElement() {/* Default constructor used by JAXB. */}

    /** @return the name */
    public String getName() {
        return name;
    }

    /** @return the system */
    public String getSystem() {
        return system;
    }

    /** @return the subsystem */
    public String getSubsystem() {
        return subsystem;
    }

    /** @return the cableClass */
    public String getCableClass() {
        return cableClass;
    }

    /** @return the seqNumber */
    public Integer getSeqNumber() {
        return seqNumber;
    }

    /** @return the owners */
    public String getOwners() {
        return owners;
    }

    /** @return the cableArticle */
    public CableArticleElement getCableArticle() {
        return cableArticle;
    }

    /** @return the cableType */
    public CableTypeElement getCableType() {
        return cableType;
    }

    /**
     * @return the container (bundle)
     */
    public String getContainer() {
        return container;
    }

    /**
     * @return the electrical documentation (url)
     */
    public String getElectricalDocumentation() {
        return electricalDocumentation;
    }

    /** @return the endpointA */
    public EndpointElement getEndpointA() {
        return endpointA;
    }

    /** @return the endpointB */
    public EndpointElement getEndpointB() {
        return endpointB;
    }

    /** @return the installation package */
    public InstallationPackageElement getInstallationPackage() {
        return installationPackage;
    }

    /** @return the installationBy */
    public Date getInstallationBy() {
        return installationBy != null ? new Date(installationBy.getTime()) : null;
    }

    /** @return the created */
    public Date getCreated() {
        return created != null ? new Date(created.getTime()) : null;
    }

    /** @return the modified */
    public Date getModified() {
        return modified != null ? new Date(modified.getTime()) : null;
    }

    /** @return the status */
    public String getStatus() {
        return status;
    }

    /** @return the validity */
    public String getValidity() {
        return validity;
    }

    /** @return the comments */
    public String getComments() {
        return comments;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param system
     *            the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @param subsystem
     *            the subsystem to set
     */
    public void setSubsystem(String subsystem) {
        this.subsystem = subsystem;
    }

    /**
     * @param cableClass
     *            the cableClass to set
     */
    public void setCableClass(String cableClass) {
        this.cableClass = cableClass;
    }

    /**
     * @param seqNumber
     *            the seqNumber to set
     */
    public void setSeqNumber(Integer seqNumber) {
        this.seqNumber = seqNumber;
    }

    /**
     * @param owners
     *            the owners to set
     */
    public void setOwners(String owners) {
        this.owners = owners;
    }

    /**
     * @param cableArticle
     *            the cableArticle to set
     */
    public void setCableArticle(CableArticleElement cableArticle) {
        this.cableArticle = cableArticle;
    }

    /**
     * @param cableType
     *            the cableType to set
     */
    public void setCableType(CableTypeElement cableType) {
        this.cableType = cableType;
    }

    /**
     * @param container
     *            the container (bundle) to set
     */
    public void setContainer(String container) {
        this.container = container;
    }

    /**
     * @param electricalDocumentation
     *            the electrical documentation (url) to set
     */
    public void setElectricalDocumentation(String electricalDocumentation) {
        this.electricalDocumentation = electricalDocumentation;
    }

    /**
     * @param endpointA
     *            the endpointA to set
     */
    public void setEndpointA(EndpointElement endpointA) {
        this.endpointA = endpointA;
    }

    /**
     * @param endpointB
     *            the endpointB to set
     */
    public void setEndpointB(EndpointElement endpointB) {
        this.endpointB = endpointB;
    }

    /**
     * @param installation package
     *            the installation package to set
     */
    public void setInstallationPackage(InstallationPackageElement installationPackage) {
        this.installationPackage = installationPackage;
    }

    /**
     * @param installationBy
     *            the installationBy to set
     */
    public void setInstallationBy(Date installationBy) {
        this.installationBy = installationBy != null ? new Date(installationBy.getTime()) : null;
    }

    /**
     * @param created
     *            the created to set
     */
    public void setCreated(Date created) {
        this.created = created != null ? new Date(created.getTime()) : null;
    }

    /**
     * @param modified
     *            the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified != null ? new Date(modified.getTime()) : null;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @param validity
     *            the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }

    /**
     *
     * @param comments
     *            the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    /** @return the FBS Tag */
    public String getFbsTag() {
        return fbsTag;
    }

    /**
     * @param fbsTag FBS Tag to set
     */
    public void setFbsTag(String fbsTag) {
        this.fbsTag = fbsTag;
    }
}
