/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a cable for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name="cable")
@XmlAccessorType(XmlAccessType.FIELD)
public class CableElement {

    private String name;
    private String system;
    private String subsystem;
    private String cableClass;
    private Integer seqNumber;
    private String owners;

    @XmlElement private CableTypeElement cableType;
    private String container;
    @XmlElement private EndpointElement endpointA;
    @XmlElement private EndpointElement endpointB;

    private String routings;
    private Date installationBy;
    private Date terminationBy;
    private String qualityReport;
    private String qualityReportUri;
    private Float baseLength;
    private Float length;
    private Boolean autoCalculatedLength;
    private Date created;
    private Date modified;
    private String status;
    private String validity;

    /** Default constructor used by JAXB. */
    public CableElement() {}

    /** @return the name */
    public String getName() {
        return name;
    }

    /** @return the system */
    public String getSystem() {
        return system;
    }

    /** @return the subsystem */
    public String getSubsystem() {
        return subsystem;
    }

    /** @return the cableClass */
    public String getCableClass() {
        return cableClass;
    }

    /** @return the seqNumber */
    public Integer getSeqNumber() {
        return seqNumber;
    }

    /** @return the owners */
    public String getOwners() {
        return owners;
    }

    /** @return the cableType */
    public CableTypeElement getCableType() {
        return cableType;
    }

    public String getContainer() {
        return container;
    }

    /** @return the endpointA */
    public EndpointElement getEndpointA() {
        return endpointA;
    }

    /** @return the endpointB */
    public EndpointElement getEndpointB() {
        return endpointB;
    }

    /** @return the routing */
    public String getRoutings() {
        return routings;
    }

    /** @return the installationBy */
    public Date getInstallationBy() {
        return installationBy; //NOSONAR
    }

    /** @return the terminationBy */
    public Date getTerminationBy() {
        return terminationBy; //NOSONAR
    }

    /** @return the qualityReport */
    public String getQualityReport() {
        return qualityReport;
    }
    
    /** @return the qualityReport Uri */
    public String getQualityReportUri() {
        return qualityReportUri;
    }
    
    /** @return is length auto calculated. */
    public Boolean isAutoCalculatedLength() {
        return autoCalculatedLength;
    }
    
    /**
	 * @return the baseLength
	 */
	public Float getBaseLength() {
		return baseLength;
	}

	/** @return the length */
    public Float getLength() {
        return length;
    }

    /** @return the created */
    public Date getCreated() {
        return created; //NOSONAR
    }

    /** @return the modified */
    public Date getModified() {
        return modified; //NOSONAR
    }

    /** @return the status */
    public String getStatus() {
        return status;
    }

    /** @return the validity */
    public String getValidity() {
        return validity;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name= name;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @param subsystem the subsystem to set
     */
    public void setSubsystem(String subsystem) {
        this.subsystem = subsystem;
    }

    /**
     * @param cableClass the cableClass to set
     */
    public void setCableClass(String cableClass) {
        this.cableClass = cableClass;
    }

    /**
     * @param seqNumber the seqNumber to set
     */
    public void setSeqNumber(Integer seqNumber) {
        this.seqNumber = seqNumber;
    }

    /**
     * @param owners the owners to set
     */
    public void setOwners(String owners) {
        this.owners = owners;
    }

    /**
     * @param cableType the cableType to set
     */
    public void setCableType(CableTypeElement cableType) {
        this.cableType = cableType;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    /**
     * @param endpointA the endpointA to set
     */
    public void setEndpointA(EndpointElement endpointA) {
        this.endpointA = endpointA;
    }

    /**
     * @param endpointB the endpointB to set
     */
    public void setEndpointB(EndpointElement endpointB) {
        this.endpointB = endpointB;
    }

    /**
     * @param routings the routingRows to set
     */
    public void setRoutings(String routings) {
        this.routings = routings;
    }

    /**
     * @param installationBy the installationBy to set
     */
    public void setInstallationBy(Date installationBy) {
        this.installationBy = installationBy; //NOSONAR
    }

    /**
     * @param terminationBy the terminationBy to set
     */
    public void setTerminationBy(Date terminationBy) {
        this.terminationBy = terminationBy; //NOSONAR
    }

    /** 
     * Set the quality report.
     * 
     * @param qualityReport quality report.
     */
    public void setQualityReport(String qualityReport) {
        this.qualityReport = qualityReport;
    }
    
    /** 
     * Set the quality report URI. 
     * 
     * @param qualityReportUri quality report URI
     */
    public void setQualityReportUri(String qualityReportUri) {
        this.qualityReportUri = qualityReportUri;
    }   
    
	/**
	 * @param baseLength the baseLength to set
	 */
	public void setBaseLength(Float baseLength) {
		this.baseLength = baseLength;
	}
    
    /**
     * @param length the length to set
     */
    public void setLength(Float length) {
        this.length = length;
    }
    
    /**
     * Sets the length auto calculated value.
     * @param autoCalculatedLength is length auto calculated.
     */
    public void setAutoCalculatedLength(Boolean autoCalculatedLength) {
        this.autoCalculatedLength = autoCalculatedLength;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created; //NOSONAR
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified; //NOSONAR
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }
}
