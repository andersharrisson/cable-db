/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a cable with minimal data for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name="cable")
@XmlAccessorType(XmlAccessType.FIELD)
public class CableEndpointsElement {

    private String name;
    private String fbsTag;

    @XmlElement private EndpointElement endpointA;
    @XmlElement private EndpointElement endpointB;

    private Date modified;
    private String status;
    private String validity;

    /** Default constructor used by JAXB. */
    public CableEndpointsElement() {/* Default constructor used by JAXB. */}

    /** @return the name */
    public String getName() {
        return name;
    }

    /** @return the endpointA */
    public EndpointElement getEndpointA() {
        return endpointA;
    }

    /** @return the endpointB */
    public EndpointElement getEndpointB() {
        return endpointB;
    }

    /** @return the modified */
    public Date getModified() {
        return modified != null ? new Date(modified.getTime()) : null;
    }

    /** @return the status */
    public String getStatus() {
        return status;
    }

    /** @return the validity */
    public String getValidity() {
        return validity;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name= name;
    }

    /**
     * @param endpointA the endpointA to set
     */
    public void setEndpointA(EndpointElement endpointA) {
        this.endpointA = endpointA;
    }

    /**
     * @param endpointB the endpointB to set
     */
    public void setEndpointB(EndpointElement endpointB) {
        this.endpointB = endpointB;
    }


    /**
     * @param modified the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified != null ? new Date(modified.getTime()) : null;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }

    /** @return the FBS Tag */
    public String getFbsTag() {
        return fbsTag;
    }

    /**
     * @param fbsTag FBS Tag to set
     */
    public void setFbsTag(String fbsTag) {
        this.fbsTag = fbsTag;
    }
}
