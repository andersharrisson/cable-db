/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing an installation package for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name="installationPackage")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstallationPackageElement {

    private String name;
    private String description;
    private String location;
    private Date created;
    private Date modified;
    private String cableCoordinator;
    private String installerCable;
    private String installerConnectorA;
    private String installerConnectorB;
    private String installationPackageLeader;
    private boolean active;

    /** Default constructor used by JAXB. */
    public InstallationPackageElement() {/* Default constructor used by JAXB. */}

    /** @return the name */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    /** @return the created */
    public Date getCreated() {
        return created != null ? new Date(created.getTime()) : null;
    }

    /**
     * @param created
     *            the created to set
     */
    public void setCreated(Date created) {
        this.created = created != null ? new Date(created.getTime()) : null;
    }

    /** @return the modified */
    public Date getModified() {
        return modified != null ? new Date(modified.getTime()) : null;
    }

    /**
     * @param modified
     *            the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified != null ? new Date(modified.getTime()) : null;
    }

    public String getCableCoordinator() {
        return cableCoordinator;
    }

    public void setCableCoordinator(String cableCoordinator) {
        this.cableCoordinator = cableCoordinator;
    }

    public String getInstallerCable() {
        return installerCable;
    }

    public void setInstallerCable(String installerCable) {
        this.installerCable = installerCable;
    }

    public String getInstallerConnectorA() {
        return installerConnectorA;
    }

    public void setInstallerConnectorA(String installerConnectorA) {
        this.installerConnectorA = installerConnectorA;
    }

    public String getInstallerConnectorB() {
        return installerConnectorB;
    }

    public void setInstallerConnectorB(String installerConnectorB) {
        this.installerConnectorB = installerConnectorB;
    }

    public String getInstallationPackageLeader() {
        return installationPackageLeader;
    }

    public void setInstallationPackageLeader(String installationPackageLeader) {
        this.installationPackageLeader = installationPackageLeader;
    }

    /** @return the active */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

}
